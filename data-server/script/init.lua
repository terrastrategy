-- Server build is currently being used for
-- script benchmarking purposes

function vt3(x, y, z)
    local res = {x, y, z}
    res["add"] = function(a, b)
      a[1] = a[1] + b[1]
      a[2] = a[2] + b[2]
      a[3] = a[3] + b[3]
    end
    setmetatable(res, mt)
    return res
end

mt = { __add = function (a, b)
                 return vt3(a[1] + b[1], a[2] + b[2], a[3] + b[3])
               end }

function testluavec(t)
  local b=vt3(0,0,0)
  local c=vt3(0,1,2)

  local tim=os.clock()
  for i=0,t do
    --b = b + vt3(0,1,2)
    --b = b + c
    b:add(c)
  end
  tim = os.clock() - tim
  print(b[1], b[2], b[3])
  return tim
end

function testterravec(t)
  local b=vec3(0,0,0)
  local c=vec3(0,1,2)
  local tim=os.clock()
  for i=0,t do
    --b = b + vec3(0,1,2)
    --b = b + c
    b:add(c)
  end
  tim = os.clock() - tim
  print(b.x, b.y, b.z)
  return tim
end

-- function addvec(a,b)
--   return {a[1]+b[1],a[2]+b[2],a[3]+b[3]}
-- end
-- 
-- function testluavec(t)
--   local b={0,0,0}
--   local tim=os.clock()
--   for i=0,t do
--     b = addvec(b,{0,1,2})
--   end
--   tim = os.clock() - tim
--   print(b[1], b[2], b[3])
--   return tim
-- end
-- 
-- function testterravec(t)
--   local b=v3(0,0,0)
--   local tim=os.clock()
--   for i=0,t do
--     b = b + v3(0,1,2)
--   end
--   tim = os.clock() - tim
--   print(b[1], b[2], b[3])
--   return tim
-- end

function AppInit()
  t = testluavec(1000000)
  print(t)

  t = testterravec(1000000)
  print(t)
end

function AppEnd()
end
