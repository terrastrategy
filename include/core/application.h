//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file application.h
 *
 *  blahblah
 *
 */

#pragma once

#include "core/timer.h"

namespace tre {

  class VirtualMachine;

  class Application {
    public:
      Timer timer;
      bool running;

    public:
      Application();
      virtual ~Application();

      virtual void Run(void) = 0;

      void Quit(void) {running = false;}

      VirtualMachine * GetScriptVM(void) {return scriptVM_;}

    protected:
      VirtualMachine * scriptVM_;
  };

}
