//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file display.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <vector>

#include "types.h"

namespace tre {

  struct DisplayInfo {
    uint width;
    uint height;
    uint bpp;
    bool fullscreen;
    bool stereo;
  };

  typedef std::vector<DisplayInfo> DInfoVector;

  class Display {
    public:
      Display();

      void Init(void);

      void SetDisplayMode(const DisplayInfo & info);

      const std::string GetDriverName(void) const;
      const DisplayInfo GetDisplayInfo(void) const;
      const DInfoVector ListModes(void) const;

    private:
      bool fullscreen_;
      bool stereo_;
  };

  inline Display & sDisplay(void)
  {
    static Display r;
    return r;
  }

}
