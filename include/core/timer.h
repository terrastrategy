//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file timer.h
 *
 *  blabla
 */

#pragma once

#include "assertion.h"

#include "cpair.h"

#include <deque>
#include <boost/function.hpp>

#include "types.h"

namespace tre {

  class Timer {
    public:
      static uint32 GetSystemTime(void);

    public:
      typedef boost::function<void (void)> TimerAction;

    public:
      float mod;

    public:
      Timer();

      void UpdateTime(void);
      uint GetTime(void) {return time_;}

      void Start(void);
      void Stop(void);
      bool IsRunning(void) {return running_;}

      // returns pointer as an id
      const TimerAction * ScheduleAction(uint ms, const TimerAction & act);
      bool RemoveAction(const TimerAction * act);

    private:
      typedef cpair<uint, TimerAction *> ActionPair;
      typedef std::deque<ActionPair> ActionQueue;

    private:
      uint lastTime_;   // last snapshot
      uint time_;

      bool running_;

      ActionQueue actions_;
  };

}
