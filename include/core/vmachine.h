//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file vmachine.h
 *
 *  blahblahblah
 *
 */

#pragma once

#include <string>
#include <boost/function.hpp>

#include "luabind/luabind.hpp"

struct lua_State;

namespace tre {

  class Application;

  typedef boost::function<void (lua_State * L)> ModuleInit;

  class VirtualMachine {
    public:
      VirtualMachine(bool libs=true);
      ~VirtualMachine();

      template <typename T>
      void InitModule(const T & init) {init(luaState_);}

      bool RunFile(const std::string & filename);
      bool RunString(const std::string & str);
      bool RunGlobalFunction(const std::string & str, luabind::object *arg=NULL);

      template <typename T>
      const luabind::object WrapObject(T * obj)
      {
        return luabind::object(luaState_, obj);
      }

      const luabind::object CompileScriptChunk
        (const std::string & chunk, const std::string & name);

    private:
      lua_State * luaState_;
  };
}
