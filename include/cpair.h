//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file cpair.h
 *
 *  Comparable pair classes, based on SGI implementation of std::pair class.
 */

#pragma once

#include <utility>

template <typename T, typename U>
class cpair {
  public:
    typedef T first_type;
    typedef U second_type;

  public:
    T first;
    U second;

  public:
    // constructors
    cpair() : first(), second() {}
    cpair(const T & a, const U & b) : first(a), second(b) {}

    template <typename V1, typename V2>
    cpair(const cpair<V1, V2> & p) : first(p.first), second(p.second) {}

    // also include copy constructors for equivalent std::pair
    template <typename V1, typename V2>
    cpair(const std::pair<V1, V2> & p) : first(p.first), second(p.second) {}
};

// operator ==
template <typename T, typename U>
inline bool operator == (const cpair<T, U> & l, const cpair<T, U> & r)
{
  return l.first == r.first && l.second == r.second;
}

template <typename T, typename U>
inline bool operator == (const cpair<T, U> & l, const std::pair<T, U> & r)
{
  return l.first == r.first && l.second == r.second;
}

template <typename T, typename U>
inline bool operator == (const std::pair<T, U> & l, const cpair<T, U> & r)
{
  return l.first == r.first && l.second == r.second;
}

// operator !=
template <typename T, typename U>
inline bool operator != (const cpair<T, U> & l, const cpair<T, U> & r)
{
  return !(l == r);
}

template <typename T, typename U>
inline bool operator != (const cpair<T, U> & l, const std::pair<T, U> & r)
{
  return !(l == r);
}

template <typename T, typename U>
inline bool operator != (const std::pair<T, U> & l, const cpair<T, U> & r)
{
  return !(l == r);
}

// operator <
template <typename T, typename U>
inline bool operator < (const cpair<T, U> & l, const cpair<T, U> & r)
{
  return l.first < r.first;
}

template <typename T, typename U>
inline bool operator < (const T & l, const cpair<T, U> & r)
{
  return l < r.first;
}

template <typename T, typename U>
inline bool operator < (const cpair<T, U> & l, const T & r)
{
  return l.first < r;
}

// A convenience wrapper for creating a pair from two objects.
// template <typename T, typename U>
// inline cpair<T, U> make_cpair(T l, U r)
// {
//   return cpair<T, U>(l, r);
// }
