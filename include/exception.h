//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file exception.h
 *
 *  This file contains custom exception interface
 *
 *  Contains declaration of the Exception class, derived from std::exception.
 */

#pragma once

#include <exception>
#include <string>

/**
 *  @class Exception
 *
 *  Custom exception class. Used for every non-standard exception.
 */
class Exception : public std::exception {
  public:
    /** Gets description of the thrown exception. */
    virtual const char * what() const throw()
    {
      return whatString.c_str();
    }

    /** Copy constructor. */
    Exception(const std::string & str)
    {
      whatString = str;
    }

    virtual ~Exception() throw()
    {
    }

  private:
    /** String briefly describing the cause of the exception. */
    std::string whatString;
};
