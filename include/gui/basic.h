//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file basic.h
 *
 *  Provides collection of basic widgets.
 */

#pragma once

#include <boost/signals/connection.hpp>

#include "gui.h"
#include "widget.h"
#include "pixmap.h"
#include "richtext.h"
#include "plaintext.h"

namespace tre {

 /*
  *  Abstract widgets
  */
  class AbstractStatic : virtual public Widget {
    public:
      AbstractStatic(Gui & g, Widget * p);

      void OnMouseEnter(void) {}
      void OnMouseExit(void) {}
  };

  class AbstractButton : virtual public Widget {
    public:
      // public signals
      boost::signal<void (void)> clicked;

    public:
      AbstractButton(Gui & g, Widget * p);

      void OnMouseEnter(void);
      void OnMouseExit(void);

    private:
      // slots
      class OnButtonPressed;
      class OnButtonReleased;

      // connections
      boost::signals::connection buttonCon1;
      boost::signals::connection buttonCon2;
  };

  class AbstractEdit : virtual public Widget {
    public:
      AbstractEdit(Gui & g, Widget * p);

      void OnMouseEnter(void);

    private:
      // slots
      class OnButtonPressed;
      class OnKeyPressed;

      // connections
      boost::signals::connection buttonCon;
      boost::signals::connection keyCon;

    private:
      virtual void InsertCharacter(wchar_t chr) = 0;
  };

 /*
  *  Static text widget (without frame-like parent)
  */
  class StaticText : public Widget {
    public:
      static Widget * CreateWidget(Gui & gui, Widget * parent);

    public:
      PlainText text;

    public:
      StaticText(Gui & g, Widget * p);

      void OnMouseEnter(void) {}
      void OnMouseExit(void) {}
      void OnGuiResize(const Vector2f & scale);

      void Draw(void);
      void LoadFromXml(TiXmlElement & root);
  };

  class StaticRichText : public Widget {
    public:
      static Widget * CreateWidget(Gui & gui, Widget * parent);

    public:
      RichText text;

    public:
      StaticRichText(Gui & g, Widget * p);

      void OnMouseEnter(void) {}
      void OnMouseExit(void) {}
      void OnGuiResize(const Vector2f & scale);

      void Draw(void);
      void LoadFromXml(TiXmlElement & root);
  };

 /*
  *  Parent class for most pixmap based widgets
  */
  class PixmapWidget : virtual public Widget, private boost::noncopyable {
    public:
      PixmapWidget(Pixmap * pix) : pixmap_(pix) {}
      ~PixmapWidget();

      void OnGuiResize(const Vector2f & scale);

      void SetSize(const Vector2f & sz);
      void Draw(void);

    protected:
      Pixmap * pixmap_;

    protected:
      void SetState(uint u, WidgetState w);
  };

 /*
  *  Basic widgets
  */
  class Frame : public AbstractStatic, public PixmapWidget {
    public:
      static Widget * CreateWidget(Gui & gui, Widget * parent);

    public:
      Frame(Gui & g, Widget * p);

      void LoadFromXml(TiXmlElement & root);
  };

  class FrameRaised : public Frame {
    public:
      static Widget * CreateWidget(Gui & gui, Widget * parent);

    public:
      FrameRaised(Gui & g, Widget * p) : Frame(g, p) {}

      void LoadFromXml(TiXmlElement & root);
  };

  class FrameSunken : public Frame {
    public:
      static Widget * CreateWidget(Gui & gui, Widget * parent);

    public:
      FrameSunken(Gui & g, Widget * p) : Frame(g, p) {}

      void LoadFromXml(TiXmlElement & root);
  };

//   class StaticLine : public Pixmap2x1L {
//     public:
//       StaticLine(Gui & g, Widget * p);
//       StaticLine(Gui & g, Widget * p, Mesh & pm);
// 
//       void LoadFromXml(TiXmlElement & root);
//   };

  class PushButton : public AbstractButton, public PixmapWidget {
    public:
      static Widget * CreateWidget(Gui & gui, Widget * parent);

    public:
      PushButton(Gui & g, Widget * p);

      void OnGuiResize(const Vector2f & scale);

      const std::wstring GetCaption(void) const {return caption_.GetText();}
      void SetCaption(const std::string & text) {caption_.SetText(text);}

      void SetSize(const Vector2f & sz);
      void Draw(void);
      void LoadFromXml(TiXmlElement & root);

    private:
      class EventBinder;

    private:
      PlainText caption_;
  };

//   class EditBox : public Pixmap3x1 {
//     public:
//       EditBox(Gui & g, Widget * p);
//       EditBox(Gui & g, Widget * p, Mesh & pm);
// 
//       void LoadFromXml(TiXmlElement & root);
//   };
// 
//   class CheckBox : public Pixmap1x1 {
//     public:
//       CheckBox(Gui & g, Widget * p);
//       CheckBox(Gui & g, Widget * p, Mesh & pm);
// 
//       void LoadFromXml(TiXmlElement & root);
//   };
// 
//   class RadioButton : public Pixmap1x1 {
//     public:
//       RadioButton(Gui & g, Widget * p);
//       RadioButton(Gui & g, Widget * p, Mesh & pm);
// 
//       void LoadFromXml(TiXmlElement & root);
//   };
// 
//   class DropDownList : public Pixmap3x1 {
//     public:
//       DropDownList(Gui & g, Widget * p);
//       DropDownList(Gui & g, Widget * p, Mesh & pm);
// 
//       void LoadFromXml(TiXmlElement & root);
//   };
// 
//   class ListBox : public Widget {
//   };
// 
//   class TabWidget : public Widget {
//   };
// 
//   class Window : public Widget {
//   };
// 
//   class RenderView : public Widget {
//   };

}
