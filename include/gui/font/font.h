//
//  Copyright (C) 2007-2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file font.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <boost/smart_ptr.hpp>
#include <boost/utility.hpp>

#include "types.h"

#include "math/vector.h"
#include "renderer/texture.h"

namespace tre {

  struct GlyphInfo {
    wchar_t code;

    int bearing_x;
    int bearing_y;
    uint width;
    uint height;

    uint advance_x;

    TexturePtr texture;
    Vector4f texCoords;  // [left, top, right, bottom]
  };

  struct FaceInfo {
    uint height;
    int ascender;
    int descender;
  };

  typedef uint FontStyle;
  const FontStyle fsRegular = 0;
  const FontStyle fsBold = 1;
  const FontStyle fsItalic = 2;
  const FontStyle fsBoldItalic = 3;
  const FontStyle fsLast = 4;

  class FontFace;
  class Font;

  typedef boost::shared_ptr<Font> FontPtr;
  typedef boost::weak_ptr<Font> FontRef;

  class FontFactory {
    public:
      const FontPtr CreateInstance(const std::string & name);

    private:
      typedef std::tr1::unordered_map<std::string, FontRef> FontMap;

    private:
      FontMap map_;
  };

  class Font : public boost::noncopyable {
    public:
      static FontFactory & Factory(void)
      {
        static FontFactory fac;
        return fac;
      }

      static const GlyphInfo * GetFillerGlyph(void);

      static const uint defTextSize = 12;

    public:
      Font();
      ~Font();

      const GlyphInfo * GetGlyph(wchar_t chr, FontStyle style=fsRegular);

      int GetKerning(wchar_t chr1, wchar_t chr2, FontStyle style=fsRegular);

      void SetSize(uint size) {size_ = size;}
      void SetAntialias(bool on) {aalias_ = on;}

      const FaceInfo GetFaceInfo(FontStyle style=fsRegular);

      void LoadFromXml(const std::string & path);

    private:
      FontFace * faces_[fsLast];
      std::string files_[fsLast];

      bool aalias_;
      uint size_;
  };

}
