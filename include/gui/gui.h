//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gui.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <boost/signal.hpp>
#include <boost/utility.hpp>
#include <boost/smart_ptr.hpp>

#include "platform.h"

#ifdef PLATFORM_MSVC
#  include <unordered_map>
#else /* PLATFORM_MSVC */
#  include <tr1/unordered_map>
#endif /* PLATFORM_MSVC */

#include "math/volumes.h"
#include "input/input.h"

class TiXmlElement;

namespace tre {

  class Widget;
  class Application;
  class Gui;
  class GuiSkin;
  class Canvas;
  class Cursor;
  class CursorSet;

  class EventCombiner;

  typedef boost::shared_ptr<GuiSkin> GuiSkinPtr;
  typedef boost::shared_ptr<Canvas> CanvasPtr;
  typedef boost::shared_ptr<CursorSet> CursorSetPtr;

  typedef boost::shared_ptr<Gui> GuiPtr;
  typedef boost::weak_ptr<Gui> GuiRef;

  class GuiFactory {
    public:
      GuiPtr CreateInstance(Application & app, const CanvasPtr & canvas,
        const std::string & filename, const std::string & skin);
  };

  class Gui : private boost::noncopyable {
    public:
      static GuiFactory & Factory(void)
      {
        static GuiFactory fac;
        return fac;
      }

    public:
      class EventCombiner {
        public:
          typedef bool result_type;

          template<typename InputIterator>
          bool operator () (InputIterator first, InputIterator last) const
          {
            while(first != last) {
              if(*first)
                return true;
              ++first;
            }
            return false;
          }
      };

    public:
      // public signals
      boost::signal<void (uint)> buttonOn;
      boost::signal<void (uint)> buttonOff;
      boost::signal<bool (char chr, KeySym sym), EventCombiner> keyOn;
      boost::signal<bool (char chr, KeySym sym), EventCombiner> keyOff;
      boost::signal<void (const Vector2f &)> mouseMove;

    public:
      Gui(Application & app, const CanvasPtr & cnv);
      ~Gui();

      Application & GetApplication(void) {return app_;}

      // size & scale
      Vector2f GetSize(void) const {return size_;}
      Vector2f GetScaleFactor(void) const;

      void SetCanvas(const CanvasPtr & cnv);

      const GuiSkinPtr & GetSkin(void) const {return skin_;}
      const Vector3f GetMousePosition(void) const
        {return mousePos_.GetWPosition();};

      // widget queries
      const Widget * GetRootWidget(void) const {return rootWdg_;}
      Widget * GetRootWidget(void) {return rootWdg_;}

      const Widget * FindWidget(const std::string & name) const;
      Widget * FindWidget(const std::string & name);

      bool RegisterWidget(Widget * wdg, const std::string & name);
      void UnregisterWidget(const std::string & name);

      // skin manipulation
      bool UseSkin(const std::string & filename);
      void ResetSkin(void);

      // cursor manipulation
      void ShowCursor(bool on);
      void SetCursor(const std::string & name);

      // input handling
      void MouseMove(const Vector2f & abs, const Vector2f & rel);
      bool ProcessInput(const InputEvent & event);

      // draws recursively the entire gui
      void Draw(void);

      void LoadFromFile(const std::string & filename);

// TODO: drag & drop - StartDrag(dragwidget)

    private:
      typedef std::tr1::unordered_map<std::string, Widget*> WidgetMap;

    private:
      Application & app_;
      CanvasPtr canvas_;

      //  dimensions & scaling
      bool scale_;   /// gui scales to fit the canvas, keeping it's proportions
      Vector2f size_;
      Vector2f scaleBase_;

      // widget templates & cursor set
      GuiSkinPtr skin_;
      CursorSetPtr cursors_;

      // root widget
      Widget * rootWdg_;

      // various states
      Widget * lastTop_;
      Transformf mousePos_;

      // default 2d camera
      PVolume cam_;

      // cursor
      const Cursor * actCursor_;
      std::string curName_;
      bool showCursor_;

      // widget map
      WidgetMap wdgRegistry_;
  };

}
