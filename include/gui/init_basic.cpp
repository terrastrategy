//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file init_basic.cpp
 *
 *  blahblah
 */

#include "widget.h"
#include "basic.h"

namespace {

  class InitBasicWidgets {
    public:
      InitBasicWidgets()
      {
        tre::Widget::Factory().RegisterClass("Frame",
          &tre::Frame::CreateWidget);
        tre::Widget::Factory().RegisterClass("FrameRaised",
          &tre::FrameRaised::CreateWidget);
        tre::Widget::Factory().RegisterClass("FrameSunken",
          &tre::FrameSunken::CreateWidget);
        tre::Widget::Factory().RegisterClass("PushButton",
          &tre::PushButton::CreateWidget);
//         tre::Widget::Factory().RegisterClass("EditBox",
//           &tre::EditBox::CreateWidget);
//         tre::Widget::Factory().RegisterClass("CheckBox",
//           &tre::CheckBox::CreateWidget);
//         tre::Widget::Factory().RegisterClass("RadioButton",
//           &tre::RadioButton::CreateWidget);
//         tre::Widget::Factory().RegisterClass("DropDownList",
//           &tre::DropDownList::CreateWidget);
//         tre::Widget::Factory().RegisterClass("ListBox",
//           &tre::ListBox::CreateWidget);
        tre::Widget::Factory().RegisterClass("StaticText",
          &tre::StaticText::CreateWidget);
        tre::Widget::Factory().RegisterClass("StaticRichText",
          &tre::StaticRichText::CreateWidget);
      }
  };

  InitBasicWidgets sInitBasicWidgets;

}
