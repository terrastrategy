//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file pixmap.h
 *
 *  Ble.
 */

#pragma once

#include <string>

#include "renderer/buffers.h"
#include "renderer/effect.h"

#include "math/transform.h"
#include "gui/widget.h"

class TiXmlElement;

namespace tre {

  class WidgetTemplate;

  class Pixmap {
    public:
      Pixmap(Transformf & t, float scale);
      ~Pixmap();

      void SetMap(uint u, WidgetState w);

      void DrawPixmap(void);
      virtual void ResizePixmap(const Vector2f & sz) = 0;

      void SetScaleFactor(const Vector2f & scale) {scaleFactor_ = scale;}

      void LoadFromXmlTemplate(TiXmlElement & root,
        const GuiSkinPtr & skin, const std::string & name, const Vector2f & sz);

    protected:
      // ustate x wstate -> texture maps
      TexturePtr maps_[Widget::maxUState][wsLast];

      // geometry info
      AttribBufferSet * attribs_;
      IndexBuffer * indices_;
      EffectPtr effect_;
      EffectVars vars_;

      Vector2f scaleFactor_;

    protected:
      virtual void SetUpFromTemplate(
        const WidgetTemplate & tmpl, const Vector2f & sz) = 0;

    private:
      Transformf & trans_;

    private:
      void LoadStateMaps(const WidgetTemplate & tmpl);
  };

  class Pixmap3x3 : public Pixmap {
    public:
      Pixmap3x3(Transformf & t, float scale) : Pixmap(t, scale) {}

      void ResizePixmap(const Vector2f & sz);

      void SetUpFromTemplate(const WidgetTemplate & tmpl, const Vector2f & sz);

    private:
      // padding
      float pLeft_;
      float pRight_;
      float pTop_;
      float pBottom_;
  };

  class Pixmap3x1 : public Pixmap {
    public:
      Pixmap3x1(Transformf & t, float scale) : Pixmap(t, scale) {}

      float GetHeight(void) const {return pHeight_;}

      void ResizePixmap(const Vector2f & sz);

      void SetUpFromTemplate(const WidgetTemplate & tmpl, const Vector2f & sz);

    private:
      // padding
      float pLeft_;
      float pRight_;
      float pHeight_;
  };

  class Pixmap2x1L : public Pixmap {
    public:
      Pixmap2x1L(Transformf & t, float scale) : Pixmap(t, scale) {}

      float GetHeight(void) const {return pHeight_;}

      void ResizePixmap(const Vector2f & sz);

      void SetUpFromTemplate(const WidgetTemplate & tmpl, const Vector2f & sz);

    private:
      // padding
      float pLeft_;
      float pHeight_;
  };

  class Pixmap1x1 : public Pixmap {
    public:
      Pixmap1x1(Transformf & t, float scale) : Pixmap(t, scale) {}

      float GetHeight(void) const {return pHeight_;}
      float GetWidth(void) const {return pWidth_;}

      void ResizePixmap(const Vector2f & sz);

      void SetUpFromTemplate(const WidgetTemplate & tmpl, const Vector2f &);

    private:
      float pWidth_;
      float pHeight_;
  };

}
