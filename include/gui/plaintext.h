//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file plaintext.h
 *
 *  Ble.
 */

#pragma once

#include <wctype.h>
#include <vector>
#include <string>

#include "types.h"
#include "math/vector.h"

#include "text.h"

namespace tre {

  class PlainText : public Text {
    public:
      PlainText();
      ~PlainText();

      void SetStyle(const TextStyle & style);
      const TextStyle & GetStyle(void) const {return style_;}

      void SetText(const std::string & text);
      void SetText(const std::wstring & text);

      void ClearText(void);

    protected:
      void RedrawText(bool reset);

    private:
      // structure used for text processing
      struct GlyphLine {
        GlyphPrepVector glyphs;
        int offset;
        int baseline;
        uint height;
        uint width;
      };

      typedef std::vector<GlyphLine> GLineVector;

      typedef std::vector<const GlyphInfo*> GlyphVector;

    private:
      TextStyle style_;
      GlyphVector glyphs_;

    private:
      void WriteText(const GLineVector & lines, uint width, uint height);
  };

}
