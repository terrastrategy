//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file richtext.h
 *
 *  Ble.
 */

#pragma once

#include <list>
#include <vector>
#include <string>

#include "types.h"
#include "math/vector.h"

#include "text.h"

class TiXmlElement;

namespace tre {

  enum StyleSheet {
    ssBase = 0,
    ssLink,
    ssLinkHover,
    ssLinkPressed,
    ssEmph,
    ssLast
  };

  enum CaretTarget {
    ctTextStart,
    ctTextEnd,
    ctLineStart,
    ctLineEnd,
    ctWordLeft,
    ctWordRight,
    ctLeft,
    ctRight,
    ctUp,
    ctDown
  };

  class RichText : public Text {
    public:
      static const uint caretWidth = 3;

    public:
      RichText();
      ~RichText();

      void SetStyleSheet(StyleSheet sheet, const TextStyle & style);
      const TextStyle & GetStyleSheet(StyleSheet sheet) const
        {return styles_[sheet];}

      void ResetStyles(void);

      void SetStyle(const TextStyle & style);
      const TextStyle & GetStyle(void) const {return styles_[ssBase];}

      void SetText(const std::string & text);
      void SetText(const std::wstring & text);

      void ClearText(void);
      const std::wstring GetText(void) const;

      // auto-format
      bool GetWrap(void) const {return wrap_;}
      void SetWrap(bool wrap);

      void SetParagraphSpacing(int space);
      void SetLineSpacing(int space);

      const std::wstring GetWord(const Vector2f & pos) const;
      const std::string GetLink(const Vector2f & pos) const;

      void SetHoverLink(const std::string & id);
      void SetPressedLink(const std::string & id);

      void ShowCaret(bool show);
      bool IsCaretVisible(void) const {return showCaret_;}
      void SetCaretToCursor(const Vector2f & pos);
      void MoveCaret(CaretTarget pos);

    protected:
      void RedrawText(bool reset);

    /*
     * Datatypes for the tree used to store the styled rich text
     */
    private:
      struct RichNode;

      typedef std::vector<const GlyphInfo*> GlyphVector;

      struct RichLeaf {
        RichNode * parent;
        std::wstring text;
        GlyphVector glyphs;
        TextStyle style;

        RichLeaf() : parent(NULL) {}
        RichLeaf(RichNode * p);

        void ResetStyle(void);
      };

      typedef std::list<RichLeaf> LeavesList;

      struct RichNodeChild {
        union {
          RichNode * node;
          RichLeaf * leaf;
        };
        bool isNode;

        RichNodeChild() : node(NULL), isNode(true) {}
        RichNodeChild(RichNode * n) : node(n), isNode(true) {}
        RichNodeChild(RichLeaf * l) : leaf(l), isNode(false) {}
      };

      typedef std::vector<RichNodeChild> NodeChildVector;

      enum NodeType {
        ntPreDef,     // TODO: custom styles? extra would contain style name
        ntLink,       /// the same as ntPreDef, but extra contains link id
        ntColour,
        ntSize,
        ntFontStyle
      };

      struct RichNode {
        union {
          TextStyle * predef;
          float colour[4];
          float size;
          FontStyle fstyle;
        };
        NodeType type;

        std::string extra;    /// depends on the node type (link = id)

        RichNode * parent;
        NodeChildVector children;

        ~RichNode();
      };

    /*
     * Datatypes for the look-up table
     */
    private:
      /// [text chunk, letter number]
      typedef std::pair<LeavesList::iterator, uint> LetterPair;
      typedef std::vector<LetterPair> LetterPairVector;

      struct Letter {
        int start;
        uint width;

        LetterPair letter;

        Letter(uint s, uint w, LetterPair p) : start(s), width(w), letter(p) {}

        bool operator < (const Letter & r) const
        {
          return start < r.start;
        }

        friend bool operator < (const Letter & l, int r)
        {
          return l.start < r;
        }

        friend bool operator < (int l, const Letter & r)
        {
          return l < r.start;
        }
      };

      typedef std::vector<Letter> LetterVector;

      /// letters x [line offset, line height]
      typedef std::pair<LetterVector, Vector2u> LinePair; 
      typedef std::vector<LinePair> LineVector;

      class LineLess;

    /*
     * Text processing
     */
    private:
      struct GlyphLine {
        GlyphPrepVector glyphs;
        LetterPairVector letters;   // needed to create the look-up table
        int offset;
        int baseline;
        uint height;
        uint width;
        uint wspace;        // number of spaces on line (for stretching)
        HorizAlign align;   // line alignment
      };

      typedef std::vector<GlyphLine> GLineVector;

    /*
     * Private attributes
     */
    private:
      /// predefined styles
      TextStyle styles_[ssLast];

      // auto-format parameters
      bool wrap_;         /// word wrap
      int lineSpace_;     /// distance between two lines
      int parSpace_;      /// distance between two paragraphs

      // style tree
      RichNode root_;     /// root node of the style tree
      LeavesList leaves_; /// list of leaves of the style tree

      LineVector lines_;  /// look-up table for position (x,y) based search

      // caret
      Vector2u caret_;    /// caret position (row, col) in lines_
      bool showCaret_;

      const GlyphInfo * fillerGlyph_;
      AttribBufferSet * caretAttribs_;
      IndexBuffer * caretIndices_;
      EffectVars * caretVars_;

    private:
      const LetterPair * GetLetter(const Vector2f & pos) const;

      void ParseText(TiXmlElement & root, RichNode * rnode, bool & prev_spc);

      void RedrawCaret(void);

      void CalcLineHeight(GlyphLine & line);
      void WriteText(const GLineVector & lines, uint width, uint height);
      void WriteLine(const GlyphPrepVector & glyphs, const LetterPairVector
        & letters, int x_off, int y_off, uint ws_fill, uint ws);
  };

}
