//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file text.h
 *
 *  Ble.
 */

#pragma once

#include <vector>
#include <deque>
#include <string>
#include <boost/utility.hpp>

#include "types.h"
#include "math/vector.h"
#include "math/matrix.h"

#include "font/font.h"

#include "renderer/renderer.h"
#include "renderer/buffers.h"
#include "renderer/effect.h"

namespace tre {

  struct GlyphInfo;
  class Font;

  enum HorizAlign {
    haLeft,
    haCenter,
    haRight,
    haStretch
  };

  enum VertAlign {
    vaTop,
    vaMiddle,
    vaBottom
  };

  struct TextStyle {
    uint size;
    FontStyle fstyle;
    Vector4f colour;

    TextStyle() {}
    TextStyle(uint s, FontStyle t, const Vector4f & c)
      : size(s), fstyle(t), colour(c) {}
  };

  class Text : private boost::noncopyable {
    public:
      Text();
      virtual ~Text();

      void SetRect(uint width, uint height);
      void SetHAlign(HorizAlign align);
      void SetVAlign(VertAlign align);

      void SetFont(const std::string & font);

      void SetEffect(const std::string & fx);

      void SetAntialias(bool on);
      void SetScaleFactor(const Vector2f scale);

      virtual void SetStyle(const TextStyle & style) = 0;
      virtual const TextStyle & GetStyle(void) const = 0;

      virtual void SetText(const std::string & text) = 0;
      virtual void SetText(const std::wstring & text) = 0;

      virtual void ClearText(void) = 0;
      virtual const std::wstring GetText(void) const {return text_;}

      const Vector4i & GetTextRect(void) const {return textRect_;}

      void SetTransform(const Matrix4x4f * mat);
      GeometryBatch & GetGeometry(void) {return batch_;}

    protected:
      // structures for text parsing and mesh creation
      struct GlyphPrep {
        const GlyphInfo * glyph;
        TextStyle * style;
        int offset;

        GlyphPrep() {}
        GlyphPrep(const GlyphInfo * g, TextStyle * ts, int o)
          : glyph(g), style(ts), offset(o) {}
      };

      typedef std::vector<GlyphPrep> GlyphPrepVector;

      // geometry
      typedef std::pair<const GlyphPrep*, Vector2i> PagePrepPair;
      typedef std::deque<PagePrepPair> PagePrepQueue;

      struct MeshPage {
        IndexBuffer * indices;
        EffectVars * vars;

        // used for refilling
        PagePrepQueue prep;
      };

      typedef std::vector<MeshPage> PageVector;

    protected:
      FontPtr font_;

      std::wstring text_;

      // determines, whether font will be rendered via blending or alpha testing
      bool antialias_;

      // alignment
      Vector2u bounds_;
      HorizAlign halign_;
      VertAlign valign_;

      Vector4i textRect_;     // [left, top, right, bottom]

      // scale factor - for matching different resolutions
      Vector2f scaleFactor_;

      // render batch related info
      EffectPtr effect_;
      const Matrix4x4f * trans_;

      GeometryBatch batch_;   // pre-built batch

    protected:
      virtual void RedrawText(bool reset) = 0;

      void ResetPages(void);
      MeshPage & GetPageByTexture(const TexturePtr & texture);

    private:
      // geometry information
      AttribBufferSet * abuffers_;
      PageVector pages_;      // pages in texture cache
  };

}
