//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file widget.h
 *
 *  Ble.
 */

#pragma once

#include <vector>
#include <string>
#include <map>

#include "types.h"

#include "input/input.h"
#include "math/transform.h"

// TODO: minimal constraints (perhaps constraints stuct + max)
//   for internal use, filled in constructor (according to pixmap)

class TiXmlElement;

namespace tre {

  enum WidgetState {
    wsNormal = 0,
    wsDisabled,
    wsPressed,
    wsHover,
    wsLast
  };

  typedef byte WidgetAnchor;
  const WidgetAnchor waNone = 0x00;
  const WidgetAnchor waLeft = 0x01;
  const WidgetAnchor waTop = 0x02;
  const WidgetAnchor waRight = 0x04;
  const WidgetAnchor waBottom = 0x08;
  const WidgetAnchor waWidth = 0x10;
  const WidgetAnchor waHeight = 0x20;
  const WidgetAnchor waSize = 0x30;

  class Gui;
  class Widget;

  typedef Widget*(CreateWidgetPtr(Gui & gui, Widget * parent));

  class WidgetFactory {
    public:
      Widget * CreateInstance(const std::string & filename, Gui & gui,
        Widget * parent=NULL);
      Widget * CreateInstance(TiXmlElement & root, Gui & gui,
        Widget * parent=NULL);

      void RegisterClass(const std::string & classname, CreateWidgetPtr * func);

    private:
      typedef std::map<std::string, CreateWidgetPtr*> FactoryMap;

    private:
      FactoryMap ptrMap_;
  };

  class Widget {
    public:
      static WidgetFactory & Factory(void)
      {
        static WidgetFactory fac;
        return fac;
      }

      static const uint maxUState = 3;

    public:
      typedef std::vector<Widget*> WdgVector;

    public:
      Widget * parent;
      WdgVector children;  // the last is the topmost

      bool visible;

    public:
      Widget();
      virtual ~Widget();

      // initialization
      void Init(Gui & g, Widget * p);

      // widget names
      const std::string & GetName(void) const {return name_;}
      void SetName(const std::string & nm);

      // position and size
      const Vector2f GetPosition(void) const;
      const Vector2f GetAbsPosition(void) const;

      void SetPosition(const Vector2f & pos);

      const Vector2f & GetSize(void) const {return size_;}
      virtual void SetSize(const Vector2f & sz);

      // child manipulation routines (for export)
      Widget * GetChild(uint n);
      void AppendChild(Widget * wdg);
      void InsertChild(Widget * wdg, uint pos);

      // rect test
      bool IsInRegion(const Vector2f & pos) const;

      // state
      void Enable(bool state);
      void SetUserState(uint state);

      void RaiseChild(Widget * child);

      virtual void Draw(void) = 0;

      // hardcoded internal events
      virtual void OnMouseEnter(void);
      virtual void OnMouseExit(void);
      virtual void OnGuiResize(const Vector2f & scale);

      virtual void LoadFromXml(TiXmlElement & root);
      void LoadChildrenFromXml(TiXmlElement & root);

    protected:
      // gui this widget belongs to
      Gui * gui_;

      // widget id
      std::string name_;

      // size & pos
      Vector2f size_;
      WidgetAnchor anchor_;

      bool floating_;   // may move up/down the Z axis (when receiving focus)

      Transformf trans_;

      // state
      uint ustate_;
      WidgetState wstate_;

      // widget is locked by it's parent when it gets disabled
      bool locked_;
      WidgetState prevState_;

    protected:
      void DrawChildren(void);

      virtual void SetState(uint u, WidgetState w);
  };

}
