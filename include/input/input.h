//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file input.h
 *
 *  Encapsulates chosen input subsystem
 */

#pragma once

#include "types.h"
#include "math/vector.h"

namespace tre {

 /*
  *  Common input related data types
  */

  typedef unsigned int KeyMod;
  const KeyMod kmLShift = 0x0001;
  const KeyMod kmRShift = 0x0002;
  const KeyMod kmLCtrl  = 0x0040;
  const KeyMod kmRCtrl  = 0x0080;
  const KeyMod kmLAlt   = 0x0100;
  const KeyMod kmRAlt   = 0x0200;
  const KeyMod kmLMeta  = 0x0400;
  const KeyMod kmRMeta  = 0x0800;
  const KeyMod kmNum    = 0x1000;
  const KeyMod kmCaps   = 0x2000;
  const KeyMod kmMode   = 0x4000;
  const KeyMod kmShift  = kmLShift | kmRShift;
  const KeyMod kmCtrl   = kmLCtrl | kmRCtrl;
  const KeyMod kmAlt    = kmLAlt | kmRAlt;
  const KeyMod kmMeta   = kmLMeta | kmRMeta;

  // keysymbols for control characters
  enum KeySym {
    ksNone,
    ksEscape,
    ks1, ks2, ks3, ks4, ks5, ks6, ks7, ks8, ks9, ks0,
    ksMinus,      // - on main keyboard
    ksEquals,
    ksBack,       // backspace
    ksTab,
    ksQ, ksW, ksE, ksR, ksT, ksY, ksU, ksI, ksO, ksP,
    ksLBrack,     // [
    ksRBrack,     // ]
    ksReturn,     // enter on main keyboard
    ksLCtrl,
    ksA, ksS, ksD, ksF, ksG, ksH, ksJ, ksK, ksL,
    ksSemicol,    // semicolon
    ksApos,       // apostrophe
    ksGrave,      // accent
    ksLShift,
    ksBacksl,     // backslash
    ksZ, ksX, ksC, ksV, ksB, ksN, ksM,
    ksComma,
    ksPer,        // . on main keyboard
    ksSlash,      // / on main keyboard
    ksRShift,
    ksKpMul,      // * on numeric keypad
    ksLMenu,      // left alt
    ksSpace,
    ksCap,        // capital
    ksF1, ksF2, ksF3, ksF4, ksF5, ksF6, ksF7, ksF8, ksF9, ksF10,
    ksNum,        // num lock
    ksScroll,     // scroll lock
    ksKp7, ksKp8, ksKp9,
    ksKpSub,      // - on numeric keypad
    ksKp4, ksKp5, ksKp6,
    ksKpAdd,      // + on numeric keypad
    ksKp1, ksKp2, ksKp3, ksKp0,
    ksKpDec,      // . on numeric keypad
    ksOem_102,    // < > | on UK/Germany keyboards
    ksF11, ksF12,
    ksF13, ksF14, ksF15, // (NEC PC98)
    ksKana,       // (Japanese keyboard)
    ksAbnt_C1,    // / ? on Portugese (Brazilian) keyboards
    ksConvert,    // (Japanese keyboard)
    ksNoconvert,  // (Japanese keyboard)
    ksYen,        // (Japanese keyboard)
    ksAbnt_C2,    // Numpad . on Portugese (Brazilian) keyboards
    ksKpEquals,   // = on numeric keypad (NEC PC98)
    ksPrevTrack,  // Previous Track (ksCIRCUMFLEX on Japanese keyboard)
    ksAt,         // (NEC PC98)
    ksColon,      // (NEC PC98)
    ksUnderline,  // (NEC PC98)
    ksKanji,      // (Japanese keyboard)
    ksStop,       // (NEC PC98)
    ksAx,         // (Japan AX)
    ksUnlabeled,  // (J3100)
    ksNextTrack,  // Next Track
    ksKpEnter,    // Enter on numeric keypad
    ksRCtrl,
    ksMute,       // Mute
    ksCalc,       // Calculator
    ksPlayPause,  // Play / Pause
    ksMediaStop,  // Media Stop
    ksVolDown,    // Volume -
    ksVolUp,      // Volume +
    ksWebHome,    // Web home
    ksKpComma,    // , on numeric keypad (NEC PC98)
    ksKpDiv,      // / on numeric keypad
    ksSysRq,
    ksRMenu,      // right Alt
    ksPause,      // Pause
    ksHome,       // Home on arrow keypad
    ksUp,         // UpArrow on arrow keypad
    ksPgUp,       // PgUp on arrow keypad
    ksLeft,       // LeftArrow on arrow keypad
    ksRight,      // RightArrow on arrow keypad
    ksEnd,        // End on arrow keypad
    ksDown,       // DownArrow on arrow keypad
    ksPgDown,     // PgDn on arrow keypad
    ksIns,        // Insert on arrow keypad
    ksDel,        // Delete on arrow keypad
    ksLWin,       // Left Windows key
    ksRWin,       // Right Windows key
    ksApps,       // AppMenu key
    ksPower,      // System Power
    ksSleep,      // System Sleep
    ksWake,       // System Wake
    ksWebSearch,  // Web Search
    ksWebFav,     // Web Favorites
    ksWebRefresh, // Web Refresh
    ksWebStop,    // Web Stop
    ksWebForward, // Web Forward
    ksWebBack,    // Web Back
    ksMyComp,     // My Computer
    ksMail,       // Mail
    ksMediaSelect,  // Media Select
    // last value
    ksLast
  };

  enum EventType {
    etNone,
    etQuit,
    etButtonOn,
    etButtonOff,
    etKeyOn,
    etKeyOff
  };

  struct InputEvent {
    EventType type;

    union {
      uint button;

      struct {
        char chr;
        KeySym sym;
      } key;
    };
  };

 /*
  *  Input interface
  */

  class InputDevices;

  class Input {
    public:
      Input();
      ~Input();

      const Vector2f GetPosition(void);
      const Vector2f GetPositionChange(void);
      void SetPosition(const Vector2f & v);

      bool GetButtonState(uint id);
      bool GetCharacterState(char chr);
      bool GetKeysymState(KeySym sym);
      KeyMod GetKeyMod(void);

      void PumpEvents(void);
      void DropEvents(void);

      InputEvent GetNextEvent(void);

    private:
      class InputInternals;

      InputInternals * internals_;
  };

  Input & sInput(void);
}
