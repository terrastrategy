//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file math.h
 *
 *  Contains all relevant math related classes.
 */

#pragma once

#include <math.h>

#include "vector.h"

namespace tre {

 /*
  *  Constants
  */

  /** PI constant */
  const float pi = 3.1415926535897932384626433832795f;

 /*
  *  Helper functions
  */

  /** Converts angle from degrees to radians
      ((d)*2.0f*PI/360.0f) */
  template <typename T>
  inline T Deg2Rad(T a) {return a * 0.01745329252f;}

  /** Converts angle from radians to degrees
      ((r)*360.0f/(2.0f*PI)) */
  template <typename T>
  inline T Rad2Deg(T a) {return a * 57.29577951f;}

 /*
  *  Declarations of all classes
  */

  template <typename T> class Line2;
  template <typename T> class Line3;
  template <typename T> class Plane;

 /*
  *  Convenience typedefs
  */
  typedef Plane<float> Planef;
  typedef Plane<double> Planed;

 /*
  *  Declarations of utility functions
  */

  template <typename T> T Distance
    (const Vector2<T> & v, const Line2<T> & l);
  template <typename T> T Distance
    (const Line2<T> & l, const Vector2<T> & v);
  template <typename T> T Distance
    (const Vector3<T> & v, const Line3<T> & l);
  template <typename T> T Distance
    (const Line3<T> & l, const Vector3<T> & v);
  template <typename T> T Distance
    (const Vector3<T> & v, const Plane<T> & p);
  template <typename T> T Distance
    (const Plane<T> & p, const Vector3<T> & v);

  template <typename T> T BezierInterpolation
  (T t, const T & cp1, const T & cp2, const T & cp3, const T & cp4);

  template <typename T>
  T Lerp(T t, const T & p1, const T & p2);

 /*
  *  Line classes
  */

  template <typename T>
  class Line2 {
    public:
      Vector2<T> A;  // X = A + t*s
      Vector2<T> s;

      Vector2<T> n;  // ax + by + c = 0 => a*n.x + b*n.y - c = 0
      T c;    // represents distance from origin

    public:
      Line2() {}
      Line2(const Vector2<T> & v1, const Vector2<T> & v2);
      Line2(const Line2<T> & l) : A(l.A), s(l.s), n(l.n), c(l.c) {}

      void operator = (const Line2<T> & l);

      void Set(const Vector2<T> & vA, const Vector2<T> & vs);

      T GetParam(const Vector2<T> & v) const;
  };

  template <typename T>
  class Line3 {
    public:
      Vector3<T> A; // parametrical representation: X = A + t*s
      Vector3<T> s;

    public:
      Line3() {}
      Line3(const Vector3<T> & v1, const Vector3<T> & v2) : A(v1), s(v2) {}
      Line3(const Line3<T> & l) : A(l.A), s(l.s) {}

      Line3<T> & operator = (const Line3<T> & l);

      void Set(const Vector3<T> & b, const Vector3<T> & t);

      T GetParam(Vector3<T> & v) const;
  };

 /*
  *  Plane
  */

  template <typename T>
  class Plane {
    public:
      Vector3<T> n;  // ax + by + cz + d = 0 => a*n.x + b*n.y + c*n.z - d = 0
      T d;    // represents distance from origin

    public:
      Plane() {}
      Plane(const Vector3<T> & v1, T f) : n(v1), d(f) {}
      Plane(const Vector3<T> & v1, const Vector3<T> & v2, const Vector3<T> & v3)
        {ABCD(v1,v2,v3);}

      Plane<T> & operator = (const Plane<T> & p);

      void ABCD(const Vector3<T> & v1, const Vector3<T> & v2,
        const Vector3<T> & v3);
      void Set(const Vector3<T> & v, T f);
  };

 /*
  *  Inline methods
  */
  template <typename T>
  inline Line2<T>::Line2(const Vector2<T> & v1, const Vector2<T> & v2)
    : A(v1), s(v2)
  {
    n.x = s.y;
    n.y = -s.x;
    n.Normalize();
    c = A.x*n.x + A.y*n.y;
  }

  template <typename T>
  inline void Line2<T>::operator = (const Line2<T> & l)
  {
    A = l.A;
    s = l.s;
    n = l.n;
    c = l.c;
  }

  template <typename T>
  inline void Line2<T>::Set(const Vector2<T> & vA, const Vector2<T> & vs)
  {
    A = vA;
    s = vs;

    n.x = s.y;
    n.y = -s.x;
    n.Normalize();
    c = Dot(A, n);
  }

  // returns parameter t for point v (v = A + s*t)
  template <typename T>
  inline T Line2<T>::GetParam(const Vector2<T> & v) const
  {
    return s.x != 0.0f ? (v.x - A.x) / s.x : (v.y - A.y) / s.y;
  }

  template <typename T>
  inline Line3<T> & Line3<T>::operator = (const Line3<T> & l)
  {
    A = l.A;
    s = l.s;
    return *this;
  }

  template <typename T>
  inline void Line3<T>::Set(const Vector3<T> & b, const Vector3<T> & t)
  {
    A = b;
    s = t;
  }

  // returns parameter t for point v (v = A + s*t)
  template <typename T>
  inline T Line3<T>::GetParam(Vector3<T> & v) const
  {
    if(s.x != 0.0f)
      return (v.x - A.x) / s.x;
    else if(s.y != 0.0f)
      return (v.y - A.y) / s.y;
    else {
      return (v.z - A.z) / s.z;
    }
  }

  template <typename T>
  inline void Plane<T>::ABCD
  (const Vector3<T> & v1, const Vector3<T> & v2, const Vector3<T> & v3)
  {
    n.Cross(v2 - v1, v3 - v1);
    n.Normalize();
    d = Dot(n, v2);
  }

  template <typename T>
  inline Plane<T> & Plane<T>::operator = (const Plane<T> & p)
  {
    n = p.n;
    d = p.d;
    return *this;
  }

  template <typename T>
  inline void Plane<T>::Set(const Vector3<T> & v, T f)
  {
    n = v;
    d = f;
  }

 /*
  *  Utility functions
  */

  template <typename T>
  inline T Distance(const Vector2<T> & v, const Line2<T> & l)
  {
    return Distance(l, v);
  }

  template <typename T>
  inline T Distance(const Line2<T> & l, const Vector2<T> & v)
  {
    return Dot(l.n, v) - l.c;
  }

  template <typename T>
  inline T Distance(const Vector3<T> & v, const Line3<T> & l)
  {
    return Distance(l, v);
  }

  template <typename T>
  inline T Distance(const Line3<T> & l, const Vector3<T> & v)
  {
    Vector3<T> v2, up;
    Plane<T> p;

    v2 = l.A + l.s;
    up = l.A + v2.Cross(v - l.A);
    p.ABCD(l.A, v2, up);
    return Distance(p, v);
  }

  template <typename T>
  inline T Distance(const Vector3<T> & v, const Plane<T> & p)
  {
    return Distance(p, v);
  }

  template <typename T>
  inline T Distance(const Plane<T> & p, const Vector3<T> & v)
  {
    return Dot(p.n, v) - p.d;
  }

 /** Interpolates curve using simple linear equation.
  *
  *  @param[in] t Time - position on curve.
  *  @param[in] p1 Start point.
  *  @param[in] p2 End point.
  *  @return point on curve.
  */
  template <typename T>
  T LinearInterpolation(float t, const T & p1, const T & p2)
  {
    return t * p1 + (1.0f - t) * p2;
  }

 /** Interpolates curve using Bernstein multinominals.
  *
  *  @param[in] t Time - position on curve.
  *  @param[in] p1 Start point.
  *  @param[in] p2 First control point.
  *  @param[in] p3 Second control point.
  *  @param[in] p4 End point.
  *  @return point on curve.
  */
  template <typename T> T BezierInterpolation
  (T t, const T & cp1, const T & cp2, const T & cp3, const T & cp4)
  {
    T comp = 1.0 - t; // substitution

    T res = (comp * comp * comp) * cp1;
    res += (3.0f * comp * comp * t) * cp2;
    res += (3.0f * comp * t * t) * cp3;
    res += (t * t * t) * cp4;
    return res;
  }
}
