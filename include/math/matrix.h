//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file matrix.h
 *
 *  Contains all relevant math related classes.
 */

#pragma once

#include <algorithm>

#include "assertion.h"
#include "vector.h"

namespace tre {

 /*
  *  Declarations of all classes
  */

  template <typename T> class Matrix2x2;
  template <typename T> class Matrix3x3;
  template <typename T> class Matrix4x4;

 /*
  *  Convenience typedefs
  */
  typedef Matrix2x2<float> Matrix2x2f;
  typedef Matrix3x3<float> Matrix3x3f;
  typedef Matrix4x4<float> Matrix4x4f;
  typedef Matrix2x2<double> Matrix2x2d;
  typedef Matrix3x3<double> Matrix3x3d;
  typedef Matrix4x4<double> Matrix4x4d;

 /*
  *  Declarations of utility functions
  */

  template <typename T> const Matrix2x2<T> operator *
    (const Matrix2x2<T> & l, const Matrix2x2<T> & r);
  template <typename T> const Matrix3x3<T> operator *
    (const Matrix3x3<T> & l, const Matrix3x3<T> & r);
  template <typename T> const Matrix4x4<T> operator *
    (const Matrix4x4<T> & l, const Matrix4x4<T> & r);

  template <typename T> const Matrix4x4<T> operator *
    (const Matrix4x4<T> & l, const Matrix3x3<T> & r);
  template <typename T> const Matrix4x4<T> operator *
    (const Matrix3x3<T> & l, const Matrix4x4<T> & r);

  template <typename T> const Vector2<T> operator *
    (const Matrix2x2<T> & l, const Vector2<T> & r);
  template <typename T> const Vector3<T> operator *
    (const Matrix3x3<T> & l, const Vector3<T> & r);
  template <typename T> const Vector3<T> operator *
    (const Matrix4x4<T> & l, const Vector3<T> & r);
  template <typename T> const Vector4<T> operator *
    (const Matrix4x4<T> & l, const Vector4<T> & r);

 /*
  *  Matrix classes
  */

  template <typename T>
  class Matrix2x2 {
    public:
      Vector2<T> c1, c2;

    public:
      Matrix2x2() {}
      Matrix2x2(const Matrix2x2<T> & m) : c1(m.c1), c2(m.c2) {}
      Matrix2x2(const Vector2<T> & v1, const Vector2<T> & v2)
        : c1(v1), c2(v2) {}

      const Matrix2x2<T> operator + (const Matrix2x2<T> & m) const;
      const Matrix2x2<T> operator - (const Matrix2x2<T> & m) const;
      const Matrix2x2<T> operator * (T f) const;
      const Matrix2x2<T> operator / (T f) const;

      const Matrix2x2<T> operator - () const;

      Matrix2x2<T> & operator += (const Matrix2x2<T> & m);
      Matrix2x2<T> & operator -= (const Matrix2x2<T> & m);
      Matrix2x2<T> & operator *= (T f);
      Matrix2x2<T> & operator /= (T f);

      bool operator == (const Matrix2x2<T> & m) const;
      bool operator != (const Matrix2x2<T> & m) const;

      Matrix2x2<T> & operator = (const Matrix2x2<T> & m);

      Vector2<T> & operator [] (unsigned i);
      const Vector2<T> & operator [] (unsigned i) const;

      void Set(const Vector2<T> & v1, const Vector2<T> & v2);

      void Identity(void);

      const Matrix2x2<T> Transpose(void) const;
      void TransposeSelf(void);
  };

  template <typename T>
  class Matrix3x3 {
    public:
      static const Matrix3x3<T> CreateSMatrix(const Vector3<T> & s);
      static const Matrix3x3<T> CreateInvSMatrix(const Vector3<T> & s);
      static const Matrix3x3<T> CreateRMatrix(const Quaternion<T> & r);

    public:
      Vector3<T> c1, c2, c3;

    public:
      Matrix3x3() {}
      Matrix3x3(const Matrix3x3<T> & m) : c1(m.c1), c2(m.c2), c3(m.c3) {}
      Matrix3x3(const Vector3<T> & v1, const Vector3<T> & v2,
        const Vector3<T> & v3) : c1(v1), c2(v2), c3(v3) {}

      const Matrix3x3<T> operator + (const Matrix3x3<T> & m) const;
      const Matrix3x3<T> operator - (const Matrix3x3<T> & m) const;
      const Matrix3x3<T> operator * (T f) const;
      const Matrix3x3<T> operator / (T f) const;

      const Matrix3x3<T> operator - () const;

      Matrix3x3<T> & operator += (const Matrix3x3<T> & m);
      Matrix3x3<T> & operator -= (const Matrix3x3<T> & m);
      Matrix3x3<T> & operator *= (T f);
      Matrix3x3<T> & operator /= (T f);

      bool operator == (const Matrix3x3<T> & m) const;
      bool operator != (const Matrix3x3<T> & m) const;

      Matrix3x3<T> & operator = (const Matrix3x3<T> & m);

      Vector3<T> & operator [] (unsigned i);
      const Vector3<T> & operator [] (unsigned i) const;

      void Set(const Vector3<T> & v1, const Vector3<T> & v2,
        const Vector3<T> & v3);

      void Identity(void);

      const Matrix3x3<T> Transpose(void) const;
      void TransposeSelf(void);
  };

  template <typename T>
  class Matrix4x4 {
    public:
      static const Matrix4x4<T> CreateTMatrix(const Vector3<T> & p);
      static const Matrix4x4<T> CreateInvTMatrix(const Vector3<T> & p);

    public:
      Vector4<T> c1, c2, c3, c4;

    public:
      Matrix4x4() {}
      Matrix4x4(const Matrix4x4<T> & m)
        : c1(m.c1), c2(m.c2), c3(m.c3), c4(m.c4) {}
      Matrix4x4(const Vector4<T> & v1, const Vector4<T> & v2,
        const Vector4<T> & v3, const Vector4<T> & v4)
        : c1(v1), c2(v2), c3(v3), c4(v4) {}

      const Matrix4x4<T> operator + (const Matrix4x4<T> & m) const;
      const Matrix4x4<T> operator - (const Matrix4x4<T> & m) const;
      const Matrix4x4<T> operator * (T f) const;
      const Matrix4x4<T> operator / (T f) const;

      const Matrix4x4<T> operator - () const;

      Matrix4x4<T> & operator += (const Matrix4x4<T> & m);
      Matrix4x4<T> & operator -= (const Matrix4x4<T> & m);
      Matrix4x4<T> & operator *= (T f);
      Matrix4x4<T> & operator /= (T f);

      bool operator == (const Matrix4x4<T> & m) const;
      bool operator != (const Matrix4x4<T> & m) const;

      Matrix4x4<T> & operator = (const Matrix4x4<T> & m);

      Vector4<T> & operator [] (unsigned i);
      const Vector4<T> & operator [] (unsigned i) const;

      void Set(const Vector4<T> & v1, const Vector4<T> & v2,
               const Vector4<T> & v3, const Vector4<T> & v4);

      void Identity(void);

      const Matrix4x4<T> Transpose(void) const;
      void TransposeSelf(void);
  };

 /*
  *  Static member functions
  */

  template <typename T>
  const Matrix3x3<T> Matrix3x3<T>::CreateSMatrix(const Vector3<T> & s)
  {
    return Matrix3x3<T>(
      Vector3<T>(s.x, 0.0f, 0.0f),
      Vector3<T>(0.0f, s.y, 0.0f),
      Vector3<T>(0.0f, 0.0f, s.z));
  }

  template <typename T>
  const Matrix3x3<T> Matrix3x3<T>::CreateInvSMatrix(const Vector3<T> & s)
  {
    return Matrix3x3f(
      Vector3f(1.0f / s.x, 0.0f, 0.0f),
      Vector3f(0.0f, 1.0f / s.y, 0.0f),
      Vector3f(0.0f, 0.0f, 1.0f / s.z));
  }

  template <typename T>
  const Matrix3x3<T> Matrix3x3<T>::CreateRMatrix(const Quaternion<T> & r)
  {
    T wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;

    // calculate coefficients
    x2 = r.x + r.x; y2 = r.y + r.y;
    z2 = r.z + r.z;
    xx = r.x * x2; xy = r.x * y2; xz = r.x * z2;
    yy = r.y * y2; yz = r.y * z2; zz = r.z * z2;
    wx = r.w * x2; wy = r.w * y2; wz = r.w * z2;

    Matrix3x3<T> m;

    m.c1.x = 1.0f - (yy + zz);
    m.c1.y = xy - wz;
    m.c1.z = xz + wy;
    m.c2.x = xy + wz;
    m.c2.y = 1.0f - (xx + zz);
    m.c2.z = yz - wx;
    m.c3.x = xz - wy;
    m.c3.y = yz + wx;
    m.c3.z = 1.0f - (xx + yy);
    return m;
  }

  template <typename T>
  const Matrix4x4<T> Matrix4x4<T>::CreateTMatrix(const Vector3<T> & p)
  {
    return Matrix4x4<T>(
      Vector4<T>(1.0f, 0.0f, 0.0f, 0.0f),
      Vector4<T>(0.0f, 1.0f, 0.0f, 0.0f),
      Vector4<T>(0.0f, 0.0f, 1.0f, 0.0f),
      Vector4<T>(p.x, p.y, p.z, 1.0f));
  }

  template <typename T>
  const Matrix4x4<T> Matrix4x4<T>::CreateInvTMatrix(const Vector3<T> & p)
  {
    return Matrix4x4<T>(
      Vector4<T>(1.0f, 0.0f, 0.0f, 0.0f),
      Vector4<T>(0.0f, 1.0f, 0.0f, 0.0f),
      Vector4<T>(0.0f, 0.0f, 1.0f, 0.0f),
      Vector4<T>(-p.x, -p.y, -p.z, 1.0f));
  }

 /*
  *  Inline methods
  */

  template <typename T>
  inline const Matrix2x2<T> Matrix2x2<T>::operator +
  (const Matrix2x2<T> & m) const
  {
    return Matrix2x2(c1 + m.c1, c2 + m.c2);
  }

  template <typename T>
  inline const Matrix2x2<T> Matrix2x2<T>::operator -
  (const Matrix2x2<T> & m) const
  {
    return Matrix2x2(c1 - m.c1, c2 - m.c2);
  }

  template <typename T>
  inline const Matrix2x2<T> Matrix2x2<T>::operator * (T f) const
  {
    return Matrix2x2(c1 * f, c2 * f);
  }

  template <typename T>
  inline const Matrix2x2<T> Matrix2x2<T>::operator / (T f) const
  {
    return Matrix2x2(c1 / f, c2 / f);
  }

  template <typename T>
  inline const Matrix2x2<T> Matrix2x2<T>::operator - () const
  {
    return Matrix2x2(-c1, -c2);
  }

  template <typename T>
  inline Matrix2x2<T> & Matrix2x2<T>::operator += (const Matrix2x2<T> & m)
  {
    c1 += m.c1;
    c2 += m.c2;
    return *this;
  }

  template <typename T>
  inline Matrix2x2<T> & Matrix2x2<T>::operator -= (const Matrix2x2<T> & m)
  {
    c1 -= m.c1;
    c2 -= m.c2;
    return *this;
  }

  template <typename T>
  inline Matrix2x2<T> & Matrix2x2<T>::operator *= (T f)
  {
    c1 *= f;
    c2 *= f;
    return *this;
  }

  template <typename T>
  inline Matrix2x2<T> & Matrix2x2<T>::operator /= (T f)
  {
    c1 /= f;
    c2 /= f;
    return *this;
  }

  template <typename T>
  inline bool Matrix2x2<T>::operator == (const Matrix2x2<T> & m) const
  {
    return (c1 == m.c1) && (c2 == m.c2);
  }

  template <typename T>
  inline bool Matrix2x2<T>::operator != (const Matrix2x2<T> & m) const
  {
    return (c1 != m.c1) || (c2 != m.c2);
  }

  template <typename T>
  inline Matrix2x2<T> & Matrix2x2<T>::operator = (const Matrix2x2<T> & m)
  {
    c1 = m.c1;
    c2 = m.c2;
    return *this;
  }

  template <typename T>
  inline Vector2<T> & Matrix2x2<T>::operator [] (unsigned i)
  {
    DEBUG_ASSERT(i < 2);
    return *(&c1 + i);
  }

  template <typename T>
  inline const Vector2<T> & Matrix2x2<T>::operator [] (unsigned i) const
  {
    DEBUG_ASSERT(i < 2);
    return *(&c1 + i);
  }

  template <typename T>
  inline void Matrix2x2<T>::Set(const Vector2<T> & v1, const Vector2<T> & v2)
  {
    c1 = v1;
    c2 = v2;
  }

  template <typename T>
  inline void Matrix2x2<T>::Identity(void)
  {
    c1.Set(1.0f, 0.0f);
    c2.Set(0.0f, 1.0f);
  }

  template <typename T>
  inline const Matrix2x2<T> Matrix2x2<T>::Transpose(void) const
  {
    return Matrix2x2(Vector2<T>(c1.x, c2.x), Vector2<T>(c1.y, c2.y));
  }

  template <typename T>
  inline void Matrix2x2<T>::TransposeSelf(void)
  {
    std::swap(c1.y, c2.x);
  }

  template <typename T>
  inline const Matrix3x3<T> Matrix3x3<T>::operator +
  (const Matrix3x3<T> & m) const
  {
    return Matrix3x3(c1 + m.c1, c2 + m.c2, c3 + m.c3);
  }

  template <typename T>
  inline const Matrix3x3<T> Matrix3x3<T>::operator -
  (const Matrix3x3<T> & m) const
  {
    return Matrix3x3(c1 - m.c1, c2 - m.c2, c3 - m.c3);
  }

  template <typename T>
  inline const Matrix3x3<T> Matrix3x3<T>::operator - () const
  {
    return Matrix3x3(-c1, -c2, -c3);
  }

  template <typename T>
  inline const Matrix3x3<T> Matrix3x3<T>::operator * (T f) const
  {
    return Matrix3x3(c1 * f, c2 * f, c3 * f);
  }

  template <typename T>
  inline const Matrix3x3<T> Matrix3x3<T>::operator / (T f) const
  {
    return Matrix3x3(c1 / f, c2 / f, c3 / f);
  }

  template <typename T>
  inline Matrix3x3<T> & Matrix3x3<T>::operator += (const Matrix3x3<T> & m)
  {
    c1 += m.c1;
    c2 += m.c2;
    c3 += m.c3;
    return *this;
  }

  template <typename T>
  inline Matrix3x3<T> & Matrix3x3<T>::operator -= (const Matrix3x3<T> & m)
  {
    c1 -= m.c1;
    c2 -= m.c2;
    c3 -= m.c3;
    return *this;
  }

  template <typename T>
  inline Matrix3x3<T> & Matrix3x3<T>::operator *= (T f)
  {
    c1 *= f;
    c2 *= f;
    c3 *= f;
    return *this;
  }

  template <typename T>
  inline Matrix3x3<T> & Matrix3x3<T>::operator /= (T f)
  {
    c1 /= f;
    c2 /= f;
    c3 /= f;
    return *this;
  }

  template <typename T>
  inline bool Matrix3x3<T>::operator == (const Matrix3x3<T> & m) const
  {
    return (c1 == m.c1) && (c2 == m.c2) && (c3 == m.c3);
  }

  template <typename T>
  inline bool Matrix3x3<T>::operator != (const Matrix3x3<T> & m) const
  {
    return (c1 != m.c1) || (c2 != m.c2) || (c3 != m.c3);
  }

  template <typename T>
  inline Matrix3x3<T> & Matrix3x3<T>::operator = (const Matrix3x3<T> & m)
  {
    c1 = m.c1;
    c2 = m.c2;
    c3 = m.c3;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Matrix3x3<T>::operator [] (unsigned i)
  {
    DEBUG_ASSERT(i < 3);
    return *(&c1 + i);
  }

  template <typename T>
  inline const Vector3<T> & Matrix3x3<T>::operator [] (unsigned i) const
  {
    DEBUG_ASSERT(i < 3);
    return *(&c1 + i);
  }

  template <typename T>
  inline void Matrix3x3<T>::Set
  (const Vector3<T> & v1, const Vector3<T> & v2, const Vector3<T> & v3)
  {
    c1 = v1;
    c2 = v2;
    c3 = v3;
  }

  template <typename T>
  inline void Matrix3x3<T>::Identity(void)
  {
    c1.Set(1.0f, 0.0f, 0.0f);
    c2.Set(0.0f, 1.0f, 0.0f);
    c3.Set(0.0f, 0.0f, 1.0f);
  }

  template <typename T>
  inline const Matrix3x3<T> Matrix3x3<T>::Transpose(void) const
  {
    return Matrix3x3(Vector3<T>(c1.x, c2.x, c3.x), Vector3<T>(c1.y, c2.y, c3.y),
                     Vector3<T>(c1.z, c2.z, c3.z));
  }

  template <typename T>
  inline void Matrix3x3<T>::TransposeSelf(void)
  {
    std::swap(c1.y, c2.x);
    std::swap(c1.z, c3.x);
    std::swap(c2.z, c3.y);
  }

  template <typename T>
  inline const Matrix4x4<T> Matrix4x4<T>::operator +
  (const Matrix4x4<T> & m) const
  {
    return Matrix4x4(c1 + m.c1, c2 + m.c2, c3 + m.c3, c4 + m.c4);
  }

  template <typename T>
  inline const Matrix4x4<T> Matrix4x4<T>::operator -
  (const Matrix4x4<T> & m) const
  {
    return Matrix4x4(c1 - m.c1, c2 - m.c2, c3 - m.c3, c4 - m.c4);
  }

  template <typename T>
  inline const Matrix4x4<T> Matrix4x4<T>::operator - () const
  {
    return Matrix4x4(-c1, -c2, -c3, -c4);
  }

  template <typename T>
  inline const Matrix4x4<T> Matrix4x4<T>::operator * (T f) const
  {
    return Matrix4x4<T>(c1 * f, c2 * f, c3 * f, c4 * f);
  }

  template <typename T>
  inline const Matrix4x4<T> Matrix4x4<T>::operator / (T f) const
  {
    return Matrix4x4(c1 / f, c2 / f, c3 / f, c4 / f);
  }

  template <typename T>
  inline Matrix4x4<T> & Matrix4x4<T>::operator += (const Matrix4x4<T> & m)
  {
    c1 += m.c1;
    c2 += m.c2;
    c3 += m.c3;
    c4 += m.c4;
    return *this;
  }

  template <typename T>
  inline Matrix4x4<T> & Matrix4x4<T>::operator -= (const Matrix4x4<T> & m)
  {
    c1 -= m.c1;
    c2 -= m.c2;
    c3 -= m.c3;
    c4 -= m.c4;
    return *this;
  }

  template <typename T>
  inline Matrix4x4<T> & Matrix4x4<T>::operator *= (T f)
  {
    c1 *= f;
    c2 *= f;
    c3 *= f;
    c4 *= f;
    return *this;
  }

  template <typename T>
  inline Matrix4x4<T> & Matrix4x4<T>::operator /= (T f)
  {
    c1 /= f;
    c2 /= f;
    c3 /= f;
    c4 /= f;
    return *this;
  }

  template <typename T>
  inline bool Matrix4x4<T>::operator == (const Matrix4x4<T> & m) const
  {
    return (c1 == m.c1) && (c2 == m.c2) && (c3 == m.c3) && (c4 == m.c4);
  }

  template <typename T>
  inline bool Matrix4x4<T>::operator != (const Matrix4x4<T> & m) const
  {
    return (c1 != m.c1) || (c2 != m.c2) || (c3 != m.c3) || (c4 != m.c4);
  }

  template <typename T>
  inline Matrix4x4<T> & Matrix4x4<T>::operator = (const Matrix4x4<T> & m)
  {
    c1 = m.c1;
    c2 = m.c2;
    c3 = m.c3;
    c4 = m.c4;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Matrix4x4<T>::operator [] (unsigned i)
  {
    DEBUG_ASSERT(i < 4);
    return *(&c1 + i);
  }

  template <typename T>
  inline const Vector4<T> & Matrix4x4<T>::operator [] (unsigned i) const
  {
    DEBUG_ASSERT(i < 4);
    return *(&c1 + i);
  }

  template <typename T>
  inline void Matrix4x4<T>::Set(const Vector4<T> & v1, const Vector4<T> & v2,
    const Vector4<T> & v3, const Vector4<T> & v4)
  {
    c1 = v1;
    c2 = v2;
    c3 = v3;
    c4 = v4;
  }

  template <typename T>
  inline void Matrix4x4<T>::Identity(void)
  {
    c1.Set(1.0f, 0.0f, 0.0f, 0.0f);
    c2.Set(0.0f, 1.0f, 0.0f, 0.0f);
    c3.Set(0.0f, 0.0f, 1.0f, 0.0f);
    c4.Set(0.0f, 0.0f, 0.0f, 1.0f);
  }

  template <typename T>
  inline const Matrix4x4<T> Matrix4x4<T>::Transpose(void) const
  {
    return Matrix4x4(Vector4<T>(c1.x, c2.x, c3.x, c4.x),
                     Vector4<T>(c1.y, c2.y, c3.y, c4.y),
                     Vector4<T>(c1.z, c2.z, c3.z, c4.x),
                     Vector4<T>(c1.w, c2.w, c3.w, c4.w));
  }

  template <typename T>
  inline void Matrix4x4<T>::TransposeSelf(void)
  {
    std::swap(c1.y, c2.x);
    std::swap(c1.z, c3.x);
    std::swap(c1.w, c4.x);
    std::swap(c2.z, c3.y);
    std::swap(c2.w, c4.y);
    std::swap(c3.w, c4.z);
  }

 /*
  *  Utility functions
  */

  template <typename T>
  const Matrix2x2<T> operator * (const Matrix2x2<T> & l, const Matrix2x2<T> & r)
  {
    return Matrix2x2<T>(
      Vector2<T>(l.c1.x*r.c1.x + l.c2.x*r.c1.y,l.c1.y*r.c1.x + l.c2.y*r.c1.y),
      Vector2<T>(l.c1.x*r.c2.x + l.c2.x*r.c2.y,l.c1.y*r.c2.x + l.c2.y*r.c2.y));
  }

  template <typename T>
  const Matrix3x3<T> operator * (const Matrix3x3<T> & l, const Matrix3x3<T> & r)
  {
    return Matrix3x3<T>(
      Vector3<T>(l.c1.x*r.c1.x + l.c2.x*r.c1.y + l.c3.x*r.c1.z,
                 l.c1.y*r.c1.x + l.c2.y*r.c1.y + l.c3.y*r.c1.z,
                 l.c1.z*r.c1.x + l.c2.z*r.c1.y + l.c3.z*r.c1.z),
      Vector3<T>(l.c1.x*r.c2.x + l.c2.x*r.c2.y + l.c3.x*r.c2.z,
                 l.c1.y*r.c2.x + l.c2.y*r.c2.y + l.c3.y*r.c2.z,
                 l.c1.z*r.c2.x + l.c2.z*r.c2.y + l.c3.z*r.c2.z),
      Vector3<T>(l.c1.x*r.c3.x + l.c2.x*r.c3.y + l.c3.x*r.c3.z,
                 l.c1.y*r.c3.x + l.c2.y*r.c3.y + l.c3.y*r.c3.z,
                 l.c1.z*r.c3.x + l.c2.z*r.c3.y + l.c3.z*r.c3.z));
  }

  template <typename T>
  const Matrix4x4<T> operator * (const Matrix4x4<T> & l, const Matrix4x4<T> & r)
  {
    return Matrix4x4<T>(
      Vector4<T>(l.c1.x*r.c1.x + l.c2.x*r.c1.y + l.c3.x*r.c1.z + l.c4.x*r.c1.w,
              l.c1.y*r.c1.x + l.c2.y*r.c1.y + l.c3.y*r.c1.z + l.c4.y*r.c1.w,
              l.c1.z*r.c1.x + l.c2.z*r.c1.y + l.c3.z*r.c1.z + l.c4.z*r.c1.w,
              l.c1.w*r.c1.x + l.c2.w*r.c1.y + l.c3.w*r.c1.z + l.c4.w*r.c1.w),
      Vector4<T>(l.c1.x*r.c2.x + l.c2.x*r.c2.y + l.c3.x*r.c2.z + l.c4.x*r.c2.w,
              l.c1.y*r.c2.x + l.c2.y*r.c2.y + l.c3.y*r.c2.z + l.c4.y*r.c2.w,
              l.c1.z*r.c2.x + l.c2.z*r.c2.y + l.c3.z*r.c2.z + l.c4.z*r.c2.w,
              l.c1.w*r.c2.x + l.c2.w*r.c2.y + l.c3.w*r.c2.z + l.c4.w*r.c2.w),
      Vector4<T>(l.c1.x*r.c3.x + l.c2.x*r.c3.y + l.c3.x*r.c3.z + l.c4.x*r.c3.w,
              l.c1.y*r.c3.x + l.c2.y*r.c3.y + l.c3.y*r.c3.z + l.c4.y*r.c3.w,
              l.c1.z*r.c3.x + l.c2.z*r.c3.y + l.c3.z*r.c3.z + l.c4.z*r.c3.w,
              l.c1.w*r.c3.x + l.c2.w*r.c3.y + l.c3.w*r.c3.z + l.c4.w*r.c3.w),
      Vector4<T>(l.c1.x*r.c4.x + l.c2.x*r.c4.y + l.c3.x*r.c4.z + l.c4.x*r.c4.w,
              l.c1.y*r.c4.x + l.c2.y*r.c4.y + l.c3.y*r.c4.z + l.c4.y*r.c4.w,
              l.c1.z*r.c4.x + l.c2.z*r.c4.y + l.c3.z*r.c4.z + l.c4.z*r.c4.w,
              l.c1.w*r.c4.x + l.c2.w*r.c4.y + l.c3.w*r.c4.z + l.c4.w*r.c4.w));
  }

  template <typename T>
  const Matrix4x4<T> operator * (const Matrix4x4<T> & l, const Matrix3x3<T> & r)
  {
    return Matrix4x4<T>(
      Vector4<T>(l.c1.x*r.c1.x + l.c2.x*r.c1.y + l.c3.x*r.c1.z,
              l.c1.y*r.c1.x + l.c2.y*r.c1.y + l.c3.y*r.c1.z,
              l.c1.z*r.c1.x + l.c2.z*r.c1.y + l.c3.z*r.c1.z,
              l.c1.w*r.c1.x + l.c2.w*r.c1.y + l.c3.w*r.c1.z),
      Vector4<T>(l.c1.x*r.c2.x + l.c2.x*r.c2.y + l.c3.x*r.c2.z,
              l.c1.y*r.c2.x + l.c2.y*r.c2.y + l.c3.y*r.c2.z,
              l.c1.z*r.c2.x + l.c2.z*r.c2.y + l.c3.z*r.c2.z,
              l.c1.w*r.c2.x + l.c2.w*r.c2.y + l.c3.w*r.c2.z),
      Vector4<T>(l.c1.x*r.c3.x + l.c2.x*r.c3.y + l.c3.x*r.c3.z,
              l.c1.y*r.c3.x + l.c2.y*r.c3.y + l.c3.y*r.c3.z,
              l.c1.z*r.c3.x + l.c2.z*r.c3.y + l.c3.z*r.c3.z,
              l.c1.w*r.c3.x + l.c2.w*r.c3.y + l.c3.w*r.c3.z),
      Vector4<T>(l.c4.x, l.c4.y, l.c4.z, l.c4.w));
  }

  template <typename T>
  const Matrix4x4<T> operator * (const Matrix3x3<T> & l, const Matrix4x4<T> & r)
  {
    return Matrix4x4<T>(
      Vector4<T>(l.c1.x*r.c1.x + l.c2.x*r.c1.y + l.c3.x*r.c1.z,
              l.c1.y*r.c1.x + l.c2.y*r.c1.y + l.c3.y*r.c1.z,
              l.c1.z*r.c1.x + l.c2.z*r.c1.y + l.c3.z*r.c1.z,
              r.c1.w),
      Vector4<T>(l.c1.x*r.c2.x + l.c2.x*r.c2.y + l.c3.x*r.c2.z,
              l.c1.y*r.c2.x + l.c2.y*r.c2.y + l.c3.y*r.c2.z,
              l.c1.z*r.c2.x + l.c2.z*r.c2.y + l.c3.z*r.c2.z,
              r.c2.w),
      Vector4<T>(l.c1.x*r.c3.x + l.c2.x*r.c3.y + l.c3.x*r.c3.z,
              l.c1.y*r.c3.x + l.c2.y*r.c3.y + l.c3.y*r.c3.z,
              l.c1.z*r.c3.x + l.c2.z*r.c3.y + l.c3.z*r.c3.z,
              r.c3.w),
      Vector4<T>(l.c1.x*r.c4.x + l.c2.x*r.c4.y + l.c3.x*r.c4.z,
              l.c1.y*r.c4.x + l.c2.y*r.c4.y + l.c3.y*r.c4.z,
              l.c1.z*r.c4.x + l.c2.z*r.c4.y + l.c3.z*r.c4.z,
              r.c4.w));
  }

  template <typename T>
  const Vector2<T> operator * (const Matrix2x2<T> & l, const Vector2<T> & r)
  {
    return Vector2<T>(l.c1.x*r.x + l.c2.x*r.y, l.c1.y*r.x + l.c2.y*r.y);
  }

  template <typename T>
  const Vector3<T> operator * (const Matrix3x3<T> & l, const Vector3<T> & r)
  {
    return Vector3<T>(l.c1.x*r.x + l.c2.x*r.y + l.c3.x*r.z,
                      l.c1.y*r.x + l.c2.y*r.y + l.c3.y*r.z,
                      l.c1.z*r.x + l.c2.z*r.y + l.c3.z*r.z);
  }

  template <typename T>
  const Vector3<T> operator * (const Matrix4x4<T> & l, const Vector3<T> & r)
  {
    return Vector3<T>(l.c1.x*r.x + l.c2.x*r.y + l.c3.x*r.z + l.c4.x,
                      l.c1.y*r.x + l.c2.y*r.y + l.c3.y*r.z + l.c4.y,
                      l.c1.z*r.x + l.c2.z*r.y + l.c3.z*r.z + l.c4.z);
  }

  template <typename T>
  const Vector4<T> operator * (const Matrix4x4<T> & l, const Vector4<T> & r)
  {
    return Vector4<T>(l.c1.x*r.x + l.c2.x*r.y + l.c3.x*r.z + l.c4.x*r.w,
                      l.c1.y*r.x + l.c2.y*r.y + l.c3.y*r.z + l.c4.y*r.w,
                      l.c1.z*r.x + l.c2.z*r.y + l.c3.z*r.z + l.c4.z*r.w,
                      l.c1.w*r.x + l.c2.w*r.y + l.c3.w*r.z + l.c4.w*r.w);
  }

}
