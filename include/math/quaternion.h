//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file quaternion.h
 *
 *  Contains all relevant math related classes.
 */

#pragma once

#include <math.h>
#include <limits>

#include "assertion.h"

#include "math/matrix.h"
#include "math/vector.h"

namespace tre {

 /*
  *  Declarations of all classes
  */

  template <typename T> class Quaternion;

 /*
  *  Convenience typedefs
  */
  typedef Quaternion<float> Quaternionf;
  typedef Quaternion<double> Quaterniond;

 /*
  *  Quaternion class
  */

  template <typename T>
  class Quaternion {
    public:
      static const Quaternion<T> CreateQuatFromRMatrix(const Matrix3x3<T> & m);
      static const Quaternion<T> CreateAxisQuaternion(
        const Vector3<T> & axis, T angle);
      static const Quaternion<T> CreateLookAtQuaternion(const Vector3<T> & eye,
        const Vector3<T> & at, const Vector3<T> & up);

      static const Quaternion<T> CreateZxyYawPitchRoll(const Vector3<T> & ypr);
      static const Quaternion<T> CreateZxYawPitch(const Vector2<T> & yp);

    public:
      T w, x, y, z;

    public:
      Quaternion() {}
      Quaternion(T f1, T f2, T f3, T f4)
        : w(f1), x(f2), y(f3), z(f4) {}
      Quaternion(T f, const Vector3<T> & v) :
        w(f), x(v.x), y(v.y), z(v.z) {}
      Quaternion(const Quaternion<T> & q) : w(q.w), x(q.x), y(q.y), z(q.z) {}

      const Quaternion<T> operator + (const Quaternion<T> & q) const;
      const Quaternion<T> operator - (const Quaternion<T> & q) const;
      const Quaternion<T> operator - () const;
      const Quaternion<T> operator * (T f) const;
      const Quaternion<T> operator * (const Quaternion<T> & q) const;
      const Quaternion<T> operator / (T f) const;

      Quaternion<T> & operator += (const Quaternion<T> & q);
      Quaternion<T> & operator -= (const Quaternion<T> & q);
      Quaternion<T> & operator *= (T f);
      Quaternion<T> & operator *= (const Quaternion<T> & q);
      Quaternion<T> & operator /= (T f);

      bool operator == (const Quaternion<T> & q) const;
      bool operator != (const Quaternion<T> & q) const;

      Quaternion<T> & operator = (const Quaternion<T> & q);

      T & operator [] (unsigned i);
      const T & operator [] (unsigned i) const;

      void Identity(void);

      void Set(T f1, T f2, T f3, T f4);
      void Set(T f, const Vector3<T> & v);

      T Length(void) const;
      void Normalize(void);

      const Quaternion<T> Conjugate(void) const;
  };

 /*
  *  Static member functions
  */

  template <typename T> const Quaternion<T>
  Quaternion<T>::CreateQuatFromRMatrix(const Matrix3x3<T> & m)
  {
    Quaternion<T> quat;
    T s;
    T tr = 1.0f + m.c1.x + m.c2.y + m.c3.z;

    if (tr > std::numeric_limits<T>::epsilon()) {
      s = sqrtf(tr) * 2.0f;
      quat.x = (m.c2.z - m.c3.y) / s;
      quat.y = (m.c3.x - m.c1.z) / s;
      quat.z = (m.c1.y - m.c2.x) / s;
      quat.w = 0.25f * s;
    } else {
      if (m.c1.x > m.c2.y && m.c1.x > m.c3.z)  { // Column 0: 
        s = sqrtf(1.0f + m.c1.x - m.c2.y - m.c3.z) * 2.0f;
        quat.x = 0.25f * s;
        quat.y = (m.c1.y + m.c2.x) / s;
        quat.z = (m.c3.x + m.c1.z) / s;
        quat.w = (m.c2.z - m.c3.y) / s;
      } else if (m.c2.y > m.c3.z) {                // Column 1:
        s = sqrtf(1.0f + m.c2.y - m.c1.x - m.c3.z) * 2.0f;
        quat.x = (m.c1.y + m.c2.x) / s;
        quat.y = 0.25f * s;
        quat.z = (m.c2.z + m.c3.y) / s;
        quat.w = (m.c3.x - m.c1.z) / s;
      } else {                                     // Column 2:
        s = sqrtf(1.0f + m.c3.z - m.c1.x - m.c2.y) * 2.0f;
        quat.x = (m.c3.x + m.c1.z) / s;
        quat.y = (m.c2.z + m.c3.y) / s;
        quat.z = 0.25f * s;
        quat.w = (m.c1.y - m.c2.x) / s;
      }
    }
    return quat;
  }

  template <typename T> const Quaternion<T>
  Quaternion<T>::CreateAxisQuaternion(const Vector3<T> & axis, T angle)
  {
    float s = sinf(angle / 2.0f);
    return Quaternion<T>(cosf(angle / 2.0f),
      axis.x * s, axis.y * s, axis.z * s);
  }

  template <typename T> const Quaternion<T>
  Quaternion<T>::CreateLookAtQuaternion
  (const Vector3<T> & eye, const Vector3<T> & at, const Vector3<T> & up)
  {
    Matrix3x3<T> tmpm;
    Vector3<T> dir = at - eye;
    dir.Normalize();

    Vector3<T> right;
    right.Cross(dir, up);
    right.Normalize();

    Vector3<T> newup;
    newup.Cross(right, dir);
    newup.Normalize();

    tmpm[0][0] = right[0]; tmpm[1][0] = right[1]; tmpm[2][0] = right[2];
    tmpm[0][1] = newup[0]; tmpm[1][1] = newup[1]; tmpm[2][1] = newup[2];
    tmpm[0][2] = -dir[0]; tmpm[1][2] = -dir[1];  tmpm[2][2] = -dir[2];
    return CreateQuatFromRMatrix(tmpm);
  }

  template <typename T> const Quaternion<T>
  Quaternion<T>::CreateZxyYawPitchRoll(const Vector3<T> & ypr)
  {
    T cr, cp, cy, sr, sp, sy, cpcy, spsy;

    // calculate trig identities
    cr = cosf(ypr.z / 2.0f);
    cp = cosf(ypr.y / 2.0f);
    cy = cosf(ypr.x / 2.0f);
    sr = sinf(ypr.z / 2.0f);
    sp = sinf(ypr.y / 2.0f);
    sy = sinf(ypr.x / 2.0f);

    cpcy = cp * cy;
    spsy = sp * sy;

    return Quaternionf(cr * cpcy + sr * spsy,
                      cr * sp * cy + sr * cp * sy,
                      sr * cpcy - cr * spsy,
                      cr * cp * sy - sr * sp * cy);
  }

  template <typename T> const Quaternion<T>
  Quaternion<T>::CreateZxYawPitch(const Vector2<T> & yp)
  {
    T cy, cp, sy, sp;
    cy = cosf(yp.x / 2.0f);
    cp = cosf(yp.y / 2.0f);
    sy = sinf(yp.x / 2.0f);
    sp = sinf(yp.y / 2.0f);

    return Quaternion<T>(cy * cp, cy * sp, -sy * sp, cp * sy);
  }

 /*
  *  Inline methods
  */

  template <typename T>
  inline const Quaternion<T> Quaternion<T>::operator +
  (const Quaternion<T> & q) const
  {
    return Quaternion(w + q.w, x + q.x, y + q.y, z + q.z);
  }

  template <typename T>
  inline const Quaternion<T> Quaternion<T>::operator -
  (const Quaternion & q) const
  {
    return Quaternion(w - q.w, x - q.x, y - q.y, z - q.z);
  }

  template <typename T>
  inline const Quaternion<T> Quaternion<T>::operator - () const
  {
    return Quaternion(-w, -x, -y, -z);
  }

  template <typename T>
  inline const Quaternion<T> Quaternion<T>::operator * (T f) const
  {
    return Quaternion(f * w, f * x, f * y, f * z);
  }

  template <typename T>
  inline const Quaternion<T> Quaternion<T>::operator *
  (const Quaternion<T> & q) const
  {
    return Quaternion(w*q.w - x*q.x - y*q.y - z*q.z,
                      w*q.x + q.w*x + y*q.z - z*q.y,
                      w*q.y + q.w*y + z*q.x - x*q.z,
                      w*q.z + q.w*z + x*q.y - y*q.x);
  }

  template <typename T>
  inline const Quaternion<T> Quaternion<T>::operator / (T f) const
  {
    return Quaternion(w / f, x / f, y / f, z / f);
  }

  template <typename T>
  inline Quaternion<T> & Quaternion<T>::operator += (const Quaternion<T> & q)
  {
    w += q.w;
    x += q.x;
    y += q.y;
    z += q.z;
    return *this;
  }

  template <typename T>
  inline Quaternion<T> & Quaternion<T>::operator -= (const Quaternion<T> & q)
  {
    w -= q.w;
    x -= q.x;
    y -= q.y;
    z -= q.z;
    return *this;
  }

  template <typename T>
  inline Quaternion<T> & Quaternion<T>::operator *= (T f)
  {
    w *= f;
    x *= f;
    y *= f;
    z *= f;
    return *this;
  }

  template <typename T>
  inline Quaternion<T> & Quaternion<T>::operator *= (const Quaternion<T> & q)
  {
    float a = w*q.w - x*q.x - y*q.y - z*q.z;
    float b = w*q.x + q.w*x + y*q.z - z*q.y;
    float c = w*q.y + q.w*y + z*q.x - x*q.z;
    float d = w*q.z + q.w*z + x*q.y - y*q.x;
    w = a;
    x = b;
    y = c;
    z = d;
    return *this;
  }

  template <typename T>
  inline Quaternion<T> & Quaternion<T>::operator /= (T f)
  {
    w /= f;
    x /= f;
    y /= f;
    z /= f;
    return *this;
  }

  template <typename T>
  inline bool Quaternion<T>::operator == (const Quaternion<T> & q) const
  {
    return (w == q.w) && (x == q.x) && (y == q.y) && (z == q.z);
  }

  template <typename T>
  inline bool Quaternion<T>::operator != (const Quaternion<T> & q) const
  {
    return (w != q.w) || (x != q.x) || (y != q.y) || (z != q.z);
  }

  template <typename T>
  inline Quaternion<T> & Quaternion<T>::operator = (const Quaternion<T> & q)
  {
    w = q.w;
    x = q.x;
    y = q.y;
    z = q.z;
    return *this;
  }

  template <typename T>
  inline T & Quaternion<T>::operator [] (unsigned i)
  {
    DEBUG_ASSERT(i < 4);
    return *(&w + i);
  }

  template <typename T>
  inline const T & Quaternion<T>::operator [] (unsigned i) const
  {
    DEBUG_ASSERT(i < 4);
    return *(&w + i);
  }

  template <typename T>
  inline void Quaternion<T>::Identity(void)
  {
    w = 1.0f;
    x = y = z = 0.0f;
  }

  template <typename T>
  inline void Quaternion<T>::Set(T f1, T f2, T f3, T f4)
  {
    w = f1;
    x = f2;
    y = f3;
    z = f4;
  }

  template <typename T>
  inline void Quaternion<T>::Set(T f, const Vector3<T> & v)
  {
    w = f;
    x = v.x;
    y = v.y;
    z = v.z;
  }

  template <>
  inline float Quaternion<float>::Length(void) const
  {
    return sqrtf(w*w + x*x + y*y + z*z);
  }

  template <>
  inline double Quaternion<double>::Length(void) const
  {
    return sqrt(w*w + x*x + y*y + z*z);
  }

  template <typename T>
  inline void Quaternion<T>::Normalize(void)
  {
    T len = Length();

    if(len > std::numeric_limits<T>::epsilon())
      *this /= len;
  }

  template <typename T>
  inline const Quaternion<T> Quaternion<T>::Conjugate(void) const
  {
    return Quaternion(w, -x, -y, -z);
  }

}
