//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file transform.h
 *
 *  Encapsulation of object transformations.
 */

#pragma once

#include <limits>

#include "math/vector.h"
#include "math/matrix.h"
#include "math/quaternion.h"

namespace tre {

  template <typename T>
  class Transform;

  typedef Transform<float> Transformf;
  typedef Transform<double> Transformd;

  template <typename T>
  class Transform {
    public:
    public:
      Transform();
      Transform(const Transform<T> & t);

      void LookAtObject(const Vector3<T> & at);
      void LookAtObject(const Vector3<T> & at, const Vector3<T> & up);
      void LookAtWorld(const Vector3<T> & at);
      void LookAtWorld(const Vector3<T> & at, const Vector3<T> & up);

      void RotateAxis(const Vector3<T> & axis, float angle);
      void RotateAbsX(T angle);
      void RotateAbsY(T angle);
      void RotateAbsZ(T angle);

      void RotateRelX(T angle) {RotateAxis(GetObjectX(), angle);}
      void RotateRelY(T angle) {RotateAxis(GetObjectY(), angle);}
      void RotateRelZ(T angle) {RotateAxis(GetObjectZ(), angle);}

      void MoveAbs(const Vector3<T> & pos);
      void MoveRel(const Vector3<T> & pos);

      const bool HasScale(void) const {return hasScale_;}

      const Transform * GetWParent(void) const {return wParent_;}

      const Vector3<T> & GetOPosition(void) const {return oPosition_;}
      const Vector3<T> GetWPosition(void) const
        {return Vector3<T>(wTransform_.c4);}
      const Vector3<T> & GetOScale(void) const {return oScale_;}
      const Quaternion<T> & GetORotation(void) const {return oRotation_;}

      const Vector3<T> & GetObjectX(void) const {return vMatrix_.c1;}
      const Vector3<T> & GetObjectY(void) const {return vMatrix_.c2;}
      const Vector3<T> & GetObjectZ(void) const {return vMatrix_.c3;}

      const Vector3<T> GetWorldX(void) const;
      const Vector3<T> GetWorldY(void) const;
      const Vector3<T> GetWorldZ(void) const;

      const Matrix4x4<T> & GetWTransform(void) const {return wTransform_;}
      const Matrix4x4<T> GetOTransform(void) const;
      const Matrix4x4<T> GetInverseWTransform(void) const;
      const Matrix4x4<T> GetInverseOTransform(void) const;

      void SetWParent(const Transform * parent);
      void SetOPosition(const Vector3<T> & pos);

      void SetOScale(const Vector3<T> & scale);
      void SetORotation(const Quaternion<T> & rot);

      void ResetOPosition(void);
      void ResetOScale(void);
      void ResetORotation(void);

    private:
      bool hasScale_;

      const Transform<T> * wParent_;

      Vector3<T> oPosition_;
      Vector3<T> oScale_;
      Quaternion<T> oRotation_;

      Matrix4x4<T> wTransform_;

      Matrix3x3<T> vMatrix_;

      void UpdateTransform(void);
  };

  template <typename T>
  Transform<T>::Transform() : hasScale_(false), wParent_(NULL),
    oPosition_(0.0f), oScale_(1.0f)
  {
    oRotation_.Identity();
    UpdateTransform();
  }

  template <typename T>
  Transform<T>::Transform(const Transform & t)
    : hasScale_(false), wParent_(t.wParent_), oPosition_(t.oPosition_),
      oScale_(t.oScale_), oRotation_(t.oRotation_),
      wTransform_(t.wTransform_), vMatrix_(t.vMatrix_)
  {
  }

  template <typename T>
  inline void Transform<T>::LookAtObject(const Vector3<T> & at)
  {
    LookAtObject(at, GetObjectY());
  }

  template <typename T> inline void
  Transform<T>::LookAtObject(const Vector3<T> & at, const Vector3<T> & up)
  {
    SetORotation(CreateLookAtQuaternion(oPosition_, at, up));
  }

  template <typename T>
  inline void Transform<T>::LookAtWorld(const Vector3<T> & at)
  {
    LookAtWorld(at, Vector3<T>(0.0f, 0.0f, 1.0f));
  }

  template <typename T>
  inline void Transform<T>::LookAtWorld
  (const Vector3<T> & at, const Vector3<T> & up)
  {
    Vector3<T> newat(GetInverseWTransform() * at);
    SetORotation(CreateLookAtQuaternion(oPosition_, newat, up));
  }

  template <typename T>
  void Transform<T>::RotateAxis(const Vector3<T> & axis, float angle)
  {
    oRotation_ *= CreateAxisQuaternion(axis, angle);
    oRotation_.Normalize();
    UpdateTransform();
  }

  template <typename T>
  inline void Transform<T>::RotateAbsX(T angle)
  {
    RotateAxis(Vector3<T>(1.0f, 0.0f, 0.0f), angle);
  }

  template <typename T>
  inline void Transform<T>::RotateAbsY(T angle)
  {
    RotateAxis(Vector3<T>(0.0f, 1.0f, 0.0f), angle);
  }

  template <typename T>
  inline void Transform<T>::RotateAbsZ(T angle)
  {
    RotateAxis(Vector3<T>(0.0f, 0.0f, 1.0f), angle);
  }

  template <typename T>
  inline void Transform<T>::MoveAbs(const Vector3<T> & pos)
  {
    oPosition_ += pos;
    UpdateTransform();
  }

  template <typename T>
  inline void Transform<T>::MoveRel(const Vector3<T> & pos)
  {
    oPosition_ += GetObjectX() * pos.x + GetObjectY() * pos.y
                + GetObjectZ() * pos.z;
    UpdateTransform();
  }

  template <typename T>
  inline const Vector3<T> Transform<T>::GetWorldX(void) const
  {
    T len = vMatrix_.c1.Length();
    if(len > std::numeric_limits<T>::epsilon())
      return vMatrix_.c1 / len;
    else
      return vMatrix_.c1;
  }

  template <typename T>
  inline const Vector3<T> Transform<T>::GetWorldY(void) const
  {
    T len = vMatrix_.c2.Length();
    if(len > std::numeric_limits<T>::epsilon())
      return vMatrix_.c2 / len;
    else
      return vMatrix_.c2;
  }

  template <typename T>
  inline const Vector3<T> Transform<T>::GetWorldZ(void) const
  {
    T len = vMatrix_.c3.Length();
    if(len > std::numeric_limits<T>::epsilon())
      return vMatrix_.c3 / len;
    else
      return vMatrix_.c3;
  }

  template <typename T>
  const Matrix4x4<T> Transform<T>::GetOTransform(void) const
  {
    Matrix4x4<T> tmpm(CreateTMatrix(oPosition_));

    if(!hasScale_) {
      return tmpm * vMatrix_;
    } else {
      tmpm = tmpm * vMatrix_;
      return tmpm * CreateSMatrix(oScale_);
    }
  }

  template <typename T>
  const Matrix4x4<T> Transform<T>::GetInverseWTransform(void) const
  {
    if(wParent_) {
      return GetInverseOTransform() * wParent_->GetInverseWTransform();
    } else {
      return GetInverseOTransform();
    }
  }

  template <typename T>
  const Matrix4x4<T> Transform<T>::GetInverseOTransform(void) const
  {
    if(hasScale_)
      return CreateInvSMatrix(oScale_) * CreateRMatrix(oRotation_.Conjugate())
             * CreateInvTMatrix(oPosition_);
    else
      return CreateRMatrix(oRotation_.Conjugate())
        * CreateInvTMatrix(oPosition_);
  }

  template <typename T>
  inline void Transform<T>::SetWParent(const Transform * parent)
  {
    wParent_ = parent;
    UpdateTransform();
  }

  template <typename T>
  inline void Transform<T>::SetOPosition(const Vector3<T> & pos)
  {
    oPosition_ = pos;
    UpdateTransform();
  }

  template <typename T>
  void Transform<T>::SetOScale(const Vector3<T> & scale)
  {
    if(scale.x != 0.0f && scale.y != 0.0f && scale.z != 0.0f) {
      hasScale_ = true;
      oScale_ = scale;
      UpdateTransform();
    }
  }

  template <typename T>
  inline void Transform<T>::SetORotation(const Quaternion<T> & rot)
  {
    oRotation_ = rot;
    UpdateTransform();
  }

  template <typename T>
  inline void Transform<T>::ResetOPosition(void)
  {
    oPosition_ = 0.0f;
    UpdateTransform();
  }

  template <typename T>
  inline void Transform<T>::ResetOScale(void)
  {
    hasScale_ = wParent_ ? wParent_->hasScale_ : false;
    oScale_ = 1.0f;
    UpdateTransform();
  }

  template <typename T>
  inline void Transform<T>::ResetORotation(void)
  {
    oRotation_.Identity();
    UpdateTransform();
  }

  template <typename T>
  inline void Transform<T>::UpdateTransform(void)
  {
    vMatrix_ = Matrix3x3<T>::CreateRMatrix(oRotation_);
    wTransform_ = Matrix4x4<T>::CreateTMatrix(oPosition_) * vMatrix_;

    if(hasScale_)
      wTransform_ = wTransform_ * Matrix3x3<T>::CreateSMatrix(oScale_);

    if(wParent_)
      wTransform_ = wParent_->GetWTransform() * wTransform_;
  }

}
