//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file vector.h
 *
 *  Contains all relevant math related classes.
 */

#pragma once

#include <math.h>
#include <limits>

#include "assertion.h"

namespace tre {

 /*
  *  Helper functions
  */

  /** Encodes normal float into char */
  template <typename T>
  inline char Real2Char(T a) {return static_cast<char>((a) * 127.0f);}

  /** Decodes normal float from char */
  template <typename T>
  inline T Char2Real(char a) {return (a) / 127.0f;}

 /*
  *  Declarations of all classes
  */

  class VectorNorm2;
  class VectorNorm3;

  template <typename T> class Vector2;
  template <typename T> class Vector3;
  template <typename T> class Vector4;

  template <typename T> class Matrix2x2;
  template <typename T> class Matrix3x3;
  template <typename T> class Matrix4x4;

  template <typename T> class Quaternion;

  template <typename T> class Line2;
  template <typename T> class Line3;
  template <typename T> class Plane;

 /*
  *  Convenience typedefs
  */
  typedef Vector2<float> Vector2f;
  typedef Vector3<float> Vector3f;
  typedef Vector4<float> Vector4f;
  typedef Vector2<double> Vector2d;
  typedef Vector3<double> Vector3d;
  typedef Vector4<double> Vector4d;
  typedef Vector2<int> Vector2i;
  typedef Vector3<int> Vector3i;
  typedef Vector4<int> Vector4i;
  typedef Vector2<uint> Vector2u;
  typedef Vector3<uint> Vector3u;
  typedef Vector4<uint> Vector4u;

 /*
  *  Declarations of utility functions
  */

  template <typename T> T Dot(const Vector2<T> & v1, const Vector2<T> & v2);
  template <typename T> T Dot(const Vector3<T> & v1, const Vector3<T> & v2);
  template <typename T> T Dot(const Vector4<T> & v1, const Vector4<T> & v2);

  template <typename T> T Distance
    (const Vector2<T> & v1, const Vector2<T> & v2);
  template <typename T> T Distance
    (const Vector3<T> & v1, const Vector3<T> & v2);

 /*
  *  Normal vector classes
  */

  class VectorNorm2 {
    public:
      char x, y;

    public:
      template <typename T> VectorNorm2 &  operator = (const Vector2<T> & v);
      template <typename T> VectorNorm2 &  operator = (const Vector3<T> & v);
      VectorNorm2 &  operator = (char i);
      template <typename T> VectorNorm2 &  operator = (T f);

      void Set(char cX, char cY);
      template <typename T> void Set(T fX, T fY);
  };

  class VectorNorm3 {
    public:
      char x, y, z;

    public:
      template <typename T> VectorNorm3 & operator = (const Vector3<T> & v);
      VectorNorm3 & operator = (char i);
      template <typename T> VectorNorm3 & operator = (T f);

      void Set(char cX, char cY, char cZ);
  };

 /*
  *  Common vector classes
  */

  template <typename T>
  class Vector2 {
    public:
      T x, y;

    public:
      Vector2() {}
      Vector2(const VectorNorm2 & v)
        : x(Char2Real<T>(v.x)), y(Char2Real<T>(v.y)) {}
      Vector2(const Vector2<T> & v) : x(v.x), y(v.y) {}
      explicit Vector2(T f) : x(f), y(f) {}
      explicit Vector2(const Vector3<T> & v) : x(v.x), y(v.y) {}
      Vector2(T fX, T fY) : x(fX), y(fY) {}

      const Vector2<T> operator + (const Vector2<T> & v) const;
      const Vector2<T> operator + (T f) const;
      const Vector2<T> operator - (const Vector2<T> & v) const;
      const Vector2<T> operator - (T f) const;
      const Vector2<T> operator * (T f) const;
      const Vector2<T> operator / (T f) const;

      const Vector2<T> operator - () const;

      Vector2<T> & operator += (const Vector2<T> & v);
      Vector2<T> & operator -= (const Vector2<T> & v);
      Vector2<T> & operator *= (T f);
      Vector2<T> & operator /= (T f);

      bool operator == (const Vector2<T> & v) const;
      bool operator != (const Vector2<T> & v) const;

      Vector2<T> & operator = (T f);
      Vector2<T> & operator = (const VectorNorm2 & v);
      Vector2<T> & operator = (const Vector2<T> & v);

      T & operator [] (unsigned i);
      const T & operator [] (unsigned i) const;

      void Set(T fX, T fY);

      T Length(void) const;
      T LengthSquared(void) const;
      void Normalize(void);
  };

  template <typename T>
  class Vector3 {
    public:
      T x, y, z;

    public:
      Vector3() {}
      explicit Vector3(const Vector2<T> & v) : x(v.x), y(v.y), z(0.0f) {}
      Vector3(const Vector3<T> & v) : x(v.x), y(v.y), z(v.z) {}
      explicit Vector3(const Vector4<T> & v);
      explicit Vector3(float f) : x(f), y(f), z(f) {}
      Vector3(float fX,float fY,float fZ)
        : x(fX), y(fY), z(fZ) {}

      const Vector3<T> operator + (const Vector3<T> & v) const;
      const Vector3<T> operator + (T f) const;
      const Vector3<T> operator - (const Vector3<T> & v) const;
      const Vector3<T> operator - (T f) const;
      const Vector3<T> operator * (T f) const;
      const Vector3<T> operator / (T f) const;

      const Vector3<T> operator - () const;

      Vector3<T> & operator += (const Vector3<T> & v);
      Vector3<T> & operator -= (const Vector3<T> & v);
      Vector3<T> & operator *= (T f);
      Vector3<T> & operator /= (T f);

      bool operator == (const Vector3<T> & v) const;
      bool operator == (T f) const;
      bool operator != (const Vector3<T> & v) const;
      bool operator != (T f) const;

      Vector3<T> & operator = (T f);
      Vector3<T> & operator = (const VectorNorm3 & v);
      Vector3<T> & operator = (const VectorNorm2 & v);
      Vector3<T> & operator = (const Vector4<T> & v);
      Vector3<T> & operator = (const Vector3<T> & v);
      Vector3<T> & operator = (const Vector2<T> & v);

      T & operator [] (unsigned i);
      const T & operator [] (unsigned i) const;

      void Set(T fX, T fY, T fZ);

      T Length(void) const;
      T LengthSquared(void) const;
      void Normalize(void);

      const Vector3<T> Cross(const Vector3<T> & v) const;
      void Cross(const Vector3<T> & v1, const Vector3<T> & v2);
  };

  template <typename T>
  class Vector4 {
    public:
      T x, y, z, w;

    public:
      Vector4() {}

      // assigning homogenous coordinates?
      explicit Vector4(T f, T fW = 1.0f)
        : x(f), y(f), z(f), w(fW) {}
      explicit Vector4(const Vector2<T> & v, T fW = 1.0f)
        : x(v.x), y(v.y), z(0.0f), w(fW) {}
      explicit Vector4(const Vector3<T> & v, T fW = 1.0f)
        : x(v.x), y(v.y), z(v.z), w(fW) {}
      Vector4(const Vector4<T> & v) : x(v.x), y(v.y), z(v.z), w(v.w) {}
      Vector4(T fX, T fY, T fZ, T fW = 1.0f) : x(fX), y(fY), z(fZ), w(fW) {}

      const Vector4<T> operator + (const Vector4<T> & v) const;
      const Vector4<T> operator - (const Vector4<T> & v) const;
      const Vector4<T> operator * (T f) const;
      const Vector4<T> operator / (T f) const;

      const Vector4<T> operator - () const;

      Vector4<T> & operator += (const Vector4<T> & v);
      Vector4<T> & operator -= (const Vector4<T> & v);
      Vector4<T> & operator *= (T f);
      Vector4<T> & operator /= (T f);

      bool operator == (const Vector4<T> & v) const;
      bool operator != (const Vector4<T> & v) const;

      Vector4<T> & operator = (T f);
      Vector4<T> & operator = (const VectorNorm3 & v);
      Vector4<T> & operator = (const VectorNorm2 & v);
      Vector4<T> & operator = (const Vector4<T> & v);
      Vector4<T> & operator = (const Vector3<T> & v);
      Vector4<T> & operator = (const Vector2<T> & v);

      T & operator [] (unsigned i);
      const T & operator [] (unsigned i) const;

      void Set(T fX, T fY, T fZ, T fW);

      T Length(void) const;
  };

 /*
  *  Inline methods
  */

  template <typename T>
  inline VectorNorm2 & VectorNorm2::operator = (const Vector2<T> & v)
  {
    x = Real2Char(v.x);
    y = Real2Char(v.y);
    return *this;
  }

  template <typename T>
  inline VectorNorm2 & VectorNorm2::operator = (const Vector3<T> & v)
  {
    x = Real2Char(v.x);
    y = Real2Char(v.y);
    return *this;
  }

  inline VectorNorm2 & VectorNorm2::operator = (char i)
  {
    x = y = i;
    return *this;
  }

  template <typename T>
  inline VectorNorm2 & VectorNorm2::operator = (T f)
  {
    x = y = Real2Char(f);
    return *this;
  }

  inline void VectorNorm2::Set(const char cX, const char cY)
  {
    x = cX;
    y = cY;
  }

  template <typename T>
  inline void VectorNorm2::Set(T fX, T fY)
  {
    x = Real2Char(fX);
    y = Real2Char(fY);
  }

  template <typename T>
  inline VectorNorm3 & VectorNorm3::operator = (const Vector3<T> & v)
  {
    x = Real2Char(v.x);
    y = Real2Char(v.y);
    z = Real2Char(v.z);
    return *this;
  }

  inline VectorNorm3 & VectorNorm3::operator = (const char i)
  {
    x = y = i;
    return *this;
  }

  template <typename T>
  inline VectorNorm3 & VectorNorm3::operator = (T f)
  {
    x = y = Real2Char(f);
    return *this;
  }

  inline void VectorNorm3::Set(const char cX, const char cY, const char cZ)
  {
    x = cX;
    y = cY;
    z = cZ;
  }

  template <typename T>
  inline const Vector2<T> Vector2<T>::operator + (const Vector2<T> & v) const
  {
    return Vector2(x + v.x, y + v.y);
  }

  template <typename T>
  inline const Vector2<T> Vector2<T>::operator + (T f) const
  {
    return Vector2(x + f, y + f);
  }

  template <typename T>
  inline const Vector2<T> Vector2<T>::operator - (const Vector2<T> & v) const
  {
    return Vector2(x - v.x, y - v.y);
  }

  template <typename T>
  inline const Vector2<T> Vector2<T>::operator - (T f) const
  {
    return Vector2(x - f, y - f);
  }

  template <typename T>
  inline const Vector2<T> Vector2<T>::operator * (T f) const
  {
    return Vector2(x * f, y * f);
  }

  template <typename T>
  inline const Vector2<T> Vector2<T>::operator / (T f) const
  {
    return Vector2(x / f, y / f);
  }

  template <typename T>
  inline const Vector2<T> Vector2<T>::operator - () const
  {
    return Vector2(-x, -y);
  }

  template <typename T>
  inline Vector2<T> & Vector2<T>::operator += (const Vector2<T> & v)
  {
    x += v.x;
    y += v.y;
    return *this;
  }

  template <typename T>
  inline Vector2<T> & Vector2<T>::operator -= (const Vector2<T> & v)
  {
    x -= v.x;
    y -= v.y;
    return *this;
  }

  template <typename T>
  inline Vector2<T> & Vector2<T>::operator *= (T f)
  {
    x *= f;
    y *= f;
    return *this;
  }

  template <typename T>
  inline Vector2<T> & Vector2<T>::operator /= (T f)
  {
    x /= f;
    y /= f;
    return *this;
  }

  template <typename T>
  inline bool Vector2<T>::operator == (const Vector2<T> & v) const
  {
    return (x == v.x) && (y == v.y);
  }

  template <typename T>
  inline bool Vector2<T>::operator != (const Vector2<T> & v) const
  {
    return (x != v.x) || (y != v.y);
  }

  template <typename T>
  inline Vector2<T> & Vector2<T>::operator = (T f)
  {
    x = y = f;
    return *this;
  }

  template <typename T>
  inline Vector2<T> & Vector2<T>::operator = (const VectorNorm2 & v)
  {
    x = Char2Real<T>(v.x);
    y = Char2Real<T>(v.y);
    return *this;
  }

  template <typename T>
  inline Vector2<T> & Vector2<T>::operator = (const Vector2<T> & v)
  {
    x = v.x;
    y = v.y;
    return *this;
  }

  template <typename T>
  inline T & Vector2<T>::operator [] (unsigned i)
  {
    DEBUG_ASSERT(i < 2);
    return *(&x + i);
  }

  template <typename T>
  inline const T & Vector2<T>::operator [] (unsigned i) const
  {
    DEBUG_ASSERT(i < 2);
    return *(&x + i);
  }

  template <typename T>
  inline void Vector2<T>::Set(T fX, T fY)
  {
    x = fX;
    y = fY;
  }

  template <>
  inline float Vector2<float>::Length(void) const
  {
    return sqrtf(x*x + y*y);
  }

  template <>
  inline double Vector2<double>::Length(void) const
  {
    return sqrt(x*x + y*y);
  }

  template <typename T>
  inline T Vector2<T>::LengthSquared(void) const
  {
    return x*x + y*y;
  }

  template <typename T>
  inline void Vector2<T>::Normalize(void)
  {
    T len = Length();

    if(len > std::numeric_limits<T>::epsilon())
      *this /= len;
  }

  template <typename T>
  inline Vector3<T>::Vector3(const Vector4<T> & v)
    : x(v.x), y(v.y), z(v.z)
  {
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::operator + (const Vector3<T> & v) const
  {
    return Vector3(x + v.x, y + v.y, z + v.z);
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::operator + (T f) const
  {
    return Vector3(x + f, y + f, z + f);
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::operator - (const Vector3<T> & v) const
  {
    return Vector3(x - v.x, y - v.y, z - v.z);
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::operator - (T f) const
  {
    return Vector3(x - f, y - f, z - f);
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::operator * (T f) const
  {
    return Vector3(x * f, y * f, z * f);
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::operator / (T f) const
  {
    return Vector3(x / f, y / f, z / f);
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::operator - () const
  {
    return Vector3(-x, -y, -z);
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator += (const Vector3<T> & v)
  {
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator -= (const Vector3<T> & v)
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator *= (T f)
  {
    x *= f;
    y *= f;
    z *= f;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator /= (T f)
  {
    x /= f;
    y /= f;
    z /= f;
    return *this;
  }

  template <typename T>
  inline bool Vector3<T>::operator == (const Vector3<T> & v) const
  {
    return ((x == v.x) && (y == v.y) && (z == v.z));
  }

  template <typename T>
  inline bool Vector3<T>::operator == (T f) const
  {
    return ((x == f) && (y == f) && (z == f));
  }

  template <typename T>
  inline bool Vector3<T>::operator != (const Vector3<T> & v) const
  {
    return ((x != v.x) || (y != v.y) || (z != v.z));
  }

  template <typename T>
  inline bool Vector3<T>::operator != (T f) const
  {
    return ((x != f) || (y != f) || (z != f));
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator = (T f)
  {
    x = y = z = f;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator = (const VectorNorm3 & v)
  {
    x = Char2Real<T>(v.x);
    y = Char2Real<T>(v.y);
    z = Char2Real<T>(v.z);
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator = (const VectorNorm2 & v)
  {
    x = Char2Real<T>(v.x);
    y = Char2Real<T>(v.y);
    z = 0.0f;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator = (const Vector4<T> & v)
  {
    x = v.x;
    y = v.y;
    z = v.z;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator = (const Vector3<T> & v)
  {
    x = v.x;
    y = v.y;
    z = v.z;
    return *this;
  }

  template <typename T>
  inline Vector3<T> & Vector3<T>::operator = (const Vector2<T> & v)
  {
    x = v.x;
    y = v.y;
    z = 0.0f;
    return *this;
  }

  template <typename T>
  inline T & Vector3<T>::operator [] (unsigned i)
  {
    DEBUG_ASSERT(i < 3);
    return *(&x + i);
  }

  template <typename T>
  inline const T & Vector3<T>::operator [] (unsigned i) const
  {
    DEBUG_ASSERT(i < 3);
    return *(&x + i);
  }

  template <typename T>
  inline void Vector3<T>::Set(T fX, T fY, T fZ)
  {
    x = fX;
    y = fY;
    z = fZ;
  }

  template <>
  inline float Vector3<float>::Length(void) const
  {
    return sqrtf(x*x + y*y + z*z);
  }

  template <>
  inline double Vector3<double>::Length(void) const
  {
    return sqrt(x*x + y*y + z*z);
  }

  template <typename T>
  inline T Vector3<T>::LengthSquared(void) const
  {
    return x*x + y*y + z*z;
  }

  template <typename T>
  inline void Vector3<T>::Normalize(void)
  {
    T len = Length();

    if(len > std::numeric_limits<T>::epsilon())
      *this /= len;
  }

  template <typename T>
  inline const Vector3<T> Vector3<T>::Cross(const Vector3<T> & v) const
  {
    return Vector3(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
  }

  template <typename T>
  inline void Vector3<T>::Cross
  (const Vector3<T> & v1, const Vector3<T> & v2)
  {
    x = v1.y * v2.z - v1.z * v2.y;
    y = v1.z * v2.x - v1.x * v2.z;
    z = v1.x * v2.y - v1.y * v2.x;
  }

  template <typename T>
  inline const Vector4<T> Vector4<T>::operator + (const Vector4<T> & v) const
  {
    return Vector4<T>(x + v.x, y + v.y, z + v.z, w + v.w);
  }

  template <typename T>
  inline const Vector4<T> Vector4<T>::operator - (const Vector4<T> & v) const
  {
    return Vector4(x - v.x, y - v.y, z - v.z, w - v.w);
  }

  template <typename T>
  inline const Vector4<T> Vector4<T>::operator * (T f) const
  {
    return Vector4(x * f, y * f, z * f, w * f);
  }

  template <typename T>
  inline const Vector4<T> Vector4<T>::operator / (T f) const
  {
    return Vector4(x / f, y / f, z / f, w / f);
  }

  template <typename T>
  inline const Vector4<T> Vector4<T>::operator - () const
  {
    return Vector4(-x, -y, -z, -w);
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator += (const Vector4<T> & v)
  {
    x += v.x;
    y += v.y;
    z += v.z;
    w += v.w;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator -= (const Vector4<T> & v)
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    w -= v.w;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator *= (T f)
  {
    x *= f;
    y *= f;
    z *= f;
    w *= f;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator /= (T f)
  {
    x /= f;
    y /= f;
    z /= f;
    w /= f;
    return *this;
  }

  template <typename T>
  inline bool Vector4<T>::operator == (const Vector4<T> & v) const
  {
    return ((x == v.x) && (y == v.y) && (z == v.z) && (w == v.w));
  }

  template <typename T>
  inline bool Vector4<T>::operator != (const Vector4<T> & v) const
  {
    return ((x != v.x) || (y != v.y) || (z != v.z) || (w != v.w));
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator = (T f)
  {
    x = y = z = w = f;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator = (const VectorNorm3 & v)
  {
    x = Char2Real<T>(v.x);
    y = Char2Real<T>(v.y);
    z = Char2Real<T>(v.z);
    w = 1.0f;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator = (const VectorNorm2 & v)
  {
    x = Char2Real<T>(v.x);
    y = Char2Real<T>(v.y);
    z = 0.0f;
    w = 1.0f;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator = (const Vector4<T> & v)
  {
    x = v.x;
    y = v.y;
    z = v.z;
    w = v.w;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator = (const Vector3<T> & v)
  {
    x = v.x;
    y = v.y;
    z = v.z;
    w = 1.0f;
    return *this;
  }

  template <typename T>
  inline Vector4<T> & Vector4<T>::operator = (const Vector2<T> & v)
  {
    x = v.x;
    y = v.y;
    z = 0.0f;
    w = 1.0f;
    return *this;
  }

  template <typename T>
  inline T & Vector4<T>::operator [] (unsigned i)
  {
    DEBUG_ASSERT(i < 4);
    return *(&x + i);
  }

  template <typename T>
  inline const T & Vector4<T>::operator [] (unsigned i) const
  {
    DEBUG_ASSERT(i < 4);
    return *(&x + i);
  }

  template <typename T>
  inline void Vector4<T>::Set(T fX, T fY, T fZ, T fW)
  {
    x = fX;
    y = fY;
    z = fZ;
    w = fW;
  }

  template <>
  inline float Vector4<float>::Length(void) const
  {
    return sqrtf(x*x + y*y + z*z + w*w);
  }

  template <>
  inline double Vector4<double>::Length(void) const
  {
    return sqrt(x*x + y*y + z*z + w*w);
  }

 /*
  *  Utility functions
  */

  template <typename T>
  inline T Dot(const Vector2<T> & v1, const Vector2<T> & v2)
  {
    return v1.x*v2.x + v1.y*v2.y;
  }

  template <typename T>
  inline T Dot(const Vector3<T> & v1, const Vector3<T> & v2)
  {
    return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
  }

  template <typename T>
  inline T Dot(const Vector4<T> & v1, const Vector4<T> & v2)
  {
    return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z + v1.w*v2.w;
  }

  template <>
  inline float Distance<float>(const Vector2f & v1, const Vector2f & v2)
  {
    return sqrtf((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y));
  }

  template <>
  inline double Distance<double>(const Vector2d & v1, const Vector2d & v2)
  {
    return sqrt((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y));
  }

  template <>
  inline float Distance<float>(const Vector3f & v1, const Vector3f & v2)
  {
    return sqrtf((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y)
                 + (v1.z - v2.z) * (v1.z - v2.z));
  }

  template <>
  inline double Distance<double>(const Vector3d & v1, const Vector3d & v2)
  {
    return sqrt((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y)
                 + (v1.z - v2.z) * (v1.z - v2.z));
  }

}
