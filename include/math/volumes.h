//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file volumes.h
 *
 *  Huhu
 */

#pragma once

#include "stdtypes.h"

#include "math/math.h"
#include "math/vector.h"

#include "transform.h"

namespace tre {

  class BndVolume;
  class BndSphere;
  class AABBox;
  class OBBox;
  class PVolume;

  enum ProjectionType {
    ptPerspective,
    ptOrtho
  };

  struct ProjectionBase {
    float left;
    float right;
    float bottom;
    float top;
    float near;
    float far;
  };

  float Distance(const BndVolume & bv1, const BndVolume & bv2);

  // every bounding volume instance is also a bounding sphere
  class BndVolume {
    public:
      Vector3f center;
      float radius;

    public:
      BndVolume() {}
      BndVolume(const Vector3f & c, float r) : center(c), radius(r) {}
      virtual ~BndVolume() {}

      // creation
      virtual void CreateFromData(const Float3Vector & data) = 0;

      // transformation
      virtual void SetTransform(const Transformf & t) = 0;
      virtual void ResetTransform(void) = 0;

      // boundig sphere tests
      bool SphereCollision(const BndVolume & bvol) const;
      bool SphereCollision(const Vector3f & v) const;
      int SphereIntersection(const BndVolume & bvol) const;

      // true/false collision testing
      virtual bool TestCollision(const BndVolume & bvol) const = 0;

      virtual bool TestCollision(const Vector3f & v) const;
      virtual bool TestCollision(const BndSphere & bsp) const;
      virtual bool TestCollision(const AABBox & bbox) const;
      virtual bool TestCollision(const OBBox & bbox) const;
      virtual bool TestCollision(const PVolume & pvol) const;

      // more thorough testing
      virtual int TestIntersection(const BndVolume & bvol) const = 0;

      virtual int TestIntersection(const BndSphere & bsp) const;
      virtual int TestIntersection(const AABBox & bbox) const;
      virtual int TestIntersection(const OBBox & bbox) const;
      virtual int TestIntersection(const PVolume & pvol) const;
  };

  class BndSphere : public BndVolume {
    public:
      BndSphere() {}
      BndSphere(const Vector3f & c, float r)
        : BndVolume(c, r), baseCenter_(c), baseRadius_(r) {}

      void CreateFromData(const Float3Vector & data);

      void SetTransform(const Transformf & t);
      void ResetTransform(void);

      // true/false collision testing
      bool TestCollision(const BndVolume & bvol) const;
      bool TestCollision(const AABBox & bbox) const;
      bool TestCollision(const OBBox & bbox) const;
      bool TestCollision(const PVolume & pvol) const;

      // more thorough testing
      int TestIntersection(const BndVolume & bvol) const;
      int TestIntersection(const AABBox & bbox) const;
      int TestIntersection(const OBBox & bbox) const;
      int TestIntersection(const PVolume & pvol) const;

      void Set(const Vector3f & c, float r);

    private:
      Vector3f baseCenter_;
      float baseRadius_;
  };

  // axis aligned box
  class AABBox : public BndVolume {
    public:
      Vector3f halfSize;

    public:
      AABBox() {}
      AABBox(const Vector3f & c, const Vector3f & hs)
        : BndVolume(c, hs.Length()), halfSize(hs),
          baseCenter_(c), baseHSize_(hs) {}

      void CreateFromData(const Float3Vector & data);

      void SetTransform(const Transformf & t);
      void ResetTransform(void);

      int GetQuadrant(const BndVolume & bvol) const;

      // true/false collision testing
      bool TestCollision(const BndVolume & bvol) const;
      bool TestCollision(const Vector3f & v) const;
      bool TestCollision(const BndSphere & bsp) const;
      bool TestCollision(const AABBox & bbox) const;
      bool TestCollision(const OBBox & bbox) const;
      bool TestCollision(const PVolume & pvol) const;

      // more thorough testing
      int TestIntersection(const BndVolume & bvol) const;
      int TestIntersection(const BndSphere & bsp) const;
      int TestIntersection(const AABBox & bbox) const;
      int TestIntersection(const OBBox & bbox) const;
      int TestIntersection(const PVolume & pvol) const;

      void Set(const Vector3f & c, const Vector3f & hs);

    private:
      Vector3f baseCenter_;
      Vector3f baseHSize_;
  };

  // oriented bouding box
  class OBBox : public BndVolume {
    public:
      Vector3f halfSize;

      // perpendicular to axes X, Y and Z
      Planef centralPlanes[3];

    public:
      OBBox() {}
      OBBox(const Vector3f & c, const Vector3f & hs, const Vector3f & d,
            const Vector3f & u, const Vector3f & r);

      void CreateFromData(const Float3Vector & data);

      void SetTransform(const Transformf & t);
      void ResetTransform(void);

      // true/false collision testing
      bool TestCollision(const BndVolume & bvol) const;
      bool TestCollision(const Vector3f & v) const;
      bool TestCollision(const BndSphere & bsp) const;
      bool TestCollision(const AABBox & bbox) const;
      bool TestCollision(const OBBox & bbox) const;
      bool TestCollision(const PVolume & pvol) const;

      // more thorough testing
      int TestIntersection(const BndVolume & bvol) const;
      int TestIntersection(const BndSphere & bsp) const;
      int TestIntersection(const AABBox & bbox) const;
      int TestIntersection(const OBBox & bbox) const;
      int TestIntersection(const PVolume & pvol) const;

      void Set(const Vector3f & c, const Vector3f & hs, const Vector3f & d,
               const Vector3f & r, const Vector3f & u);

    private:
      Vector3f baseCenter_;
      Vector3f baseHSize_;

      Vector3f baseDir_;
      Vector3f baseUp_;
      Vector3f baseRight_;
  };

  // frustum or box, depending on the projection type
  class PVolume : public BndVolume {
    public:
      // left, right, bottom, top, near, far
      // normals pointing inwards
      Planef clipPlane[6];
//       Vector3f corner[8];

    public:
      PVolume();

      const Vector3f & GetEye(void) const {return eye_;}
      const Vector3f & GetDir(void) const {return dir_;}
      const Vector3f & GetRight(void) const {return right_;}
      const Vector3f & GetUp(void) const {return up_;}

      void CreateFromData(const Float3Vector & data) {}

      void SetTransform(const Transformf & t);
      void ResetTransform(void);

      // true/false collision testing
      bool TestCollision(const BndVolume & bvol) const;
      bool TestCollision(const Vector3f & v) const;
      bool TestCollision(const BndSphere & bsp) const;
      bool TestCollision(const AABBox & bbox) const;
      bool TestCollision(const OBBox & bbox) const;

      // more thorough testing
      int TestIntersection(const BndVolume & bvol) const;
      int TestIntersection(const BndSphere & bsp) const;
      int TestIntersection(const AABBox & bbox) const;
      int TestIntersection(const OBBox & bbox) const;

      ProjectionType GetProjectionType(void) const {return type_;}
      const ProjectionBase & GetProjection(void) const {return proj_;}

      void SetPerspective(float fov, float asp, float near, float far);
      void SetOrtho(float left, float right, float bottom,
        float top, float near, float far);

    private:
      ProjectionType type_;
      ProjectionBase proj_;

      Vector3f eye_;
      Vector3f dir_;
      Vector3f right_;
      Vector3f up_;

    private:
      void UpdatePerspective(void);
      void UpdateOrtho(void);
  };
}
