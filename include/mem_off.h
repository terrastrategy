//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

// ----------------------------------------------------------------------------
// Originally created on 12/22/2000 by Paul Nettle
//
// Copyright 2000, Fluid Studios, Inc., all rights reserved.
//
// For more information, visit HTTP://www.FluidStudios.com
// ----------------------------------------------------------------------------

/**
 *  @file mem_off.h
 *
 *  Contains macros for disabling the overriden new/delete macros.
 */

// defines MEM_MANAGER_ON macro
#include "config.h"

#ifdef MEM_MANAGER_ON

#ifdef new
#  undef new
#endif

#ifdef delete
#  undef delete
#endif

#ifdef malloc
#  undef malloc
#endif

#ifdef calloc
#  undef calloc
#endif

#ifdef realloc
#  undef realloc
#endif

#ifdef free
#  undef free
#endif

#endif /* MEM_MANAGER_ON */
