//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

// ----------------------------------------------------------------------------
// Originally created on 12/22/2000 by Paul Nettle
//
// Copyright 2000, Fluid Studios, Inc., all rights reserved.
//
// For more information, visit HTTP://www.FluidStudios.com
// ----------------------------------------------------------------------------

/**
 *  @file mem_on.h
 *
 *  Contains macros overriding the standard new/delete operators.
 */

// defines MEM_MANAGER_ON macro
#include "config.h"

#ifdef MEM_MANAGER_ON

#define new (tre::SetOwner(__FILE__,__LINE__,__FUNCTION__),false) ? NULL : new
#define delete (tre::SetOwner(__FILE__,__LINE__,__FUNCTION__),false) ? \
  tre::SetOwner("",0,"") : delete
#define malloc(sz) tre::Allocator(__FILE__,__LINE__,\
  __FUNCTION__,tre::atMalloc,sz)
#define calloc(sz) tre::Allocator(__FILE__,__LINE__,\
  __FUNCTION__,tre::atCalloc,sz)
#define realloc(ptr,sz) tre::Reallocator(__FILE__,__LINE__,\
  __FUNCTION__,tre::atRealloc,sz,ptr)
#define free(ptr) tre::Deallocator(__FILE__,__LINE__,\
  __FUNCTION__,tre::atFree,ptr)

#endif /* MEM_MANAGER_ON */
