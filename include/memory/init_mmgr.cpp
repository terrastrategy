//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file init_mmgr.cpp
 *
 *  This file should be *included* to unit defining static global objects.
 *  Logger should be initialized prior to memory manager.
 */

#include "config.h"

#ifdef MEM_MANAGER_ON

#ifndef BUILD_NAME
#  error BUILD_NAME not defined!
#endif /* BUILD_NAME */

namespace tre {
  bool InitializeMemoryManager(const char * buildName);
}

namespace {

 /**
  *  @class MemInit
  *
  *  Used for initialization of memory manager.
  */
  class MemInit {
    public:
      MemInit() {tre::InitializeMemoryManager(BUILD_NAME);}
  };

  /** Memory manager object */
  MemInit sMemInit;

}

#endif /* MEM_MANAGER_ON */
