//
//  Copyright (C) 2007 by Martin Moracek
//  Portions Copyright (C) 2001 by Peter Dalton
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

// ----------------------------------------------------------------------------
// Originally created on 12/22/2000 by Paul Nettle
//
// Copyright 2000, Fluid Studios, Inc., all rights reserved.
//
// For more information, visit HTTP://www.FluidStudios.com
// ----------------------------------------------------------------------------

/**
 *  @file mmgr.h
 *
 *  Contains declarations of exported memory management routines.
 *
 *  This file exposes control routines for modifying memory manager's
 *  behaviour and creating log reports.
 */

#pragma once

// defines MEM_MANAGER_ON macro
#include "config.h"

#ifdef MEM_MANAGER_ON

#include <stdlib.h>

#include <iosfwd>
#include <new>

namespace tre {

  struct MemStats {
    unsigned totalReportedMemory;
    unsigned totalActualMemory;
    unsigned peakReportedMemory;
    unsigned peakActualMemory;
    unsigned accumReportedMemory;
    unsigned accumActualMemory;
    unsigned accumAllocUnitCount;
    unsigned totalAllocUnitCount;
    unsigned peakAllocUnitCount;
  };

 /*
  *  Constants
  */
  /// Posible allocation/deallocation types.
  typedef unsigned char AllocType;

  const AllocType atUnknown = 0;      /// Unknown allocation.
  const AllocType atNew = 1;          /// new
  const AllocType atNewArray = 2;     /// new []

  // C library functions
  const AllocType atMalloc = 3;       /// malloc()
  const AllocType atCalloc = 4;       /// calloc()
  const AllocType atRealloc = 5;      /// realloc()

  // deallocations
  const AllocType atDelete = 6;       /// delete
  const AllocType atDeleteArray = 7;  /// delete []
  const AllocType atFree = 8;         /// free()

 /*
  * Init functions
  */
  bool InitializeMemoryManager(const char * buildName);
  void ReleaseMemoryManager(void);

 /*
  *  Used by macros
  */
  void SetOwner(const char * file, unsigned line, const char * func);

 /*
  *  Control functions
  */
  void SetAlwaysValidateAll(bool st);
  void SetAlwaysLogAll(bool st);
  void SetAlwaysWipeAll(bool st);
  void SetRandomWipe(bool st);
  void SetBreakOnRealloc(void * address, bool st);
  void SetBreakOnDealloc(void * address, bool st);
  void SetBreakOnAllocation(unsigned int count);

 /*
  *  The meat of the memory tracking software
  */
  void * Allocator(const char * file, unsigned line,
    const char * func, AllocType type, size_t size);
  void * Reallocator(const char * file, unsigned line,
    const char * func, AllocType type, size_t size, void * address);
  void Deallocator(const char * file, unsigned line,
    const char * func, AllocType type, const void * address);

 /*
  *  Utilitarian functions
  */
  bool ValidateAddress(const void * address);
  bool ValidateAllAllocUnits(void);

 /*
  *  Unused RAM calculations
  */
  unsigned CalcAllUnused(void);

 /*
  *  Logging and reporting
  */
  void DumpLogReport(std::ostream & out);
  MemStats GetMemoryStatistics(void);
}

/*
 *  Variants of overloaded new & delete operators
 */

void * operator new(size_t size) throw(std::bad_alloc);
void * operator new[](size_t size) throw(std::bad_alloc);
void * operator new(size_t size, const char * file, int line)
  throw(std::bad_alloc);
void * operator new[](size_t size, const char * file, int line)
  throw(std::bad_alloc);

void operator delete(void * address) throw();
void operator delete[](void * address) throw();

#include "mem_on.h"

#endif /* MEM_MANAGER_ON */
