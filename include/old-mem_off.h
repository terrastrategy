//
//  Copyright (C) 2007 by Martin Moracek
//  Portions Copyright (C) 2001 by Peter Dalton
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/*!
 *  @file mem_off.h
 *
 *  @brief Contains macros for disabling the overriden new/delete macros.
 */

// defines MEM_MANAGER_ON
#include "config.h"

#ifdef MEM_MANAGER_ON

#undef new
#undef delete
#undef malloc
#undef calloc
#undef realloc
#undef free

#endif  /* MEM_MANAGER_ON */
