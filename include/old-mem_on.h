//
//  Copyright (C) 2007 by Martin Moracek
//  Portions Copyright (C) 2001 by Peter Dalton
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/*!
 *  @file mem_on.h
 *
 *  @brief Contains macros overring the standard new/delete operators.
 */

// defines MEM_MANAGER_ON
#include "config.h"

#ifdef MEM_MANAGER_ON

#define new new(__FILE__,__LINE__)
#define delete mem::SetOwner(__FILE__,__LINE__),delete
#define malloc(sz) mem::AllocateMemory(__FILE__,__LINE__,sz,mem::mmMalloc)
#define calloc(num,sz) mem::AllocateMemory(__FILE__,__LINE__,sz*num,mem::mmCalloc)
#define realloc(ptr,sz) mem::AllocateMemory(__FILE__,__LINE__,sz,mem::mmRealloc,ptr)
#define free(sz) mem::SetOwner(__FILE__,__LINE__),mem::DeallocateMemory(sz,mem::mmFree)

#endif  /* MEM_MANAGER_ON */
