//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file platform.h
 *
 *  Header file for platform dependent macros and constants.
 */

#pragma once

/*
 *  Little/big endianism
 */

#ifdef IS_BIG_ENDIAN

#  include "types.h"

#  if (IS_BIG_ENDIAN == 1)
#    define PLATFORM_BIG_ENDIAN

// platform conversion functions
inline uint16 ToLittleEndian(uint16 v)
{
  return (0x00FF & v) << 8 |
         (0xFF00 & v) >> 8;
}

inline uint32 ToLittleEndian(uint32 v)
{
  return (0x000000FF & v) << 24 |
         (0x0000FF00 & v) << 8  |
         (0x00FF0000 & v) >> 8  |
         (0xFF000000 & v) >> 24;
}

inline uint16 ToBigEndian(uint16 v)
{
  return v;
}

inline uint32 ToBigEndian(uint32 v)
{
  return v;
}

#  else /* (IS_BIG_ENDIAN == 1) */
#    define PLATFORM_LITTLE_ENDIAN

// platform conversion functions
inline uint16 ToLittleEndian(uint16 v)
{
  return v;
}

inline uint32 ToLittleEndian(uint32 v)
{
  return v;
}

inline uint16 ToBigEndian(uint16 v)
{
  return (0x00FF & v) << 8 |
         (0xFF00 & v) >> 8;
}

inline uint32 ToBigEndian(uint32 v)
{
  return (0x000000FF & v) << 24 |
         (0x0000FF00 & v) << 8  |
         (0x00FF0000 & v) >> 8  |
         (0xFF000000 & v) >> 24;
}

#  endif /* (IS_BIG_ENDIAN == 1) */
#endif /* IS_BIG_ENDIAN */

/*
 *  Platform/compiler detection
 */

#ifdef _MSC_VER
#  define PLATFORM_MSVC
#else
#  ifdef __GNUC__
#    define PLATFORM_GCC
#    ifdef linux
#      define PLATFORM_GCC_LINUX
#    else
#      define PLATFORM_GCC_WIN
#    endif /* linux */
#  else
#    error Unknown compiler!
#  endif /* __GNUC__ */
#endif /* _MSC_VER */

#if defined(_DEBUG) && !defined(DEBUG)
#  define DEBUG
#endif

/*
 *  Platform dependent macros
 */
#ifdef PLATFORM_MSVC

#  define lseek _lseek
#  define read _read
#  define write _write

#  define snprintf _snprintf
#  define strcasecmp _stricmp

   // to get clearer compiler output
#  pragma warning( disable : 4290 )
#  pragma warning( disable : 4996 )

// conversion from 'x' to 'y', possible loss of data
#  pragma warning( disable : 4244 )

#  pragma warning( disable : 4345 )

#endif /* PLATFORM_MSVC */

#ifdef PLATFORM_GCC

#define __FUNCTION__ __PRETTY_FUNCTION__

#endif /* PLATFORM_GCC */

/*
 *  Build name - for creating project dependent logfiles, etc.
 */
extern const char * gBuildName;
