//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file buffers.h
 *
 *  Ble.
 */

#pragma once

#include <numeric>
#include <boost/smart_ptr.hpp>
#include <boost/utility.hpp>

#include "stdtypes.h"

#include "math/vector.h"

namespace tre {

  typedef uint AttribType;
  const AttribType atPosition = 0;
  const AttribType atBlendweight = 1;
  const AttribType atNormal = 2;
  const AttribType atDiffuse = 3;
  const AttribType atColour = 3;
  const AttribType atSpecular = 4;
  const AttribType atSecondaryColour = 4;
  const AttribType atTessFactor = 5;
  const AttribType atFogCoords = 5;
  const AttribType atPointSize = 6;
  const AttribType atBlendIndices = 7;
  const AttribType atTexCoord0 = 8;
  const AttribType atTexCoord1 = 9;
  const AttribType atTexCoord2 = 10;
  const AttribType atTexCoord3 = 11;
  const AttribType atTexCoord4 = 12;
  const AttribType atTexCoord5 = 13;
  const AttribType atTexCoord6 = 14;
  const AttribType atlTexCoord7 = 15;
  const AttribType atTangent = 14;
  const AttribType atBitangent = 15;
  const AttribType atGeneric0 = 0;
  const AttribType atGeneric1 = 1;
  const AttribType atGeneric2 = 2;
  const AttribType atGeneric3 = 3;
  const AttribType atGeneric4 = 4;
  const AttribType atGeneric5 = 5;
  const AttribType atGeneric6 = 6;
  const AttribType atGeneric7 = 7;
  const AttribType atGeneric8 = 8;
  const AttribType atGeneric9 = 9;
  const AttribType atGeneric10 = 10;
  const AttribType atGeneric11 = 11;
  const AttribType atGeneric12 = 12;
  const AttribType atGeneric13 = 13;
  const AttribType atGeneric14 = 14;
  const AttribType atGeneric15 = 15;

  enum AttribDataType {
    adFloat1 = 0,
    adFloat2,
    adFloat3,
    adFloat4,
    adUInt1,
    adUInt2,
    adUInt3,
    adUInt4
  };

  enum AccessMode {
    amRead = 1,
    amWrite,
    amReadWrite
  };

  class AttribBufferSet;
  class IndexBuffer;

  typedef boost::shared_ptr<AttribBufferSet> AttribBufPtr;
  typedef boost::weak_ptr<AttribBufferSet> AttribBufRef;
  typedef boost::shared_ptr<IndexBuffer> IndexBufPtr;
  typedef boost::weak_ptr<IndexBuffer> IndexBufRef;

  class AttribBufferSetFactory {
    public:
      AttribBufferSet * CreateInstance(void);
  };

  class IndexBufferFactory {
    public:
      IndexBuffer * CreateInstance(void);
  };

  class AttribBufferSet : private boost::noncopyable {
    public:
      static AttribBufferSetFactory & Factory(void)
      {
        static AttribBufferSetFactory fac;
        return fac;
      }

    public:
      AttribBufferSet() {}
      virtual ~AttribBufferSet() {}

      // initialization & destruction
      virtual void Init(AttribType loc, AttribDataType dat,
        uint size, AccessMode mode=amRead) = 0;
      virtual void Clear(AttribType loc) = 0;

      // memory mapping
      virtual void * Lock(AttribType loc, AccessMode mode=amWrite,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void Unlock(AttribType loc) = 0;

      // Set automatically initializes (creates) buffer
      virtual void Set(AttribType loc, const FloatVector & data,
        AccessMode mode=amRead) = 0;
      virtual void Set(AttribType loc, const Float2Vector & data,
        AccessMode mode=amRead) = 0;
      virtual void Set(AttribType loc, const Float3Vector & data,
        AccessMode mode=amRead) = 0;
      virtual void Set(AttribType loc, const Float4Vector & data,
        AccessMode mode=amRead) = 0;
      virtual void Set(AttribType loc, const UIntVector & data,
        AccessMode mode=amRead) = 0;
      virtual void Set(AttribType loc, const UInt2Vector & data,
        AccessMode mode=amRead) = 0;
      virtual void Set(AttribType loc, const UInt3Vector & data,
        AccessMode mode=amRead) = 0;
      virtual void Set(AttribType loc, const UInt4Vector & data,
        AccessMode mode=amRead) = 0;

      virtual void UpdateRange(AttribType loc, const FloatVector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void UpdateRange(AttribType loc, const Float2Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void UpdateRange(AttribType loc, const Float3Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void UpdateRange(AttribType loc, const Float4Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void UpdateRange(AttribType loc, const UIntVector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void UpdateRange(AttribType loc, const UInt2Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void UpdateRange(AttribType loc, const UInt3Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void UpdateRange(AttribType loc, const UInt4Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
  };

  class IndexBuffer : private boost::noncopyable {
    public:
      static IndexBufferFactory & Factory(void)
      {
        static IndexBufferFactory fac;
        return fac;
      }

    public:
      // index limits, might be used during rendering
      uint vFrom;
      uint vTo;

    public:
      IndexBuffer() : vFrom(0), vTo(std::numeric_limits<uint>::max()) {}
      virtual ~IndexBuffer() {}

      // initialization & destruction
      virtual void Init(uint size, AccessMode mode=amRead) = 0;
      virtual void Clear(void) = 0;

      // memory mapping
      virtual uint * Lock(AccessMode mode=amWrite, uint offset=0,
        uint size=std::numeric_limits<uint>::max()) = 0;
      virtual void Unlock(void) = 0;

      // Set automatically initializes (creates) buffer
      virtual void Set(const UIntVector & data, AccessMode mode=amRead) = 0;
      virtual void UpdateRange(const UIntVector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max()) = 0;
  };

}
