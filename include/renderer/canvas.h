//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file canvas.h
 *
 *  Ble.
 */

#pragma once

#include "stdtypes.h"

#include "texture.h"

namespace tre {

  class Canvas;

  typedef boost::shared_ptr<Canvas> CanvasPtr;
  typedef boost::weak_ptr<Canvas> CanvasRef;

  class CanvasFactory {
    public:
      CanvasPtr CreateInstance(void);
      CanvasPtr CreateInstance(uint w, uint h);
  };

  class Canvas : private boost::noncopyable {
    public:
      static CanvasFactory & Factory(void)
      {
        static CanvasFactory fac;
        return fac;
      }

    public:
      Vector4f bgColour;

    public:
      Canvas(uint w, uint h) : bgColour(0.0f), width_(w), height_(h) {}
      virtual ~Canvas() {}

      uint GetWidth(void) const {return width_;}
      uint GetHeight(void) const {return height_;}

      TexturePtr GetTexture(void) {return texture_;}

    protected:
      uint width_;
      uint height_;

      TexturePtr texture_;
   };
}
