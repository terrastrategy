//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file effect.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <vector>
#include <boost/utility.hpp>
#include <boost/array.hpp>
#include <boost/smart_ptr.hpp>

#include <tr1/unordered_map>

#include "types.h"
#include "cpair.h"
#include "renderer/texture.h"

namespace tre {

  // data types for uniform variables
  enum EffectVarType {
    vtFloat1,
    vtFloat2,
    vtFloat3,
    vtFloat4,
    vtInt1,
    vtInt2,
    vtInt3,
    vtInt4,
    vtTexture
  };

  // builtins for all shader types
  enum InternalsType {  // Semantic, annotations...
    itTime,             // Time
    itSysTime,          // Time, type = "system"
    // state matrices
    itMVPMatrix,        // ModelViewProjection
    itMVPMatrixInv,     // ModelViewProjection, inverse = true
    itMVPMatrixTrans,   // ModelViewProjection, transpose=true
    itMVPMatrixInvTrans,// ModelViewProjection, inverse = true, transpose=true
    itMVMatrix,         // ModelView
    itMVMatrixInv,      // ModelView, inverse = true
    itMVMatrixTrans,    // ModelView, transpose=true
    itMVMatrixInvTrans, // ModelView, transpose=true
    itPMatrix,          // Projection
    itPMatrixInv,       // Projection, inverse = true
    itPMatrixTrans,     // Projection, transpose=true
    itPMatrixInvTrans,  // Projection, transpose=true
    itTMatrix,          // Texture
    itTMatrixInv,       // Texture, inverse = true
    itTMatrixTrans,     // Texture, transpose=true
    itTMatrixInvTrans,  // Texture, transpose=true
    // last entry
    itLast
  };

  // variable declarations for general use
  typedef boost::array<float, 4> FxFVar;
  typedef boost::array<int, 4> FxIVar;

  typedef cpair<std::string, FxFVar> FxFPair;
  typedef cpair<std::string, FxIVar> FxIPair;
  typedef cpair<std::string, TexturePtr> FxSPair;

  typedef std::vector<FxFPair> FVarVector;
  typedef std::vector<FxIPair> IVarVector;
  typedef std::vector<FxSPair> SVarVector;

  struct EffectVars {
    FVarVector fvars;
    IVarVector ivars;
    SVarVector samplers;

    EffectVars();
    ~EffectVars();

    void SetTexture(const std::string & name, const std::string & var);
    void SetTexture(const std::string & name, const TexturePtr & var);
  };

 /*
  *  Effect class
  */
  class Effect;

  typedef boost::shared_ptr<Effect> EffectPtr;
  typedef boost::weak_ptr<Effect> EffectRef;

  class EffectFactory {
    public:
      const EffectPtr CreateInstance(const std::string & name);

    private:
      typedef std::tr1::unordered_map<std::string, EffectRef> EffectMap;

    private:
      EffectMap map_;
  };

  class Effect : private boost::noncopyable {
    public:
      static EffectFactory & Factory(void)
      {
        static EffectFactory fac;
        return fac;
      }

    public:
      virtual ~Effect() {}
  };

}
