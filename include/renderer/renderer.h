//
//  Copyright (C) 2007-2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file renderer.h
 *
 *  Encapsulates chosen renderer/display
 */

#pragma once

#include <vector>

#include "types.h"
#include "math/matrix.h"
#include "core/client/display.h"

namespace tre {

  class PVolume;
  class AttribBufferSet;
  class IndexBuffer;
  class Canvas;
  class Effect;
  struct EffectVars;

  struct FrameStats {
    uint triangles;
    uint batches;   // number of draw calls

    // visible lights
    // fps
    

   /*
   -frame:
  -triangles (unique vs. total)
  -batches (unique vs. total) - draw calls
  -visible lights
  -fps
  -seconds per frame
 -global:
  -total frames
  -time spent rendering?
  -average fps
  -average triangles
  -average batches
  -average lights
   */

    uint time;
    
    uint count;   // frame count
  };

  class GeometryInfo {
    public:
      const Matrix4x4f * trans;
      const AttribBufferSet * attribs;
      const IndexBuffer * indices;
      const Effect * effect;
      const EffectVars * fxVars;

      // bounding volume?

    public:
      GeometryInfo(const Matrix4x4f & t, const AttribBufferSet * a,
        const IndexBuffer * i, const Effect * e, const EffectVars & ev)
        : trans(&t), attribs(a), indices(i), effect(e), fxVars(&ev) {}

      GeometryInfo(const Matrix4x4f * t, const AttribBufferSet * a,
        const IndexBuffer * i, const Effect * e, const EffectVars * ev)
        : trans(t), attribs(a), indices(i), effect(e), fxVars(ev) {}
  };

  typedef std::vector<GeometryInfo> GeometryBatch;

  class Renderer {
    public:
      static bool CheckErrors(const char * msg=NULL, bool confirmOK=false);

    public:
      FrameStats frame;

    public:
      Renderer() {}
      virtual ~Renderer() {}

      virtual void Init(void) = 0;

      virtual void NewFrame(void) = 0;
      virtual void FlushFrame(void) = 0;
      virtual void PresentFrame(void) = 0;

      virtual void SetCamera(const PVolume & cam) = 0;
      virtual void SetCameraTransform(const Matrix4x4f * cnv) = 0;

      virtual void PushCanvas(const Canvas * cnv) = 0;
      virtual void PopCanvas(void) = 0;

      virtual void DrawGeometry(const GeometryBatch::iterator & begin,
                                const GeometryBatch::iterator & end) = 0;
  };

  Renderer & sRenderer(void);
}
