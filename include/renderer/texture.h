//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file texture.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <boost/smart_ptr.hpp>
#include <boost/utility.hpp>

#include <tr1/unordered_map>

#include "stdtypes.h"
#include "math/vector.h"

namespace tre {

 /*
  *  Enumerations and constants
  */
  enum TextureType {
    ttTexture1D,
    ttTexture2D,
    ttTexture3D,
    ttCubeMap
  };

  enum TextureFormat {
    tfUnknown,
    tfR8G8B8,
    tfR8G8B8A8,
    
    // FIXME: rename to be more OpenGL/D3D10 like (endinanism)
    tfA8R8G8B8,
    tfX8R8G8B8,
    tfA4R4G4B4,
    tfR3G3B2,
    tfA8R3G3B2,
    tfX4R4G4B4,
    tfR5G6B5,
    tfX1R5G5B5,
    tfA1R5G5B5,
    tfA8,
    tfA2B10G10R10,
    tfA2R10G10B10,
    tfA8B8G8R8,
    tfX8B8G8R8,
    tfG16R16,
    tfA16B16G16R16,
    tfR16F,
    tfG16R16F,
    tfA16B16G16R16F,
    tfR32F,
    tfG32R32F,
    tfA32B32G32R32F,
    tfLast
  };

 /*
  *  Texture class
  */

  class Texture;

  typedef boost::shared_ptr<Texture> TexturePtr;
  typedef boost::weak_ptr<Texture> TextureRef;

  class TextureFactory {
    public:
      const TexturePtr CreateInstance(const std::string & name);
      const TexturePtr CreateInstance(const std::string & name,
        TextureType type, uint width, uint height, TextureFormat format);

      // create anonymous texture
      const TexturePtr CreateInstance(TextureType type,
        uint width, uint height, TextureFormat format);

    private:
      typedef std::tr1::unordered_map<std::string, TextureRef> TextureMap;

    private:
      TextureMap map_;
  };

  class Texture : private boost::noncopyable {
    public:
      static TextureFactory & Factory(void)
      {
        static TextureFactory fac;
        return fac;
      }

    public:
      Texture() {}
      virtual ~Texture() {}

      virtual void UpdateRect(const Vector2u & origin,
        const Vector2u & size, const ByteVector & data) = 0;

#ifdef DEBUG
      virtual void DumpToFile(const std::string & name) const = 0;
#endif
  };

}
