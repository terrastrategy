//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file stdtypes.h
*
 *  Header file containing definitions of specialized templated data types.
 */

#pragma once

#include <vector>
#include <string>

#include "types.h"
#include "math/vector.h"
#include "math/matrix.h"

/*
 *  Common vector typedefs
 */

typedef std::vector<bool> BitVector;
typedef std::vector<byte> ByteVector;

typedef std::vector<int> IntVector;
typedef std::vector<tre::Vector2i> Int2Vector;
typedef std::vector<tre::Vector3i> Int3Vector;
typedef std::vector<tre::Vector4i> Int4Vector;

typedef std::vector<uint> UIntVector;
typedef std::vector<tre::Vector2u> UInt2Vector;
typedef std::vector<tre::Vector3u> UInt3Vector;
typedef std::vector<tre::Vector4u> UInt4Vector;

typedef std::vector<float> FloatVector;
typedef std::vector<tre::Vector2f> Float2Vector;
typedef std::vector<tre::Vector3f> Float3Vector;
typedef std::vector<tre::Vector4f> Float4Vector;

typedef std::vector<std::string> StrVector;

typedef std::vector<tre::Matrix4x4f> Mat4Vector;
