//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file types.h
 *
 *  Header file containing definitions of custom data types.
 */

#pragma once

#include <limits.h>

/*
 *  Common data-types
 */
typedef unsigned char byte;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;

/*
 *  Integral types
 */
typedef char int8;
typedef unsigned char uint8;

#if SHRT_MAX == 32767
  typedef short int16;
  typedef unsigned short int uint16;
#endif

#if INT_MAX == 2147483647
  typedef int int32;
  typedef unsigned int uint32;
#elif SHRT_MAX == 2147483647
  typedef short int32;
  typedef unsigned short uint32;
#elif LONG_MAX == 2147483647
  typedef long int32;
  typedef unsigned long uint32;
#endif

#if LONG_MAX == +9223372036854775807
  typedef long int64;
  typedef unsigned long uint64;
#elif LLONG_MAX == +9223372036854775807
  typedef long long int64;
  typedef unsigned long long uint64;
#endif

#if POINTER_SIZE == 4
  typedef uint32 uptr;
#elif POINTER_SIZE == 8
  typedef uint64 uptr;
#endif
