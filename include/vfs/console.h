//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file console.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <iosfwd>
#include <deque>

#include "types.h"

namespace tre {

  /** Default size of IO buffers. */
  const int defConsoleLines = 80;

  class ConsoleBuffer : public std::streambuf {
    public:
      ConsoleBuffer(uint l) : sq_(l), lines_(l) {}

    protected:
      // write one character
      int overflow(int c);

      // write multiple characters
      std::streamsize xsputn(const char * s, std::streamsize num);

    private:
      typedef std::deque<std::string> StringQueue;

      StringQueue sq_;
      uint lines_;
  };

  class ConsoleStream : public std::iostream {
    public:
      static bool ready;

    public:
      ConsoleStream(uint lines=defConsoleLines);
      ~ConsoleStream();

      int format(const char * fmt, ...);

    private:
      ConsoleBuffer buf_;
  };

  extern ConsoleStream vOut;

}
