/*
 *  Copyright (C) 2007 by Martin Moracek
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 *  @file export.h
 *
 *  Contains declarations of functions and variables exported from vfs.
 *
 *  This file contains declarations of functions and variables to be "exported"
 *  from C++ vfs namespace into ANSI C parts of the program (lua).
 */

#pragma once

#include <stdarg.h>

typedef void VFS_FILE;
typedef unsigned long vfs_size_t;
typedef unsigned long vfs_fpos_t;

/* Constants */
/* Pity this can't be const int because of ANSI C */
#define VFS_BUFSIZ      8192
#define VFS_EOF         -1

#define VFS_FILENAME_MAX 255
#define VFS_FOPEN_MAX    255

/* Not supported */
#define VFS_L_tmpnam     1
#define VFS_TMP_MAX      0

#define VFS_SEEK_SET    0
#define VFS_SEEK_CUR	  1
#define VFS_SEEK_END    2

/* full/line/no buffering */
#define VFS_IOFBF 0
#define VFS_IOLBF 1
#define VFS_IONBF 2

/* Macros */
#define vfs_setbuf(f,b,s)   vfs_setvbuf((f),(b),VFS_IOFBF,VFS_BUFSIZ)

#define vfs_getc(f)         vfs_fgetc(f)
#define vfs_getchar()       vfs_fgetc(vfs_stdin)
/* No gets, just fgets */
#define vfs_gets            vfs_fgets
#define vfs_putc(c,s)       vfs_fputc((c),(s))
#define vfs_putchar(c)      vfs_fputc((c),vfs_stdout)
#define vfs_puts(s)     {vfs_fputs((s),vfs_stdout);vfs_fputc('\n',vfs_stdout);}

#define vfs_rewind(s)       vfs_fseek((s),0,VFS_SEEK_SET)

/* stdin, stdout, stderr */
extern VFS_FILE * vfs_stdin;   /** Wrapper for game console input. */
extern VFS_FILE * vfs_stdout;  /** Wrapper for game console output. */
extern VFS_FILE * vfs_stderr;  /** Wrapper for game error output. */

/*
 *  Operations on files
 */

extern int vfs_remove(const char * filename);
extern int vfs_rename(const char * oldname,const char * newname);
extern VFS_FILE * vfs_tmpfile(void);
extern char * vfs_tmpnam(char *s);

/*
 *  File access
 */

extern int vfs_fclose(VFS_FILE * stream);
extern int vfs_fflush(VFS_FILE * stream);
extern VFS_FILE * vfs_fopen(const char * filename,const char * mode);
extern VFS_FILE * vfs_freopen(const char * filename,
                    const char * mode,VFS_FILE * stream);
extern int vfs_setvbuf(VFS_FILE * file,char * buf,int mode,vfs_size_t size);

/*
 *  Formatted input/output
 */

extern int vfs_fprintf(VFS_FILE * stream,const char * format,...);
extern int vfs_fscanf(VFS_FILE * stream,const char * format,...);
extern int vfs_printf(const char * format,...);
extern int vfs_scanf(const char * format,...);
extern int vfs_vfprintf(VFS_FILE * stream, const char * format,va_list ap);
extern int vfs_vfscanf(VFS_FILE * stream, const char * format,va_list ap);
extern int vfs_vprintf(const char * format,va_list ap);
extern int vfs_vscanf(const char * format,va_list ap);

/*
 *  Character input/output
 */

extern int vfs_fgetc(VFS_FILE * stream);
extern char * vfs_fgets(char * s, int size,VFS_FILE * stream);
extern int vfs_fputc(int c,VFS_FILE * stream);
extern int vfs_fputs(const char * s,VFS_FILE * stream);
extern int vfs_ungetc(int c,VFS_FILE * stream);

/*
 *  Direct input/output
 */

extern vfs_size_t vfs_fread(void * ptr,vfs_size_t size,
                       vfs_size_t nmemb,VFS_FILE * stream);
extern vfs_size_t vfs_fwrite(const void * ptr,vfs_size_t size,
                       vfs_size_t nmemb,VFS_FILE * stream);

/*
 * File positioning
 */

extern int vfs_fgetpos(VFS_FILE * stream,vfs_fpos_t * position);
extern int vfs_fseek(VFS_FILE * stream,long offset,int whence);
extern int vfs_fsetpos(VFS_FILE * stream,const vfs_fpos_t * pos);
extern long vfs_ftell(VFS_FILE * stream);

/*
 *  Error-handling
 */

extern void vfs_clearerr(VFS_FILE * file);
extern int vfs_feof(VFS_FILE * file);
extern int vfs_ferror(VFS_FILE * file);
extern void vfs_perror(const char * s);
