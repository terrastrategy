//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file fstream.h
 *
 *  blablabla
 */

#pragma once

#include <stdarg.h>
#include <istream>

#include "types.h"

namespace tre {

 /*
  *  File stream classes
  *
  *  names of public methods follow STL naming convention
  */
  class FileIStream : public std::istream {
    public:
      FileIStream();
      FileIStream(const std::string & filename,
        std::ios_base::openmode mode=std::ios_base::binary);
      ~FileIStream();

      bool open(const std::string & filename,
        std::ios_base::openmode mode=std::ios_base::binary);

      void close(void);

      bool is_open(void) const;

      ulong length(void) const;

      int iformat(const char * fmt, ...);
  };

  class FileOStream : public std::ostream {
    public:
      FileOStream();
      FileOStream(const std::string & filename,
        std::ios_base::openmode mode=std::ios_base::binary
        | std::ios_base::trunc);
      ~FileOStream();

      bool open(const std::string & filename,
        std::ios_base::openmode mode=std::ios_base::binary
        | std::ios_base::trunc);

      void close(void);

      bool is_open(void) const;

      ulong length(void) const;

      int oformat(const char * fmt, ...);
  };

  class FileIOStream : public std::iostream {
    public:
      FileIOStream();
      FileIOStream(const std::string & filename,
        std::ios_base::openmode mode=std::ios_base::binary
        | std::ios_base::in);
      ~FileIOStream();

      bool open(const std::string & filename,
        std::ios_base::openmode mode=std::ios_base::binary
        | std::ios_base::in);

      void close(void);

      bool is_open(void) const;

      ulong length(void) const;

      int iformat(const char * fmt, ...);
      int oformat(const char * fmt, ...);
  };

  /*
   *  FileBuffer interface
   */
  class FileBuffer : public std::streambuf {
    public:
      virtual ulong GetSize(void) const = 0;
  };

} // vfs
