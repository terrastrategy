//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file interface.h
 *
 *  Contains declarations of common virtual file system interface.
 *
 *  The whole VFS interface is wrapped in vfs namespace in order to
 *  reinforce it's autonomous nature.
 */

#pragma once

#include <vector>
#include <string>
#include <ios>

#include "stdtypes.h"

/**
 *  @namespace vfs
 *
 *  Encapsulates the whole virtual file system.
 *
 *  Contains all VFS related functions classes. Functionality provided is
 *  opening and closing files, mounting and unmounting directories (including
 *  transparent support for zip-like archives), querying directories and
 *  retrieving basic file related information.
 */
namespace tre {

 /*
  *  Additional structures
  */

  struct FileInfo {
    bool isDir;
    bool readOnly;
    ulong size;
    ulong mtime;
  };

 /*
  *  Forward class declarations
  */
  class Vfs;
  class MountNode;

  class FileIStream;
  class FileOStream;
  class FileIOStream;

 /*
  *  Global variables
  */

  Vfs & sVfs(void);

 /*
  *  Class declarations
  */

 /**
  *  @class Vfs
  *
  *  Interface for all non-file operations.
  *
  *  Contains all mounted directories stored in an array and provides basic
  *  methods for mounting and unmounting directories. Note that root
  *  directory must be mounted prior to any file or directory operation. Also
  *  offers some means for directory parsing.
  */
  class Vfs {
    public:
      struct MountInfo {
        std::string name;
        std::string path;
        bool readOnly;
      };

      typedef std::vector<MountInfo> MountInfoVector;

    public:
      Vfs();
      ~Vfs();

      bool MountPath(const std::string & path, const std::string & dir,
                     bool readOnly=true);

      bool UnMountPath(const std::string & path);
      bool UnMountDir(const std::string & dir);
      bool UnMountDirPath(const std::string & dir, const std::string & path);

      bool OpenFile(const std::string & filename, FileIStream & file,
                    std::ios_base::openmode mode=std::ios_base::binary) const;
      bool OpenFile(const std::string & filename, FileOStream & file,
                    std::ios_base::openmode mode=std::ios_base::binary
                    | std::ios_base::trunc) const;
      bool OpenFile(const std::string & filename, FileIOStream & file,
                    std::ios_base::openmode mode=std::ios_base::binary
                    | std::ios_base::in) const;

      const std::string TranslatePath(const std::string & filename,
        std::ios_base::openmode mode=std::ios_base::in);

      bool Stat(const std::string & filename, FileInfo & info) const;
      bool Exists(const std::string & filename) const;

      const StrVector QueryDir(const std::string & dir) const;
      const MountInfoVector QueryMounts(void) const;

    private:
      typedef std::pair<std::string, MountNode*> MountPair;
      typedef std::vector<MountPair> MountVector;

    private:
      MountVector mounts_;  /// Mount points
  };

 /**
  *  @class MountNode
  *
  *  Abstract interface for all mountable sources.
  *
  *  Virtual class serving as a parent for deriving all classes encapsulating
  *  virtual drives. Provides basic interface.
  */
  class MountNode {
    public:
      MountNode(const std::string & path, bool mode)
        : path_(path), readOnly_(mode) {}

      virtual ~MountNode() {}

      /// Simple inspector function returning _strPath member.
      const std::string & GetPath(void) const {return path_;}

      bool IsReadOnly(void) const {return readOnly_;}

     /** Opens a file in specified mode diven it's virtual path.
      *
      *  @param[in] filename Name of file to be opened.
      *  @param[out] file Stream class to be initialized.
      *  @param[in] mode Mode in which the file should be opened.
      *  @return true on successfull operation, otherwise false.
      */
      virtual bool OpenFile(const std::string & filename,
        FileIStream & file, std::ios_base::openmode mode) const = 0;

      virtual bool OpenFile(const std::string & filename,
        FileOStream & file, std::ios_base::openmode mode) const = 0;

      virtual bool OpenFile(const std::string & filename,
        FileIOStream & file, std::ios_base::openmode mode) const = 0;

      virtual bool Stat(const std::string & filename,
        FileInfo & info) const = 0;

     /** Tests if the specified file exists in the vfs or not.
      *
      *  @param[in] filename Virtual path to the file to be tested.
      *  @return true if the file is found, otherwise false.
      */
      virtual bool Exists(const std::string & filename) const = 0;

      virtual const StrVector QueryDir(const std::string & dir) const = 0;

    protected:
      std::string path_; /// Real path of the mounted node.
      bool readOnly_;
  };

  // Vfs singleton
  inline Vfs & sVfs(void)
  {
    static Vfs vfs;

    return vfs;
  }

} // vfs
