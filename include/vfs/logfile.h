//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file logfile.h
 *
 *  Provides interface for the logging facility.
 */

#pragma once

#include <stdio.h>

#include <ostream>
#include <istream>
#include <streambuf>

namespace tre {

 /*
  *  Constants and types
  */

  const unsigned int maxOutPerFac = 5;

  enum LogFacility {
    lfDefault = 0,
    lfSystem,
    lfMemory,
    lfVideo,
    lfAudio,
    lfNetwork,
    lfDebug,
    lfUser,
    lfLocal0,
    lfLocal1,
    lfLocal2,
    lfLast
  };

  enum LogType {
    ltNone = 0,
    ltCerr,
    ltConsole,
    ltFile
  };

 /*
  *  Log stream utility stuff
  */
  class LogStream;

  class fac {
    public:
      fac(LogFacility facility=lfDefault) : fc_(facility) {}
      friend LogStream & operator << (LogStream & ls, const fac & m);

    private:
      LogFacility fc_;
  };

  class msg {
    public:
      msg(const char * sender=NULL) : snd_(sender) {}
      friend std::ostream & operator << (std::ostream & ls, const msg & m);

    private:
      const char * snd_;
  };

  class ok {
    public:
      ok(const char * sender=NULL) : snd_(sender) {}
      friend std::ostream & operator << (std::ostream & ls, const ok & m);

    private:
      const char * snd_;
  };

  class info {
    public:
      info(const char * sender=NULL) : snd_(sender) {}
      friend std::ostream & operator << (std::ostream & ls, const info & m);

    private:
      const char * snd_;
  };

  class warn {
    public:
      warn(const char * sender=NULL) : snd_(sender) {}
      friend std::ostream & operator << (std::ostream & ls, const warn & m);

    private:
      const char * snd_;
  };

  class err {
    public:
      err(const char * sender=NULL) : snd_(sender) {}
      friend std::ostream & operator << (std::ostream & ls, const err & f);

    private:
      const char * snd_;
  };

 /*
  *  Class declarations
  */
  class LogBuffer : public std::streambuf {
    public:
      LogBuffer(const char * deffile);
      ~LogBuffer();

      void AddOutput(LogFacility facility, LogType type,
        const char * file, bool trunc);
      void SetFacility(LogFacility facility) {activeFac_ = facility;}

    protected:
      int overflow(int c);
      std::streamsize xsputn(const char * s, std::streamsize num);
      int sync(void);

    private:
      struct OutputSlot {
        LogType type;
        FILE * file;
      };

    private:
      LogFacility activeFac_;     //  active facility
      OutputSlot out_[lfLast][maxOutPerFac];
  };

  class LogStream : public std::iostream {
    public:
      LogStream(const char * logfile);

      int oformat(const char * fmt, ...);

      void AddOutput(LogFacility facility, LogType type,
        const char * file=NULL, bool trunc=true);
      void SetFacility(LogFacility facility) {buf_.SetFacility(facility);}

    private:
      LogBuffer buf_;
  };

  extern LogStream vLog;
}
