//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file operators.h
 *
 *  Ble.
 */

#pragma once

#include <ostream>
#include <string>

#include "stdtypes.h"
#include "math/vector.h"

std::ostream & operator << (std::ostream & o, const tre::Vector2f & v);
std::ostream & operator << (std::ostream & o, const tre::Vector3f & v);
std::ostream & operator << (std::ostream & o, const tre::Vector4f & v);

std::ostream & operator << (std::ostream & o, const std::wstring & str);

template <typename T1, typename T2>
inline std::ostream & operator <<
(std::ostream & o, const std::pair<T1, T2> & p)
{
  o << "(" << p.first << ", " << p.second << ")";
  return o;
}

template <typename T>
inline std::ostream & operator << (std::ostream & o, const std::vector<T> & vec)
{
  o << "[";
  if(!vec.empty()) {
    o << vec.front();
    for(typename std::vector<T>::const_iterator pos = vec.begin() + 1;
        pos != vec.end(); ++pos) {
      o << ", " << *pos;
    }
  }
  o << "]";
  return o;
}
