//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file vario.h
 *
 *  Contains declaration of standard var io helper functions.
 *
 *  This file contains declarations of all functions for standard input and
 *  output with variable arguments. Belongs to the vfs namespace.
 */

#pragma once

#include <stdarg.h>
#include <iosfwd>

#include "platform.h"

#ifdef PLATFORM_MSVC
#  define VFS_VA_LIST va_list&
#else /* PLATFORM_MSVC */
#  define VFS_VA_LIST va_list
#endif /* PLATFORM_MSVC */

namespace tre {


  int StreamScanf(std::istream & is, const char * fmt, ...);
  int StreamPrintf(std::ostream & os, const char * fmt, ...);

  int StreamScanfV(std::istream & is, const char * fmt, VFS_VA_LIST l_va);
  int StreamPrintfV(std::ostream & os, const char * fmt, VFS_VA_LIST l_va);

}
