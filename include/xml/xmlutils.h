//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file xmlutils.h
 *
 *  Ble.
 */

#pragma once

#include <string>

class TiXmlElement;

namespace tre {

  std::string CheckText(TiXmlElement & el, bool require=true);

  int CheckIntText(TiXmlElement & el, bool require=true, int def=0);
  float CheckFloatText(TiXmlElement & el, bool require=true, float def=0.0f);
  double CheckFloatText(TiXmlElement & el, bool require=true, double def=0.0);

  std::string CheckAttribute(TiXmlElement & el,
    const char * name, bool require=true);

  int CheckIntAttribute(TiXmlElement & el,
    const char * name, bool require=true, int def=0);
  float CheckFloatAttribute(TiXmlElement & el,
    const char * name, bool require=true, float def=0.0f);
  float CheckDoubleAttribute(TiXmlElement & el,
    const char * name, bool require=true, double def=0.0);

}
