project(lib-core)

cmake_minimum_required(VERSION 2.6)

set(CMAKE_VERBOSE_MAKEFILE ON)

set(HEADER_DIR ../../include/core)
set(SRC_DIR ../../src/core)

set(LIB_SOURCES
  ${HEADER_DIR}/application.h
  ${HEADER_DIR}/timer.h
  ${HEADER_DIR}/vmachine.h
  ${HEADER_DIR}/script_core.h

  ${SRC_DIR}/application.cpp
  ${SRC_DIR}/timer.cpp
  ${SRC_DIR}/vmachine.cpp
  ${SRC_DIR}/script_core.cpp
  ${SRC_DIR}/conf_core.cpp
)

include_directories(../../include)
link_directories(../../lib)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ../../lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ../../lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../../)

add_definitions(-DTIXML_USE_STL)

# build commands
add_library(core SHARED ${LIB_SOURCES})

if(UNIX OR MINGW)
  set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -DDEBUG")

  set_target_properties(core PROPERTIES COMPILE_FLAGS
    "-ansi -pedantic -Wall -Wno-long-long")
endif(UNIX OR MINGW)

target_link_libraries(core lua SDL)
