project(lib-math)

cmake_minimum_required(VERSION 2.6)

set(CMAKE_VERBOSE_MAKEFILE ON)

set(HEADER_DIR ../../include/math)
set(SRC_DIR ../../src/math)

set(LIB_SOURCES
  ${HEADER_DIR}/math.h
  ${HEADER_DIR}/vector.h
  ${HEADER_DIR}/matrix.h
  ${HEADER_DIR}/quaternion.h
  ${HEADER_DIR}/transform.h
  ${HEADER_DIR}/volumes.h

  ${SRC_DIR}/volumes.cpp
)

include_directories(../../include)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ../../lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ../../lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../../)

# build commands
add_library(math STATIC ${LIB_SOURCES})

if(UNIX OR MINGW)
  set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -DDEBUG")

  if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
    set_target_properties(math PROPERTIES COMPILE_FLAGS
      "-fPIC -ansi -pedantic -Wall -Wno-long-long")
  else( CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64" )
    set_target_properties(math PROPERTIES COMPILE_FLAGS
      "-ansi -pedantic -Wall -Wno-long-long")
  endif( CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64" )
endif(UNIX OR MINGW)

