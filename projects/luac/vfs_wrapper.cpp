/*
 *  Copyright (C) 2007 by Martin Moracek
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>

#include "vfs/export.h"

VFS_FILE * vfs_stdin;
VFS_FILE * vfs_stdout;
VFS_FILE * vfs_stderr;

int vfs_fclose(VFS_FILE * stream)
{
  return fclose(static_cast<FILE *>(stream));
}

VFS_FILE * vfs_fopen(const char * filename, const char * mode)
{
  return fopen(filename, mode);
}

VFS_FILE * vfs_freopen(char const * path, char const * mode, VFS_FILE * stream)
{
  return freopen(path, mode, static_cast<FILE*>(stream));
}


int vfs_fprintf(VFS_FILE * stream, const char * format, ...)
{
  va_list l_va;
  int res;

  va_start(l_va, format);

  res = vfprintf(static_cast<FILE*>(stream), format, l_va);

  va_end(l_va);

  return res;
}

int vfs_fgetc(VFS_FILE * stream)
{
  return fgetc(static_cast<FILE*>(stream));
}

int vfs_ungetc(int c, VFS_FILE * stream)
{
  return ungetc(c, static_cast<FILE*>(stream));
}

vfs_size_t vfs_fread
(void * ptr, vfs_size_t size, vfs_size_t nmemb, VFS_FILE * stream)
{
  return fread(ptr, size, nmemb, static_cast<FILE*>(stream));
}

int vfs_feof(VFS_FILE * stream)
{
  return feof(static_cast<FILE*>(stream));
}

int vfs_ferror(VFS_FILE * stream)
{
  return ferror(static_cast<FILE*>(stream));
}
