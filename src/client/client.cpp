//
//  Copyright (C) 2008 by Martin Moracek
//
//  This pRight_ogram is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your opTop_ion) any later version.
//
//  This pRight_ogram is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the impLeft_ied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this pRight_ogram; if not, write to the Free Software
//  Foundation, Inc., 59 TempLeft_e Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file client.cpp
 *
 *  blah.
 */

// #ifdef PLATFORM_GCC
// #  include <sched.h>
// #  include <unistd.h>
// #endif 

#include <functional>

#include "vfs/logfile.h"

#include "core/vmachine.h"
#include "core/script_core.h"
#include "renderer/renderer.h"
#include "renderer/canvas.h"
#include "input/input.h"

#include "script_client.h"

#include "client.h"

namespace tre {

  GameClient::GameClient()
  {
    scriptVM_->InitModule(CoreInitializer());
    scriptVM_->InitModule(ClientInitializer());

    scriptVM_->RunFile("script/init.lua");

    luabind::object obj(scriptVM_->WrapObject(this));

    running = scriptVM_->RunGlobalFunction("AppInit", &obj);

    timer.Start();
  }

  GameClient::~GameClient()
  {
    scriptVM_->RunGlobalFunction("AppEnd");
  }

  void GameClient::Run(void)
  {
    if(!desktop_) {
      vLog << err("App") << "No gui loaded." << std::endl;
      return;
    }

    // the main "application" loop
    while(running) {
      sRenderer().NewFrame();
      desktop_->Draw();
      sRenderer().FlushFrame();

      timer.UpdateTime();

      ProcessInput();

      scriptVM_->RunGlobalFunction("AppMain");

// #ifdef PLATFORM_GCC
//       sched_yield();
// #endif 

      sRenderer().PresentFrame();
    }

    // dump rendering stats
    vLog << info("Stats")
         << "Batches: " << sRenderer().frame.batches << std::endl
         << "Triangles: " << sRenderer().frame.triangles << std::endl
         << "Time: " << sRenderer().frame.time << std::endl
         << "Frames: " << sRenderer().frame.count << std::endl
         << "Average FPS: " << sRenderer().frame.count / 
            (timer.GetTime() / 1000.0f) << std::endl;
  }

  void GameClient::ProcessInput(void)
  {
    sInput().PumpEvents();

    Vector2f delta(sInput().GetPositionChange());

    // since may events are handled in lua, we must check for possible errors
    try {
      if(delta.x != 0.0f || delta.y != 0.0f)
        desktop_->MouseMove(sInput().GetPosition(), delta);

      InputEvent event;

      while(event = sInput().GetNextEvent(), event.type != etNone) {
        desktop_->ProcessInput(event);
      }
    } catch(luabind::error & e) {
      luabind::object error_msg(luabind::from_stack(e.state(), -1));
      vLog << err("Script") << error_msg
          << "\nin VirtualMachine::ProcessInput()." << std::endl;
    }
  }

  bool GameClient::SetDesktop
  (const std::string & filename, const std::string & skin)
  {
    GuiPtr newgui = Gui::Factory().CreateInstance(*this,
      Canvas::Factory().CreateInstance(), filename, skin);

    if(newgui) {
      desktop_ = newgui;
      return true;
    }
    return false;
  }

}
