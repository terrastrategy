//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file conf_client.cpp
 *
 *  blahblah
 */

#include "exception.h"

#include "vfs/logfile.h"

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "core/client/display.h"
#include "renderer/renderer.h"

namespace tre {

  void InitClientConf(TiXmlElement & root)
  {
    // get the best video mode to serve as fallback
    DisplayInfo info = sDisplay().GetDisplayInfo();

    TiXmlElement * section = root.FirstChildElement("display");
    if(section) {
      TiXmlElement * elem = section->FirstChildElement("width");
      if(elem)
        info.width = CheckIntText(*elem);

      if((elem = section->FirstChildElement("height")))
        info.height = CheckIntText(*elem);

      if((elem = section->FirstChildElement("bpp")))
        info.bpp = CheckIntText(*elem);

      info.stereo = CheckIntAttribute(*section, "stereo", false) != 0;
      info.fullscreen = CheckIntAttribute(*section, "fullscreen", false) != 0;
    } else {
      vLog << warn("Init") << "Missing display configuration, guessing."
           << std::endl;
    }
    sDisplay().SetDisplayMode(info);
    sRenderer().Init();
  }

}
