//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file client/main.cpp
 *
 *  This file contains the main() function for the terra client.
 *
 *  main() simply initializes few core subsystems, parses command line and
 *  executes the application instance.
 */

#include <fstream>

#include "exception.h"

#include "vfs/logfile.h"

#include "client.h"

#include "xml/tinyxml.h"

const char * configPath = "config/client.xml";

namespace tre {
  void InitCoreConf(TiXmlElement & el);
  void InitClientConf(TiXmlElement & el);
}

void ParseArguments(int argc, char * argv[])
{
  for(int i = 1; i < argc; ++i) {
    if(argv[i][0] == '-') {
      switch(argv[i][1]) {
        case 'c':  // config path
          if(i >= argc - 2) {
            tre::vLog << tre::err("Init")
                      << "-c option requires one parameter." << std::endl;
            exit(EXIT_FAILURE);
          }
          configPath = argv[i + 1];
          i += 2;
          break;
        default:
          tre::vLog << tre::warn("Init") << "Unknown command line argument '"
                    << argv[i] << "'." << std::endl;
          return;
      }
    }
  }
}

void LoadConfiguration(const char * path)
{
  try {
    // open file
    std::ifstream file(path);
    if(!file.is_open())
      throw(Exception("Cannot open config file"));

    TiXmlDocument doc;

    if(!(file >> doc) || doc.Error())
      throw(Exception(doc.ErrorDesc()));

    TiXmlElement * section = doc.RootElement();
    DEBUG_ASSERT(section);
    if(section->ValueTStr() != "config")
      throw(Exception("Wrong root element"));

    tre::InitCoreConf(*section);
    tre::InitClientConf(*section);

    tre::vLog << tre::ok("Init")
              << "System initialized successfuly." << std::endl;
  } catch(Exception & exc) {
    tre::vLog << tre::err("Init") << "Unable to load configuration (" << path
              << ")\nReason: " << exc.what() << "." << std::endl;
    exit(EXIT_FAILURE);
  }
}

int main(int argc, char * argv[])
{
  try {
#ifdef DEBUG
    tre::vLog.AddOutput(tre::lfDefault, tre::ltCerr);
#endif /* DEBUG */

    // check program arguments
    ParseArguments(argc, argv);

    // load configuration
    LoadConfiguration(configPath);

    // create and run the game instance
    tre::GameClient client;

    client.Run();

  } catch(std::exception & exc) {
    tre::vLog << tre::err() << "Caught fatal exception: "
              << exc.what() << "!" << std::endl
              << tre::err() << "Program termination imminent!" << std::endl;
    return 1;
  } catch(...) {
    tre::vLog << tre::err() << "Caught unhandled exception!" << std::endl
              << tre::err() << "Program termination imminent!" << std::endl;
    return 1;
  }

  tre::vLog << tre::ok() << "Application finished successfully." << std::endl;

  return 0;
}
