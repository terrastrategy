//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file script_client.cpp
 *
 *  blahblah
 */

#include "lua/lua.h"

#include "luabind/luabind.hpp"

#include "exception.h"

#include "vfs/logfile.h"

#include "core/client/display.h"
#include "renderer/renderer.h"

#include "gui/gui.h"
#include "gui/widget.h"
#include "gui/basic.h"

#include "client.h"
#include "script_client.h"
#include "core/script_core.h"

namespace tre {

  namespace {

    void GlueDisplayListModes(lua_State* L, const Display & dsp)
    {
      using namespace luabind;

      const DInfoVector & lst =  dsp.ListModes();

      object res = newtable(L);
      for(uint i = 0; i < lst.size(); ++i) {
        res[i + 1] = lst[i];
      }
      res.push(L);
    }

  }

  void ClientInitializer::operator () (lua_State * L) const
  {
    using namespace luabind;

    module(L)
    [
      // display interface
      class_<DisplayInfo>("DisplayInfo")
        .def_readwrite("width", &DisplayInfo::width)
        .def_readwrite("height", &DisplayInfo::height)
        .def_readwrite("bpp", &DisplayInfo::bpp)
        .def_readwrite("fullscreen", &DisplayInfo::fullscreen)
        .def_readwrite("stereo", &DisplayInfo::stereo),

      class_<Display>("Display")
        .def("GetDriverName", &Display::GetDriverName)
        .def("GetDisplayInfo", &Display::GetDisplayInfo)
        .def("ListModes", &GlueDisplayListModes, raw(_1)),

      def("Display", &sDisplay),

      // application

      class_<GameClient, Application>("GameClient")
        .def("GetDesktop", &GameClient::GetDesktop)
        .def("SetDesktop", &GameClient::SetDesktop)
        ,

      // gui
      class_<Gui>("Gui")
        .def("GetSize", &Gui::GetSize)
//         .def("Resize", &Gui::Resize)
        .def("GetRootWidget", (Widget*(Gui::*)(void))&Gui::GetRootWidget)
        .def("GetRootWidget", (const Widget*(Gui::*)(void) const
          )&Gui::GetRootWidget)
        .def("FindWidget", (Widget*(Gui::*)(const std::string & name)
          )&Gui::FindWidget)
        .def("FindWidget", (const Widget*(Gui::*)(const std::string & name
          ) const)&Gui::FindWidget)
        .def("RegisterWidget", &Gui::RegisterWidget)
        .def("UnregisterWidget", &Gui::RegisterWidget)
        .def("ShowCursor", &Gui::ShowCursor)
        .def("SetCursor", &Gui::SetCursor),

      class_<Widget>("Widget")
        .def_readonly("parent", &Widget::parent)
        .def_readonly("children", &Widget::children)
        .property("name", &Widget::GetName, &Widget::SetName)
        .def_readwrite("visible", &Widget::visible)
        .def("GetPostion", &Widget::GetPosition)
        .def("GetAbsPostion", &Widget::GetAbsPosition)
        .def("SetPosition", &Widget::SetPosition)
        .def("GetSize", &Widget::GetSize)
        .def("SetSize", &Widget::SetSize)
        .def("Enable", &Widget::Enable)
        .def("SetUserState", &Widget::SetUserState)
,

      class_<PushButton, Widget>("PushButton")
        .property("caption", &PushButton::GetCaption, &PushButton::SetCaption)
    ];
  }

}
