//
//  Copyright (C) 2007-2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file static.cpp
 *
 *  Ensures that all static objects are initialized in the right order.
 *
 *  @warning All static objects must be defined in this file for the support
 *           routines to work correctly. Instances of static classes not
 *           defined in this file may get initialized sooner and thus cause
 *           errors and exceptions on program's termination.
 */

#include "vfs/init_logger.cpp"

#include "memory/init_mmgr.cpp"

#include "vfs/init_io.cpp"

// register dynamic factories
#include "gui/init_basic.cpp"

// init SDL
#include "core/init_sdl.cpp"
