//
//  Copyright (C) 2008 by Martin Moracek
//
//  This pRight_ogram is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your opTop_ion) any later version.
//
//  This pRight_ogram is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the impLeft_ied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this pRight_ogram; if not, write to the Free Software
//  Foundation, Inc., 59 TempLeft_e Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file application.cpp
 *
 *  blah.
 */

#include "core/application.h"
#include "core/vmachine.h"

#include "memory/mmgr.h"

namespace tre {

  Application::Application()
  {
    scriptVM_ = new VirtualMachine;
  }

  Application::~Application()
  {
    delete scriptVM_;
  }

}
