//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file display.cpp
 *
 *  blahblah
 */

#include <SDL/SDL.h>

#include "assertion.h"
#include "exception.h"

#include "vfs/logfile.h"

#include "core/client/display.h"

namespace tre {

  Display::Display() : fullscreen_(false), stereo_(false)
  {
    vLog << info("SDL") << "Initializing display..." << std::endl;

    // init SDL video
    if(SDL_InitSubSystem(SDL_INIT_VIDEO) != 0) {
      vLog << err("SDL") << "Unable to initialize SDL.\nReason: "
           << SDL_GetError() << std::endl;
      throw(Exception("Error intializing display"));
    }

    vLog << ok("SDL") << "Success. Using driver: "
         << GetDriverName() << std::endl;
  }

  void Display::Init(void)
  {
    SetDisplayMode(GetDisplayInfo());
  }

  void Display::SetDisplayMode(const DisplayInfo & nfo)
  {
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

    // let's try to create a big depth buffer possible
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    // disable the default SDL cursor
    SDL_ShowCursor(0);

    Uint32 flags = SDL_OPENGL | SDL_HWSURFACE;
    if(nfo.fullscreen)
      flags |= SDL_FULLSCREEN;
    if(nfo.stereo)
      flags |= SDL_GL_STEREO;

    stereo_ = nfo.stereo;
    fullscreen_ = nfo.fullscreen;

    vLog << info("SDL") << "New video mode: " << nfo.width << "x"
         << nfo.height << "x" << nfo.bpp;
    if(fullscreen_)
      vLog << " [fullscreen]";
    if(stereo_)
      vLog << " [stereo]";
    vLog << std::endl;

    if(!SDL_SetVideoMode(nfo.width, nfo.height, nfo.bpp, flags)) {
      vLog << err("SDL") << "Unable to set requested video mode.\nReason: "
           << SDL_GetError() << std::endl;
      throw(Exception("Error intializing display"));
    }
  }

  const std::string Display::GetDriverName(void) const
  {
    char tmpstr[40];

    DEBUG_ASSERT(SDL_VideoDriverName(tmpstr, 40));

    return std::string(tmpstr);
  }

  const DisplayInfo Display::GetDisplayInfo(void) const
  {
    const SDL_VideoInfo * info = SDL_GetVideoInfo();

    DisplayInfo res;
    res.width = info->current_w;
    res.height = info->current_h;
    res.bpp = info->vfmt->BitsPerPixel;
    res.fullscreen = fullscreen_;
    res.stereo = stereo_;
    return res;
  }

  const DInfoVector Display::ListModes(void) const
  {
    SDL_Rect ** modes =  SDL_ListModes(NULL, SDL_FULLSCREEN | SDL_HWSURFACE);

    if(!modes)
      return DInfoVector();

    DisplayInfo info;
    DInfoVector res;

    uint bits = GetDisplayInfo().bpp;

    for(uint i = 0; modes[i]; ++i) {
      info.width = modes[i]->w;
      info.height = modes[i]->h;
      info.bpp = bits;
      info.fullscreen = true;
      info.stereo = false;
      res.push_back(info);
    }
    return res;
  }

}
