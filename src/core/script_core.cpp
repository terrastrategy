//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file script_core.cpp
 *
 *  blahblah
 */

#include "lua/lua.h"

#include "luabind/luabind.hpp"
#include "luabind/operator.hpp"
#include "luabind/return_reference_to_policy.hpp"

#include "exception.h"

#include "core/application.h"
#include "core/timer.h"
#include "vfs/interface.h"
#include "vfs/logfile.h"

#include "core/script_core.h"

namespace tre {

  void CoreInitializer::operator () (lua_State * L) const
  {
    using namespace luabind;

    // register GameClient class
    module(L)
    [
      class_<Timer>("Timer")
        .def("ScheduleAction", &Timer::ScheduleAction)
        .def("RemoveAction", &Timer::RemoveAction)
        .def("GetSystemTime", &Timer::GetSystemTime),

      class_<Application>("Application")
        .def("Quit", &Application::Quit)
        .def_readonly("timer", &Application::timer),

      // math classes
      class_<Vector2<float> >("vec2")
        .def(constructor<float, float>())
        .def(self + other<Vector2<float> >())
        .def(self - other<Vector2<float> >())
        .def(self * float())
        .def(self / float())
        .def(self == other<Vector2<float> >())
        .def("add", &Vector2<float>::operator+=, return_reference_to(_1))
        .def("sub", &Vector2<float>::operator-=, return_reference_to(_1))
        .def("mul", &Vector2<float>::operator*=, return_reference_to(_1))
        .def("div", &Vector2<float>::operator/=, return_reference_to(_1))
        .def("len", &Vector2<float>::Length)
        .def("normalize", &Vector2<float>::Normalize)
        .def_readwrite("x", &Vector2<float>::x)
        .def_readwrite("y", &Vector2<float>::y),

      class_<Vector3<float> >("vec3")
        .def(constructor<float, float, float>())
        .def(self + other<Vector3<float> >())
        .def(self - other<Vector3<float> >())
        .def(self * float())
        .def(self / float())
        .def(self == other<Vector3<float> >())
        .def("add", &Vector3<float>::operator+=, return_reference_to(_1))
        .def("sub", &Vector3<float>::operator-=, return_reference_to(_1))
        .def("mul", &Vector3<float>::operator*=, return_reference_to(_1))
        .def("div", &Vector3<float>::operator/=, return_reference_to(_1))
        .def("len", &Vector3<float>::Length)
        .def("normalize", &Vector3<float>::Normalize)
        .def_readwrite("x", &Vector3<float>::x)
        .def_readwrite("y", &Vector3<float>::y)
        .def_readwrite("z", &Vector3<float>::z)
    ];
  }

}
