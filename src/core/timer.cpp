//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file timer.cpp
 *
 *  Ble.
 */

#include <SDL/SDL.h>

#include <algorithm>

#include "vfs/logfile.h"
#include "core/timer.h"

#include "memory/mmgr.h"

namespace tre {

  uint32 Timer::GetSystemTime(void)
  {
    return SDL_GetTicks();
  }

  Timer::Timer() : time_(0), running_(false)
  {
  }

  void Timer::UpdateTime(void)
  {
    if(!running_)
      return;

    uint tmp_time = GetSystemTime();
    uint time_step = tmp_time - lastTime_;
    lastTime_ = tmp_time;

    if(mod > 0.0f)
      time_step = static_cast<float>(time_step) * mod;

    time_ += time_step;

    while(!actions_.empty() && actions_.front().first < time_) {
      DEBUG_ASSERT(actions_.front().second != 0);
      DEBUG_ASSERT(*actions_.front().second != 0);
      (*actions_.front().second)();
      actions_.pop_front();
    }
  }

  void Timer::Start(void)
  {
    lastTime_ = GetSystemTime();
    running_ = true;
  }

  void Timer::Stop(void)
  {
    running_ = false;
  }

  const Timer::TimerAction * Timer::ScheduleAction
  (uint ms, const TimerAction & act)
  {
    if(!act) {
      vLog << warn("Timer")
           << "Attempting to register invalid callback!" << std::endl;
      return NULL;
    }
    ActionQueue::iterator pos = std::lower_bound(
      actions_.begin(), actions_.end(), ms);

    TimerAction * res = new TimerAction(act);

    actions_.insert(pos, std::make_pair(time_ + ms, res));

    return res;
  }

  bool Timer::RemoveAction(const TimerAction * act)
  {
    for(ActionQueue::iterator pos = actions_.begin();
        pos != actions_.end(); ++pos) {
      if(pos->second == act) {
        actions_.erase(pos);
        return true;
      }
    }
    return false;
  }

}
