//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file vmachine.cpp
 *
 *  blahblahblah
 *
 */

#include "exception.h"

#include "vfs/logfile.h"

#include "core/application.h"
#include "core/vmachine.h"

#include "lua/lua.h"
#include "lua/lualib.h"
#include "lua/lauxlib.h"

// #include "luabind/luabind.hpp"

namespace tre {

 /*
  *  Virtual machine helper lua functions
  */

  static lua_State * vm_GetThread (lua_State *L, int *arg);
  static int vm_Traceback(lua_State * L);

  // must be exported because of ScriptAction
  int vm_ErrorHandler(lua_State * L);

  static lua_State * vm_GetThread (lua_State *L, int *arg)
  {
    if (lua_isthread(L, 1)) {
      *arg = 1;
      return lua_tothread(L, 1);
    }
    else {
      *arg = 0;
      return L;
    }
  }

  #define LEVELS1	12	/* size of the first part of the stack */
  #define LEVELS2	10	/* size of the second part of the stack */

  static int vm_Traceback(lua_State * L)
  {
    int level;
    int firstpart = 1;  /* still before eventual `...' */
    int arg;
    lua_State *L1 = vm_GetThread(L, &arg);
    lua_Debug ar;
    if (lua_isnumber(L, arg + 2)) {
      level = (int)lua_tointeger(L, arg + 2);
      lua_pop(L, 1);
    }
    else
      level = (L == L1) ? 1 : 0;  /* level 0 may be this own function */
    if (lua_gettop(L) == arg)
      lua_pushliteral(L, "");
    else if (!lua_isstring(L, arg + 1)) return 1; /* message is not a string */
    else lua_pushliteral(L, "\n");
    lua_pushliteral(L, "stack traceback:");
    while (lua_getstack(L1, level++, &ar)) {
      if (level > LEVELS1 && firstpart) {
        /* no more than `LEVELS2' more levels? */
        if (!lua_getstack(L1, level + LEVELS2, &ar))
          level--;  /* keep going */
        else {
          lua_pushliteral(L, "\n\t...");  /* too many levels */
          while (lua_getstack(L1, level + LEVELS2, &ar)) /* find last levels */
            level++;
        }
        firstpart = 0;
        continue;
      }
      lua_pushliteral(L, "\n\t");
      lua_getinfo(L1, "Snl", &ar);
      lua_pushfstring(L, "%s:", ar.short_src);
      if (ar.currentline > 0)
        lua_pushfstring(L, "%d:", ar.currentline);
      if (*ar.namewhat != '\0')  /* is there a name? */
          lua_pushfstring(L, " in function " LUA_QS, ar.name);
      else {
        if (*ar.what == 'm')  /* main? */
          lua_pushfstring(L, " in main chunk");
        else if (*ar.what == 'C' || *ar.what == 't')
          lua_pushliteral(L, " ?");  /* C function or tail call */
        else
          lua_pushfstring(L, " in function <%s:%d>",
                          ar.short_src, ar.linedefined);
      }
      lua_concat(L, lua_gettop(L) - arg);
    }
    lua_concat(L, lua_gettop(L) - arg);
    return 1;
  }

  #undef LEVELS1
  #undef LEVELS2

  int vm_ErrorHandler(lua_State * L)
  {
    std::string message;

    if(lua_isstring(L, -1)) {
      message = luaL_checkstring(L, -1);
    }

    lua_pop(L, 1);

    vm_Traceback(L);

    if(lua_isstring(L, -1)) {
      message.push_back('\n');
      message += luaL_checkstring(L, -1);
    }

    lua_pushstring(L, message.c_str());
    return 1;
  }

 /*
  *  VirtualMachine implementation
  */

  VirtualMachine::VirtualMachine(bool libs/*=true*/)
  {
    luaState_ = lua_open();

    if (luaState_ == NULL)
      throw(Exception("Unable to initialize Lua"));

    // initialize lua standard library functions
    if(libs)
      luaL_openlibs(luaState_);

    // prepare state for registering new modules with luabind
//     luabind::open(luaState_);

    // default error handler for Cpp<->Lua communication
//     luabind::set_pcall_callback(vm_ErrorHandler);
  }

  VirtualMachine::~VirtualMachine()
  {
    lua_close(luaState_);
  }

  bool VirtualMachine::RunFile(const std::string & filename)
  {
    int res = luaL_dofile(luaState_, filename.c_str());
    if(res != 0) {
      if(lua_isstring(luaState_, -1)) {
        vLog << tre::err("Script")
             << luaL_checkstring(luaState_, -1) << std::endl;
      }
      return false;
    }
    // clear the stack
    lua_pop(luaState_, lua_gettop(luaState_));
    return true;
  }

  bool VirtualMachine::RunString(const std::string & str)
  {
    int res = luaL_dostring(luaState_, str.c_str());
    if(res != 0) {
      if(lua_isstring(luaState_, -1)) {
        vLog << tre::err("Script")
             << luaL_checkstring(luaState_, -1) << std::endl;
      }
      return false;
    }
    // clear the stack
    lua_pop(luaState_, lua_gettop(luaState_));
    return true;
  }

  bool VirtualMachine::RunGlobalFunction(const std::string & str, luabind::object * arg/*=NULL*/)
  {
    try {
      if(arg)
        luabind::call_function<void>(luaState_, str.c_str(), *arg);
      else
        luabind::call_function<void>(luaState_, str.c_str());
    } catch(luabind::error & e) {
      luabind::object error_msg(luabind::from_stack(e.state(), -1));
      vLog << err("Script") << error_msg
           << "\nin VirtualMachine::RunGlobalFunction("
           << str << ")." << std::endl;
      return false;
    }
    return true;
  }

  const luabind::object VirtualMachine::CompileScriptChunk
  (const std::string & chunk, const std::string & name)
  {
    luaL_loadbuffer(luaState_, chunk.c_str(), chunk.size(), name.c_str());
    if(lua_isstring(luaState_, -1)) {
      vLog << err("Script") << "Error compiling chunk '" << name << "'.\n"
           << "Reason: " << luaL_checkstring(luaState_, -1) << std::endl;
      lua_pop(luaState_, 1);
      return luabind::object();
    }

    luabind::object res(luabind::from_stack(luaState_, -1));
    lua_pop(luaState_, 1);

    return res;
  }
}
