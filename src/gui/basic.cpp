//
//  Copyright (C) 2008 by Martin Moracek
//
//  This pRight_ogram is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your opTop_ion) any later version.
//
//  This pRight_ogram is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the impLeft_ied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this pRight_ogram; if not, write to the Free Software
//  Foundation, Inc., 59 TempLeft_e Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file basic.cpp
 *
 *  Provides collection of basic widgets.
 */

#include <algorithm>
#include <functional>

#include "utf8.h"
#include "exception.h"

#include "vfs/logfile.h"

#include "input/input.h"
#include "renderer/renderer.h"

#include "core/application.h"
#include "core/vmachine.h"

#include "gui/font/font.h"

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "gui/gui.h"
#include "gui/basic.h"

#include "skin.h"

#include "memory/mmgr.h"

namespace tre {

  /*
   * Utility functions
   */
  namespace {

    void LoadTextFromXml(PlainText & text,
      TiXmlElement & root, const GuiSkinPtr & skin)
    {
      DEBUG_ASSERT(skin);
      const GuiSkin::TextDefaults & defs = skin->GetTextDefaults();

      // load effect (with fallback)
      TiXmlElement * elem = root.FirstChildElement("effect");
      if(elem)
        text.SetEffect(CheckText(*elem));
      else if(!defs.effect.empty())
        text.SetEffect(defs.effect);
      else
        throw(Exception("Missing effect information"));

      if((elem = root.FirstChildElement("font")))
        text.SetFont(CheckText(*elem));
      else if(!defs.font.empty())
        text.SetFont(defs.font);
      else
        throw(Exception("Missing font information"));

      TextStyle style = defs.plainStyle;

      if((elem = root.FirstChildElement("style"))) {
        TiXmlElement * subel = elem->FirstChildElement("size");
        if(subel)
          style.size = CheckIntText(*subel);

        if((subel = elem->FirstChildElement("fontStyle"))) {
          std::string tmpstr(CheckText(*subel));
          if(tmpstr == "regular") {
            style.fstyle = fsRegular;
          } else if(tmpstr == "bold") {
            style.fstyle = fsBold;
          } else if(tmpstr == "italic") {
            style.fstyle = fsItalic;
          } else if(tmpstr == "boldItalic") {
            style.fstyle = fsBoldItalic;
          } else {
            throw(Exception("Unknown font style"));
          }
        }
        if((subel = elem->FirstChildElement("colour"))) {
          style.colour.Set(CheckFloatAttribute(*subel, "r"),
            CheckFloatAttribute(*subel, "g"),
            CheckFloatAttribute(*subel, "b"),
            CheckFloatAttribute(*subel, "a", false, 1.0f));
        }
      }
      text.SetStyle(style);

      if((elem = root.FirstChildElement("text"))) {
        if(elem->Attribute("size")) {
          style.size = CheckIntAttribute(*elem, "size",
            false, Font::defTextSize);
        }
        text.SetStyle(style);

        bool aa = CheckIntAttribute(*elem, "smooth", false, 1) != 0;
        text.SetAntialias(aa);

        std::string tmp_text(CheckText(*elem, false));
        std::wstring tmp_wtext;

        tmp_wtext.reserve(tmp_text.size());
        utf8::utf8to16(tmp_text.begin(), tmp_text.end(),
          std::back_inserter(tmp_wtext));

        text.SetText(tmp_wtext);
      }
    }

    void LoadRichTextFromXml(RichText & text,
      TiXmlElement & root, const GuiSkinPtr & skin)
    {
      DEBUG_ASSERT(skin);
      const GuiSkin::TextDefaults & defs = skin->GetTextDefaults();

      // load effect (with fallback)
      TiXmlElement * elem = root.FirstChildElement("effect");
      if(elem)
        text.SetEffect(CheckText(*elem));
      else if(!defs.effect.empty())
        text.SetEffect(defs.effect);
      else
        throw(Exception("Missing effect information"));

      if((elem = root.FirstChildElement("font")))
        text.SetFont(CheckText(*elem));
      else if(!defs.font.empty())
        text.SetFont(defs.font);
      else
        throw(Exception("Missing font information"));

      TextStyle styles[ssLast];
      StyleSheet sheet;
      std::string tmpstr;

      for(int i = 0; i < ssLast; ++i) {
        styles[i] = defs.richStyles[i];
      }

      // parse all style elements
      elem = root.FirstChildElement("style");
      while(elem) {
        // determine style type (name)
        tmpstr = CheckAttribute(*elem, "type");
        if(tmpstr == "base") {
          sheet = ssBase;
        } else if(tmpstr == "link") {
          sheet = ssLink;
        } else if(tmpstr == "linkHover") {
          sheet = ssLinkHover;
        } else if(tmpstr == "linkPressed") {
          sheet = ssLinkPressed;
        } else if(tmpstr == "emph") {
          sheet = ssEmph;
        } else {
          throw(Exception("Unknown style sheet"));
        }

        TiXmlElement * subel = elem->FirstChildElement("size");
        if(subel) {
          styles[sheet].size = CheckIntText(*subel);
        }

        if((subel = elem->FirstChildElement("fontStyle"))) {
          tmpstr = CheckText(*subel);
          if(tmpstr == "regular") {
            styles[sheet].fstyle = fsRegular;
          } else if(tmpstr == "bold") {
            styles[sheet].fstyle = fsBold;
          } else if(tmpstr == "italic") {
            styles[sheet].fstyle = fsItalic;
          } else if(tmpstr == "boldItalic") {
            styles[sheet].fstyle = fsBoldItalic;
          } else {
            throw(Exception("Unknown font style"));
          }
        }

        if((subel = elem->FirstChildElement("colour"))) {
          styles[sheet].colour.Set(CheckFloatAttribute(*subel, "r"),
            CheckFloatAttribute(*subel, "g"),
            CheckFloatAttribute(*subel, "b"),
            CheckFloatAttribute(*subel, "a", false, 1.0f));
        }
        elem = elem->NextSiblingElement("style");
      }

      for(int i = 0; i < ssLast; ++i) {
        text.SetStyleSheet(static_cast<StyleSheet>(i), styles[i]);
      }

      if((elem = root.FirstChildElement("text"))) {
        if(elem->Attribute("size")) {
          styles[ssBase].size = CheckIntAttribute(*elem,
            "size", false, Font::defTextSize);
          text.SetStyleSheet(ssBase, styles[ssBase]);
        }

        if(elem->Attribute("halign")) {
          tmpstr = CheckAttribute(*elem, "halign");
          if(tmpstr == "left") {
            text.SetHAlign(haLeft);
          } else if(tmpstr == "center") {
            text.SetHAlign(haCenter);
          } else if(tmpstr == "right") {
            text.SetHAlign(haRight);
          } else if(tmpstr == "stretch") {
            text.SetHAlign(haStretch);
          } else {
            throw(Exception("Unknown horizontal alignment"));
          }
        }

        if(elem->Attribute("valign")) {
          tmpstr = CheckAttribute(*elem, "valign");
          if(tmpstr == "top") {
            text.SetVAlign(vaTop);
          } else if(tmpstr == "middle") {
            text.SetVAlign(vaMiddle);
          } else if(tmpstr == "bottom") {
            text.SetVAlign(vaBottom);
          } else {
            throw(Exception("Unknown vertical alignment"));
          }
        }

        bool aa = CheckIntAttribute(*elem, "smooth", false, 1) != 0;
        text.SetAntialias(aa);

        bool wr = CheckIntAttribute(*elem, "wrap", false, 1) != 0;
        text.SetWrap(wr);

        text.SetText(CheckText(*elem, false));
      }
    }

  }

  AbstractStatic::AbstractStatic(Gui & g, Widget * p)
  {
    Init(g, p);
  }

 /*
  * Abstract button
  */
  AbstractButton::AbstractButton(Gui & g, Widget * p)
  {
    Init(g, p);
  }

  class AbstractButton::OnButtonPressed {
    public:
      OnButtonPressed(AbstractButton & p) : par_(p) {}

      void operator () (uint id);
    private:
      AbstractButton & par_;
  };

  class AbstractButton::OnButtonReleased {
    public:
      OnButtonReleased(AbstractButton & p) : par_(p) {}

      void operator () (uint id);
    private:
      AbstractButton & par_;
  };

  void AbstractButton::OnMouseEnter()
  {
    DEBUG_ASSERT(gui_);

    // connect onButton slot
    buttonCon1 = gui_->buttonOn.connect(OnButtonPressed(*this));

    if(wstate_ != wsPressed)
      Widget::OnMouseEnter();
  }

  void AbstractButton::OnMouseExit()
  {
    // connect onButton slot
    buttonCon1.disconnect();

    if(wstate_ != wsPressed)
      Widget::OnMouseExit();
  }

  void AbstractButton::OnButtonPressed::operator () (uint id)
  {
    DEBUG_ASSERT(par_.gui_);

    // left mouse button
    if(id == 0) {
      par_.SetState(par_.ustate_, wsPressed);
      par_.buttonCon2 = par_.gui_->buttonOff.connect(OnButtonReleased(par_));
    }
    if(par_.parent)
      par_.parent->RaiseChild(&par_);
  }

  void AbstractButton::OnButtonReleased::operator () (uint id)
  {
    DEBUG_ASSERT(par_.gui_);

    // left mouse button
    if(id == 0) {
      Vector3f pos = par_.gui_->GetMousePosition();
      if(par_.IsInRegion(Vector2f(pos.x, pos.y))) {
        par_.SetState(par_.ustate_, wsHover);
        par_.clicked();
      } else {
        par_.SetState(par_.ustate_, wsNormal);
      }

      par_.buttonCon2.disconnect();
    }
  }

 /*
  * Abstract edit box
  */
  AbstractEdit::AbstractEdit(Gui & g, Widget * p)
  {
    Init(g, p);
  }

  class AbstractEdit::OnButtonPressed {
    public:
      OnButtonPressed(AbstractEdit & p) : par_(p) {}

      void operator () (uint id);
    private:
      AbstractEdit & par_;
  };

  class AbstractEdit::OnKeyPressed {
    public:
      OnKeyPressed(AbstractEdit & p) : par_(p) {}

      bool operator () (char chr, KeySym sym);
    private:
      AbstractEdit & par_;
  };

  void AbstractEdit::OnMouseEnter()
  {
    DEBUG_ASSERT(gui_);

    // connect onButton slot
    buttonCon = gui_->buttonOn.connect(OnButtonPressed(*this));

    Widget::OnMouseEnter();
  }

  void AbstractEdit::OnButtonPressed::operator () (uint id)
  {
    DEBUG_ASSERT(par_.gui_);

    // if clicked outside, lose focus
    Vector3f pos = par_.gui_->GetMousePosition();
    if(!par_.IsInRegion(Vector2f(pos.x, pos.y))) {
      par_.SetUserState(0);
      par_.keyCon.disconnect();
      par_.buttonCon.disconnect();
      return;
    }
    // left mouse button
    if(id == 0) {
      // set focus
      par_.SetUserState(1);
      par_.keyCon = par_.gui_->keyOn.connect(OnKeyPressed(par_));
    }
    if(par_.parent)
      par_.parent->RaiseChild(&par_);
  }

  bool AbstractEdit::OnKeyPressed::operator () (char chr, KeySym sym)
  {
    if(chr == 0)
      return false;

    par_.InsertCharacter(chr);
    return true;
  }

 /*
  * Static text widget
  */
  Widget * StaticText::CreateWidget(Gui & gui, Widget * parent)
  {
    return new StaticText(gui, parent);
  }

  StaticText::StaticText(Gui & g, Widget * p)
  {
    Init(g, p);

    text.SetTransform(&trans_.GetWTransform());
    text.SetScaleFactor(g.GetScaleFactor());
  }

  void StaticText::OnGuiResize(const Vector2f & scale)
  {
    text.SetScaleFactor(scale);
  }

  void StaticText::Draw(void)
  {
    GeometryBatch & batch = text.GetGeometry();

    if(!batch.empty()) {
      sRenderer().DrawGeometry(batch.begin(), batch.end());
    }
  }

  void StaticText::LoadFromXml(TiXmlElement & root)
  {
    DEBUG_ASSERT(gui_);

    Widget::LoadFromXml(root);

    text.SetRect(size_.x, size_.y);

    LoadTextFromXml(text, root, gui_->GetSkin());
  }

  Widget * StaticRichText::CreateWidget(Gui & gui, Widget * parent)
  {
    return new StaticRichText(gui, parent);
  }

  StaticRichText::StaticRichText(Gui & g, Widget * p)
  {
    Init(g, p);

    text.SetTransform(&trans_.GetWTransform());
    text.SetScaleFactor(g.GetScaleFactor());
  }

  void StaticRichText::OnGuiResize(const Vector2f & scale)
  {
    text.SetScaleFactor(scale);
  }

  void StaticRichText::Draw(void)
  {
    GeometryBatch & batch = text.GetGeometry();

    if(!batch.empty()) {
      sRenderer().DrawGeometry(batch.begin(), batch.end());
    }
  }

  void StaticRichText::LoadFromXml(TiXmlElement & root)
  {
    DEBUG_ASSERT(gui_);

    Widget::LoadFromXml(root);

    text.SetRect(size_.x, size_.y);

    LoadRichTextFromXml(text, root, gui_->GetSkin());
  }

 /*
  * Basic pixmap widget
  */
  PixmapWidget::~PixmapWidget()
  {
    delete pixmap_;
  }

  void PixmapWidget::OnGuiResize(const Vector2f & scale)
  {
    pixmap_->SetScaleFactor(scale);
    pixmap_->ResizePixmap(size_);
  }

  void PixmapWidget::SetSize(const Vector2f & sz)
  {
    DEBUG_ASSERT(pixmap_);

    if(sz != size_) {
      Widget::SetSize(sz);
      pixmap_->ResizePixmap(sz);
    }
  }

  void PixmapWidget::Draw(void)
  {
    DEBUG_ASSERT(pixmap_);

    pixmap_->DrawPixmap();

    // draw children
    DrawChildren();
  }

  void PixmapWidget::SetState(uint u, WidgetState w)
  {
    // no change
    if(u == ustate_ && w == wstate_)
      return;

    ustate_ = u;
    wstate_ = w;

    pixmap_->SetMap(ustate_, wstate_);
  }

 /*
  * Basic widgets
  */

 /*
  * Frame and it's variants
  */
  Widget * Frame::CreateWidget(Gui & gui, Widget * parent)
  {
    return new Frame(gui, parent);
  }

  Frame::Frame(Gui & g, Widget * p) : AbstractStatic(g, p),
    PixmapWidget(new Pixmap3x3(trans_, g.GetScaleFactor().y))
  {
  }

  void Frame::LoadFromXml(TiXmlElement & root)
  {
    DEBUG_ASSERT(gui_);

    Widget::LoadFromXml(root);

    pixmap_->LoadFromXmlTemplate(root, gui_->GetSkin(), "frame-plain", size_);
    pixmap_->SetMap(ustate_, wstate_);

    LoadChildrenFromXml(root);
  }

  Widget * FrameRaised::CreateWidget(Gui & gui, Widget * parent)
  {
    return new FrameRaised(gui, parent);
  }

  void FrameRaised::LoadFromXml(TiXmlElement & root)
  {
    DEBUG_ASSERT(gui_);

    Widget::LoadFromXml(root);

    pixmap_->LoadFromXmlTemplate(root, gui_->GetSkin(), "frame-raised", size_);
    pixmap_->SetMap(ustate_, wstate_);

    LoadChildrenFromXml(root);
  }

  Widget * FrameSunken::CreateWidget(Gui & gui, Widget * parent)
  {
    return new FrameSunken(gui, parent);
  }

  void FrameSunken::LoadFromXml(TiXmlElement & root)
  {
    DEBUG_ASSERT(gui_);

    Widget::LoadFromXml(root);

    pixmap_->LoadFromXmlTemplate(root, gui_->GetSkin(), "frame-sunken", size_);
    pixmap_->SetMap(ustate_, wstate_);

    LoadChildrenFromXml(root);
  }

 /*
  * Push button
  */
  Widget * PushButton::CreateWidget(Gui & gui, Widget * parent)
  {
    return new PushButton(gui, parent);
  }

  PushButton::PushButton(Gui & g, Widget * p) : AbstractButton(g, p),
    PixmapWidget(new Pixmap3x1(trans_, g.GetScaleFactor().y))
  {
    caption_.SetTransform(&trans_.GetWTransform());
    caption_.SetScaleFactor(g.GetScaleFactor());
  }

  class PushButton::EventBinder {
    public:
      EventBinder(PushButton & p, const luabind::object & o)
        : par_(&p), func_(o) {}

      void operator () (void) {func_(par_);}
    private:
      PushButton * par_;
      luabind::object func_;
  };

  void PushButton::OnGuiResize(const Vector2f & scale)
  {
    PixmapWidget::OnGuiResize(scale);

    caption_.SetScaleFactor(scale);
  }

  void PushButton::SetSize(const Vector2f & sz)
  {
    PixmapWidget::SetSize(Vector2f(sz.x, size_.y));
  }

  void PushButton::Draw(void)
  {
    PixmapWidget::Draw();

    GeometryBatch & batch = caption_.GetGeometry();

    if(!batch.empty())
      sRenderer().DrawGeometry(batch.begin(), batch.end());
  }

  void PushButton::LoadFromXml(TiXmlElement & root)
  {
    DEBUG_ASSERT(gui_);

    Widget::LoadFromXml(root);

    pixmap_->LoadFromXmlTemplate(root, gui_->GetSkin(), "push_button", size_);
    pixmap_->SetMap(ustate_, wstate_);

    size_.y = static_cast<Pixmap3x1*>(pixmap_)->GetHeight();

    caption_.SetRect(size_.x, size_.y);
    caption_.SetHAlign(haCenter);
    caption_.SetVAlign(vaMiddle);

    TiXmlElement * elem = root.FirstChildElement("caption");
    if(elem)
      LoadTextFromXml(caption_, *elem, gui_->GetSkin());

    // compile and register events
    elem = root.FirstChildElement("event");
    while(elem) {
      if(CheckAttribute(*elem, "id") == "clicked") {
        luabind::object obj(gui_->GetApplication().GetScriptVM(
          )->CompileScriptChunk(CheckText(*elem), "PushButton::clicked()"));

        if(obj.is_valid()) {
          clicked.connect(EventBinder(*this, obj));
        }
      }
      elem = elem->NextSiblingElement("event");
    }
  }

 /*
  * Edit box
  */
//   EditBox::EditBox(Gui & g, Widget * p) : Pixmap3x1(g, p)
//   {
//   }
//
//   EditBox::EditBox(Gui & g, Widget * p, Mesh & pm) : Pixmap3x1(g, p, pm)
//   {
//   }
//
//   void EditBox::LoadFromXml(TiXmlElement & root)
//   {
//     LoadFromXmlTemplate(root, "edit box");
//   }

 /*
  * Check box
  */
//   CheckBox::CheckBox(Gui & g, Widget * p) : Pixmap1x1(g, p)
//   {
//   }
//
//   CheckBox::CheckBox(Gui & g, Widget * p, Mesh & pm) : Pixmap1x1(g, p, pm)
//   {
//   }
//
//   void CheckBox::LoadFromXml(TiXmlElement & root)
//   {
//     LoadFromXmlTemplate(root, "check box");
//   }

 /*
  * Radio button
  */

//   RadioButton::RadioButton(Gui & g, Widget * p) : Pixmap1x1(g, p)
//   {
//   }
//
//   RadioButton::RadioButton(Gui & g, Widget * p, Mesh & pm)
//     : Pixmap1x1(g, p, pm)
//   {
//   }
//
//   void RadioButton::LoadFromXml(TiXmlElement & root)
//   {
//     LoadFromXmlTemplate(root, "radio button");
//   }

 /*
  * Drop-down list
  */
//   DropDownList::DropDownList(Gui & g, Widget * p) : Pixmap3x1(g, p)
//   {
//   }
//
//   DropDownList::DropDownList(Gui & g, Widget * p, Mesh & pm)
//     : Pixmap3x1(g, p, pm)
//   {
//   }
//
//   void DropDownList::LoadFromXml(TiXmlElement & root)
//   {
//     LoadFromXmlTemplate(root, "drop down");
//   }

}
