//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file cursor.cpp
 *
 *  blahblah
 */

#include <string>

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "exception.h"
#include "cursor.h"

namespace tre {

 /*
  *  Cursor class implementation
  */
  void Cursor::Rescale(const Vector2f & scale, Float2Vector & verts)
  {
    DEBUG_ASSERT(indices);

    verts[indices->vFrom].Set(0.0f, baseSize_.y * scale.y);
    verts[indices->vFrom + 1].Set(baseSize_.x * scale.x, baseSize_.y * scale.y);
    verts[indices->vFrom + 2].Set(baseSize_.x * scale.x, 0.0f);
    verts[indices->vFrom + 3].Set(0.0f, 0.0f);
  }

  void Cursor::LoadFromXml(TiXmlElement & root, const Vector2f & scale,
    Float2Vector & verts, Float2Vector & coords)
  {
    uint offset = verts.size();

    TiXmlElement * elem = root.FirstChildElement("effect");
    if(!elem)
      throw(Exception("Missing effect information"));

    effect = Effect::Factory().CreateInstance(CheckText(*elem));

    elem = root.FirstChildElement("texture");
    if(!elem)
      throw(Exception("Missing texture information"));

    vars.SetTexture("gui_Texture", CheckText(*elem));

    elem = root.FirstChildElement("rect");
    if(!elem)
      throw(Exception("Missing rectangle information"));

    float tex_scale = CheckFloatAttribute(*elem, "scale", false, 1.0f);

    float left = CheckFloatAttribute(*elem, "left");
    float right = CheckFloatAttribute(*elem, "right");
    float bottom = CheckFloatAttribute(*elem, "bottom");
    float top = CheckFloatAttribute(*elem, "top");

    baseSize_.x = right - left;
    baseSize_.y = top - bottom;

    baseOffset_ = 0.0f;
    elem = root.FirstChildElement("offset");
    if(elem) {
      baseOffset_.x = CheckFloatAttribute(*elem, "x", false, 0.0f);
      baseOffset_.y = CheckFloatAttribute(*elem, "y", false, 0.0f);
    }

    left /= tex_scale;
    right /= tex_scale;
    top /= tex_scale;
    bottom /= tex_scale;

    coords.push_back(Vector2f(left, top));
    coords.push_back(Vector2f(left, bottom));
    coords.push_back(Vector2f(right, top));
    coords.push_back(Vector2f(right, bottom));

    elem = root.FirstChildElement("scale");
    if(elem) {
      baseSize_.x *= CheckFloatAttribute(*elem, "x", false, 1.0f);
      baseOffset_.x *= CheckFloatAttribute(*elem, "x", false, 1.0f);
      baseSize_.y *= CheckFloatAttribute(*elem, "y", false, 1.0f);
      baseOffset_.y *= CheckFloatAttribute(*elem, "y", false, 1.0f);
    }

    indices = IndexBufPtr(IndexBuffer::Factory().CreateInstance());
    indices->vFrom = verts.size();

    verts.push_back(Vector2f(baseOffset_.x * scale.x,
      baseOffset_.y * scale.y));
    verts.push_back(Vector2f(baseOffset_.x * scale.x,
      (baseSize_.y + baseOffset_.y) * scale.y));
    verts.push_back(Vector2f((baseSize_.x + baseOffset_.x) * scale.x,
      baseOffset_.y * scale.y));
    verts.push_back(Vector2f((baseSize_.x + baseOffset_.x) * scale.x,
      (baseSize_.y + baseOffset_.y) * scale.y));

    indices->vTo = verts.size();

    uint inds[] = {offset + 1, offset + 3, offset + 2,
                   offset + 1, offset + 2, offset};

    indices->Init(6);
    uint * ptr = indices->Lock();
    DEBUG_ASSERT(ptr);
    memcpy(ptr, inds, sizeof(uint) * 6);
    indices->Unlock();
    indices->vFrom = 0;
    indices->vTo = 6;
  }

 /*
  *  CursorSetFactory class implementation
  */
  const CursorSetPtr CursorSetFactory::CreateInstance
  (const std::string & filename, const Vector2f & scale)
  {
    CursorMap::iterator pos = cursorMap_.find(filename);

    if(pos != cursorMap_.end() && !pos->second.expired()) {
      return pos->second.lock();
    }

    CursorSetPtr newset;

    try {
      FileIStream file(filename.c_str());
      if(!file.is_open())
        throw(Exception("Cannot open GUI cursor file"));

      TiXmlDocument doc;

      if(!(file >> doc) || doc.Error())
        throw(Exception(doc.ErrorDesc()));

      TiXmlElement * section = doc.RootElement();
      DEBUG_ASSERT(section);
      if(section->ValueTStr() != "cursorSet")
        throw(Exception("Wrong root element"));

      newset = CursorSetPtr(new CursorSet);
      newset->LoadFromXml(*section, scale);
    } catch(Exception & exc) {
      vLog << err("Gui") << "Error loading cursor set (" << filename
           << ")\nReason: " << exc.what() << "." << std::endl;
      return CursorSetPtr();
    }

    cursorMap_[filename] = newset;
    return newset;
  }

  const CursorSetPtr CursorSetFactory::CreateInstance
  (TiXmlElement & root, const Vector2f & scale)
  {
    CursorSetPtr newset;

    newset = CursorSetPtr(new CursorSet);
    newset->LoadFromXml(root, scale);

    return newset;
  }

 /*
  *  CursorSet class implementation
  */
  const Cursor * CursorSet::GetCursor(const std::string & name) const
  {
    CursorVector::const_iterator pos = std::lower_bound(
      cursors_.begin(), cursors_.end(), name);

    if(pos == cursors_.end() || pos->first != name)
      return NULL;

    return &pos->second;
  }

  void CursorSet::OnGuiResize(const Vector2f & scale)
  {
    Float2Vector verts;
    verts.resize(bufSize_);

    for(CursorVector::iterator itr = cursors_.begin();
        itr != cursors_.end(); ++itr) {
      itr->second.Rescale(scale, verts);
    }

    if(!verts.empty())
      attribs_->Set(atPosition, verts);
  }

  void CursorSet::LoadFromXml(TiXmlElement & root, const Vector2f & scale)
  {
    attribs_ = AttribBufPtr(AttribBufferSet::Factory().CreateInstance());
    Float2Vector verts, coords;

    TiXmlElement * elem = root.FirstChildElement("cursor");
    while(elem) {
      cursors_.push_back(std::make_pair(CheckAttribute(*elem, "id"), Cursor()));
      cursors_.back().second.attribs = attribs_;
      cursors_.back().second.LoadFromXml(*elem, scale, verts, coords);

      elem = elem->NextSiblingElement("cursor");
    }
    std::sort(cursors_.begin(), cursors_.end());

    bufSize_ = verts.size();

    // if there's at least one cursor, create buffers
    if(bufSize_) {
      attribs_->Set(atPosition, verts);
      attribs_->Set(atTexCoord0, coords);
    }
  }

}
