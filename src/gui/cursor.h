//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file cursor.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <vector>
#include <boost/smart_ptr.hpp>

#include "platform.h"

#ifdef PLATFORM_MSVC
#  include <unordered_map>
#else /* PLATFORM_MSVC */
#  include <tr1/unordered_map>
#endif /* PLATFORM_MSVC */

#include "cpair.h"

#include "renderer/buffers.h"
#include "renderer/effect.h"

#include "math/vector.h"

class TiXmlElement;

namespace tre {

  class Cursor {
    public:
      AttribBufPtr attribs;
      IndexBufPtr indices;
      EffectPtr effect;
      EffectVars vars;

    public:
      void Rescale(const Vector2f & scale, Float2Vector & verts);

      void LoadFromXml(TiXmlElement & root, const Vector2f & scale,
        Float2Vector & verts, Float2Vector & coords);

    private:
      Vector2f baseSize_;
      Vector2f baseOffset_;
  };

  class CursorSet;

  typedef boost::shared_ptr<CursorSet> CursorSetPtr;
  typedef boost::weak_ptr<CursorSet> CursorSetRef;

  class CursorSetFactory {
    public:
      const CursorSetPtr CreateInstance(const std::string & filename,
        const Vector2f & scale);
      const CursorSetPtr CreateInstance(TiXmlElement & root,
        const Vector2f & scale);

    private:
      typedef std::tr1::unordered_map<std::string, CursorSetRef> CursorMap;

    private:
      CursorMap cursorMap_;
  };

  class CursorSet {
    public:
      static CursorSetFactory & Factory(void)
      {
        static CursorSetFactory fac;
        return fac;
      }

    public:
      typedef cpair<std::string, Cursor> CursorPair;
      typedef std::vector<CursorPair> CursorVector;

    public:
      const Cursor * GetCursor(const std::string & name) const;

      void OnGuiResize(const Vector2f & scale);

      void LoadFromXml(TiXmlElement & root, const Vector2f & scale);

    private:
      CursorVector cursors_;

      AttribBufPtr attribs_;
      ushort bufSize_;
  };

}
