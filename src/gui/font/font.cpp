//
//  Copyright (C) 2007-2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file font.cpp
 *
 *  Ble.
 */

#include <string.h>

#include "types.h"
#include "exception.h"

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "gui/font/font.h"
#include "freetype.h"

namespace tre {

  const FontPtr FontFactory::CreateInstance(const std::string & name)
  {
    FontMap::iterator pos = map_.find(name);

    if(pos != map_.end() && !pos->second.expired())
      return pos->second.lock();

    if(pos == map_.end())
      pos = map_.insert(pos, std::make_pair(name, FontPtr()));

    DEBUG_ASSERT(pos != map_.end());

    FontPtr res;
    try {
      // need to pass the name stored in the element
      res = FontPtr(new Font());
      res->LoadFromXml(name);

      vLog << ok("Font") << "Font loaded successfully ("
           << name << ")." << std::endl;
    } catch(Exception & exc) {
      vLog << err("Font") << "Unable to load font (" + name + ").\nReason: "
           << exc.what() << std::endl;
    }
    pos->second = res;
    return res;
  }

 /*
  *  Font class implementation
  */
  const GlyphInfo * Font::GetFillerGlyph(void)
  {
    return sFontLib().GetFillerGlyph();
  }

  Font::Font() : aalias_(false), size_(defTextSize)
  {
    memset(faces_, 0, sizeof(FontFace*) * fsLast);
  }

  Font::~Font()
  {
    for(uint i = 0; i < fsLast; ++i) {
      if(faces_[i]) {
        sFontLib().ClearFace(faces_[i]);
      }
    }
  }

  const GlyphInfo * Font::GetGlyph(wchar_t chr, FontStyle style/*=fsRegular*/)
  {
    DEBUG_ASSERT(style < fsLast);

    if(!faces_[style])
      faces_[style] = sFontLib().LoadFace(files_[style]);

    if(!faces_[style])
      return NULL;

    return faces_[style]->GetGlyph(chr, size_, aalias_);
  }

  int Font::GetKerning
  (wchar_t chr1, wchar_t chr2, FontStyle style/*=fsRegular*/)
  {
    DEBUG_ASSERT(style < fsLast);

    if(!faces_[style])
      faces_[style] = sFontLib().LoadFace(files_[style]);

    if(!faces_[style])
      return NULL;

    return faces_[style]->GetKerning(chr1, chr2, size_);
  }

  const FaceInfo Font::GetFaceInfo(FontStyle style/*=fsRegular*/)
  {
    DEBUG_ASSERT(style < fsLast);

    if(!faces_[style])
      faces_[style] = sFontLib().LoadFace(files_[style]);

    if(!faces_[style])
      return FaceInfo();

    return faces_[style]->GetFaceInfo(size_);
  }

  void Font::LoadFromXml(const std::string & path)
  {
    FileIStream file(path.c_str());
    if(!file.is_open())
      throw(Exception("Cannot open font file"));

    TiXmlDocument doc;

    if(!(file >> doc) || doc.Error())
      throw(Exception(doc.ErrorDesc()));

    TiXmlElement * root = doc.RootElement();
    DEBUG_ASSERT(root);
    if(root->ValueTStr() != "font")
      throw(Exception("Wrong root element"));

    TiXmlElement * elem = root->FirstChildElement("regular");
    if(elem)
      files_[fsRegular] = CheckText(*elem);
    else
      throw(Exception("Regular style is not specified"));

    if((elem = root->FirstChildElement("bold")))
      files_[fsBold] = CheckText(*elem);
    else
      files_[fsBold] = files_[fsRegular];

    if((elem = root->FirstChildElement("italic")))
      files_[fsItalic] = CheckText(*elem);
    else
      files_[fsItalic] = files_[fsRegular];

    if((elem = root->FirstChildElement("boldItalic")))
      files_[fsBoldItalic] = CheckText(*elem);
    else
      files_[fsBoldItalic] = files_[fsRegular];
  }

}
