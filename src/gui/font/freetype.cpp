//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file freetype.cpp
 *
 *  blahblah
 */

#include <sstream>
#include <algorithm>

#include "config.h"
#include "exception.h"

#include "vfs/logfile.h"
#include "vfs/fstream.h"

#include "freetype.h"

namespace tre {

 /*
  *  FontFace class
  */
  FontFace::FontFace(const std::string & path) : face_(NULL), buffer_(NULL)
  {
    uint buf_siz;

    // load from file
    FileIStream file(path);

    if(!file.is_open())
      throw(Exception("Unable to open the font file!"));

    try {
      buf_siz = file.length();
      buffer_ = new byte[buf_siz];

      if(!file.read(reinterpret_cast<char*>(buffer_), buf_siz))
        throw(Exception("Unable to read the font file!"));

      if(FT_New_Memory_Face(sFontLib().lib_, buffer_, buf_siz, 0, &face_) != 0)
        throw(Exception("Error initializing font face!"));

      if((face_->face_flags & FT_FACE_FLAG_SCALABLE) == 0)
        throw(Exception("Font is not scalable!"));

      // set default size
      SetSize(Font::defTextSize);
    } catch(Exception &) {
      delete [] buffer_;
      throw;
    }
  }

  FontFace::~FontFace()
  {
    FT_Done_Face(face_);
    delete [] buffer_;

    // clear glyphs
    for(GlyphRefVector::iterator pos = glyphs_.begin();
        pos != glyphs_.end(); ++pos) {
      sFontLib().RemoveGlyph(*pos);
    }
  }

  const GlyphInfo * FontFace::GetGlyph(wchar_t chr, uint size, bool aalias)
  {
    DEBUG_ASSERT(face_);

    if(size != size_)
      SetSize(size);

    uint index = FT_Get_Char_Index(face_, chr);

    FontLib::GlyphKey key(this, chr, size, aalias);

    FontLib::GlyphMap::iterator pos = sFontLib().glyphs_.find(key);

    if(pos != sFontLib().glyphs_.end())
      return &pos->second;

#ifdef FONT_FORCE_AUTOHINT
    uint loadMode = FT_LOAD_FORCE_AUTOHINT;
#else /* FONT_FORCE_AUTOHINT */
    uint loadMode = FT_LOAD_DEFAULT;
#endif /* FONT_FORCE_AUTOHINT */

    if(FT_Load_Glyph(face_, index, loadMode) != 0) {
      vLog << err("Font") << "Unable to load glyph!" << std::endl;
      return NULL;
    }

    FT_GlyphSlot slot = face_->glyph;

    Vector2u origin;
    Vector2u dims((slot->metrics.width >> 6) + 2 * glyphPadding,
      (slot->metrics.height >> 6) + 2 * glyphPadding);
    GlyphInfo glyph;

    glyph = sFontLib().RequestTextureSpace(dims, origin);

    glyph.code = chr;
    glyph.bearing_x = (slot->metrics.horiBearingX >> 6) - glyphPadding;
    glyph.bearing_y = (slot->metrics.horiBearingY >> 6) + glyphPadding;
    glyph.advance_x = slot->metrics.horiAdvance >> 6;

    pos = sFontLib().glyphs_.lower_bound(key);
    pos = sFontLib().glyphs_.insert(pos, std::make_pair(key, glyph));

    ByteVector data;
    data.resize(dims.x * dims.y * 4);

    byte c;

    if(aalias) {
      if(FT_Render_Glyph(slot, FT_RENDER_MODE_NORMAL) != 0) {
        vLog << err("Font") << "Unable to render glyph!" << std::endl;
        return NULL;
      }

      for(int i = 0, k = glyphPadding; i < slot->bitmap.rows; ++i, ++k) {
        for(int j = 0, l = glyphPadding; j < slot->bitmap.width; ++j, ++l) {
          c = slot->bitmap.buffer[i*slot->bitmap.pitch + j];
          data[4 * (dims.y - k - 1) * dims.x + 4 * l] = c;
          data[4 * (dims.y - k - 1) * dims.x + 4 * l + 1] = c;
          data[4 * (dims.y - k - 1) * dims.x + 4 * l + 2] = c;
          data[4 * (dims.y - k - 1) * dims.x + 4 * l + 3] = c;
        }
      }
    } else {
      if(FT_Render_Glyph(slot, FT_RENDER_MODE_MONO) != 0) {
        vLog << err("Font") << "Unable to render glyph!" << std::endl;
        return NULL;
      }

      for(int i = 0, k = glyphPadding; i < slot->bitmap.rows; ++i, ++k) {
        for(int j = 0, l = glyphPadding; j < slot->bitmap.width; ++j, ++l) {
          c = slot->bitmap.buffer[i * slot->bitmap.pitch + (j >> 3)]
              & (0x80 >> (j & 7)) ? 255 : 0;
          data[4 * (dims.y - k - 1) * dims.x + 4 * l] = c;
          data[4 * (dims.y - k - 1) * dims.x + 4 * l + 1] = c;
          data[4 * (dims.y - k - 1) * dims.x + 4 * l + 2] = c;
          data[4 * (dims.y - k - 1) * dims.x + 4 * l + 3] = c;
        }
      }
    }

    DEBUG_ASSERT(glyph.texture);
    glyph.texture->UpdateRect(origin, dims, data);

    return &pos->second;
  }

  int FontFace::GetKerning(wchar_t chr1, wchar_t chr2, uint size)
  {
    DEBUG_ASSERT(face_);

    if(size != size_)
      SetSize(size);

    uint index1 = FT_Get_Char_Index(face_, chr1);
    uint index2 = FT_Get_Char_Index(face_, chr2);

    FT_Vector delta;
    FT_Get_Kerning(face_, index1, index2, FT_KERNING_DEFAULT, &delta);
    return delta.x >> 6;
  }

  const FaceInfo FontFace::GetFaceInfo(uint size)
  {
    DEBUG_ASSERT(face_);

    if(size != size_)
      SetSize(size);

    FaceInfo res;
    res.height = face_->size->metrics.height >> 6;
    res.ascender = face_->size->metrics.ascender >> 6;
    res.descender = face_->size->metrics.descender >> 6;
    return res;
  }

  void FontFace::SetSize(uint sz)
  {
    size_ = sz;
    if(FT_Set_Pixel_Sizes(face_, 0, size_) != 0) {
      vLog << warn("Font") << "Unable to set font size correctly." << std::endl;
    }
  }

 /*
  *  GlyphKey
  */
  FontLib::GlyphKey::GlyphKey(FontFace * f, uint idx, uint sz, bool aa)
    : face(f)
  {
    num = idx;
    num = num << 32;
    num += sz << 1;
    if(aa)
      num += 1;
  }

 /*
  *  FontLib class
  */
  FontLib::FontLib() : lib_(NULL)
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Initializing font lib." << std::endl;
#endif /* LOG_SINGLETONS */

    if(FT_Init_FreeType(&lib_) != 0) {
      vLog << err("Font") << "Unable to initialize freetype library!"
           << std::endl;
    }

    // init filler glyph
    Vector2u origin;
    filler_ = RequestTextureSpace(Vector2u(1, 1), origin);
    filler_.code = 0;
    filler_.bearing_x = 0;
    filler_.bearing_y = 0;
    filler_.advance_x = 0;

    ByteVector data(4, 255);
    filler_.texture->UpdateRect(origin, Vector2u(1,1), data);
  }

  FontLib::~FontLib()
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Releasing font lib." << std::endl;
#endif /* LOG_SINGLETONS */

    // release faces
    for(FaceVector::iterator pos = faces_.begin(); pos != faces_.end(); ++pos) {
      delete pos->second;
    }
    faces_.clear();

    FT_Done_FreeType(lib_);
  }

  void FontLib::ClearFace(FontFace * face)
  {
    DEBUG_ASSERT(face);

#ifdef LOG_RESOURCES
    vLog << info("Res") << "Releasing font face: '";
#endif /* LOG_RESOURCES */

    for(FaceVector::iterator pos = faces_.begin(); pos != faces_.end(); ++pos) {
      if(pos->second == face) {
#ifdef LOG_RESOURCES
        vLog << pos->first << "'." << std::endl;
#endif /* LOG_RESOURCES */
        faces_.erase(pos);
        delete face;
        return;
      }
    }
  }

  FontFace * FontLib::LoadFace(const std::string & filename)
  {
    FaceVector::iterator pos = std::lower_bound(
      faces_.begin(), faces_.end(), filename);

    if(pos != faces_.end() && pos->first == filename)
      return pos->second;

#ifdef LOG_RESOURCES
    vLog << info("Res") << "Loading font face: "
         << filename << "." << std::endl;
#endif /* LOG_RESOURCES */

    FontFace * res = NULL;

    try {
      res = new FontFace(filename);

      faces_.insert(pos, std::make_pair(filename, res));
    } catch(Exception & exc) {
      vLog << err("Font") << "Error loading font face ("
           << filename << ")\nReason: " << exc.what() << std::endl;
    }
    return res;
  }

  const GlyphInfo * FontLib::GetFillerGlyph(void)
  {
    return &filler_;
  }

  void FontLib::RemoveGlyph(GlyphMap::iterator & itr)
  {
    // TODO: HAHAHAHAHA, we are not removing any glyphs at this time :D
  }

  const GlyphInfo FontLib::RequestTextureSpace(Vector2u dims, Vector2u & origin)
  {
    GlyphInfo res;
    uint y_off;

    res.width = dims.x;
    res.height = dims.y;

    // try to find a free space in one of the textures
    for(TexVector::iterator itr = textures_.begin();
        itr != textures_.end(); ++itr) {
      y_off = 0;
      for(TexLineVector::iterator itr2 = itr->lines.begin();
          itr2 != itr->lines.end(); y_off += (itr2++)->first) {
        // does glyph fit in the line?
        if(itr2->first < dims.y)
          continue;

        for(std::vector<Vector2u >::iterator itr3 = itr2->second.begin();
            itr3 != itr2->second.end();) {
          // is this interval wide enough?
          if(itr3->y < dims.x) {
            ++itr3;
            continue;
          }

          // reduce the interval and fill the origin var
          origin.x = itr3->x;
          origin.y = y_off;
          itr3->x += dims.x;
          itr3->y -= dims.x;
          if(itr3->y == 0)
            itr3 = itr2->second.erase(itr3);

          // fill texture related part of the GlyphInfo structure
          res.texture = itr->tex;
          res.texCoords.Set(origin.x, origin.y + dims.y,
            origin.x + dims.x, origin.y);
          res.texCoords /= static_cast<float>(fontTextureSize);
          return res;
        }
      }
      // try to add a new line
      if(y_off + dims.y <= fontTextureSize) {
        itr->lines.push_back(std::make_pair(dims.y,
          std::vector<Vector2u >(1, Vector2u(dims.x,
          fontTextureSize - dims.x))));

        origin.x = 0;
        origin.y = y_off;
        res.texture = itr->tex;
        res.texCoords.Set(origin.x, origin.y + dims.y,
          origin.x + dims.x, origin.y);
        res.texCoords /= static_cast<float>(fontTextureSize);
        return res;
      }
    }

    // otherwise create new texture with single level in it
    TexturePtr ptr(Texture::Factory().CreateInstance(ttTexture2D,
      fontTextureSize, fontTextureSize, tfR8G8B8A8));

    TextureInfo newtex;
    newtex.tex = ptr;
    newtex.lines.push_back(std::make_pair(dims.y,
      std::vector<Vector2u >(1, Vector2u(dims.x,
      fontTextureSize - dims.x))));
    textures_.push_back(newtex);

    origin.x = 0;
    origin.y = 0;
    res.texture = ptr;
    res.texCoords.Set(origin.x, origin.y + dims.y, origin.x + dims.x, origin.y);
    res.texCoords /= static_cast<float>(fontTextureSize);
    return res;
  }

}
