//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file freetype.h
 *
 *  Ble.
 */

#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#include <string>
#include <vector>
#include <map>
#include <boost/utility.hpp>

#include "stdtypes.h"
#include "cpair.h"

#include "gui/font/font.h"
#include "renderer/texture.h"

namespace tre {

  const uint fontTextureSize = 512;
  const uint glyphPadding = 1;

  struct GlyphInfo;
  class FontFace;

  class FontLib {
    public:
      FontLib();
      ~FontLib();

      void ClearFace(FontFace * face);
      FontFace * LoadFace(const std::string & filename);

      const GlyphInfo * GetFillerGlyph(void);

      friend class FontFace;

    private:
      struct GlyphKey {
        FontFace * face;
        uint64 num;

        GlyphKey(FontFace * f, uint idx, uint sz, bool aa);

        bool operator < (const GlyphKey & r) const
        {
          return face < r.face || (face == r.face && num < r.num);
        }
      };

      typedef cpair<uint, std::vector<Vector2u> > TexLinePair;
      typedef std::vector<TexLinePair> TexLineVector;

      struct TextureInfo {
        TexturePtr tex;
        TexLineVector lines;
      };

      class FaceLess;

      typedef cpair<std::string, FontFace *> FacePair;
      typedef std::vector<FacePair> FaceVector;
      typedef std::map<GlyphKey, GlyphInfo> GlyphMap;
      typedef std::vector<TextureInfo> TexVector;

    private:
      FT_Library lib_;

      FaceVector faces_;
      TexVector textures_;

      GlyphMap glyphs_;

      GlyphInfo filler_;  /// 1px dot, used for underlining and caret

    private:
      void RemoveGlyph(GlyphMap::iterator & itr);
      const GlyphInfo RequestTextureSpace(Vector2u dims, Vector2u & origin);
  };

  class FontFace : private boost::noncopyable {
    public:
      FontFace(const std::string & path);
      ~FontFace();

      const GlyphInfo * GetGlyph(wchar_t chr, uint size, bool aalias);
      int GetKerning(wchar_t chr1, wchar_t chr2, uint size);

      const FaceInfo GetFaceInfo(uint size);

    private:
      typedef std::vector<FontLib::GlyphMap::iterator> GlyphRefVector;
      typedef std::vector<TexturePtr> TexVector;

    private:
      FT_Face face_;
      byte * buffer_;

      GlyphRefVector glyphs_;
      uint size_;

      TexVector textures_;

    private:
      void SetSize(uint sz);
  };

  inline FontLib & sFontLib(void)
  {
    static FontLib f;
    return f;
  }
}
