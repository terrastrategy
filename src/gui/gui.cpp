//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gui.cpp
 *
 *  blahblah
 */

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "exception.h"
#include "math/vector.h"

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "core/application.h"

#include "gui/gui.h"
#include "gui/widget.h"

#include "core/client/display.h"
#include "math/volumes.h"
#include "renderer/renderer.h"
#include "renderer/canvas.h"

#include "skin.h"
#include "cursor.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  Local helper functions
  */
  namespace {

    Widget * GetTopmostWidget(const Vector2f & pos, Widget & wdg)
    {
      for(Widget::WdgVector::reverse_iterator itr = wdg.children.rbegin();
          itr != wdg.children.rend(); ++itr) {
        DEBUG_ASSERT(*itr);

        if((*itr)->IsInRegion(pos))
          return GetTopmostWidget(pos, *(*itr));
      }
      return &wdg;
    }

  }

 /*
  *  GuiFactory class implementation
  */
  GuiPtr GuiFactory::CreateInstance(Application & app, const CanvasPtr & canvas,
    const std::string & filename, const std::string & skin)
  {
    Gui * res = NULL;

    try {
      if(!canvas) {
        throw(Exception("Invalid canvas"));}

      res = new Gui(app, canvas);

      if(!res->UseSkin(skin)) {
        throw(Exception("Error loading skin"));}

      res->LoadFromFile(filename);  // throws
    } catch(Exception & exc) {
      vLog << err("App") << "Error loading GUI (" << filename
           << ")\nReason: " << exc.what() << "." << std::endl;
      delete res;
      res = NULL;
    }
    return GuiPtr(res);
  }

 /*
  *  Gui class implementation
  */
  Gui::Gui(Application & app, const CanvasPtr & cnv) : app_(app),
    canvas_(cnv), rootWdg_(NULL), lastTop_(NULL), actCursor_(NULL)
  {
  }

  Gui::~Gui()
  {
    delete rootWdg_;
  }

  Vector2f Gui::GetScaleFactor(void) const
  {
    return Vector2f(size_.x / scaleBase_.x, size_.y / scaleBase_.y);
  }

  /**
   * Also resizes the GUI to fit the new canvas
   * @param cnv new canvas
   */
  void Gui::SetCanvas(const CanvasPtr & cnv)
  {
    if(!cnv) {
      vLog << warn("App") << "Attempting to assign invalid canvas to a gui. "
           << "Operation ignored." << std::endl;
      return;
    }

    canvas_ = cnv;
    size_.Set(cnv->GetWidth(), cnv->GetHeight());

    if(!scale_)
      scaleBase_ = size_;

    // update camera
    cam_.SetOrtho(0.0f, canvas_->GetWidth(),
      canvas_->GetHeight(), 0.0f, -1.0f, 1.0f);

    if(cursors_)
      cursors_->OnGuiResize(GetScaleFactor());

    if(rootWdg_)
      rootWdg_->OnGuiResize(GetScaleFactor());
  }

  const Widget * Gui::FindWidget(const std::string & name) const
  {
    WidgetMap::const_iterator pos = wdgRegistry_.find(name);

    if(pos != wdgRegistry_.end())
      return pos->second;
    return NULL;
  }

  Widget * Gui::FindWidget(const std::string & name)
  {
    WidgetMap::iterator pos = wdgRegistry_.find(name);

    if(pos != wdgRegistry_.end())
      return pos->second;
    return NULL;
  }

  bool Gui::RegisterWidget(Widget * wdg, const std::string & name)
  {
    DEBUG_ASSERT(wdg);

    WidgetMap::iterator pos = wdgRegistry_.find(name);

    if(pos != wdgRegistry_.end()) {
      vLog << warn("Gui") << "Unable to register widget name '" << name << "'"
           << ", name already in use." << std::endl;
      return false;
    }

    wdgRegistry_.insert(std::make_pair(name, wdg));
    return true;
  }

  void Gui::UnregisterWidget(const std::string & name)
  {
    WidgetMap::iterator pos = wdgRegistry_.find(name);

    if(pos != wdgRegistry_.end())
      wdgRegistry_.erase(pos);
  }

  bool Gui::UseSkin(const std::string & filename)
  {
    GuiSkinPtr newskin = GuiSkin::Factory().CreateInstance(filename);

    if(newskin) {
      skin_ = newskin;
      return true;
    }
    return false;
  }

  void Gui::ResetSkin(void)
  {
    skin_.reset();
  }

  void Gui::ShowCursor(bool on)
  {
    if(cursors_)
      showCursor_ = on;
  }

  void Gui::SetCursor(const std::string & name)
  {
    if(cursors_ && curName_ != name) {
      actCursor_ = cursors_->GetCursor(name);
      curName_ = name;
    }
  }

  void Gui::MouseMove(const Vector2f & abs, const Vector2f & rel)
  {
    if(rel.x == 0.0f && rel.y == 0.0f)
      return;

    mousePos_.SetOPosition(Vector3f(abs.x, abs.y, 0.0f));

    Widget * newtop = NULL;

    // mouseEnter event
    if(lastTop_ && lastTop_->IsInRegion(abs))
      newtop = GetTopmostWidget(abs, *lastTop_);
    else if(rootWdg_)
      newtop = GetTopmostWidget(abs, *rootWdg_);

    if(newtop != lastTop_) {
      if(lastTop_)
        lastTop_->OnMouseExit();
      if(newtop)
        newtop->OnMouseEnter();
      lastTop_ = newtop;
    }
    mouseMove(rel);
  }

  bool Gui::ProcessInput(const InputEvent & event)
  {
    switch(event.type) {
      case etQuit:
        // Alt+F4 in windowed mode
        app_.Quit();
        return true;
      case etButtonOn:
        buttonOn(event.button);
        break;
      case etButtonOff:
        buttonOff(event.button);
        break;
      case etKeyOn:
// safeguard (for now)
app_.Quit();
        return keyOn(event.key.chr, event.key.sym);
      case etKeyOff:
        return keyOff(event.key.chr, event.key.sym);
        break;
      default:
        return false;
    }
    return true;
  }

  void Gui::Draw(void)
  {
    if(!rootWdg_)
      return;

    sRenderer().PushCanvas(canvas_.get());

    sRenderer().SetCamera(cam_);

    rootWdg_->Draw();

    // draw cursor if required
    if(showCursor_ && actCursor_) {
      GeometryBatch batch(1, GeometryInfo(mousePos_.GetWTransform(),
        actCursor_->attribs.get(), actCursor_->indices.get(),
        actCursor_->effect.get(), actCursor_->vars));

      sRenderer().DrawGeometry(batch.begin(), batch.end());
    }

    sRenderer().PopCanvas();
  }

  void Gui::LoadFromFile(const std::string & filename)
  {
    FileIStream file(filename.c_str());
    if(!file.is_open())
      throw(Exception("Cannot open GUI file"));

    TiXmlDocument doc;

    if(!(file >> doc) || doc.Error())
      throw(Exception(doc.ErrorDesc()));

    TiXmlElement * root = doc.RootElement();
    DEBUG_ASSERT(root);
    if(root->ValueTStr() != "gui")
      throw(Exception("Wrong root element"));

    TiXmlElement * elem = root->FirstChildElement("dimensions");
    scaleBase_ = 0;
    if(elem) {
      scaleBase_.x = CheckIntAttribute(*elem, "x", false);
      scaleBase_.y = CheckIntAttribute(*elem, "y", false);

      scale_ = CheckIntAttribute(*elem, "scale", false, 1) != 0;
    }
    // auto-resize
    if(scaleBase_.x == 0)
      scaleBase_.x = canvas_->GetWidth();
    if(scaleBase_.y == 0)
      scaleBase_.y = canvas_->GetHeight();

    // scale
    size_.x = canvas_->GetWidth();
    size_.y = canvas_->GetHeight();

    // only resize if requested
    if(!scale_)
      scaleBase_ = size_;

    // update camera
    cam_.SetOrtho(0.0f, canvas_->GetWidth(),
      canvas_->GetHeight(), 0.0f, -1.0f, 1.0f);

    showCursor_ = false;
    elem = root->FirstChildElement("cursorSet");
    if(elem) {
      if(elem->Attribute("import")) {
        cursors_ = CursorSet::Factory().CreateInstance(
          CheckAttribute(*elem, "import"), GetScaleFactor());
      } else {
        // NOTE: another try-catch block?
        cursors_ = CursorSet::Factory().CreateInstance(
          *elem, GetScaleFactor());
      }
      if(cursors_) {
        actCursor_ = cursors_->GetCursor("default");
        showCursor_ = CheckIntAttribute(*elem, "show", false, 1) != 0;
      }
    }

    elem = root->FirstChildElement("root");
    if(!elem)
      throw(Exception("Missing root widget"));

    delete rootWdg_;
    rootWdg_ = NULL;

    if(elem->Attribute("import")) {
      rootWdg_ = Widget::Factory().CreateInstance(
        CheckAttribute(*elem, "import"), *this);
    } else {
      rootWdg_ = Widget::Factory().CreateInstance(*elem, *this);
    }
  }
}
