//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file pixmap.cpp
 *
 *  blahblah
 */

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "exception.h"
#include "renderer/effect.h"
#include "renderer/buffers.h"
#include "renderer/renderer.h"

#include "gui/gui.h"

#include "gui/pixmap.h"
#include "skin.h"

namespace tre {

  Pixmap::Pixmap(Transformf & t, float scale/*=1.0f*/) : attribs_(NULL),
    indices_(NULL), scaleFactor_(scale), trans_(t)
  {
    attribs_ = AttribBufferSet::Factory().CreateInstance();
    indices_ = IndexBuffer::Factory().CreateInstance();
  }

  Pixmap::~Pixmap()
  {
    delete attribs_;
    delete indices_;
  }

  void Pixmap::SetMap(uint u, WidgetState w)
  {
    // initialize default state
    if(maps_[u][w]) {
      vars_.SetTexture("gui_Texture", maps_[u][w]);
    }
  }

  void Pixmap::DrawPixmap(void)
  {
    GeometryBatch batch(1, GeometryInfo(&trans_.GetWTransform(), attribs_,
      indices_, effect_.get(), &vars_));

    sRenderer().DrawGeometry(batch.begin(), batch.end());
  }

  void Pixmap::LoadFromXmlTemplate(TiXmlElement & root,
    const GuiSkinPtr & skin, const std::string & name, const Vector2f & sz)
  {
    if(!skin)
      throw(Exception("Missing skin"));

    // at first check gui file for <template /> override
    TiXmlElement * elem = root.FirstChildElement("template");
    if(elem) {
      WidgetTemplate tmpl;
      tmpl.LoadFromXml(*elem);

      SetUpFromTemplate(tmpl, sz);
      LoadStateMaps(tmpl);
    } else {
      const WidgetTemplate * tmpl = skin->GetTemplate(name);

      if(!tmpl)
        throw(Exception("Missing template"));

      SetUpFromTemplate(*tmpl, sz);
      LoadStateMaps(*tmpl);
    }

    // load effect (with fallback)
    if((elem = root.FirstChildElement("effect"))) {
      effect_ = Effect::Factory().CreateInstance(CheckText(*elem));
    } else if(!skin->GetEffect().empty()) {
      effect_ = Effect::Factory().CreateInstance(skin->GetEffect());
    } else {
      throw(Exception("Missing effect information"));
    }
  }

  void Pixmap::LoadStateMaps(const WidgetTemplate & tmpl)
  {
    for(WidgetTemplate::TexVector::const_iterator pos = tmpl.states.begin();
        pos != tmpl.states.end(); ++pos) {
      DEBUG_ASSERT(pos->wstate < wsLast);
      if(pos->ustate > Widget::maxUState)
        throw(Exception("User state index out of range"));

      maps_[pos->ustate][pos->wstate] =
        Texture::Factory().CreateInstance(pos->tex);
    }
  }

  void Pixmap3x3::ResizePixmap(const Vector2f & sz)
  {
    DEBUG_ASSERT(attribs_);

    float * verts = static_cast<float*>(attribs_->Lock(atPosition));
    verts[0] = 0.0f;
    verts[1] = 0.0f;
    verts[2] = 0.0f;
    verts[3] = pTop_ * scaleFactor_.y;
    verts[4] = 0.0f;
    verts[5] = sz.y - pBottom_ * scaleFactor_.y;
    verts[6] = 0.0f;
    verts[7] = sz.y;
    verts[8] = pLeft_ * scaleFactor_.x;
    verts[9] = 0.0f;
    verts[10] = pLeft_ * scaleFactor_.x;
    verts[11] = pTop_ * scaleFactor_.y;
    verts[12] = pLeft_ * scaleFactor_.x;
    verts[13] = sz.y - pBottom_ * scaleFactor_.y;
    verts[14] = pLeft_ * scaleFactor_.x;
    verts[15] = sz.y;
    verts[16] = sz.x - pRight_ * scaleFactor_.x;
    verts[17] = 0.0f;
    verts[18] = sz.x - pRight_ * scaleFactor_.x;
    verts[19] = pTop_ * scaleFactor_.y;
    verts[20] = sz.x - pRight_ * scaleFactor_.x;
    verts[21] = sz.y - pBottom_ * scaleFactor_.y;
    verts[22] = sz.x - pRight_ * scaleFactor_.x;
    verts[23] = sz.y;
    verts[24] = sz.x;
    verts[25] = 0.0f;
    verts[26] = sz.x;
    verts[27] = pTop_ * scaleFactor_.y;
    verts[28] = sz.x;
    verts[29] = sz.y - pBottom_ * scaleFactor_.y;
    verts[30] = sz.x;
    verts[31] = sz.y;
    attribs_->Unlock(atPosition);
  }

  void Pixmap3x3::SetUpFromTemplate
  (const WidgetTemplate & tmpl, const Vector2f & sz)
  {
    DEBUG_ASSERT(attribs_);
    DEBUG_ASSERT(indices_);

    if(tmpl.texCoords.size() < 16)
      throw(Exception("Not enough vertices in template"));

    pLeft_ = tmpl.texCoords[4].x - tmpl.texCoords[0].x;
    pRight_ = tmpl.texCoords[12].x - tmpl.texCoords[8].x;
    pTop_ = tmpl.texCoords[1].y - tmpl.texCoords[0].y;
    pBottom_ = tmpl.texCoords[3].y - tmpl.texCoords[2].y;

    // position
    attribs_->Init(atPosition, adFloat2, 16);
    ResizePixmap(sz);

    // texture coordinates - rescale and invert T
    attribs_->Init(atTexCoord0, adFloat2, 16);
    float * tptr = static_cast<float*>(attribs_->Lock(atTexCoord0));
    for(uint i = 0; i < 16; ++i) {
      tptr[i * 2] = tmpl.texCoords[i].x / tmpl.scale;
      tptr[i * 2 + 1] = 1.0f - tmpl.texCoords[i].y / tmpl.scale;
    }
    attribs_->Unlock(atTexCoord0);

    const uint inds[] = {1, 5, 4, 1, 4, 0, 5, 9, 8, 5, 8, 4, 9, 13, 12, 9, 12,
      8, 2, 6, 5, 2, 5, 1, 6, 10, 9, 6, 9, 5, 10, 14, 13, 10, 13, 9, 3, 6, 2,
      3, 7, 6, 7, 11, 10, 7, 10, 6, 11, 15, 14, 11, 14, 10};

    indices_->vFrom = 0;
    indices_->vTo = 16;

    indices_->Init(sizeof(inds) / sizeof(uint));
    uint * iptr = indices_->Lock();
    memcpy(iptr, inds, sizeof(inds));
    indices_->Unlock();
  }

  void Pixmap3x1::ResizePixmap(const Vector2f & sz)
  {
    DEBUG_ASSERT(attribs_);

    float * verts = static_cast<float*>(attribs_->Lock(atPosition));
    verts[0] = 0.0f;
    verts[1] = 0.0f;
    verts[2] = 0.0f;
    verts[3] = pHeight_ * scaleFactor_.y;
    verts[4] = pLeft_ * scaleFactor_.x;
    verts[5] = 0.0f;
    verts[6] = pLeft_ * scaleFactor_.x;
    verts[7] = pHeight_ * scaleFactor_.y;
    verts[8] = sz.x - pRight_ * scaleFactor_.x;
    verts[9] = 0.0f;
    verts[10] = sz.x - pRight_ * scaleFactor_.x;
    verts[11] = pHeight_ * scaleFactor_.y;
    verts[12] = sz.x;
    verts[13] = 0.0f;
    verts[14] = sz.x;
    verts[15] = pHeight_ * scaleFactor_.y;
    attribs_->Unlock(atPosition);
  }

  void Pixmap3x1::SetUpFromTemplate
  (const WidgetTemplate & tmpl, const Vector2f & sz)
  {
    if(tmpl.texCoords.size() < 8)
      throw(Exception("Not enough vertices in template"));

    pLeft_ = tmpl.texCoords[2].x - tmpl.texCoords[0].x;
    pRight_ = tmpl.texCoords[6].x - tmpl.texCoords[4].x;
    pHeight_ = tmpl.texCoords[1].y - tmpl.texCoords[0].y;

    // position
    attribs_->Init(atPosition, adFloat2, 8);
    ResizePixmap(sz);

    // texture coordinates - rescale and invert T
    attribs_->Init(atTexCoord0, adFloat2, 8);
    float * tptr = static_cast<float*>(attribs_->Lock(atTexCoord0));
    for(uint i = 0; i < 8; ++i) {
      tptr[i * 2] = tmpl.texCoords[i].x / tmpl.scale;
      tptr[i * 2 + 1] = 1.0f - tmpl.texCoords[i].y / tmpl.scale;
    }
    attribs_->Unlock(atTexCoord0);

    const uint inds[] = {1, 3, 2, 1, 2, 0, 3, 5, 4, 3, 4, 2, 5, 7, 6, 5, 6, 4};

    indices_->vFrom = 0;
    indices_->vTo = 8;

    indices_->Init(sizeof(inds) / sizeof(uint));
    uint * iptr = indices_->Lock();
    memcpy(iptr, inds, sizeof(inds));
    indices_->Unlock();
  }

  void Pixmap2x1L::ResizePixmap(const Vector2f & sz)
  {
    DEBUG_ASSERT(attribs_);

    float * verts = static_cast<float*>(attribs_->Lock(atPosition));
    verts[0] = 0.0f;
    verts[1] = 0.0f;
    verts[2] = 0.0f;
    verts[3] = pHeight_ * scaleFactor_.y;
    verts[4] = pLeft_ * scaleFactor_.x;
    verts[5] = 0.0f;
    verts[6] = pLeft_ * scaleFactor_.x;
    verts[7] = pHeight_ * scaleFactor_.y;
    verts[8] = sz.x;
    verts[9] = 0.0f;
    verts[10] = sz.x;
    verts[11] = pHeight_ * scaleFactor_.y;
    attribs_->Unlock(atPosition);
  }

  void Pixmap2x1L::SetUpFromTemplate
  (const WidgetTemplate & tmpl, const Vector2f & sz)
  {
    if(tmpl.texCoords.size() < 6)
      throw(Exception("Not enough vertices in template"));

    pLeft_ = tmpl.texCoords[2].x - tmpl.texCoords[0].x;
    pHeight_ = tmpl.texCoords[1].y - tmpl.texCoords[0].y;

    // position
    attribs_->Init(atPosition, adFloat2, 6);
    ResizePixmap(sz);

    // texture coordinates - rescale and invert T
    attribs_->Init(atTexCoord0, adFloat2, 6);
    float * tptr = static_cast<float*>(attribs_->Lock(atTexCoord0));
    for(uint i = 0; i < 6; ++i) {
      tptr[i * 2] = tmpl.texCoords[i].x / tmpl.scale;
      tptr[i * 2 + 1] = 1.0f - tmpl.texCoords[i].y / tmpl.scale;
    }
    attribs_->Unlock(atTexCoord0);

    const uint inds[] = {1, 3, 2, 1, 2, 0, 3, 5, 4, 3, 4, 2};

    indices_->vFrom = 0;
    indices_->vTo = 6;

    indices_->Init(sizeof(inds) / sizeof(uint));
    uint * iptr = indices_->Lock();
    memcpy(iptr, inds, sizeof(inds));
    indices_->Unlock();
  }

  void Pixmap1x1::ResizePixmap(const Vector2f & sz)
  {
    DEBUG_ASSERT(attribs_);

    float * verts = static_cast<float*>(attribs_->Lock(atPosition));
    verts[0] = 0.0f;
    verts[1] = 0.0f;
    verts[2] = 0.0f;
    verts[3] = pHeight_ * scaleFactor_.y;
    verts[4] = pWidth_ * scaleFactor_.x;
    verts[5] = 0.0f;
    verts[6] = pWidth_ * scaleFactor_.x;
    verts[7] = pHeight_ * scaleFactor_.y;
    attribs_->Unlock(atPosition);
  }

  void Pixmap1x1::SetUpFromTemplate
  (const WidgetTemplate & tmpl, const Vector2f & sz)
  {
    if(tmpl.texCoords.size() < 4)
      throw(Exception("Not enough vertices in template"));

    pWidth_ = tmpl.texCoords[2].x - tmpl.texCoords[0].x;
    pHeight_ = tmpl.texCoords[1].y - tmpl.texCoords[0].y;

    // position
    attribs_->Init(atPosition, adFloat2, 4);
    ResizePixmap(sz);

    // texture coordinates - rescale and invert T
    attribs_->Init(atTexCoord0, adFloat2, 4);
    float * tptr = static_cast<float*>(attribs_->Lock(atTexCoord0));
    for(uint i = 0; i < 4; ++i) {
      tptr[i * 2] = tmpl.texCoords[i].x / tmpl.scale;
      tptr[i * 2 + 1] = 1.0f - tmpl.texCoords[i].y / tmpl.scale;
    }
    attribs_->Unlock(atTexCoord0);

    const uint inds[] = {1, 3, 2, 1, 2, 0};

    indices_->vFrom = 0;
    indices_->vTo = 4;

    indices_->Init(sizeof(inds) / sizeof(uint));
    uint * iptr = indices_->Lock();
    memcpy(iptr, inds, sizeof(inds));
    indices_->Unlock();
  }
}
