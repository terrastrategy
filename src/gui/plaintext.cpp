//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file plaintext.cpp
 *
 *  blahblah
 */

#include <wctype.h>
#include <algorithm>
#include <vector>

#include "gui/font/font.h"
#include "gui/plaintext.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  PlainText class
  */
  PlainText::PlainText()
    : style_(Font::defTextSize, fsRegular, Vector4f(1.0f, 1.0f, 1.0f, 1.0f))
  {
  }

  PlainText::~PlainText()
  {
  }

  void PlainText::SetStyle(const TextStyle & style)
  {
    style_ = style;

    RedrawText(true);
  }

  void PlainText::SetText(const std::string & text)
  {
    std::wstring tmp_str;

    tmp_str.resize(text.size());
    std::copy(text.begin(), text.end(), tmp_str.begin());

    SetText(tmp_str);
  }

  void PlainText::SetText(const std::wstring & text)
  {
    text_.assign(text);

    RedrawText(true);
  }

  void PlainText::ClearText(void)
  {
    SetText(L"");
  }

  void PlainText::RedrawText(bool reset)
  {
    if(!font_)
      return;

    font_->SetSize(style_.size * scaleFactor_.y);
    font_->SetAntialias(antialias_);

    if(reset) {
      glyphs_.resize(text_.size());

      std::wstring::iterator ptr = text_.begin();
      GlyphVector::iterator ptr2 = glyphs_.begin();
      while(ptr != text_.end()) {
        // not all whitespace characters have glyphs
        *ptr2 = font_->GetGlyph(iswspace(*ptr) && *ptr != '\n' ?
          ' ' : *ptr, style_.fstyle);
        ++ptr, ++ptr2;
      }
    }

    DEBUG_ASSERT(glyphs_.size() == text_.size());

    GLineVector lines;
    GlyphLine ln;
    int x_off = 0;
    uint max_width = 0;

    FaceInfo info(font_->GetFaceInfo(style_.fstyle));

    ln.offset = 0;
    ln.baseline = info.ascender;
    ln.height = info.height;

    for(uint i = 0; i < text_.size(); ++i) {
      if(text_[i] == L'\n') {
        ln.width = ln.glyphs.empty() ? 0 : ln.glyphs.back().offset
          + ln.glyphs.back().glyph->width;
        if(ln.width > max_width)
          max_width = ln.width;
        lines.push_back(ln);

        ln.glyphs.resize(0);
        ln.offset += info.height;
        x_off = 0;
        continue;
      }

      // ignore missing glyphs and errors
      if(!glyphs_[i])
        continue;

      if(!ln.glyphs.empty())
        x_off += font_->GetKerning(text_[i - 1], text_[i], style_.fstyle);

      ln.glyphs.push_back(GlyphPrep(glyphs_[i], &style_, x_off));
      x_off += glyphs_[i]->advance_x;
    }
    // finish the current line
    if(!ln.glyphs.empty()) {
      ln.width = ln.glyphs.empty() ? 0 : ln.glyphs.back().offset
        + ln.glyphs.back().glyph->width;
      if(ln.width > max_width)
        max_width = ln.width;
      lines.push_back(ln);
    }
    WriteText(lines, max_width, lines.empty() ?
      0 : lines.back().offset + info.height);
  }

  void PlainText::WriteText(const GLineVector & lines, uint width, uint height)
  {
    int x_off, y_off;

    // vertical alignment
    if(valign_ == vaMiddle && bounds_.y * scaleFactor_.y > height) {
      y_off = (bounds_.y * scaleFactor_.y - height) / 2;
    } else if(valign_ == vaBottom && bounds_.y * scaleFactor_.y > height) {
      y_off = bounds_.y * scaleFactor_.y - height;
    } else {
      y_off = 0;
    }

    // haStretch not supported for plain text
    switch(halign_) {
      case haRight:
        if(bounds_.x == 0)
          x_off = -static_cast<int>(width);
        else
          x_off = static_cast<int>(bounds_.x * scaleFactor_.x - width);
        break;
      case haCenter:
        if(bounds_.x == 0)
          x_off = -static_cast<int>(width) / 2;
        else
          x_off = static_cast<int>(bounds_.x * scaleFactor_.x - width) / 2;
        break;
      default:
        x_off = 0;
        break;
    }

    textRect_.Set(x_off, y_off, x_off + height, y_off + width);

    for(GLineVector::const_iterator pos = lines.begin();
        pos != lines.end(); ++pos) {
      for(GlyphPrepVector::const_iterator pos2 = pos->glyphs.begin();
          pos2 != pos->glyphs.end(); ++pos2) {
        MeshPage & page = GetPageByTexture(pos2->glyph->texture);
        page.prep.push_back(std::make_pair(&(*pos2),
          Vector2i(x_off + pos2->offset, y_off + pos->offset + pos->baseline)));
      }
    }

    // automatically fills mesh pages and discard unused ones
    // also resets geometry batches
    ResetPages();
  }

}
