//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file richtext.cpp
 *
 *  blahblah
 */

#include <wctype.h>
#include <algorithm>
#include <limits>

#include "utf8.h"

#include "vfs/logfile.h"

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "gui/font/font.h"
#include "gui/richtext.h"

#include "memory/mmgr.h"

namespace tre {

  namespace {

    const wchar_t lineSeparator[] = L"\u2028";
    const wchar_t paraSeparator[] = L"\u2029";

    // back_insert_iterator for rich text parsing
    class CondensingInserter {
      public:
        CondensingInserter(std::wstring & o, bool & p)
          : out_(o), prev_space_(p) {}

        CondensingInserter & operator = (wchar_t val)
        {
          if(!iswspace(val)) {
            prev_space_ = false;
            out_.push_back(val);
          } else if(!prev_space_) {
            prev_space_ = true;
            out_.push_back(' ');
          } else {
            return *this;
          }
          return *this;
        }

        CondensingInserter & operator * (void)
        {
          return *this;
        }

        CondensingInserter & operator ++ (void)
        {
          return *this;
        }

        CondensingInserter operator ++ (int)
        {
          return *this;
        }

      private:
        std::wstring & out_;
        bool & prev_space_;
    };

  }

  class RichText::LineLess {
    public:
      bool operator () (const LinePair & l, const LinePair & r)
      {
        return l.second.x < r.second.x;
      }

      bool operator () (const LinePair & l, uint r)
      {
        return l.second.x < r;
      }

      bool operator () (uint l, const LinePair & r)
      {
        return l < r.second.x;
      }
  };

  RichText::RichLeaf::RichLeaf(RichText::RichNode * p) : parent(p)
  {
    ResetStyle();
  }

  void RichText::RichLeaf::ResetStyle(void)
  {
    bool is_fs = false, is_col = false;
    float new_size = 1.0f;

    RichNode * node = parent;
    while(node && (!is_fs || !is_col)) {
      switch(node->type) {
        // the end node
        case ntPreDef:
        case ntLink:
          if(!is_col)
            style.colour = node->predef->colour;
          if(!is_fs)
            style.fstyle = node->predef->fstyle;
          style.size = new_size * node->predef->size;
          return;
        case ntColour:
          if(!is_col) {
            std::copy(node->colour, node->colour + 4, &style.colour.x);
            is_col = true;
          }
          break;
        case ntSize:
          new_size *= node->size;
          break;
        case ntFontStyle:
          if(!is_fs) {
            style.fstyle = node->fstyle;
            is_fs = true;
          }
          break;
      }
      node = node->parent;
    }
  }

  RichText::RichNode::~RichNode()
  {
    for(NodeChildVector::iterator itr = children.begin();
        itr != children.end(); ++itr) {
      if(itr->isNode)
        delete itr->node;
    }
  }

  /*
   * RichText class
   */
  RichText::RichText() : wrap_(false), lineSpace_(0),
    parSpace_(Font::defTextSize), caret_(0), showCaret_(false),
    fillerGlyph_(NULL), caretAttribs_(NULL), caretIndices_(NULL),
    caretVars_(NULL)
  {
    caretAttribs_ = AttribBufferSet::Factory().CreateInstance();
    caretIndices_ = IndexBuffer::Factory().CreateInstance();
    caretVars_ = new EffectVars();

    DEBUG_ASSERT(caretAttribs_);
    DEBUG_ASSERT(caretIndices_);

    caretVars_->SetTexture("fnt_Texture", TexturePtr());

    const uint inds[] = {0, 1, 2, 0, 2, 3};

    // init caret mesh and indices
    caretIndices_->Init(sizeof(inds) / sizeof(uint));
    caretAttribs_->Init(atPosition, adFloat2, 4, amReadWrite);
    caretAttribs_->Init(atColour, adFloat4, 4, amReadWrite);
    uint * iptr = caretIndices_->Lock();
    float * pptr = static_cast<float*>(caretAttribs_->Lock(atPosition));
    float * cptr = static_cast<float*>(caretAttribs_->Lock(atColour));
    memcpy(iptr, inds, sizeof(inds));
    memset(pptr, 0, sizeof(float) * 2 * 4);
    memset(cptr, 0, sizeof(float) * 4 * 4);
    caretIndices_->Unlock();
    caretAttribs_->Unlock(atPosition);
    caretAttribs_->Unlock(atColour);

    fillerGlyph_ = Font::GetFillerGlyph();

    // caret tex coords
    caretAttribs_->Init(atTexCoord0, adFloat2, 4, amRead);
    float * tptr = static_cast<float*>(caretAttribs_->Lock(atTexCoord0));
    if(fillerGlyph_) {
      tptr[0] = fillerGlyph_->texCoords.x;
      tptr[1] = fillerGlyph_->texCoords.w;
      tptr[2] = fillerGlyph_->texCoords.z;
      tptr[3] = fillerGlyph_->texCoords.w;
      tptr[4] = fillerGlyph_->texCoords.y;
      tptr[5] = fillerGlyph_->texCoords.z;
      tptr[6] = fillerGlyph_->texCoords.y;
      tptr[7] = fillerGlyph_->texCoords.w;
    }
    caretAttribs_->Unlock(atTexCoord0);

    // init style sheets
    styles_[ssBase].size = Font::defTextSize;
    styles_[ssBase].fstyle = fsRegular;
    styles_[ssBase].colour.Set(1.0f, 1.0f, 1.0f, 1.0f);

    // init root node
    root_.parent = NULL;
    root_.type = ntPreDef;
    root_.predef = &styles_[ssBase];

    ResetStyles();
  }

  RichText::~RichText()
  {
    // clear buffers
    delete caretAttribs_;
    delete caretIndices_;
    delete caretVars_;
  }

  void RichText::SetStyleSheet(StyleSheet sheet, const TextStyle & style)
  {
    DEBUG_ASSERT(sheet < ssLast);

    styles_[sheet] = style;

    // recalc style info
    for(LeavesList::iterator itr = leaves_.begin();
        itr != leaves_.end(); ++itr) {
      itr->ResetStyle();
    }

    RedrawText(true);
  }

  void RichText::ResetStyles(void)
  {
    styles_[ssLink] = styles_[ssBase];
    styles_[ssLinkHover] = styles_[ssBase];
    styles_[ssLinkPressed] = styles_[ssBase];
    styles_[ssEmph] = styles_[ssBase];

    // recalc style info
    for(LeavesList::iterator itr = leaves_.begin();
        itr != leaves_.end(); ++itr) {
      itr->ResetStyle();
    }

    RedrawText(true);
  }

  void RichText::SetStyle(const TextStyle & style)
  {
    styles_[ssBase] = style;

    ResetStyles();
  }

  // also forces mesh reload
  void RichText::SetText(const std::string & text)
  {
    std::string xmltext;

    xmltext.reserve(text.size() + 13);
    xmltext.append("<text>");
    xmltext.append(text);
    xmltext.append("</text>");

    TiXmlBase::SetCondenseWhiteSpace(false);
    TiXmlDocument doc;
    doc.Parse(xmltext.c_str());
    TiXmlBase::SetCondenseWhiteSpace(true);

    if(doc.Error()) {
      vLog << err("RichText") << doc.ErrorDesc() << std::endl;
      return;
    }
    DEBUG_ASSERT(doc.RootElement());

    bool prev_space = true;

    // clear style tree
    for(NodeChildVector::iterator itr = root_.children.begin();
        itr != root_.children.end(); ++itr) {
      if(itr->isNode)
        delete itr->node;
    }
    root_.children.resize(0);
    leaves_.clear();

    // get styled chunks of text - \n \t and ' ' => ' '
    ParseText(*doc.RootElement(), &root_, prev_space);

    // recalc style info
    for(LeavesList::iterator itr = leaves_.begin();
        itr != leaves_.end(); ++itr) {
      itr->ResetStyle();
    }

#ifdef TEST_RICHTEXT
    // leaf dump
    std::wcout << "Leaf dump:" << std::endl;
    for(LeavesList::iterator itr = leaves_.begin();
        itr != leaves_.end(); ++itr) {
      std::wcout << "'" << itr->text << "'" << std::endl;
    }
    std::wcout << "=================================" << std::endl;
#endif /* TEST_RICHTEXT */

    RedrawText(true);
  }

  void RichText::SetText(const std::wstring & text)
  {
    std::string tmp_str;

    tmp_str.reserve(text.size());
    utf8::utf16to8(text.begin(), text.end(), std::back_inserter(tmp_str));

    SetText(tmp_str);
  }

  void RichText::ClearText(void)
  {
    SetText("");
  }

  const std::wstring RichText::GetText(void) const
  {
    std::wstring res;

    for(LeavesList::const_iterator itr = leaves_.begin();
        itr != leaves_.end(); ++itr) {
      if(itr->text == lineSeparator || itr->text == paraSeparator) {
        res.push_back('\n');
      } else {
        res.append(itr->text);
      }
    }

    return res;
  }

  void RichText::SetWrap(bool wrap)
  {
    if(wrap_ != wrap) {
      wrap_ = wrap;

      RedrawText(false);
    }
  }

  void RichText::SetParagraphSpacing(int space)
  {
    if(parSpace_ != space) {
      parSpace_ = space;
      RedrawText(false);
    }
  }

  void RichText::SetLineSpacing(int space)
  {
    if(lineSpace_ != space) {
      lineSpace_ = space;
      RedrawText(false);
    }
  }

  const std::wstring RichText::GetWord(const Vector2f & pos) const
  {
    const LetterPair * let = GetLetter(pos);

    // in case we hit some special leaf (apart from joiner)
    if(!let || (!iswalnum(let->first->text[let->second])))
      return std::wstring();

    // find the start of the word
    LeavesList::const_iterator itr = let->first;
    while(itr != leaves_.begin() && !itr->text.empty() &&
          iswalnum(itr->text[0])) {
      --itr;
    }

    // concatenate the result
    std::wstring res;
    while(itr != leaves_.end() && !itr->text.empty() &&
          iswalnum(itr->text[0])) {
      res.append(itr->text);
      ++itr;
    }
    return res;
  }

  const std::string RichText::GetLink(const Vector2f & pos) const
  {
    const LetterPair * let = GetLetter(pos);

    if(!let)
      return std::string();

    const RichNode * node = let->first->parent;

    do {
      if(node->type == ntLink)
        return node->extra;
    } while((node = node->parent));

    return std::string();
  }

  void RichText::ShowCaret(bool show)
  {
    if(show == showCaret_)
      return;

    showCaret_ = show;

    if(!show) {
      batch_.pop_back();
    } else {
      batch_.push_back(GeometryInfo(trans_, caretAttribs_, caretIndices_,
        effect_.get(), caretVars_));
    }
  }

  void RichText::SetCaretToCursor(const Vector2f & pos)
  {
    if(lines_.empty()) {
      caret_ = 0;
      RedrawCaret();
      return;
    }

    LineVector::const_iterator itr = std::lower_bound(
      lines_.begin(), lines_.end(), pos.y, LineLess());

    if(itr == lines_.end()) {
      itr = lines_.end() - 1;
    }
    caret_.y = itr - lines_.begin();

    LetterVector::const_iterator itr2 = std::lower_bound(
      itr->first.begin(), itr->first.end(), pos.x);

    caret_.x = itr2 - itr->first.begin();

    RedrawCaret();
  }

  void RichText::MoveCaret(CaretTarget pos)
  {
    if(lines_.empty()) {
      caret_ = 0;
      RedrawCaret();
      return;
    }

    switch(pos) {
      case ctTextStart:
        caret_ = 0;
        break;
      case ctTextEnd:
        caret_.y = lines_.size() - 1;
        caret_.x = lines_[caret_.y].first.size();
        break;
      case ctLineStart:
        caret_.x = 0;
        break;
      case ctLineEnd:
        caret_.x = lines_[caret_.y].first.size();
        break;
      case ctWordLeft:
        if(caret_.x > 0)
          --caret_.x;
        while(caret_.x > 0 && iswalnum(lines_[caret_.y].first[
              caret_.x].letter.first->text[lines_[caret_.y].first[
              caret_.x].letter.second])) {
          --caret_.x;
        }
        break;
      case ctWordRight:
        if(caret_.x < lines_[caret_.y].first.size())
          ++caret_.x;
        while(caret_.x < lines_[caret_.y].first.size() &&
              iswalnum(lines_[caret_.y].first[
              caret_.x].letter.first->text[lines_[caret_.y].first[
              caret_.x].letter.second])) {
          ++caret_.x;
        }
        break;
      case ctLeft:
        if(caret_.x > 0) {
          --caret_.x;
        } else if(caret_.y > 0) {
          --caret_.y;
          caret_.x = lines_[caret_.y].first.size();
        }
        break;
      case ctRight:
        if(caret_.x < lines_[caret_.y].first.size()) {
          ++caret_.x;
        } else if(caret_.y < lines_.size() - 1) {
          ++caret_.y;
          caret_.x = 0;
        }
        break;
      case ctUp:
      case ctDown:
        if((pos == ctUp && caret_.y > 0) ||
           (pos == ctDown && caret_.y < lines_.size() - 1)) {
          int x;
          if(caret_.x < lines_[caret_.y].first.size()) {
            x = lines_[caret_.y].first[caret_.x].start;
          } else {
            x = lines_[caret_.y].first[caret_.x - 1].start
                + lines_[caret_.y].first[caret_.x - 1].width;
          }

          if(pos == ctUp)
            --caret_.y;
          else
            ++caret_.y;

          LetterVector::const_iterator itr = std::lower_bound(
            lines_[caret_.y].first.begin(), lines_[caret_.y].first.end(), x);

          if(itr == lines_[caret_.y].first.end() - 1) {
            caret_.x = lines_[caret_.y].first.size();
          } else if(x < itr->start) {
            caret_.x = 0;
          } else {
            caret_.x = itr - lines_[caret_.y].first.begin();
          }
        }
        break;
    }
    RedrawCaret();
  }

  void RichText::RedrawText(bool reset)
  {
    if(!font_)
      return;

    font_->SetAntialias(antialias_);

    // reset glyphs
    if(reset) {
      for(LeavesList::iterator itr = leaves_.begin();
          itr != leaves_.end(); ++itr) {
        itr->glyphs.resize(itr->text.size(), NULL);

        if(itr->text == lineSeparator || itr->text == paraSeparator)
          continue;

        font_->SetSize(itr->style.size * scaleFactor_.y);

        for(uint i = 0; i < itr->text.size(); ++i) {
          itr->glyphs[i] = font_->GetGlyph(iswspace(itr->text[i]) ?
            ' ' : itr->text[i], itr->style.fstyle);
        }
      }
    }

    GLineVector lines;
    GlyphLine ln;

    FaceInfo info;

    // init default values
    if(!leaves_.empty()) {
      font_->SetSize(leaves_.front().style.size * scaleFactor_.y);
      info = font_->GetFaceInfo(leaves_.front().style.fstyle);
    }

    int x_off = 0;
    uint max_width = 0;
    wchar_t prev_char = 0;

    ln.offset = 0;
    ln.height = 0;
    ln.wspace = 0;

    for(LeavesList::iterator itr = leaves_.begin(),
        line_begin = leaves_.begin(); itr != leaves_.end(); ++itr) {
      // hard break
      if(itr->text == lineSeparator || itr->text == paraSeparator) {
        ln.width = ln.glyphs.empty() ? 0 : ln.glyphs.back().offset
          + ln.glyphs.back().glyph->width;
        if(ln.width > max_width)
          max_width = ln.width;

        // do not stretch last lines
        ln.align = (halign_ == haStretch) ? haLeft : halign_;

        // empty line?
        if(ln.height == 0) {
          ln.height = info.height;
          ln.baseline = info.ascender;
        }

        lines.push_back(ln);

        ln.glyphs.resize(0);
        ln.letters.resize(0);

        // space betweeen paragraphs/lines
        if(itr->text == paraSeparator)
          ln.offset += parSpace_ * scaleFactor_.y + ln.height;
        else
          ln.offset += lineSpace_ * scaleFactor_.y + ln.height;

        ln.height = 0;
        ln.wspace = 0;
        x_off = 0;

        // increment the stop marker
        line_begin = itr;
        ++line_begin;
        continue;
      }

#ifdef TEST_RICHTEXT
      std::wcout << "Loop: " << "'" << itr->text << "'" << std::endl;
#endif /* TEST_RICHTEXT */

      // fetch text style for this leaf
      font_->SetSize(itr->style.size * scaleFactor_.y);
      info = font_->GetFaceInfo(itr->style.fstyle);

      // update line height
      if(info.height > ln.height) {
        ln.height = info.height;
        ln.baseline = info.ascender;
      }

      // each leaf shares the same style
      for(uint i = 0; i < itr->text.size(); ++i) {
        // ignore missing glyphs and errors
        if(!itr->glyphs[i])
          continue;

        // kerning
        if(!ln.glyphs.empty())
          x_off += font_->GetKerning(prev_char, text_[i], itr->style.fstyle);
        prev_char = itr->text[i];

        // soft break
        if(wrap_ && !iswspace(itr->text[i]) && static_cast<int>(
           x_off + itr->glyphs[i]->width) > bounds_.x * scaleFactor_.x) {
          bool reset = false; // is it necessary to reset height?

#ifdef TEST_RICHTEXT
        std::wcout << "Soft break: " << "'" << itr->text << "'" << std::endl;
#endif /* TEST_RICHTEXT */

          // how much to unroll ?
          LeavesList::iterator unroll_to = itr;

          while(unroll_to != line_begin && (unroll_to->text.empty() ||
                !(iswspace(unroll_to->text[0])))) {
            if(unroll_to->style.fstyle != itr->style.fstyle ||
               unroll_to->style.size != itr->style.size) {
              reset = true;
            }
            --unroll_to;
          }
#ifdef TEST_RICHTEXT
        std::wcout << "Unrolling to: " << "'" << unroll_to->text << "'" << std::endl;
#endif /* TEST_RICHTEXT */
          // oops
          if(unroll_to == line_begin) {
            vLog << err("RichText") << "Bounding box too thin!" << std::endl;
            break;  // cut the current leaf and continue with the next
          }

          // pop glyphs
          while(itr != unroll_to) {
            for(;i > 0; --i) {
              if(!itr->glyphs[i - 1])
                continue;
              ln.glyphs.pop_back();
              ln.letters.pop_back();
            }
            --itr;
            i = itr->text.size();
          }

          // pop whitespace
          while(!ln.letters.empty() && iswspace(
                ln.letters.back().first->text[ln.letters.back().second])) {
            if(ln.letters.back().first->style.fstyle != itr->style.fstyle ||
               ln.letters.back().first->style.size != itr->style.size) {
              reset = true;
            }
            ln.glyphs.pop_back();
            ln.letters.pop_back();
            --ln.wspace;
          }

          // break the line
          ln.width = ln.glyphs.empty() ? 0 : ln.glyphs.back().offset
            + ln.glyphs.back().glyph->width;
          if(ln.width > max_width)
            max_width = ln.width;
          ln.align = halign_;

          if(reset)
            CalcLineHeight(ln);
#ifdef TEST_RICHTEXT
          std::wcout << "Wspace: " << ln.wspace << std::endl;
          std::wcout << "Last: " << ln.letters.back().first->text[
            ln.letters.back().second] << std::endl;
#endif /* TEST_RICHTEXT */
          lines.push_back(ln);

          // reset counters and continue
          ln.glyphs.resize(0);
          ln.letters.resize(0);

          ln.offset += lineSpace_ * scaleFactor_.y + ln.height;
          ln.height = 0;
          ln.wspace = 0;
          x_off = 0;
          prev_char = '\n';
        } else {
          // upadte whitespace count
          if(iswspace(itr->text[i]))
            ++ln.wspace;

          // advance x offset
          ln.glyphs.push_back(GlyphPrep(itr->glyphs[i], &itr->style, x_off));
          ln.letters.push_back(std::make_pair(itr, i));
          x_off += itr->glyphs[i]->advance_x;
        }
      }
    }

    // anything unfinished?
    if(!ln.glyphs.empty()) {
      ln.width = ln.glyphs.back().offset + ln.glyphs.back().glyph->width;
      if(ln.width > max_width)
        max_width = ln.width;

      // last line of a paragraph is not stretched!
      ln.align = (halign_ == haStretch) ? haLeft : halign_;
      lines.push_back(ln);
    }

    if(showCaret_)
      batch_.pop_back();

    WriteText(lines, max_width, lines.empty() ?
      0 : lines.back().offset + info.height);

    RedrawCaret();

    if(showCaret_)
      batch_.push_back(GeometryInfo(trans_, caretAttribs_, caretIndices_,
        effect_.get(), caretVars_));
  }

  const RichText::LetterPair * RichText::GetLetter(const Vector2f & pos) const
  {
    LineVector::const_iterator itr = std::lower_bound(
      lines_.begin(), lines_.end(), pos.y, LineLess());

    if(itr == lines_.end() || pos.y < itr->second.x ||
       pos.y >= itr->second.y) {
      return NULL;
    }

    LetterVector::const_iterator itr2 = std::lower_bound(
      itr->first.begin(), itr->first.end(), pos.x);

    if(itr2 == itr->first.end() - 1 || pos.x < itr2->start)
      return NULL;

    return &itr2->letter;
  }

  // Suported formatting tags:
  // <colour> - sets text colour
  // <i>, <b>, <em> - italics, bold and ephasized (custom) style
  // <a> - link
  // <size>, <small>, <big> - absolute and relative size
  // <p>, <br> - layout
  void RichText::ParseText(TiXmlElement & root,
    RichNode * rnode, bool & prev_spc)
  {
    TiXmlNode * node = root.FirstChild();
    RichNode * tmp_node = NULL;

    while(node) {
      // text node
      if(node->Type() == TiXmlNode::TEXT) {
        // transform the text
        std::wstring tmp_str;
        utf8::utf8to16(node->ValueTStr().begin(), node->ValueTStr().end(),
          CondensingInserter(tmp_str, prev_spc));

        DEBUG_ASSERT(!tmp_str.empty());

        if(rnode->children.empty() || rnode->children.back().isNode) {
          leaves_.push_back(RichLeaf(rnode));
          rnode->children.push_back(RichNodeChild(&leaves_.back()));
        }

        // split the
        std::wstring::iterator itr = tmp_str.begin();
        while(itr != tmp_str.end()) {
          RichLeaf * leaf = rnode->children.back().leaf;
          DEBUG_ASSERT(leaf);

          // create leaf
          if(!leaf->text.empty() && (leaf->text == lineSeparator ||
             leaf->text == paraSeparator || (iswspace(*itr) != 0) !=
             (iswspace(leaf->text[leaf->text.size() - 1]) != 0))) {
            leaves_.push_back(RichLeaf(rnode));
            rnode->children.push_back(RichNodeChild(&leaves_.back()));
          }
          rnode->children.back().leaf->text.push_back(*itr);
          ++itr;
        }
      } else if(node->Type() == TiXmlNode::ELEMENT) {
        // line break
        if(node->ValueTStr() == "br") {
          leaves_.push_back(RichLeaf(rnode));
          leaves_.back().text = lineSeparator;
          rnode->children.push_back(RichNodeChild(&leaves_.back()));
          prev_spc = true;
        } else {
          TiXmlElement * elem = static_cast<TiXmlElement*>(node);

          if(node->ValueTStr() == "p") {
            ParseText(*elem, rnode, prev_spc);

            leaves_.push_back(RichLeaf(rnode));
            leaves_.back().text = paraSeparator;
            rnode->children.push_back(RichNodeChild(&leaves_.back()));
            prev_spc = true;
          } else if(node->ValueTStr() == "a") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntLink;
            tmp_node->predef = &styles_[ssLink];
            rnode->children.push_back(RichNodeChild(tmp_node));

            tmp_node->extra = CheckAttribute(*elem, "id");  // throws!

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else if(node->ValueTStr() == "i") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntFontStyle;
            tmp_node->fstyle = fsItalic;
            rnode->children.push_back(RichNodeChild(tmp_node));

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else if(node->ValueTStr() == "b") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntFontStyle;
            tmp_node->fstyle = fsBold;
            rnode->children.push_back(RichNodeChild(tmp_node));

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else if(node->ValueTStr() == "em") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntPreDef;
            tmp_node->predef = &styles_[ssEmph];
            rnode->children.push_back(RichNodeChild(tmp_node));

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else if(node->ValueTStr() == "colour") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntColour;
            tmp_node->colour[0] = CheckFloatAttribute(*elem, "r");
            tmp_node->colour[1] = CheckFloatAttribute(*elem, "g");
            tmp_node->colour[2] = CheckFloatAttribute(*elem, "b");
            tmp_node->colour[3] = CheckFloatAttribute(*elem, "a", false, 1.0f);
            rnode->children.push_back(RichNodeChild(tmp_node));

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else if(node->ValueTStr() == "size") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntSize;
            tmp_node->size = CheckFloatAttribute(*elem, "em");
            rnode->children.push_back(RichNodeChild(tmp_node));

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else if(node->ValueTStr() == "small") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntSize;
            tmp_node->size = 0.75f;
            rnode->children.push_back(RichNodeChild(tmp_node));

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else if(node->ValueTStr() == "big") {
            tmp_node = new RichNode();
            tmp_node->parent = rnode;
            tmp_node->type = ntSize;
            tmp_node->size = 1.25f;
            rnode->children.push_back(RichNodeChild(tmp_node));

            ParseText(*elem, tmp_node, prev_spc);

            if(rnode->children.back().node->children.empty()) {
              delete rnode->children.back().node;
              rnode->children.pop_back();
            }
          } else {
            vLog << warn("RichText") << "Unknown formatting tag" << std::endl;
          }
        }
      }
      node = node->NextSibling();
    }
  }

  void RichText::RedrawCaret(void)
  {
    DEBUG_ASSERT(fillerGlyph_);

    float * pptr = static_cast<float*>(caretAttribs_->Lock(atPosition));
    float * cptr = static_cast<float*>(caretAttribs_->Lock(atColour));

    if(!lines_.empty()) {
      // check caret position for abnormalities
      DEBUG_ASSERT(caret_.y < lines_.size());
      DEBUG_ASSERT(caret_.x <= lines_[caret_.y].first.size());

      LinePair & ln = lines_[caret_.y];
      Letter & lt = ln.first[caret_.x < ln.first.size() ?
        caret_.x : caret_.x - 1];

      float start = caret_.x < ln.first.size() ?
        lt.start : lt.start + lt.width;

      pptr[0] = start;
      pptr[1] = static_cast<float>(ln.second.x);
      pptr[2] = start + caretWidth;
      pptr[3] = static_cast<float>(ln.second.x);
      pptr[4] = start + caretWidth;
      pptr[5] = static_cast<float>(ln.second.y);
      pptr[6] = start;
      pptr[7] = static_cast<float>(ln.second.y);

      memcpy(cptr, &lt.letter.first->style.colour.x, sizeof(float) * 4);
      memcpy(cptr + 4, &lt.letter.first->style.colour.x, sizeof(float) * 4);
      memcpy(cptr + 8, &lt.letter.first->style.colour.x, sizeof(float) * 4);
      memcpy(cptr + 12, &lt.letter.first->style.colour.x, sizeof(float) * 4);
    } else {
      pptr[0] = 0.0f;
      pptr[1] = static_cast<float>(styles_[ssBase].size);
      pptr[2] = caretWidth;
      pptr[3] = static_cast<float>(styles_[ssBase].size);
      pptr[4] = caretWidth;
      pptr[5] = 0.0f;
      pptr[6] = pptr[7] = 0.0f;

      memcpy(cptr, &styles_[ssBase].colour.x, sizeof(float) * 4);
      memcpy(cptr + 4, &styles_[ssBase].colour.x, sizeof(float) * 4);
      memcpy(cptr + 8, &styles_[ssBase].colour.x, sizeof(float) * 4);
      memcpy(cptr + 12, &styles_[ssBase].colour.x, sizeof(float) * 4);
    }

    caretAttribs_->Unlock(atPosition);
    caretAttribs_->Unlock(atColour);
  }

  void RichText::CalcLineHeight(GlyphLine & line)
  {
    if(line.letters.empty() || !font_)
      return;

    FontStyle fstyle = line.letters.front().first->style.fstyle;
    uint size = line.letters.front().first->style.size;

    font_->SetSize(size * scaleFactor_.y);
    FaceInfo info = font_->GetFaceInfo(fstyle);

    line.height = info.height;
    line.baseline = info.ascender;

    for(LetterPairVector::const_iterator itr = line.letters.begin();
        itr != line.letters.end(); ++itr) {
      if(itr->first->style.fstyle != fstyle || itr->first->style.size != size) {
        fstyle = itr->first->style.fstyle;
        size = itr->first->style.size;

        font_->SetSize(size);
        FaceInfo info = font_->GetFaceInfo(fstyle);

        if(info.height > line.height) {
          line.height = info.height;
          line.baseline = info.ascender;
        }
      }
    }
  }

  void RichText::WriteText(const GLineVector & lines, uint width, uint height)
  {
    // vertical alignment
    if(valign_ == vaMiddle && bounds_.y * scaleFactor_.y > height) {
      textRect_.y = (bounds_.y * scaleFactor_.y - height) / 2;
    } else if(valign_ == vaBottom && bounds_.y * scaleFactor_.y > height) {
      textRect_.y = bounds_.y * scaleFactor_.y - height;
    } else {
      textRect_.y = 0;
    }
    textRect_.w = textRect_.y + height;

    textRect_.x = std::numeric_limits<int>::max();
    textRect_.z = std::numeric_limits<int>::min();

    int ws_fill, x_off, y_off = textRect_.y;

    for(uint i = 0; i < lines.size(); ++i) {
      ws_fill = 0;
      // horizontal alignment
      switch(lines[i].align) {
        case haRight:
          if(bounds_.x == 0)
            x_off = -static_cast<int>(lines[i].width);
          else
            x_off = static_cast<int>(bounds_.x * scaleFactor_.x
              - lines[i].width);
          break;
        case haCenter:
          if(bounds_.x == 0)
            x_off = -static_cast<int>(lines[i].width) / 2;
          else
            x_off = static_cast<int>(bounds_.x * scaleFactor_.x
              - lines[i].width) / 2;
          break;
        case haStretch:
          if(bounds_.x * scaleFactor_.x > lines[i].width)
            ws_fill = static_cast<int>(bounds_.x * scaleFactor_.x
              - lines[i].width);
        default:
          x_off = 0;
          break;
      }

      // update text rectangle
      if(x_off < textRect_.x)
        textRect_.x = x_off;
      if(x_off + static_cast<int>(lines[i].width) > textRect_.z)
        textRect_.z = x_off + lines[i].width;

      // prepare line in letter LUT
      lines_.push_back(std::make_pair(LetterVector(), Vector2u(
        y_off + lines[i].offset, y_off + lines[i].offset + lines[i].height)));

      WriteLine(lines[i].glyphs, lines[i].letters, x_off,
        y_off + lines[i].offset + lines[i].baseline, ws_fill, lines[i].wspace);
    }

    // in case nothing gets rendered
    if(textRect_.x == std::numeric_limits<int>::max())
      textRect_.x = 0;
    if(textRect_.z == std::numeric_limits<int>::min())
      textRect_.z = 0;

    // automatically fills mesh pages and discard unused ones
    // also resets geometry batches
    ResetPages();
  }

  void RichText::WriteLine
  (const GlyphPrepVector & glyphs, const LetterPairVector & letters,
   int x_off, int y_off, uint ws_fill, uint ws)
  {
    uint ws_count = 0;  // number of paddings issued
    int wspace = 0;     // issued padding

    for(uint i = 0; i < glyphs.size(); ++i) {
      DEBUG_ASSERT(!iswspace(letters[i].first->text[letters[i].second]) ||
        (ws - ws_count));
      if(iswspace(letters[i].first->text[letters[i].second]))
        wspace += (ws_fill - wspace) / (ws - ws_count++);

      // push back glyph info
      MeshPage & page = GetPageByTexture(glyphs[i].glyph->texture);
      page.prep.push_back(std::make_pair(&glyphs[i],
        Vector2i(x_off + glyphs[i].offset + wspace, y_off)));

      // add a letter to the LUT
      lines_.back().first.push_back(Letter(
        x_off + glyphs[i].offset + wspace, glyphs[i].glyph->width, letters[i]));
    }
  }
}
