//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file skin.cpp
 *
 *  blahblah
 */

#include <string>

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "exception.h"
#include "skin.h"

namespace tre {

 /*
  *  WidgetTemplate class implementation
  */
  void WidgetTemplate::LoadFromXml(TiXmlElement & root)
  {
    float s,t;
    TiXmlElement * elem = root.FirstChildElement("mesh");
    if(elem) {
      scale = CheckFloatAttribute(*elem, "scale");

      elem = elem->FirstChildElement("tc");
      while(elem) {
        s = CheckFloatAttribute(*elem, "s");
        t = CheckFloatAttribute(*elem, "t");
        texCoords.push_back(Vector2f(s, t));
        elem = elem->NextSiblingElement("tc");
      }
    }

    int ustate;
    std::string tmpc;
    WidgetState wstate;

    elem = root.FirstChildElement("texture");
    while(elem) {
      ustate = CheckIntAttribute(*elem, "ustate", false, 0);
      tmpc = CheckAttribute(*elem, "wstate", false);
      if(tmpc.empty() || tmpc == "normal") {
        wstate = wsNormal;
      } else if(tmpc == "disabled") {
        wstate = wsDisabled;
      } else if(tmpc == "pressed") {
        wstate = wsPressed;
      } else if(tmpc == "hover") {
        wstate = wsHover;
      } else {
        throw(Exception("Unknown widget state"));
      }
      states.push_back(StateTexture(ustate, wstate, CheckText(*elem)));

      elem = elem->NextSiblingElement("texture");
    }
  }

 /*
  *  GuiSkinFactory class implementation
  */
  const GuiSkinPtr GuiSkinFactory::CreateInstance(const std::string & filename)
  {
    SkinMap::iterator pos = skinMap_.find(filename);

    if(pos != skinMap_.end() && !pos->second.expired()) {
      return pos->second.lock();
    }

    GuiSkinPtr newskin;

    try {
      FileIStream file(filename.c_str());
      if(!file.is_open())
        throw(Exception("Cannot open GUI skin file"));

      TiXmlDocument doc;

      if(!(file >> doc) || doc.Error())
        throw(Exception(doc.ErrorDesc()));

      TiXmlElement * section = doc.RootElement();
      DEBUG_ASSERT(section);
      if(section->ValueTStr() != "skin")
        throw(Exception("Wrong root element"));

      newskin = GuiSkinPtr(new GuiSkin);
      newskin->LoadFromXml(*section);

      vLog << ok("Gui") << "Skin loaded successfully ("
           << filename << ")." << std::endl;
    } catch(Exception & exc) {
      vLog << err("Gui") << "Error loading GUI skin (" << filename
           << ")\nReason: " << exc.what() << "." << std::endl;
      return GuiSkinPtr();
    }

    skinMap_[filename] = newskin;
    return newskin;
  }

 /*
  *  GuiSkin class implementation
  */
  const WidgetTemplate * GuiSkin::GetTemplate(const std::string & name) const
  {
    TempVector::const_iterator pos = std::lower_bound(
      templates_.begin(), templates_.end(), name);

    if(pos == templates_.end() || pos->first != name)
      return NULL;

    return &pos->second;
  }

  void GuiSkin::LoadFromXml(TiXmlElement & root)
  {
    // gui defaults
    TiXmlElement * elem = root.FirstChildElement("effect");
    if(elem)
      effect_ = CheckText(*elem);

    memset(textDefs_.richStyles, 0, sizeof(TextStyle) * ssLast);
    memset(&textDefs_.plainStyle, 0, sizeof(TextStyle));

    TiXmlElement * txt_root = root.FirstChildElement("text");
    if(txt_root) {
      if((elem = txt_root->FirstChildElement("effect")))
        textDefs_.effect = CheckText(*elem);

      if((elem = txt_root->FirstChildElement("font")))
        textDefs_.font = CheckText(*elem);

      TextStyle style;
      std::string tmpstr;

      // parse all style elements
      elem = txt_root->FirstChildElement("style");
      while(elem) {
        TiXmlElement * subel = elem->FirstChildElement("size");
        if(subel)
          style.size = CheckIntText(*subel);
        else
          style.size = Font::defTextSize;

        if((subel = elem->FirstChildElement("fontStyle"))) {
          tmpstr = CheckText(*subel);
          if(tmpstr == "regular") {
            style.fstyle = fsRegular;
          } else if(tmpstr == "bold") {
            style.fstyle = fsBold;
          } else if(tmpstr == "italic") {
            style.fstyle = fsItalic;
          } else if(tmpstr == "boldItalic") {
            style.fstyle = fsBoldItalic;
          } else {
            throw(Exception("Unknown font style"));
          }
        } else {
          style.fstyle = fsRegular;
        }

        if((subel = elem->FirstChildElement("colour"))) {
          style.colour.Set(CheckFloatAttribute(*subel, "r"),
            CheckFloatAttribute(*subel, "g"),
            CheckFloatAttribute(*subel, "b"),
            CheckFloatAttribute(*subel, "a", false, 1.0f));
        } else {
          style.colour = 1.0f;
        }

        // determine style type (name)
        tmpstr = CheckAttribute(*elem, "type");
        if(tmpstr == "plain") {
          textDefs_.plainStyle = style;
        } else if(tmpstr == "base") {
          textDefs_.richStyles[ssBase] = style;
        } else if(tmpstr == "link") {
          textDefs_.richStyles[ssLink] = style;
        } else if(tmpstr == "linkHover") {
          textDefs_.richStyles[ssLinkHover] = style;
        } else if(tmpstr == "linkPressed") {
          textDefs_.richStyles[ssLinkPressed] = style;
        } else if(tmpstr == "emph") {
          textDefs_.richStyles[ssEmph] = style;
        } else {
          throw(Exception("Unknown style sheet"));
        }
        elem = elem->NextSiblingElement("style");
      }
    }

    // pixmap templates
    elem = root.FirstChildElement("template");
    while(elem) {
      templates_.push_back(std::make_pair(
        CheckAttribute(*elem, "name"), WidgetTemplate()));
      templates_.back().second.LoadFromXml(*elem);

      elem = elem->NextSiblingElement("template");
    }
    std::sort(templates_.begin(), templates_.end());
  }

}
