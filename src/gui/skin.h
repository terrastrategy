//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file skin.h
 *
 *  Ble.
 */

#pragma once

#include <string>
#include <vector>
#include <boost/smart_ptr.hpp>

#include "platform.h"

#ifdef PLATFORM_MSVC
#  include <unordered_map>
#else /* PLATFORM_MSVC */
#  include <tr1/unordered_map>
#endif /* PLATFORM_MSVC */

#include "stdtypes.h"
#include "cpair.h"

#include "gui/widget.h"
#include "gui/richtext.h"

class TiXmlElement;

namespace tre {

  class WidgetTemplate {
    public:
      struct StateTexture {
        uint ustate;
        WidgetState wstate;
        std::string tex;

        StateTexture(uint us, WidgetState ws, const std::string & t)
          : ustate(us), wstate(ws), tex(t) {}
      };

      typedef std::vector<StateTexture> TexVector;

    public:
      Float2Vector texCoords;
      TexVector states;

      float scale;

    public:
      void LoadFromXml(TiXmlElement & root);
  };

  class GuiSkin;

  typedef boost::shared_ptr<GuiSkin> GuiSkinPtr;
  typedef boost::weak_ptr<GuiSkin> GuiSkinRef;

  class GuiSkinFactory {
    public:
      const GuiSkinPtr CreateInstance(const std::string & filename);

    private:
      typedef std::tr1::unordered_map<std::string, GuiSkinRef> SkinMap;

    private:
      SkinMap skinMap_;
  };

  class GuiSkin {
    public:
      static GuiSkinFactory & Factory(void)
      {
        static GuiSkinFactory fac;
        return fac;
      }

    public:
      typedef cpair<std::string, WidgetTemplate> TemplatePair;
      typedef std::vector<TemplatePair> TempVector;

      struct TextDefaults {
        std::string effect;
        std::string font;
        TextStyle plainStyle;
        TextStyle richStyles[ssLast];
      };

    public:
      const std::string & GetEffect(void) const {return effect_;}
      const TextDefaults & GetTextDefaults(void) const {return textDefs_;}

      const WidgetTemplate * GetTemplate(const std::string & name) const;

      void LoadFromXml(TiXmlElement & root);

    private:
      std::string effect_;
      TextDefaults textDefs_;

      TempVector templates_;
  };

}
