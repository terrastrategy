//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file text.cpp
 *
 *  blahblah
 */

#include <wctype.h>
#include <algorithm>
#include <vector>

#include <string.h>

#include "gui/font/font.h"
#include "gui/text.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  Text class
  */
  Text::Text() : antialias_(false), bounds_(0.0f),
    halign_(haLeft), valign_(vaTop), trans_(NULL), abuffers_(NULL)
  {
    abuffers_ = AttribBufferSet::Factory().CreateInstance();

    DEBUG_ASSERT(abuffers_);
  }

  Text::~Text()
  {
    // clear buffers
    delete abuffers_;

    // clear pages
    for(PageVector::iterator pos = pages_.begin();
        pos != pages_.end(); ++pos) {
      delete pos->indices;
      delete pos->vars;
    }
  }

  void Text::SetRect(uint width, uint height)
  {
    bounds_.Set(width, height);

    RedrawText(false);
  }

  void Text::SetHAlign(HorizAlign align)
  {
    halign_ = align;

    RedrawText(false);
  }

  void Text::SetVAlign(VertAlign align)
  {
    valign_ = align;

    RedrawText(false);
  }

  void Text::SetFont(const std::string & font)
  {
    // fonts are shared resources
    FontPtr fnt = Font::Factory().CreateInstance(font);
    if(fnt == font_)
      return;

    font_ = fnt;

    RedrawText(true);
  }

  void Text::SetEffect(const std::string & fx)
  {
    effect_ = Effect::Factory().CreateInstance(fx);

    for(GeometryBatch::iterator pos = batch_.begin();
        pos != batch_.end(); ++pos) {
      pos->effect = effect_.get();
    }
  }

  void Text::SetAntialias(bool on)
  {
    if(antialias_ != on) {
      antialias_ = on;
      RedrawText(true);
    }
  }

  void Text::SetScaleFactor(const Vector2f scale)
  {
    if(scaleFactor_ != scale) {
      scaleFactor_ = scale;
      RedrawText(true);
    }
  }

  void Text::SetTransform(const Matrix4x4f * trans)
  {
    trans_ = trans;

    for(GeometryBatch::iterator pos = batch_.begin();
        pos != batch_.end(); ++pos) {
      pos->trans = trans_;
    }
  }

  void Text::ResetPages(void)
  {
    uint quads = 0, cur_verts = 0;
    std::vector<uint*> iptrs(pages_.size(), NULL);
    float * verts, * cols, * tex;

    const uint inds[] = {0, 1, 2, 0, 2, 3};

    // first pass for reinitializing buffers
    for(uint i = 0; i < pages_.size(); ++i) {
      if(!pages_[i].prep.empty()) {
        pages_[i].indices->Init(pages_[i].prep.size()
          * sizeof(inds) / sizeof(uint));
        iptrs[i] = pages_[i].indices->Lock();
        quads += pages_[i].prep.size();

        DEBUG_ASSERT(iptrs[i]);
      } else {
        delete pages_[i].indices;
        pages_[i].indices = NULL;
        delete pages_[i].vars;
        pages_[i].vars = NULL;
      }
    }
    if(quads) {
      abuffers_->Init(atPosition, adFloat2, quads * 4);
      abuffers_->Init(atColour, adFloat4, quads * 4);
      abuffers_->Init(atTexCoord0, adFloat2, quads * 4);
      verts = static_cast<float*>(abuffers_->Lock(atPosition));
      cols = static_cast<float*>(abuffers_->Lock(atColour));
      tex = static_cast<float*>(abuffers_->Lock(atTexCoord0));
      DEBUG_ASSERT(verts);
      DEBUG_ASSERT(cols);
      DEBUG_ASSERT(tex);
    }

    // fill the buffers
    for(uint i = 0, cur_inds = 0; i < pages_.size(); ++i, cur_inds = 0) {
      pages_[i].indices->vFrom = cur_verts;
      for(PagePrepQueue::iterator itr = pages_[i].prep.begin();
          itr != pages_[i].prep.end(); ++itr) {
        const GlyphInfo * gi = itr->first->glyph;

        // index buffers
        std::transform(inds, inds + sizeof(inds) / sizeof(uint),
          iptrs[i] + cur_inds, std::bind1st(std::plus<uint>(), cur_verts));
        cur_inds += sizeof(inds) / sizeof(uint);

        // attrib buffers
        verts[cur_verts * 2] = itr->second.x + gi->bearing_x;
        verts[cur_verts * 2 + 1] = itr->second.y - gi->bearing_y
          + static_cast<int>(gi->height);
        memcpy(&cols[cur_verts * 4],
          &itr->first->style->colour.x, sizeof(Vector4f));
        tex[cur_verts * 2] = gi->texCoords.x;
        tex[cur_verts * 2 + 1] = gi->texCoords.w;
        ++cur_verts;

        verts[cur_verts * 2] = itr->second.x + gi->bearing_x
          + static_cast<int>(gi->width);
        verts[cur_verts * 2 + 1] = itr->second.y - gi->bearing_y
          + static_cast<int>(gi->height);
        memcpy(&cols[cur_verts * 4],
          &itr->first->style->colour.x, sizeof(Vector4f));
        tex[cur_verts * 2] = gi->texCoords.z;
        tex[cur_verts * 2 + 1] = gi->texCoords.w;
        ++cur_verts;

        verts[cur_verts * 2] = itr->second.x + gi->bearing_x
          + static_cast<int>(gi->width);
        verts[cur_verts * 2 + 1] = itr->second.y - gi->bearing_y;
        memcpy(&cols[cur_verts * 4],
          &itr->first->style->colour.x, sizeof(Vector4f));
        tex[cur_verts * 2] = gi->texCoords.z;
        tex[cur_verts * 2 + 1] = gi->texCoords.y;
        ++cur_verts;

        verts[cur_verts * 2] = itr->second.x + gi->bearing_x;
        verts[cur_verts * 2 + 1] = itr->second.y - gi->bearing_y;
        memcpy(&cols[cur_verts * 4],
          &itr->first->style->colour.x, sizeof(Vector4f));
        tex[cur_verts * 2] = gi->texCoords.x;
        tex[cur_verts * 2 + 1] = gi->texCoords.y;
        ++cur_verts;
      }
      pages_[i].indices->vTo = cur_verts;
    }

    if(quads) {
      abuffers_->Unlock(atPosition);
      abuffers_->Unlock(atColour);
      abuffers_->Unlock(atTexCoord0);
    }

    // finish and close pages (and clean queues)
    for(uint i = 0; i < pages_.size(); ++i) {
      if(pages_[i].indices) {
        pages_[i].indices->Unlock();

        // let's assume we're not changing text mesh every frame
        pages_[i].prep.clear();
        pages_[i].prep.swap(pages_[i].prep);
      }
    }

    DEBUG_ASSERT(batch_.size() >= pages_.size());
    // remove empty pages (and batches)
    uint i = 0;
    while(i < pages_.size()) {
      if(pages_[i].indices) {
        ++i;
      } else {
        pages_.erase(pages_.begin() + i);
        batch_.erase(batch_.begin() + i);
      }
    }
  }

  Text::MeshPage & Text::GetPageByTexture(const TexturePtr & texture)
  {
    DEBUG_ASSERT(batch_.size() == pages_.size());

    for(PageVector::iterator pos = pages_.begin();
        pos != pages_.end(); ++pos) {
      // text materials contain exactly one texture, so we can afford this
      if(pos->vars->samplers[0].second == texture)
        return *pos;
    }
    // else
    MeshPage page;
    page.indices = IndexBuffer::Factory().CreateInstance();
    page.vars = new EffectVars();
    page.vars->SetTexture("fnt_Texture", texture);
    pages_.push_back(page);
    batch_.push_back(GeometryInfo(trans_, abuffers_, page.indices,
      effect_.get(), page.vars));

    return pages_.back();
  }

}
