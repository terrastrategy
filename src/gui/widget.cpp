//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file widget.cpp
 *
 *  blahblah
 */

#include <algorithm>

#include "types.h"
#include "exception.h"

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "xml/tinyxml.h"
#include "xml/xmlutils.h"

#include "renderer/renderer.h"

#include "gui/gui.h"
#include "gui/widget.h"

#include "gui/basic.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  WidgetFactory class implementation
  */
  Widget * WidgetFactory::CreateInstance
  (const std::string & filename, Gui & gui, Widget * parent/*=NULL*/)
  {
    Widget * res = NULL;

    try {
      FileIStream file(filename.c_str());
      if(!file.is_open())
        throw(Exception("Cannot open widget file"));

      TiXmlDocument doc;

      if(!(file >> doc) || doc.Error())
        throw(Exception(doc.ErrorDesc()));

      TiXmlElement * section = doc.RootElement();
      DEBUG_ASSERT(section);
      if(section->ValueTStr() != "widget")
        throw(Exception("Wrong root element"));

      res = CreateInstance(*section, gui, parent);

      vLog << ok("Gui") << "Widget loaded successfully ("
           << filename << ")." << std::endl;
    } catch(Exception & exc) {
      vLog << err("Gui") << "Error loading widget (" << filename
           << ")\nReason: " << exc.what() << "." << std::endl;
      delete res;
      res = NULL;
    }
    return res;
  }

  Widget * WidgetFactory::CreateInstance
  (TiXmlElement & root, Gui & gui, Widget * parent/*=NULL*/)
  {
    Widget * res = NULL;

    std::string cls = CheckAttribute(root, "class");

    FactoryMap::iterator itr = ptrMap_.find(cls);
    if(itr != ptrMap_.end())
      res = itr->second(gui, parent);

    try {
      if(res)
        res->LoadFromXml(root);
    } catch(...) {
      delete res;
      throw;
    }

    return res;
  }

  void WidgetFactory::RegisterClass
  (const std::string & classname, CreateWidgetPtr * func)
  {
    ptrMap_.insert(std::make_pair(classname, func));
  }

 /*
  *  Widget class implementation
  */
  Widget::Widget() : parent(NULL), visible(true), gui_(NULL), anchor_(waNone),
    floating_(false), ustate_(0), wstate_(wsNormal), locked_(false)
  {
  }

  Widget::~Widget()
  {
    DEBUG_ASSERT(gui_);

    if(!name_.empty())
      gui_->UnregisterWidget(name_);

    for(WdgVector::iterator pos = children.begin();
        pos != children.end(); ++pos) {
      delete *pos;
    }
  }

  void Widget::Init(Gui & g, Widget * p)
  {
    parent = p;
    gui_ = &g;

    if(parent)
      trans_.SetWParent(&parent->trans_);
  }

  void Widget::SetName(const std::string & nm)
  {
    DEBUG_ASSERT(gui_);

    if(nm != name_) {
      if(!name_.empty())
        gui_->UnregisterWidget(name_);

      if(!nm.empty() && gui_->RegisterWidget(this, nm)) {
        name_ = nm;
      } else {
        name_ = "";
      }
    }
  }

  const Vector2f Widget::GetPosition(void) const
  {
    Vector3f pos(trans_.GetOPosition());
    return Vector2f(pos.x, pos.y);
  }

  const Vector2f Widget::GetAbsPosition(void) const
  {
    Vector3f pos(trans_.GetWPosition());
    return Vector2f(pos.x, pos.y);
  }

  void Widget::SetPosition(const Vector2f & pos)
  {
    trans_.SetOPosition(Vector3f(pos.x, pos.y, 0.0f));
  }

  void Widget::SetSize(const Vector2f & sz)
  {
    Vector4f rect;
    Vector2f tmpv;
    WidgetAnchor anchor;
    bool resize = false;

    // resize all children (according to their anchors)
    for(WdgVector::iterator itr = children.begin();
        itr != children.end(); ++itr, resize = false) {
      DEBUG_ASSERT(*itr != NULL);

      tmpv = (*itr)->GetPosition();
      rect.Set(tmpv.x, tmpv.y, tmpv.x, tmpv.y);
      tmpv = (*itr)->GetSize();
      rect.z += tmpv.x;
      rect.w += tmpv.y;
      anchor = (*itr)->anchor_;

      // x axis
      if(!(anchor & waLeft)) {
        rect.x = size_.x / rect.x * sz.x;
      }
      if(!(anchor & waWidth)) {
        if(anchor & waRight)
          rect.z = sz.x - (size_.x - rect.z);
        else
          rect.z = size_.x / rect.z * sz.x;
      }

      // y axis
      if(!(anchor & waTop)) {
        rect.y = size_.y / rect.y * sz.y;
      }
      if(!(anchor & waWidth)) {
        if(anchor & waRight)
          rect.w = sz.y - (size_.y - rect.w);
        else
          rect.w = size_.y / rect.w * sz.y;
      }

      // update changes
      if(resize)
        (*itr)->SetSize(Vector2f(rect.z - rect.x, rect.w - rect.y));
      (*itr)->SetPosition(Vector2f(rect.x, rect.y));
    }
    // and set parent's size
    size_ = sz;
  }

  Widget * Widget::GetChild(uint n)
  {
    if(n >= children.size())
      return NULL;

    return children[n];
  }

  void Widget::AppendChild(Widget * wdg)
  {
    children.push_back(wdg);
  }

  void Widget::InsertChild(Widget * wdg, uint pos)
  {
    if(pos > children.size())
      return;

    children.insert(children.begin() + pos, wdg);
  }

  bool Widget::IsInRegion(const Vector2f & pos) const
  {
    Vector2f wpos = GetPosition();
    return visible && wstate_ != wsDisabled && pos.x >= wpos.x &&
      pos.y >= wpos.y && pos.x <= (wpos.x + size_.x) &&
      pos.y <= (wpos.y + size_.y);
  }

  void Widget::Enable(bool state)
  {
    if(locked_)
      return;

    // enable
    if(state) {
      SetState(ustate_, prevState_);

      // also enable all children
      for(WdgVector::reverse_iterator itr = children.rbegin();
          itr != children.rend(); ++itr) {
        DEBUG_ASSERT(*itr != NULL);
        (*itr)->locked_ = false;
        (*itr)->Enable(true);
      }
    // disable
    } else {
      prevState_ = wstate_;
      SetState(ustate_, wsDisabled);

      // disable all children, including hidden widgets
      for(WdgVector::reverse_iterator itr = children.rbegin();
          itr != children.rend(); ++itr) {
        DEBUG_ASSERT(*itr != NULL);
        (*itr)->Enable(false);
        (*itr)->locked_ = true;
      }
    }
  }

  void Widget::SetUserState(uint state)
  {
    SetState(state, wstate_);
  }

  void Widget::RaiseChild(Widget * child)
  {
    DEBUG_ASSERT(child);

    if(child->floating_)
      *std::remove(children.begin(), children.end(), child) = child;

    if(parent)
      parent->RaiseChild(this);
  }

  void Widget::OnMouseEnter(void)
  {
    // cannot be entered if disabled or invisible
    SetState(ustate_, wsHover);
  }

  void Widget::OnMouseExit(void)
  {
    SetState(ustate_, wsNormal);
  }

  void Widget::OnGuiResize(const Vector2f & scale)
  {
    for(WdgVector::iterator pos = children.begin();
        pos != children.end(); ++pos) {
      DEBUG_ASSERT(*pos != NULL);
      (*pos)->OnGuiResize(scale);
    }
  }

  void Widget::LoadFromXml(TiXmlElement & root)
  {
    DEBUG_ASSERT(gui_);

    TiXmlElement * elem = root.FirstChildElement("name");
    if(elem && name_.empty()) {
      name_ = CheckText(*elem);

      // reset, if name is already in use
      if(!gui_->RegisterWidget(this, name_))
        name_ = "";
    }

//     Vector2f sf = gui_->GetScaleFactor();

    elem = root.FirstChildElement("position");
    if(elem) {
      if(parent) {
        Vector2f pos;
        pos.x = CheckFloatAttribute(*elem, "x")/* * sf.x*/;
        pos.y = CheckFloatAttribute(*elem, "y")/* * sf.y*/;

        SetPosition(pos);
      } else {
        vLog << warn("Widget")
             << "Position of the root element is ignored." << std::endl;
      }
    }

    elem = root.FirstChildElement("dimensions");
    if(parent && elem) {
      size_.x = CheckFloatAttribute(*elem, "x", false, 0.0f)/* * sf.x*/;
      size_.y = CheckFloatAttribute(*elem, "y", false, 0.0f)/* * sf.y*/;
    } else if(elem) {
      vLog << warn("Widget")
           << "Dimensions of the root element are ignored." << std::endl;
    }
    if(!parent) {
      size_.Set(gui_->GetSize().x, gui_->GetSize().y);
    }

    elem = root.FirstChildElement("visible");
    if(elem && CheckText(*elem, false) == "false") {
      if(parent) {
        visible = false;
      } else {
        vLog << warn("Widget")
             << "Root widget is always visible. Value ignored," << std::endl;
      }
    }

    elem = root.FirstChildElement("enabled");
    if(elem && CheckText(*elem, false) == "false") {
      if(parent) {
        wstate_ = wsDisabled;
      } else {
        vLog << warn("Widget")
             << "Root widget cannot be disabled. Value ignored," << std::endl;
      }
    }
    // check if parent is also disabled
    if(parent && parent->wstate_ == wsDisabled) {
      locked_ = true;
      prevState_ = wstate_;
      wstate_ = wsDisabled;
    }

    // initial user state
    elem = root.FirstChildElement("userState");
    if(elem) {
      ustate_ = CheckIntText(*elem, false, 0);
      if(ustate_ > maxUState)
        throw(Exception("Invalid user state value"));
    }
  }

  void Widget::LoadChildrenFromXml(TiXmlElement & root)
  {
    TiXmlElement * elem = root.FirstChildElement("children");
    if(elem) {
      elem = elem->FirstChildElement("widget");
      while(elem) {
        Widget * neww = NULL;

        if(elem->Attribute("import")) {
          neww = Widget::Factory().CreateInstance(
            CheckAttribute(*elem, "import", true), *gui_, this);

          if(neww && elem->Attribute("x")) {
            Vector2f pos;
            pos.x = CheckFloatAttribute(*elem, "x");
            pos.y = CheckFloatAttribute(*elem, "y");
            neww->SetPosition(pos);
          }
          if(neww && (elem->Attribute("w") || elem->Attribute("h"))) {
            Vector2f sz = neww->size_;
            if(elem->Attribute("w"))
              sz.x = CheckFloatAttribute(*elem, "w");
            if(elem->Attribute("h"))
              sz.y = CheckFloatAttribute(*elem, "h");
            neww->SetSize(sz);
          }
          if(neww && elem->Attribute("anchor")) {
            std::string str = CheckAttribute(*elem, "anchor");

            for(std::string::iterator itr = str.begin();
                itr != str.end(); ++itr) {
              switch(*itr) {
                case 'l':
                  neww->anchor_ |= waLeft;
                  break;
                case 'r':
                  neww->anchor_ |= waRight;
                  break;
                case 't':
                  neww->anchor_ |= waTop;
                  break;
                case 'b':
                  neww->anchor_ |= waBottom;
                  break;
                case 'w':
                  neww->anchor_ |= waWidth;
                  break;
                case 'h':
                  neww->anchor_ |= waHeight;
                  break;
              }
            }
          }
        } else {
          // skips on error
          try {
            neww = Widget::Factory().CreateInstance(*elem, *gui_, this);
          } catch(Exception & exc) {
            vLog << warn("Gui") << "Error loading child widget of class '"
                 << CheckAttribute(*elem, "class", false)
                 << "'\nReason: " << exc.what() << "." << std::endl;
          }
        }
        if(neww)
          children.push_back(neww);
        elem = elem->NextSiblingElement("widget");
      }
    }
    // trim excess memory
    children.swap(children);
  }

  void Widget::DrawChildren(void)
  {
    for(WdgVector::iterator pos = children.begin();
        pos != children.end(); ++pos) {
      DEBUG_ASSERT(*pos != NULL);
      if((*pos)->visible)
        (*pos)->Draw();
    }
  }

  void Widget::SetState(uint u, WidgetState w)
  {
    ustate_ = u;
    wstate_ = w;
  }

}
