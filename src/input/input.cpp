//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

// TODO: event filter should be critical section for threaded filtering

/**
 *  @file input.cpp
 *
 *  blahblah
 *
 */

#include <deque>
#include <vector>
#include <algorithm>
#include <SDL/SDL.h>

#include "types.h"
#include "input/input.h"
#include "vfs/logfile.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  Local helper functions & vars
  */

  namespace {

    Vector2f sMousePos;
    Vector2f sMouseDelta;

    SDLKey KeysymToSDL(KeySym key)
    {
      SDLKey ksm;
      switch(key) {
        case ksEscape: ksm = SDLK_ESCAPE; break;
        case ks1: ksm = SDLK_1; break;
        case ks2: ksm = SDLK_2; break;
        case ks3: ksm = SDLK_3; break;
        case ks4: ksm = SDLK_4; break;
        case ks5: ksm = SDLK_5; break;
        case ks6: ksm = SDLK_6; break;
        case ks7: ksm = SDLK_7; break;
        case ks8: ksm = SDLK_8; break;
        case ks9: ksm = SDLK_9; break;
        case ks0: ksm = SDLK_0; break;
        case ksMinus: ksm = SDLK_MINUS; break;
        case ksEquals: ksm = SDLK_EQUALS; break;
        case ksBack: ksm = SDLK_BACKSPACE; break;
        case ksTab: ksm = SDLK_TAB; break;
        case ksQ: ksm = SDLK_q; break;
        case ksW: ksm = SDLK_w; break;
        case ksE: ksm = SDLK_e; break;
        case ksR: ksm = SDLK_r; break;
        case ksT: ksm = SDLK_t; break;
        case ksY: ksm = SDLK_y; break;
        case ksU: ksm = SDLK_u; break;
        case ksI: ksm = SDLK_i; break;
        case ksO: ksm = SDLK_o; break;
        case ksP: ksm = SDLK_p; break;
        case ksA: ksm = SDLK_a; break;
        case ksS: ksm = SDLK_s; break;
        case ksD: ksm = SDLK_d; break;
        case ksF: ksm = SDLK_f; break;
        case ksG: ksm = SDLK_g; break;
        case ksH: ksm = SDLK_h; break;
        case ksJ: ksm = SDLK_j; break;
        case ksK: ksm = SDLK_k; break;
        case ksL: ksm = SDLK_l; break;
        case ksZ: ksm = SDLK_z; break;
        case ksX: ksm = SDLK_x; break;
        case ksC: ksm = SDLK_c; break;
        case ksV: ksm = SDLK_v; break;
        case ksB: ksm = SDLK_b; break;
        case ksN: ksm = SDLK_n; break;
        case ksM: ksm = SDLK_m; break;
        case ksLBrack: ksm = SDLK_LEFTBRACKET; break;
        case ksRBrack: ksm = SDLK_RIGHTBRACKET; break;
        case ksReturn: ksm = SDLK_RETURN; break;
        case ksLCtrl: ksm = SDLK_LCTRL; break;
        case ksSemicol: ksm = SDLK_SEMICOLON; break;
        case ksApos: ksm = SDLK_QUOTE; break;
        case ksGrave: ksm = SDLK_BACKQUOTE; break;
        case ksLShift: ksm = SDLK_LSHIFT; break;
        case ksBacksl: ksm = SDLK_BACKSLASH; break;
        case ksComma: ksm = SDLK_COMMA; break;
        case ksPer: ksm = SDLK_PERIOD; break;
        case ksSlash: ksm = SDLK_SLASH; break;
        case ksRShift: ksm = SDLK_RSHIFT; break;
        case ksKpMul: ksm = SDLK_KP_MULTIPLY; break;
        case ksLMenu: ksm = SDLK_LALT; break;
        case ksSpace: ksm = SDLK_SPACE; break;
        case ksCap: ksm = SDLK_CAPSLOCK; break;
        case ksF1: ksm = SDLK_F1; break;
        case ksF2: ksm = SDLK_F2; break;
        case ksF3: ksm = SDLK_F3; break;
        case ksF4: ksm = SDLK_F4; break;
        case ksF5: ksm = SDLK_F5; break;
        case ksF6: ksm = SDLK_F6; break;
        case ksF7: ksm = SDLK_F7; break;
        case ksF8: ksm = SDLK_F8; break;
        case ksF9: ksm = SDLK_F9; break;
        case ksF10: ksm = SDLK_F10; break;
        case ksF11: ksm = SDLK_F11; break;
        case ksF12: ksm = SDLK_F12; break;
        case ksF13: ksm = SDLK_F13; break;
        case ksF14: ksm = SDLK_F14; break;
        case ksF15: ksm = SDLK_F15; break;
        case ksNum: ksm = SDLK_NUMLOCK; break;
        case ksScroll: ksm = SDLK_SCROLLOCK; break;
        case ksKp0: ksm = SDLK_KP0; break;
        case ksKp1: ksm = SDLK_KP1; break;
        case ksKp2: ksm = SDLK_KP2; break;
        case ksKp3: ksm = SDLK_KP3; break;
        case ksKp4: ksm = SDLK_KP4; break;
        case ksKp5: ksm = SDLK_KP5; break;
        case ksKp6: ksm = SDLK_KP6; break;
        case ksKp7: ksm = SDLK_KP7; break;
        case ksKp8: ksm = SDLK_KP8; break;
        case ksKp9: ksm = SDLK_KP9; break;
        case ksKpSub: ksm = SDLK_KP_MINUS; break;
        case ksKpAdd: ksm = SDLK_KP_PLUS; break;
        case ksKpDec: ksm = SDLK_KP_PERIOD; break;
        case ksRCtrl: ksm = SDLK_RCTRL; break;
        case ksKpEquals: ksm = SDLK_KP_EQUALS; break;
        case ksKpDiv: ksm = SDLK_KP_DIVIDE; break;
        case ksSysRq: ksm = SDLK_SYSREQ; break;
        case ksRMenu: ksm = SDLK_RALT; break;
        case ksPause: ksm = SDLK_PAUSE; break;
        case ksHome: ksm = SDLK_HOME; break;
        case ksUp: ksm = SDLK_UP; break;
        case ksPgUp: ksm = SDLK_PAGEUP; break;
        case ksLeft: ksm = SDLK_LEFT; break;
        case ksRight: ksm = SDLK_RIGHT; break;
        case ksEnd: ksm = SDLK_END; break;
        case ksDown: ksm = SDLK_DOWN; break;
        case ksPgDown: ksm = SDLK_PAGEDOWN; break;
        case ksIns: ksm = SDLK_INSERT; break;
        case ksDel: ksm = SDLK_DELETE; break;
        case ksPower: ksm = SDLK_POWER; break;
        case ksLWin: ksm = SDLK_LSUPER; break;
        case ksRWin: ksm = SDLK_RSUPER; break;
        default: ksm = SDLK_a; break;
      }
      return ksm;
    }

    KeySym SDLToKeysym(SDLKey key)
    {
      KeySym ksm;
      switch(key) {
        case SDLK_BACKSPACE: ksm = ksBack; break;
        case SDLK_TAB: ksm = ksTab; break;
        case SDLK_RETURN: ksm = ksReturn; break;
        case SDLK_PAUSE: ksm = ksPause; break;
        case SDLK_ESCAPE: ksm = ksEscape; break;
        case SDLK_SPACE: ksm = ksSpace; break;
        case SDLK_QUOTE: ksm = ksApos; break;
        case SDLK_COMMA: ksm = ksComma; break;
        case SDLK_MINUS: ksm = ksMinus; break;
        case SDLK_PERIOD: ksm = ksPer; break;
        case SDLK_SLASH: ksm = ksSlash; break;
        case SDLK_0: ksm = ks0; break;
        case SDLK_1: ksm = ks1; break;
        case SDLK_2: ksm = ks2; break;
        case SDLK_3: ksm = ks3; break;
        case SDLK_4: ksm = ks4; break;
        case SDLK_5: ksm = ks5; break;
        case SDLK_6: ksm = ks6; break;
        case SDLK_7: ksm = ks7; break;
        case SDLK_8: ksm = ks8; break;
        case SDLK_9: ksm = ks9; break;
        case SDLK_SEMICOLON: ksm = ksSemicol; break;
        case SDLK_EQUALS: ksm = ksEquals; break;
        case SDLK_LEFTBRACKET: ksm = ksLBrack; break;
        case SDLK_BACKSLASH: ksm = ksBacksl; break;
        case SDLK_RIGHTBRACKET: ksm = ksRBrack; break;
        case SDLK_BACKQUOTE: ksm = ksGrave; break;
        case SDLK_a: ksm = ksA; break;
        case SDLK_b: ksm = ksB; break;
        case SDLK_c: ksm = ksC; break;
        case SDLK_d: ksm = ksD; break;
        case SDLK_e: ksm = ksE; break;
        case SDLK_f: ksm = ksF; break;
        case SDLK_g: ksm = ksG; break;
        case SDLK_h: ksm = ksH; break;
        case SDLK_i: ksm = ksI; break;
        case SDLK_j: ksm = ksJ; break;
        case SDLK_k: ksm = ksK; break;
        case SDLK_l: ksm = ksL; break;
        case SDLK_m: ksm = ksM; break;
        case SDLK_n: ksm = ksN; break;
        case SDLK_o: ksm = ksO; break;
        case SDLK_p: ksm = ksP; break;
        case SDLK_q: ksm = ksQ; break;
        case SDLK_r: ksm = ksR; break;
        case SDLK_s: ksm = ksS; break;
        case SDLK_t: ksm = ksT; break;
        case SDLK_u: ksm = ksU; break;
        case SDLK_v: ksm = ksV; break;
        case SDLK_w: ksm = ksW; break;
        case SDLK_x: ksm = ksX; break;
        case SDLK_y: ksm = ksY; break;
        case SDLK_z: ksm = ksZ; break;
        case SDLK_DELETE: ksm = ksDel; break;
        case SDLK_KP0: ksm = ksKp0; break;
        case SDLK_KP1: ksm = ksKp1; break;
        case SDLK_KP2: ksm = ksKp2; break;
        case SDLK_KP3: ksm = ksKp3; break;
        case SDLK_KP4: ksm = ksKp4; break;
        case SDLK_KP5: ksm = ksKp5; break;
        case SDLK_KP6: ksm = ksKp6; break;
        case SDLK_KP7: ksm = ksKp7; break;
        case SDLK_KP8: ksm = ksKp8; break;
        case SDLK_KP9: ksm = ksKp9; break;
        case SDLK_KP_PERIOD: ksm = ksKpDec; break;
        case SDLK_KP_DIVIDE: ksm = ksKpDiv; break;
        case SDLK_KP_MULTIPLY: ksm = ksKpMul; break;
        case SDLK_KP_MINUS: ksm = ksKpSub; break;
        case SDLK_KP_PLUS: ksm = ksKpAdd; break;
        case SDLK_KP_ENTER: ksm = ksKpEnter; break;
        case SDLK_KP_EQUALS: ksm = ksKpEquals; break;
        case SDLK_UP: ksm = ksUp; break;
        case SDLK_DOWN: ksm = ksDown; break;
        case SDLK_RIGHT: ksm = ksRight; break;
        case SDLK_LEFT: ksm = ksLeft; break;
        case SDLK_INSERT: ksm = ksIns; break;
        case SDLK_HOME: ksm = ksHome; break;
        case SDLK_END: ksm = ksEnd; break;
        case SDLK_PAGEUP: ksm = ksPgUp; break;
        case SDLK_PAGEDOWN: ksm = ksPgDown; break;
        case SDLK_F1: ksm = ksF1; break;
        case SDLK_F2: ksm = ksF2; break;
        case SDLK_F3: ksm = ksF3; break;
        case SDLK_F4: ksm = ksF4; break;
        case SDLK_F5: ksm = ksF5; break;
        case SDLK_F6: ksm = ksF6; break;
        case SDLK_F7: ksm = ksF7; break;
        case SDLK_F8: ksm = ksF8; break;
        case SDLK_F9: ksm = ksF9; break;
        case SDLK_F10: ksm = ksF10; break;
        case SDLK_F11: ksm = ksF11; break;
        case SDLK_F12: ksm = ksF12; break;
        case SDLK_F13: ksm = ksF13; break;
        case SDLK_F14: ksm = ksF14; break;
        case SDLK_F15: ksm = ksF15; break;
        case SDLK_NUMLOCK: ksm = ksNum; break;
        case SDLK_CAPSLOCK: ksm = ksCap; break;
        case SDLK_SCROLLOCK: ksm = ksScroll; break;
        case SDLK_RSHIFT: ksm = ksRShift; break;
        case SDLK_LSHIFT: ksm = ksLShift; break;
        case SDLK_RCTRL: ksm = ksRCtrl; break;
        case SDLK_LCTRL: ksm = ksLCtrl; break;
        case SDLK_RALT: ksm = ksRMenu; break;
        case SDLK_LALT: ksm = ksLMenu; break;
        case SDLK_LSUPER: ksm = ksLWin; break;
        case SDLK_RSUPER: ksm = ksRWin; break;
        case SDLK_SYSREQ: ksm = ksSysRq; break;
        case SDLK_POWER: ksm = ksPower; break;
        default: ksm = ksNone;
      }
      return ksm;
    }

  }

  int EventFilter(const SDL_Event *event)
  {
    if(event == NULL)
      return 0;

    if(event->type == SDL_MOUSEMOTION) {
      sMouseDelta.x += event->motion.xrel;
      sMouseDelta.y += event->motion.yrel;
      sMousePos.x = event->motion.x;
      sMousePos.y = event->motion.y;
      return 0;
    }

    return event->type == SDL_QUIT || event->type == SDL_MOUSEBUTTONDOWN ||
      event->type == SDL_MOUSEBUTTONUP || event->type == SDL_KEYDOWN ||
      event->type == SDL_KEYUP;
  }

 /*
  *  InputInternals class
  */

  const uint maxButtons = 5;

  class Input::InputInternals {
    public:
      typedef std::deque<InputEvent> InputQueue;

    public:
      InputQueue iqueue;

      bool buttons[maxButtons];

    public:
      InputInternals();
      ~InputInternals();

      void ProcessEvent(const SDL_Event & event);

      friend int EventFilter(const SDL_Event *event);
  };

  Input::InputInternals::InputInternals()
  {
    SDL_EventState(SDL_ACTIVEEVENT, SDL_IGNORE);
    SDL_EventState(SDL_JOYAXISMOTION, SDL_IGNORE);
    SDL_EventState(SDL_JOYBALLMOTION, SDL_IGNORE);
    SDL_EventState(SDL_JOYHATMOTION, SDL_IGNORE);
    SDL_EventState(SDL_JOYBUTTONDOWN, SDL_IGNORE);
    SDL_EventState(SDL_JOYBUTTONUP, SDL_IGNORE);
    SDL_EventState(SDL_VIDEORESIZE, SDL_IGNORE);
    SDL_EventState(SDL_VIDEOEXPOSE, SDL_IGNORE);
    SDL_EventState(SDL_USEREVENT, SDL_IGNORE);
    SDL_EventState(SDL_SYSWMEVENT, SDL_IGNORE);

//     SDL_EnableUNICODE(1);
    SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

    // init values
    sMousePos.Set(0.0f, 0.0f);
    sMouseDelta.Set(0.0f, 0.0f);

    std::fill(buttons, buttons + maxButtons, false);

    SDL_SetEventFilter(EventFilter);
  }

  Input::InputInternals::~InputInternals()
  {
    SDL_SetEventFilter(NULL);
  }

  void Input::InputInternals::ProcessEvent(const SDL_Event & event)
  {
    InputEvent ev;
    switch(event.type) {
      case SDL_QUIT:
        ev.type = etQuit;
        iqueue.push_back(ev);
        break;
      case SDL_MOUSEBUTTONDOWN:
        if(event.button.button > 0 && event.button.button <= maxButtons) {
          buttons[event.button.button - 1] = true;
          ev.type = etButtonOn;
          ev.button = event.button.button - 1;
          iqueue.push_back(ev);
        }
        break;
      case SDL_MOUSEBUTTONUP:
        if(event.button.button > 0 && event.button.button <= 3) {
          buttons[event.button.button - 1] = false;
          ev.type = etButtonOff;
          ev.button = event.button.button - 1;
          iqueue.push_back(ev);
        }
        break;
      case SDL_KEYDOWN:
        ev.type = etKeyOn;
        ev.key.sym = SDLToKeysym(event.key.keysym.sym);
        ev.key.chr = event.key.keysym.sym > 255 ?
          0 : static_cast<char>(event.key.keysym.sym);
        iqueue.push_back(ev);
        break;
      case SDL_KEYUP:
        ev.type = etKeyOff;
        ev.key.sym = SDLToKeysym(event.key.keysym.sym);
        ev.key.chr = event.key.keysym.sym > 255 ?
          0 : static_cast<char>(event.key.keysym.sym);
        iqueue.push_back(ev);
        break;
    }
  }

 /*
  *  Input class implementation
  */

  Input & sInput(void)
  {
    static Input input;

    return input;
  }

  Input::Input() : internals_(new InputInternals)
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Initializing input." << std::endl;
#endif /* LOG_SINGLETONS */
  }

  Input::~Input()
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Releasing input." << std::endl;
#endif /* LOG_SINGLETONS */
    delete internals_;
  }

  const Vector2f Input::GetPosition(void)
  {
    return sMousePos;
  }

  const Vector2f Input::GetPositionChange(void)
  {
    Vector2f res = sMouseDelta;
    sMouseDelta.Set(0.0f, 0.0f);
    return res;
  }

  void Input::SetPosition(const Vector2f & v)
  {
    sMouseDelta.Set(0.0f, 0.0f);
    sMouseDelta = v;
  }

  bool Input::GetButtonState(uint id)
  {
    if(id < maxButtons) {
      return internals_->buttons[id];
    } else {
      vLog << warn("Input")
           << "Indexing unavailable mouse button." << std::endl;
    }
    return false;
  }

  bool Input::GetCharacterState(char chr)
  {
    uint8 * keystate = SDL_GetKeyState(NULL);
    return (keystate[SDLK_a+tolower(chr)] == 1);
  }

  bool Input::GetKeysymState(KeySym sym)
  {
    uint8 * keystate = SDL_GetKeyState(NULL);
    return (keystate[KeysymToSDL(sym)] == 1);
  }

  KeyMod Input::GetKeyMod(void)
  {
    return SDL_GetModState();
  }

  void Input::PumpEvents(void)
  {
    SDL_PumpEvents();

    // process events and fill up the queue
    SDL_Event event;
    while(SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_ALLEVENTS) > 0) {
      internals_->ProcessEvent(event);
    }
  }

  void Input::DropEvents(void)
  {
    internals_->iqueue.resize(0);
  }

  InputEvent Input::GetNextEvent(void)
  {
    InputEvent event;

    if(internals_->iqueue.empty()) {
      event.type = etNone;
    } else {
      event = internals_->iqueue.front();
      internals_->iqueue.pop_front();
    }
    return event;
  }
}
