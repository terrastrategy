//
//  Copyright (C) 2007-2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file volumes.cpp
 *
 *  Ble.
 */

#include <numeric>
#include <limits>
#include <algorithm>

#include "math/volumes.h"

#include "vfs/logfile.h"

#include "memory/mmgr.h"

// TODO: unify >= < etc.

namespace tre {

 /*
  *  Local helper functions
  */
  namespace {

    inline bool TestSphereAABBox(const BndSphere & bsp, const AABBox & bbox)
    {
      Vector3f tc(bsp.center - bbox.center);
      Vector3f th(bbox.halfSize + bsp.radius);

      tc.x = fabs(tc.x);
      tc.y = fabs(tc.y);
      tc.z = fabs(tc.z);

      if(tc.x > th.x || tc.y > th.y || tc.z > th.z)
        return false;

      // test other cases when sphere *doesn't* intersect the box
      if(tc.x > bbox.halfSize.x) {
        if(tc.y > bbox.halfSize.y) {
          if(tc.z > bbox.halfSize.z) {
            // x && y && z - test against point
            return (tc - bbox.halfSize).LengthSquared() <=
              bsp.radius * bsp.radius;
          } else {
            // x && y - test against line by masking one coordinate
            Vector2f v(tc.x - bbox.halfSize.x, tc.y - bbox.halfSize.y);
            return v.LengthSquared() <= bsp.radius * bsp.radius;
          }
        } else if(tc.z > bbox.halfSize.z) {
          // x && z - test against line
          Vector2f v(tc.x - bbox.halfSize.x, tc.z - bbox.halfSize.z);
          return v.LengthSquared() <= bsp.radius * bsp.radius;
        }
      } else if(tc.y > bbox.halfSize.y && tc.z > bbox.halfSize.z) {
        // y && z
        Vector2f v(tc.y - bbox.halfSize.y, tc.z - bbox.halfSize.z);
        return v.LengthSquared() <= bsp.radius * bsp.radius;
      }
      return true;
    }

    inline bool TestSphereOBBox(const BndSphere & bsp, const OBBox & bbox)
    {
      // this method is not as accurate as it should be, it reports certains
      // spheres as being 'hit', even if they are not
      return fabs(Distance(bbox.centralPlanes[0], bsp.center)) <=
              bsp.radius + bbox.halfSize.x &&
            fabs(Distance(bbox.centralPlanes[1], bsp.center)) <=
              bsp.radius + bbox.halfSize.y &&
            fabs(Distance(bbox.centralPlanes[2], bsp.center)) <=
              bsp.radius + bbox.halfSize.z;
    }

    inline bool TestSpherePVolume
    (const BndSphere & bsp, const PVolume & pvol)
    {
      // this method is not as accurate as it should be, it reports certains
      // spheres as being 'hit', even if they are not
      // (although this is probably the most common implementation)
//       float a = Distance(pvol.clipPlane[0], bsp.center);
//       float b = Distance(pvol.clipPlane[1], bsp.center);
//       float c = Distance(pvol.clipPlane[2], bsp.center);
//       float d = Distance(pvol.clipPlane[3], bsp.center);
//       float e = Distance(pvol.clipPlane[4], bsp.center);
//       float f = Distance(pvol.clipPlane[5], bsp.center);
      return Distance(pvol.clipPlane[0], bsp.center) >= -bsp.radius &&
            Distance(pvol.clipPlane[1], bsp.center) >= -bsp.radius &&
            Distance(pvol.clipPlane[2], bsp.center) >= -bsp.radius &&
            Distance(pvol.clipPlane[3], bsp.center) >= -bsp.radius &&
            Distance(pvol.clipPlane[4], bsp.center) >= -bsp.radius &&
            Distance(pvol.clipPlane[5], bsp.center) >= -bsp.radius;
    }

    inline bool TestAABBoxOBBox(const AABBox & bbox1, const OBBox & bbox2)
    {
      Vector3f axis_x(bbox2.centralPlanes[0].n * bbox2.halfSize.x);
      Vector3f axis_y(bbox2.centralPlanes[1].n * bbox2.halfSize.y);
      Vector3f axis_z(bbox2.centralPlanes[2].n * bbox2.halfSize.z);

      if(bbox1.TestCollision(bbox2.center + axis_x + axis_y + axis_z) ||
        bbox1.TestCollision(bbox2.center - axis_x + axis_y + axis_z) ||
        bbox1.TestCollision(bbox2.center + axis_x - axis_y + axis_z) ||
        bbox1.TestCollision(bbox2.center - axis_x - axis_y + axis_z) ||
        bbox1.TestCollision(bbox2.center + axis_x + axis_y - axis_z) ||
        bbox1.TestCollision(bbox2.center - axis_x + axis_y - axis_z) ||
        bbox1.TestCollision(bbox2.center + axis_x - axis_y - axis_z) ||
        bbox1.TestCollision(bbox2.center - axis_x - axis_y - axis_z)) {
        return true;
      }

      if(bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z)) ||
        bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z)) ||
        bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z)) ||
        bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z)) ||
        bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z)) ||
        bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z)) ||
        bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z)) ||
        bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                                          bbox1.center.y + bbox1.halfSize.y,
                                          bbox1.center.z + bbox1.halfSize.z))) {
        return true;
      }
      return false;
    }

    inline bool TestAABBoxPVolume(const AABBox & bbox, const PVolume & pvol)
    {
      // this method is not as accurate as it should be, it reports certains
      // spheres as being intersected, even if they are not
      // FIXME: write better function
      return Distance(pvol.clipPlane[0], bbox.center) >= -bbox.radius &&
            Distance(pvol.clipPlane[1], bbox.center) >= -bbox.radius &&
            Distance(pvol.clipPlane[2], bbox.center) >= -bbox.radius &&
            Distance(pvol.clipPlane[3], bbox.center) >= -bbox.radius &&
            Distance(pvol.clipPlane[4], bbox.center) >= -bbox.radius &&
            Distance(pvol.clipPlane[5], bbox.center) >= -bbox.radius;
    }

    inline bool TestOBBoxPVolume(const OBBox & bbox, const PVolume & pvol)
    {
      // TODO: write better function
      vLog << warn("Collision") << "Testing obbox vs frustum. "
              "Accurate algorithm not implemented yet." << std::endl;
      return bbox.SphereCollision(pvol);
    }

    inline int CollisionSphereAABBox(const BndSphere & bsp, const AABBox & bbox)
    {
      Vector3f tc(bsp.center - bbox.center);
      Vector3f th(bbox.halfSize + bsp.radius);

      tc.x = fabs(tc.x);
      tc.y = fabs(tc.y);
      tc.z = fabs(tc.z);

      if(tc.x <= th.x && tc.y <= th.y && tc.z <= th.z)
        return false;
      if(TestSphereAABBox(bsp, bbox)) {
        if(tc.x <= th.x && tc.y <= th.y && tc.z <= th.z) {
          return 2;
        } else {
          // get the corner which is furthest from sphere's center
          tc.x = std::max(fabs(tc.x + bbox.halfSize.x),
                          fabs(tc.x - bbox.halfSize.x));
          tc.y = std::max(fabs(tc.y + bbox.halfSize.y),
                          fabs(tc.y - bbox.halfSize.y));
          tc.z = std::max(fabs(tc.z + bbox.halfSize.z),
                          fabs(tc.z - bbox.halfSize.z));
          if(tc.LengthSquared() <= bsp.radius * bsp.radius)
            return 1;
        }
        return -4;
      }
      return 0;
    }

    inline int CollisionSphereOBBox(const BndSphere & bsp, const OBBox & bbox)
    {
      float fx = fabs(Distance(bbox.centralPlanes[0], bsp.center));
      float fy = fabs(Distance(bbox.centralPlanes[1], bsp.center));
      float fz = fabs(Distance(bbox.centralPlanes[2], bsp.center));

      if(fx <= bsp.radius + bbox.halfSize.x &&
        fy <= bsp.radius + bbox.halfSize.y &&
        fz <= bsp.radius + bbox.halfSize.z) {
        if(fx <= bbox.halfSize.x - bsp.radius &&
          fy <= bbox.halfSize.y - bsp.radius &&
          fz <= bbox.halfSize.z - bsp.radius) {
          return 2;
        } else {
          Vector3f axis_x(bbox.centralPlanes[0].n * bbox.halfSize.x);
          Vector3f axis_y(bbox.centralPlanes[1].n * bbox.halfSize.y);
          Vector3f axis_z(bbox.centralPlanes[2].n * bbox.halfSize.z);
          uint ptcol;
          float rr = bsp.radius * bsp.radius;

          ptcol = (bbox.center + axis_x + axis_y
                   + axis_z).LengthSquared() <= rr;
          ptcol += (bbox.center - axis_x + axis_y
                    + axis_z).LengthSquared() <= rr;
          ptcol += (bbox.center + axis_x - axis_y
                    + axis_z).LengthSquared() <= rr;
          ptcol += (bbox.center - axis_x - axis_y
                    + axis_z).LengthSquared() <= rr;
          ptcol += (bbox.center + axis_x + axis_y
                    - axis_z).LengthSquared() <= rr;
          ptcol += (bbox.center - axis_x + axis_y
                    - axis_z).LengthSquared() <= rr;
          ptcol += (bbox.center + axis_x - axis_y
                    - axis_z).LengthSquared() <= rr;
          ptcol += (bbox.center - axis_x - axis_y
                    - axis_z).LengthSquared() <= rr;

          if(ptcol == 0)
            return 1;
        }
        return -4;
      }
      return 0;
    }

    inline int CollisionSpherePVolume
    (const BndSphere & bsp, const PVolume & pvol)
    {
      float d0 = Distance(pvol.clipPlane[0], bsp.center);
      float d1 = Distance(pvol.clipPlane[1], bsp.center);
      float d2 = Distance(pvol.clipPlane[2], bsp.center);
      float d3 = Distance(pvol.clipPlane[3], bsp.center);
      float d4 = Distance(pvol.clipPlane[4], bsp.center);
      float d5 = Distance(pvol.clipPlane[5], bsp.center);

      if(d0 >= -bsp.radius && d1 >= -bsp.radius && d2 >= -bsp.radius &&
        d3 >= -bsp.radius && d4 >= -bsp.radius && d5>= -bsp.radius) {
        if(d0 >= bsp.radius && d1 >= bsp.radius && d2 >= bsp.radius &&
          d3 >= bsp.radius && d4 >= bsp.radius && d5>= bsp.radius) {
          return 2;
        } else {
  //         if(volume in) {
            // TODO: write correct function
            vLog << warn("Collision") << "Testing sphere vs frustum. "
                    "Accurate algorithm not implemented yet." << std::endl;
            return bsp.SphereIntersection(pvol);
  //         }
        }
        return -4;
      }
      return 0;
    }

    inline int CollisionAABBoxOBBox(const AABBox & bbox1, const OBBox & bbox2)
    {
      Vector3f axis_x(bbox2.centralPlanes[0].n * bbox2.halfSize.x);
      Vector3f axis_y(bbox2.centralPlanes[1].n * bbox2.halfSize.y);
      Vector3f axis_z(bbox2.centralPlanes[2].n * bbox2.halfSize.z);

      uint ptcol;
      ptcol = bbox1.TestCollision(bbox2.center + axis_x + axis_y + axis_z);
      ptcol += bbox1.TestCollision(bbox2.center - axis_x + axis_y + axis_z);
      ptcol += bbox1.TestCollision(bbox2.center + axis_x - axis_y + axis_z);
      ptcol += bbox1.TestCollision(bbox2.center - axis_x - axis_y + axis_z);
      ptcol += bbox1.TestCollision(bbox2.center + axis_x + axis_y - axis_z);
      ptcol += bbox1.TestCollision(bbox2.center - axis_x + axis_y - axis_z);
      ptcol += bbox1.TestCollision(bbox2.center + axis_x - axis_y - axis_z);
      ptcol += bbox1.TestCollision(bbox2.center - axis_x - axis_y - axis_z);

      if(ptcol > 0)
        return ptcol == 8 ? 1 : -4;

      ptcol = bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                       bbox1.center.y + bbox1.halfSize.y,
                                           bbox1.center.z + bbox1.halfSize.z));
      ptcol += bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                      bbox1.center.y + bbox1.halfSize.y,
                      bbox1.center.z + bbox1.halfSize.z));
      ptcol += bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                      bbox1.center.y + bbox1.halfSize.y,
                      bbox1.center.z + bbox1.halfSize.z));
      ptcol += bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                      bbox1.center.y + bbox1.halfSize.y,
                      bbox1.center.z + bbox1.halfSize.z));
      ptcol += bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                      bbox1.center.y + bbox1.halfSize.y,
                      bbox1.center.z + bbox1.halfSize.z));
      ptcol += bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                      bbox1.center.y + bbox1.halfSize.y,
                      bbox1.center.z + bbox1.halfSize.z));
      ptcol += bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                      bbox1.center.y + bbox1.halfSize.y,
                      bbox1.center.z + bbox1.halfSize.z));
      ptcol += bbox2.TestCollision(Vector3f(bbox1.center.x + bbox1.halfSize.x,
                      bbox1.center.y + bbox1.halfSize.y,
                      bbox1.center.z + bbox1.halfSize.z));
      if(ptcol > 0)
        return ptcol == 8 ? 2 : -4;

      return 0;
    }

    inline int CollisionAABBoxPVolume(const AABBox & bbox, const PVolume & pvol)
    {
      // TODO: write better function
      vLog << warn("Collision") << "Testing obbox vs frustum. "
              "Accurate algorithm not implemented yet." << std::endl;
      return bbox.SphereIntersection(pvol);
    }

    inline int CollisionOBBoxPVolume(const OBBox & bbox, const PVolume & pvol)
    {
      // TODO: write better function
      vLog << warn("Collision") << "Testing obbox vs frustum. "
              "Accurate algorithm not implemented yet." << std::endl;
      return bbox.SphereIntersection(pvol);
    }

  }

 /*
  *  Global functions
  */

  //  simple distance
  float Distance(const BndVolume & bv1,const BndVolume & bv2)
  {
    return (bv1.center - bv2.center).Length() - bv1.radius - bv2.radius;
  }

 /*
  *  Bounding volumes implementation
  */
  bool BndVolume::SphereCollision(const BndVolume & bvol) const
  {
    Vector3f tmpv(center);
    tmpv -= bvol.center;
    float r = radius + bvol.radius;
    return tmpv.LengthSquared() <= r * r;
  }

  bool BndVolume::SphereCollision(const Vector3f & v) const
  {
    Vector3f tmpv(center - v);
    return tmpv.LengthSquared() <= radius * radius;
  }

  // for return values see comments
  int BndVolume::SphereIntersection(const BndVolume & bvol) const
  {
    Vector3f tmpv(center);
    tmpv -= bvol.center;
    float ls = tmpv.LengthSquared();
    if(ls <= (radius + bvol.radius) * (radius + bvol.radius)) {
      if(radius > bvol.radius) {
        // 1: this object contains the other
        // -4: objects intersect
        return ls <= (radius - bvol.radius) * (radius - bvol.radius) ? 1 : -4;
      } else {
        return ls <= (bvol.radius - radius) * (bvol.radius - radius) ? 2 : -4;
      }
    }
    // no collision
    return 0;
  }

  bool BndVolume::TestCollision(const Vector3f & v) const
  {
    return SphereCollision(v);
  }

  bool BndVolume::TestCollision(const BndSphere & bsp) const
  {
    return SphereCollision(bsp);
  }

  bool BndVolume::TestCollision(const AABBox & bbox) const
  {
    return SphereCollision(bbox);
  }

  bool BndVolume::TestCollision(const OBBox & bbox) const
  {
    return SphereCollision(bbox);
  }

  bool BndVolume::TestCollision(const PVolume & pvol) const
  {
    return SphereCollision(pvol);
  }

  // more thorough testing
  int BndVolume::TestIntersection(const BndSphere & bsp) const
  {
    return SphereIntersection(bsp);
  }

  int BndVolume::TestIntersection(const AABBox & bbox) const
  {
    return SphereIntersection(bbox);
  }

  int BndVolume::TestIntersection(const OBBox & bbox) const
  {
    return SphereIntersection(bbox);
  }

  int BndVolume::TestIntersection(const PVolume & pvol) const
  {
    return SphereIntersection(pvol);
  }

 /*
  *  BndSphere implementation
  */
  void BndSphere::CreateFromData(const Float3Vector & data)
  {
// TODO: here
  }

  void BndSphere::SetTransform(const Transformf & t)
  {
    center = t.GetWTransform() * baseCenter_;

    // if transform doesn't change scale, then the radius will remain the same
    if(t.HasScale()) {
      if(!t.GetWParent() || !t.GetWParent()->HasScale()) {
        radius = baseRadius_ * std::max(t.GetOScale().x,
          std::max(t.GetOScale().y, t.GetOScale().z));
      } else {
        // nasty! avoid nesting of scale transforms
        radius = baseRadius_ * std::max(t.GetWTransform().c1.Length(),
          std::max(t.GetWTransform().c2.Length(),
          t.GetWTransform().c3.Length()));
      }
    }
  }

  void BndSphere::ResetTransform(void)
  {
    center = baseCenter_;
    radius = baseRadius_;
  }

  bool BndSphere::TestCollision(const BndVolume & bvol) const
  {
    return bvol.TestCollision(*this);
  }

  bool BndSphere::TestCollision(const AABBox & bbox) const
  {
  return TestSphereAABBox(*this, bbox);
  }

  bool BndSphere::TestCollision(const OBBox & bbox) const
  {
  return TestSphereOBBox(*this, bbox);
  }

  bool BndSphere::TestCollision(const PVolume & pvol) const
  {
  return TestSpherePVolume(*this, pvol);
  }

  int BndSphere::TestIntersection(const BndVolume & bvol) const
  {
    int res = bvol.TestIntersection(*this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int BndSphere::TestIntersection(const AABBox & bbox) const
  {
  return CollisionSphereAABBox(*this, bbox);
  }

  int BndSphere::TestIntersection(const OBBox & bbox) const
  {
  return CollisionSphereOBBox(*this, bbox);
  }

  int BndSphere::TestIntersection(const PVolume & pvol) const
  {
  return CollisionSpherePVolume(*this, pvol);
  }

  void BndSphere::Set(const Vector3f & c, float r)
  {
    center = baseCenter_ = c;
    radius = baseRadius_ = r;
  }

 /*
  *  Axis aligned bounding box implementation
  */
  void AABBox::CreateFromData(const Float3Vector & data)
  {
// TODO: here
  }

  void AABBox::SetTransform(const Transformf & t)
  {
    center = t.GetWTransform() * baseCenter_;

    // transform all axes
    Vector3f ax1(t.GetWTransform().c1);
    Vector3f ax2(t.GetWTransform().c2);
    Vector3f ax3(t.GetWTransform().c3);

    ax1 -= t.GetWPosition();
    ax2 -= t.GetWPosition();
    ax3 -= t.GetWPosition();

    ax1 *= baseHSize_.x;
    ax2 *= baseHSize_.y;
    ax3 *= baseHSize_.z;

    Vector3f th1 = ax1;
    th1 += ax2;
    th1 += ax3;
    Vector3f th2 = -ax1;
    th2 += ax2;
    th2 += ax3;
    Vector3f th3 = -ax1;
    th3 -= ax2;
    th3 += ax3;
    Vector3f th4 = ax1;
    th4 -= ax2;
    th4 += ax3;

    th1.Set(std::max(fabs(th1.x), fabs(th2.x)),
            std::max(fabs(th1.y), fabs(th2.y)),
            std::max(fabs(th1.z), fabs(th2.z)));
    th2.Set(std::max(fabs(th3.x), fabs(th4.x)),
            std::max(fabs(th3.y), fabs(th4.y)),
            std::max(fabs(th3.z), fabs(th4.z)));
    halfSize.Set(std::max(fabs(th1.x), fabs(th2.x)),
                 std::max(fabs(th1.y), fabs(th2.y)),
                 std::max(fabs(th1.z), fabs(th2.z)));

    radius = halfSize.Length();
  }

  void AABBox::ResetTransform(void)
  {
    center = baseCenter_;
    halfSize = baseHSize_;
    radius = halfSize.Length();
  }

  int AABBox::GetQuadrant(const BndVolume & bvol) const
  {
    Vector3f tc(bvol.center - center);

    int index = 0;

    if(fabs(tc.x) <= halfSize.x + bvol.radius &&
       fabs(tc.y) <= halfSize.y + bvol.radius &&
       fabs(tc.z) <= halfSize.z + bvol.radius) {
      // X coordinate
      if(tc.x >= bvol.radius && tc.x <= halfSize.x - bvol.radius)
        index |= 0x1;
      else if(tc.x > -bvol.radius || tc.x < -halfSize.x + bvol.radius)
        return -1;
      // Y coordinate
      if(tc.y >= bvol.radius && tc.y <= halfSize.y - bvol.radius)
        index |= 0x2;
      else if(tc.y > -bvol.radius || tc.y < -halfSize.y + bvol.radius)
        return -1;
      // Z coordinate
      if(tc.z >= bvol.radius && tc.z <= halfSize.z - bvol.radius)
        index |= 0x4;
      else if(tc.z > -bvol.radius || tc.z < -halfSize.z + bvol.radius)
        return -1;
    } else {
      // is or sticks outside
      return -2;
    }
    return index;
  }

  bool AABBox::TestCollision(const BndVolume & bvol) const
  {
    return bvol.TestCollision(*this);
  }

  bool AABBox::TestCollision(const Vector3f & v) const
  {
    return fabs(v.x - center.x) <= halfSize.x &&
           fabs(v.y - center.y) <= halfSize.x &&
           fabs(v.z - center.z) <= halfSize.z;
  }

  bool AABBox::TestCollision(const BndSphere & bsp) const
  {
    return TestSphereAABBox(bsp, *this);
  }

  bool AABBox::TestCollision(const AABBox & bbox) const
  {
    Vector3f tmph(halfSize + bbox.halfSize);

    return fabs(bbox.center.x - center.x) <= tmph.x &&
           fabs(bbox.center.y - center.y) <= tmph.x &&
           fabs(bbox.center.z - center.z) <= tmph.z;
  }

  bool AABBox::TestCollision(const OBBox & bbox) const
  {
    return TestAABBoxOBBox(*this, bbox);
  }

  bool AABBox::TestCollision(const PVolume & pvol) const
  {
    return TestAABBoxPVolume(*this, pvol);
  }

  int AABBox::TestIntersection(const BndVolume & bvol) const
  {
    int res = bvol.TestIntersection(*this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int AABBox::TestIntersection(const BndSphere & bsp) const
  {
    int res = CollisionSphereAABBox(bsp, *this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int AABBox::TestIntersection(const AABBox & bbox) const
  {
    if(this->TestCollision(bbox)) {
      Vector3f tc(bbox.center - center);
      tc.x = fabs(tc.x);
      tc.y = fabs(tc.y);
      tc.z = fabs(tc.z);

      if(tc.x + bbox.halfSize.x <= halfSize.x &&
         tc.y + bbox.halfSize.y <= halfSize.y &&
         tc.z + bbox.halfSize.z <= halfSize.z) {
        return 2;
      } else if(tc.x + halfSize.x <= bbox.halfSize.x &&
                tc.y + halfSize.y <= bbox.halfSize.y &&
                tc.z + halfSize.z <= bbox.halfSize.z) {
        return 1;
      }
      return -4;
    }
    return 0;
  }

  int AABBox::TestIntersection(const OBBox & bbox) const
  {
    return CollisionAABBoxOBBox(*this, bbox);
  }

  int AABBox::TestIntersection(const PVolume & pvol) const
  {
    return CollisionAABBoxPVolume(*this, pvol);
  }

  void AABBox::Set(const Vector3f & c, const Vector3f & hs)
  {
    center = baseCenter_ = c;
    halfSize = baseHSize_ = hs;
    radius = hs.Length();
  }

 /*
  *  Object bounding box implementation
  */
  OBBox::OBBox(const Vector3f & c, const Vector3f & hs,
    const Vector3f & d, const Vector3f & u, const Vector3f & r)
    : BndVolume(c, hs.Length()), halfSize(hs), baseCenter_(c),
      baseHSize_(hs), baseDir_(d), baseUp_(u), baseRight_(r)
  {
    centralPlanes[0].n = r;
    centralPlanes[0].d = Dot(r, c);

    centralPlanes[1].n = d;
    centralPlanes[1].d = Dot(d, c);

    centralPlanes[2].n = u;
    centralPlanes[2].d = Dot(u, c);
  }

  void OBBox::CreateFromData(const Float3Vector & data)
  {
// TODO: here!
  }

  void OBBox::SetTransform(const Transformf & t)
  {
    center = t.GetWTransform() * baseCenter_;

    centralPlanes[0].n = t.GetWTransform() * baseRight_;
    centralPlanes[0].n -= t.GetWPosition();

    centralPlanes[1].n = t.GetWTransform() * baseDir_;
    centralPlanes[1].n -= t.GetWPosition();

    centralPlanes[2].n = t.GetWTransform() * baseUp_;
    centralPlanes[2].n -= t.GetWPosition();

    if(t.HasScale()) {
      float l1 = centralPlanes[0].n.Length();
      float l2 = centralPlanes[1].n.Length();
      float l3 = centralPlanes[2].n.Length();

      halfSize.Set(l1, l2, l3);

      centralPlanes[0].n /= l1;
      centralPlanes[1].n /= l2;
      centralPlanes[2].n /= l3;

      radius = halfSize.Length();
    }

    centralPlanes[0].d = Dot(centralPlanes[0].n, center);
    centralPlanes[1].d = Dot(centralPlanes[1].n, center);
    centralPlanes[2].d = Dot(centralPlanes[2].n, center);
  }

  void OBBox::ResetTransform(void)
  {
    center = baseCenter_;

    centralPlanes[0].n = baseRight_;
    centralPlanes[0].d = Dot(baseRight_, center);

    centralPlanes[1].n = baseDir_;
    centralPlanes[1].d = Dot(baseDir_, center);

    centralPlanes[2].n = baseUp_;
    centralPlanes[2].d = Dot(baseUp_, center);

    if(halfSize != baseHSize_) {
      halfSize = baseHSize_;
      radius = halfSize.Length();
    }
  }

  bool OBBox::TestCollision(const BndVolume & bvol) const
  {
    return bvol.TestCollision(*this);
  }

  bool OBBox::TestCollision(const Vector3f & v) const
  {
    return fabs(Distance(v, centralPlanes[0])) <= halfSize.x &&
           fabs(Distance(v, centralPlanes[1])) <= halfSize.y &&
           fabs(Distance(v, centralPlanes[2])) <= halfSize.z;
  }

  bool OBBox::TestCollision(const BndSphere & bsp) const
  {
    return TestSphereOBBox(bsp, *this);
  }

  bool OBBox::TestCollision(const AABBox & bbox) const
  {
    return TestAABBoxOBBox(bbox, *this);
  }

  bool OBBox::TestCollision(const OBBox & bbox) const
  {
    Vector3f axis_x(bbox.centralPlanes[0].n * bbox.halfSize.x);
    Vector3f axis_y(bbox.centralPlanes[1].n * bbox.halfSize.y);
    Vector3f axis_z(bbox.centralPlanes[2].n * bbox.halfSize.z);

    if(TestCollision(bbox.center + axis_x + axis_y + axis_z) ||
       TestCollision(bbox.center - axis_x + axis_y + axis_z) ||
       TestCollision(bbox.center + axis_x - axis_y + axis_z) ||
       TestCollision(bbox.center - axis_x - axis_y + axis_z) ||
       TestCollision(bbox.center + axis_x + axis_y - axis_z) ||
       TestCollision(bbox.center - axis_x + axis_y - axis_z) ||
       TestCollision(bbox.center + axis_x - axis_y - axis_z) ||
       TestCollision(bbox.center - axis_x - axis_y - axis_z)) {
      return true;
    }

    axis_x = centralPlanes[0].n * halfSize.x;
    axis_y = centralPlanes[1].n * halfSize.y;
    axis_z = centralPlanes[2].n * halfSize.z;

    if(bbox.TestCollision(center + axis_x + axis_y + axis_z) ||
       bbox.TestCollision(center - axis_x + axis_y + axis_z) ||
       bbox.TestCollision(center + axis_x - axis_y + axis_z) ||
       bbox.TestCollision(center - axis_x - axis_y + axis_z) ||
       bbox.TestCollision(center + axis_x + axis_y - axis_z) ||
       bbox.TestCollision(center - axis_x + axis_y - axis_z) ||
       bbox.TestCollision(center + axis_x - axis_y - axis_z) ||
       bbox.TestCollision(center - axis_x - axis_y - axis_z)) {
      return true;
    }
    return false;
  }

  bool OBBox::TestCollision(const PVolume & pvol) const
  {
    return TestOBBoxPVolume(*this, pvol);
  }

  int OBBox::TestIntersection(const BndVolume & bvol) const
  {
    int res = bvol.TestIntersection(*this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int OBBox::TestIntersection(const BndSphere & bsp) const
  {
    int res = CollisionSphereOBBox(bsp, *this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int OBBox::TestIntersection(const AABBox & bbox) const
  {
    int res = CollisionAABBoxOBBox(bbox, *this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int OBBox::TestIntersection(const OBBox & bbox) const
  {
    Vector3f axis_x(bbox.centralPlanes[0].n * bbox.halfSize.x);
    Vector3f axis_y(bbox.centralPlanes[1].n * bbox.halfSize.y);
    Vector3f axis_z(bbox.centralPlanes[2].n * bbox.halfSize.z);

    uint ptcol;

    ptcol = TestCollision(bbox.center + axis_x + axis_y + axis_z);
    ptcol += TestCollision(bbox.center - axis_x + axis_y + axis_z);
    ptcol += TestCollision(bbox.center + axis_x - axis_y + axis_z);
    ptcol += TestCollision(bbox.center - axis_x - axis_y + axis_z);
    ptcol += TestCollision(bbox.center + axis_x + axis_y - axis_z);
    ptcol += TestCollision(bbox.center - axis_x + axis_y - axis_z);
    ptcol += TestCollision(bbox.center + axis_x - axis_y - axis_z);
    ptcol += TestCollision(bbox.center - axis_x - axis_y - axis_z);

    if(ptcol > 0)
      return ptcol == 8 ? 1 : -4;

    axis_x = centralPlanes[0].n * halfSize.x;
    axis_y = centralPlanes[1].n * halfSize.y;
    axis_z = centralPlanes[2].n * halfSize.z;

    ptcol = bbox.TestCollision(center + axis_x + axis_y + axis_z);
    ptcol += bbox.TestCollision(center - axis_x + axis_y + axis_z);
    ptcol += bbox.TestCollision(center + axis_x - axis_y + axis_z);
    ptcol += bbox.TestCollision(center - axis_x - axis_y + axis_z);
    ptcol += bbox.TestCollision(center + axis_x + axis_y - axis_z);
    ptcol += bbox.TestCollision(center - axis_x + axis_y - axis_z);
    ptcol += bbox.TestCollision(center + axis_x - axis_y - axis_z);
    ptcol += bbox.TestCollision(center - axis_x - axis_y - axis_z);

    if(ptcol > 0)
      return ptcol == 8 ? 2 : -4;

    return 0;
  }

  int OBBox::TestIntersection(const PVolume & pvol) const
  {
    return CollisionOBBoxPVolume(*this, pvol);
  }

  void OBBox::Set(const Vector3f & c, const Vector3f & hs,
    const Vector3f & d, const Vector3f & r, const Vector3f & u)
  {
    center = c;
    halfSize = baseHSize_ = hs;
    radius = hs.Length();

    centralPlanes[0].n = r;
    centralPlanes[0].d = Dot(r, c);

    centralPlanes[1].n = d;
    centralPlanes[1].d = Dot(d, c);

    centralPlanes[2].n = u;
    centralPlanes[1].d = Dot(u, c);
  }

 /*
  *  Projection volume implementation
  */
  PVolume::PVolume() : eye_(0.0f), dir_(0.0f, 0.0f, -1.0f),
    right_(1.0f, 0.0f, 0.0f), up_(0.0f, 1.0f, 0.0f)
  {
  }

  void PVolume::SetTransform(const Transformf & t)
  {
    if(t.HasScale() && (t.GetOScale().x < 0.0f ||
      t.GetOScale().y < 0.0f || t.GetOScale().z < 0.0f)) {
      vLog << warn("Collision") << "Mirroring projection volumes might "
              "produce incorrect results." << std::endl;
    }

    eye_ = t.GetWPosition();
    dir_ = -t.GetObjectZ();
    right_ = t.GetObjectX();
    up_ = t.GetObjectY();

    if(type_ == ptPerspective) {
      UpdatePerspective();
    } else {
      UpdateOrtho();
    }
  }

  void PVolume::ResetTransform(void)
  {
    eye_.Set(0.0f, 0.0f, 0.0f);
    dir_.Set(0.0f, 0.0f, -1.0f);
    right_.Set(1.0f, 0.0f, 0.0f);
    up_.Set(0.0f, 1.0f, 0.0f);

    if(type_ == ptPerspective) {
      UpdatePerspective();
    } else {
      UpdateOrtho();
    }
  }

  bool PVolume::TestCollision(const BndVolume & bvol) const
  {
    return bvol.TestCollision(*this);
  }

  bool PVolume::TestCollision(const Vector3f & v) const
  {
    return Distance(clipPlane[0], v) > 0 && Distance(clipPlane[1], v) > 0 &&
           Distance(clipPlane[2], v) > 0 && Distance(clipPlane[3], v) > 0 &&
           Distance(clipPlane[4], v) > 0 && Distance(clipPlane[5], v) > 0;
  }

  bool PVolume::TestCollision(const BndSphere & bsp) const
  {
    return TestSpherePVolume(bsp, *this);
  }

  bool PVolume::TestCollision(const AABBox & bbox) const
  {
    return TestAABBoxPVolume(bbox, *this);
  }

  bool PVolume::TestCollision(const OBBox & bbox) const
  {
    return TestOBBoxPVolume(bbox, *this);
  }

  int PVolume::TestIntersection(const BndVolume & bvol) const
  {
    int res = bvol.TestIntersection(*this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int PVolume::TestIntersection(const BndSphere & bsp) const
  {
    int res = CollisionSpherePVolume(bsp, *this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int PVolume::TestIntersection(const AABBox & bbox) const
  {
    int res = CollisionAABBoxPVolume(bbox, *this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  int PVolume::TestIntersection(const OBBox & bbox) const
  {
    int res = CollisionOBBoxPVolume(bbox, *this);
    res ^= 0x3;   // swap 1 and 2
    return res;
  }

  void PVolume::SetPerspective(float fov, float asp, float near, float far)
  {
    type_ = ptPerspective;

    float w, h;
    float tan_ah = tanf(Deg2Rad(fov / 2.0f));

    // points required to determine distance
    w = near * tan_ah * asp;
    h = near * tan_ah;

    proj_.left = -w;
    proj_.right = w;
    proj_.bottom = -h;
    proj_.top = h;
    proj_.near = near;
    proj_.far = far;

    UpdatePerspective();
  }

  void PVolume::SetOrtho
  (float l, float r, float b, float t, float n, float f)
  {
    type_ = ptOrtho;

    proj_.left = l;
    proj_.right = r;
    proj_.bottom = b;
    proj_.top = t;
    proj_.near = n;
    proj_.far = f;

    UpdateOrtho();
  }

  void PVolume::UpdatePerspective(void)
  {
    // points required to determine distance
    Vector3f p1(eye_), p2(eye_);
    p1 += dir_ * proj_.near;
    p1 += right_ * proj_.left;
    p1 += up_ * proj_.bottom;
    p2 += dir_ * proj_.far;
    p2 += right_ * proj_.right;
    p2 -= up_ * proj_.top;

    // center & radius
    center = eye_;
    center += dir_ * ((proj_.near + proj_.far) / 2.0f);

    radius = (p2 - center).Length();

    // left
    clipPlane[0].n.Cross(dir_ * proj_.near + right_ * proj_.left, up_);
    clipPlane[0].n.Normalize();
    clipPlane[0].d = Dot(clipPlane[0].n, p1);

    // right
    clipPlane[1].n.Cross(up_, dir_ * proj_.near + right_ * proj_.right);
    clipPlane[1].n.Normalize();
    clipPlane[1].d = Dot(clipPlane[1].n, p2);

    // bottom
    clipPlane[2].n.Cross(right_, dir_ * proj_.near + up_ * proj_.bottom);
    clipPlane[2].n.Normalize();
    clipPlane[2].d = Dot(clipPlane[2].n, p2);

    // top
    clipPlane[3].n.Cross(dir_ * proj_.near + up_ * proj_.top, right_);
    clipPlane[3].n.Normalize();
    clipPlane[3].d = Dot(clipPlane[3].n, p1);

    // near
    clipPlane[4].n = dir_;
    clipPlane[4].d = Dot(dir_, eye_ + dir_ * proj_.near);

    // far
    clipPlane[5].n = -dir_;
    clipPlane[5].d = Dot(-dir_, eye_ + dir_ * proj_.far);
  }

  void PVolume::UpdateOrtho(void)
  {
    // center & radius
    center = dir_ * ((proj_.near + proj_.far) / 2.0f);
    center += right_ * ((proj_.left + proj_.right) / 2.0f);
    center += up_ * ((proj_.top + proj_.bottom) / 2.0f);

    Vector3f v = dir_ * proj_.far;
    v += right_ * proj_.right;
    v += up_ * proj_.top;
    v -= center;
    radius = v.Length();

    // left
    clipPlane[0].n = right_;
    clipPlane[0].d = Dot(right_, eye_ - right_ * proj_.left);

    // right
    clipPlane[1].n = -right_;
    clipPlane[1].d = Dot(-right_, eye_ + right_ * proj_.right);

    // bottom
    clipPlane[2].n = up_;
    clipPlane[2].d = Dot(up_, eye_ - up_ * proj_.bottom);

    // top
    clipPlane[3].n = -up_;
    clipPlane[3].d = Dot(-up_, eye_ + up_ * proj_.top);

    // near
    clipPlane[4].n = dir_;
    clipPlane[4].d = Dot(dir_, eye_ + dir_ * proj_.near);

    // far
    clipPlane[5].n = -dir_;
    clipPlane[5].d = Dot(-dir_, eye_ + dir_ * proj_.far);
  }

}
