//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file cg_effect.cpp
 *
 *  Ble.
 */

#include <GL/glew.h>
#include <Cg/cgGL.h>

#include <sstream>
#include <algorithm>
#include <string.h>

#ifdef BUILD_CG_PREPROCESSOR
#  include <boost/wave.hpp>
#  include <boost/wave/cpplexer/cpp_lex_token.hpp>
#  include <boost/wave/cpplexer/cpp_lex_iterator.hpp>
#  include <boost/wave/util/iteration_context.hpp>
#endif /* BUILD_CG_PREPROCESSOR */

#include "exception.h"

#include "vfs/logfile.h"
#include "vfs/fstream.h"
#include "vfs/interface.h"

#include "renderer/effect.h"
#include "renderer/texture.h"
#include "renderer/renderer.h"

#include "cg_effect.h"
#include "cg_renderer.h"

#include "gl_texture.h"

#include "memory/mmgr.h"

#ifdef BUILD_CG_PREPROCESSOR
// preprocessor input policy
namespace boost {
  namespace wave {
    namespace iteration_context_policies {

      struct vfs_file_to_string {
        template <typename IterContext>
        class inner {
          public:
            template <typename PositionT>
            static void init_iterators(IterContext &iter_ctx,
              PositionT const &act_pos, language_support language)
            {
              typedef typename IterContext::iterator_type iterator_type;

              // read in the file
              tre::FileIStream file(iter_ctx.filename.c_str());
              if(!file.is_open()) {
                  BOOST_WAVE_THROW_CTX(iter_ctx.ctx, preprocess_exception,
                      bad_include_file, iter_ctx.filename.c_str(), act_pos);
                  return;
              }
              file.unsetf(std::ios::skipws);

              iter_ctx.instring.assign(
                std::istreambuf_iterator<char>(file.rdbuf()),
                std::istreambuf_iterator<char>());

              iter_ctx.first = iterator_type(iter_ctx.instring.begin(),
                iter_ctx.instring.end(), PositionT(iter_ctx.filename),
                language);
              iter_ctx.last = iterator_type();
            }
          private:
            std::string instring;
        };
      };

    }
  }
}
#endif /* BUILD_CG_PREPROCESSOR */

namespace tre {

  EffectVars::EffectVars()
  {
  }

  EffectVars::~EffectVars()
  {
  }

  void EffectVars::SetTexture(const std::string & name, const std::string & var)
  {
    TexturePtr tex = Texture::Factory().CreateInstance(var);
    SetTexture(name, tex);
  }

  void EffectVars::SetTexture(const std::string & name, const TexturePtr & var)
  {
    SVarVector::iterator pos = std::lower_bound(
      samplers.begin(), samplers.end(), name);

    if(pos == samplers.end() || pos->first != name) {
      samplers.insert(pos, std::make_pair(name, var));
    } else {
      pos->second = var;
    }
  }

  CgEffect::EffectParameter::EffectParameter(const std::string & n,
    CGparameter p, EffectVarType t) : name(n), param(p), type(t)
  {
//     memset(&var, 0, sizeof(var));
  }

  const EffectPtr EffectFactory::CreateInstance(const std::string & name)
  {
    EffectMap::iterator pos = map_.find(name);

    if(pos != map_.end() && !pos->second.expired())
      return pos->second.lock();

    if(pos == map_.end())
      pos = map_.insert(pos, std::make_pair(name, EffectPtr()));

    DEBUG_ASSERT(pos != map_.end());

    EffectPtr res;
    try {
      // need to pass the name stored in the element
      res = EffectPtr(new CgEffect(pos->first));

      vLog << ok("Cg") << "Effect loaded successfully ("
           << name << ")." << std::endl;
    } catch(Exception & exc) {
      vLog << err("Cg") << "Unable to load effect (" + name + ").\nReason: "
           << exc.what() << std::endl;
    }
    pos->second = res;
    return res;
  }

  CgEffect::CgEffect(const std::string & filename)
  {
#ifdef BUILD_CG_PREPROCESSOR
    // read the while effect file into memory
    FileIStream file(filename);
    if(!file.is_open())
      throw(Exception("Error opening file."));

    std::string input(std::istreambuf_iterator<char>(file.rdbuf()),
        std::istreambuf_iterator<char>());

    // preprocess the cgfx file using Wave preprocessor library
    typedef boost::wave::cpplexer::lex_iterator<
      boost::wave::cpplexer::lex_token<> > lex_iterator_type;
    typedef boost::wave::iteration_context_policies::vfs_file_to_string
      input_policy_type;
    typedef boost::wave::context<std::string::iterator,
      lex_iterator_type, input_policy_type> context_type;

    context_type ctx(input.begin(), input.end(), filename.c_str());

//         ctx.add_include_path("...");
//         ctx.add_macro_definition(...);

    std::stringstream ss;

    // iterate over the tokens
    for(context_type::iterator_type itr = ctx.begin();
        itr != ctx.end(); ++itr) {
      ss << (*itr).get_value();
    }

    effect_ = cgCreateEffect(static_cast<CgRenderer&>(sRenderer()
      ).GetCgContext(), ss.str().c_str(), NULL);
#else /* BUILD_CG_PREPROCESSOR */
    effect_ = cgCreateEffectFromFile(static_cast<CgRenderer&>(sRenderer()
      ).GetCgContext(), sVfs().TranslatePath(filename).c_str(), NULL);
#endif /* BUILD_CG_PREPROCESSOR */

    if(!effect_) {
      const char * str = cgGetLastListing(
        static_cast<CgRenderer&>(sRenderer()).GetCgContext());
      throw(Exception(str ? str : "Error reading from file?"));
    }

    // find the first available technique
    technique_ = cgGetFirstTechnique(effect_);
    while(technique_ && cgValidateTechnique(technique_) == CG_FALSE) {
      vLog << warn("Cg") << "Technique " << cgGetTechniqueName(technique_)
           << " did not validate. Skipping." << std::endl;
      technique_ = cgGetNextTechnique(technique_);
    }
    if(technique_) {
      vLog << info("Cg") << "Using technique " << cgGetTechniqueName(technique_)
           << "." << std::endl;
    } else {
      throw(Exception("No valid technique."));
    }

    // enumerate effect parameters
    CGparameter par = cgGetFirstEffectParameter(effect_);
    while(par) {
      const char * sem = cgGetParameterSemantic(par);

      // internals
      if(sem && *sem) {
        if(strcasecmp(sem, "Time") == 0) {
          const char * str;

          CGannotation ann = cgGetNamedParameterAnnotation(par, "type");

          if(!ann || !(str = cgGetStringAnnotationValue(ann)) ||
             strcmp(str, "system") != 0) {
            internals_.push_back(std::make_pair(itTime, par));
          } else {
            internals_.push_back(std::make_pair(itSysTime, par));
          }
        } else if(strcasecmp(sem, "ModelViewProjection") == 0) {
          CheckMatrixVar(itMVPMatrix, par);
        } else if(strcasecmp(sem, "ModelView") == 0) {
          CheckMatrixVar(itMVMatrix, par);
        } else if(strcasecmp(sem, "Projection") == 0) {
          CheckMatrixVar(itPMatrix, par);
        } else if(strcasecmp(sem, "Texture") == 0) {
          CheckMatrixVar(itTMatrix, par);
        }
      // user uniforms
      } else {
        const char * name = cgGetParameterName(par);

        switch(cgGetParameterType(par)) {
          case CG_FLOAT:
          case CG_HALF:
          case CG_FIXED:
          case CG_FLOAT1:
          case CG_HALF1:
          case CG_FIXED1:
            params_.push_back(EffectParameter(name, par, vtFloat1));
            break;
          case CG_FLOAT2:
          case CG_HALF2:
          case CG_FIXED2:
            params_.push_back(EffectParameter(name, par, vtFloat2));
            break;
          case CG_FLOAT3:
          case CG_HALF3:
          case CG_FIXED3:
            params_.push_back(EffectParameter(name, par, vtFloat3));
            break;
          case CG_FLOAT4:
          case CG_HALF4:
          case CG_FIXED4:
            params_.push_back(EffectParameter(name, par, vtFloat4));
            break;
          case CG_INT:
          case CG_BOOL:
          case CG_INT1:
          case CG_BOOL1:
            params_.push_back(EffectParameter(name, par, vtInt1));
            break;
          case CG_INT2:
          case CG_BOOL2:
            params_.push_back(EffectParameter(name, par, vtInt2));
            break;
          case CG_INT3:
          case CG_BOOL3:
            params_.push_back(EffectParameter(name, par, vtInt3));
            break;
          case CG_INT4:
          case CG_BOOL4:
            params_.push_back(EffectParameter(name, par, vtInt4));
            break;
          case CG_SAMPLER1D:
          case CG_SAMPLER2D:
          case CG_SAMPLER3D:
          case CG_SAMPLERRECT:
          case CG_SAMPLERCUBE:
            params_.push_back(EffectParameter(name, par, vtTexture));
            break;
          // ignore the rest
          default:
            break;
        }
      }
      par = cgGetNextParameter(par);
    }
    std::sort(internals_.begin(), internals_.end());
    std::sort(params_.begin(), params_.end());
  }

  CgEffect::~CgEffect()
  {
    cgDestroyEffect(effect_);
  }

  void CgEffect::SetParameters(const EffectVars & vars) const
  {
    // floats
    for(FVarVector::const_iterator itr = vars.fvars.begin();
        itr != vars.fvars.end(); ++itr) {
      ChangeFloatVariable(itr->first, itr->second);
    }

    // ints
    for(IVarVector::const_iterator itr = vars.ivars.begin();
        itr != vars.ivars.end(); ++itr) {
      ChangeIntVariable(itr->first, itr->second);
    }

    // textures
    for(SVarVector::const_iterator itr = vars.samplers.begin();
        itr != vars.samplers.end(); ++itr) {
      ChangeSamplerVariable(itr->first, itr->second);
    }
  }

  void CgEffect::ChangeFloatVariable
  (const std::string & name, const FxFVar & value) const
  {
    ParamVector::const_iterator var = std::lower_bound(
      params_.begin(), params_.end(), name);

    if(var == params_.end() || var->name != name)
      return;

    if(var->type >= vtFloat1 && var->type <= vtFloat4)
      cgSetParameterValuefr(var->param, 4, value.data());
  }

  void CgEffect::ChangeIntVariable
  (const std::string & name, const FxIVar & value) const
  {
    ParamVector::const_iterator var = std::lower_bound(
      params_.begin(), params_.end(), name);

    if(var == params_.end() || var->name != name)
      return;

    if(var->type >= vtInt1 && var->type <= vtInt4)
      cgSetParameterValueir(var->param, 4, value.data());
  }

  void CgEffect::ChangeSamplerVariable
  (const std::string & name, const TexturePtr & value) const
  {
    ParamVector::const_iterator var = std::lower_bound(
      params_.begin(), params_.end(), name);

    if(var == params_.end() || var->name != name)
      return;

    if(var->type == vtTexture)
      cgGLSetupSampler(var->param,
        static_cast<GLTexture*>(value.get())->GetHandle());
  }

  void CgEffect::CheckMatrixVar(InternalsType base, CGparameter par)
  {
    bool inv = false, trans = false;
    const CGbool * vals;
    CGannotation ann;
    int n;

    ann = cgGetNamedParameterAnnotation(par, "inverse");
    if(ann && (vals = cgGetBoolAnnotationValues(ann, &n)) && n > 0)
      inv = vals[0];

    ann = cgGetNamedParameterAnnotation(par, "transpose");
    if(ann && (vals = cgGetBoolAnnotationValues(ann, &n)) && n > 0)
      trans = vals[0];

    if(inv && trans) {
      internals_.push_back(std::make_pair(
        static_cast<InternalsType>(base + 3), par));
    } else if(inv) {
      internals_.push_back(std::make_pair(
        static_cast<InternalsType>(base + 1), par));
    } else if(trans) {
      internals_.push_back(std::make_pair(
        static_cast<InternalsType>(base + 2), par));
    } else {
      internals_.push_back(std::make_pair(base, par));
    }
  }
}
