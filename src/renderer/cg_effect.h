//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file cg_effect.h
 *
 *  Ble.
 */

#pragma once

#include <Cg/cg.h>

#include <string>
#include <vector>

#include "renderer/effect.h"

namespace tre {

 /*
  *  Cg Effect class
  */
  class CgEffect : public Effect {
    public:
      typedef std::pair<InternalsType, CGparameter> InternalsPair;
      typedef std::vector<InternalsPair> InternalsVector;

    public:
      CgEffect(const std::string & filename);
      ~CgEffect();

      const CGeffect & GetEffect(void) const {return effect_;}
      const CGtechnique & GetTechnique(void) const {return technique_;}

      const InternalsVector & GetInternals(void) const {return internals_;}

      void SetParameters(const EffectVars & vars) const;

    private:
      struct EffectParameter {
        std::string name;
        CGparameter param;
        EffectVarType type;

        EffectParameter(const std::string & n, CGparameter p, EffectVarType t);

        bool operator < (const EffectParameter & r) const
        {
          return name < r.name;
        }

        bool operator < (const std::string & r) const
        {
          return name < r;
        }

        friend bool operator <
        (const std::string & l, const EffectParameter & r)
        {
          return l < r.name;
        }
      };

      typedef std::vector<EffectParameter> ParamVector;

    private:
      CGeffect effect_;
      CGtechnique technique_;

      ParamVector params_;
      InternalsVector internals_;

    private:
      void ChangeFloatVariable(const std::string & name,
        const FxFVar & value) const;
      void ChangeIntVariable(const std::string & name,
        const FxIVar & value) const;
      void ChangeSamplerVariable(const std::string & name,
        const TexturePtr & value) const;

      void CheckMatrixVar(InternalsType base, CGparameter par);
  };

}
