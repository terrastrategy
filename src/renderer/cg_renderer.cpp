//
//  Copyright (C) 2007-2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file cg_renderer.cpp
 *
 *  blahblah
 */

#include <GL/glew.h>

#include <Cg/cgGL.h>

#include <SDL/SDL.h>

#include "platform.h"
#include "exception.h"

#include "vfs/logfile.h"

#include "math/volumes.h"
#include "core/timer.h"

#include "core/client/display.h"

#include "gl_buffers.h"
#include "gl_canvas.h"
#include "cg_effect.h"
#include "cg_renderer.h"

namespace tre {

  Renderer & sRenderer(void)
  {
    static CgRenderer r;
    return r;
  }

  bool Renderer::CheckErrors
  (const char * msg/*=NULL*/, bool confirmOK/*=false*/)
  {
    GLenum error = glGetError();

    if(error != GL_NO_ERROR)
      vLog << err("GL");
    else if(confirmOK)
      vLog << ok("GL");

    if(msg && (error != GL_NO_ERROR || confirmOK))
      vLog << msg << ": ";

    switch(error) {
      case GL_INVALID_ENUM:
         vLog << "Invalid enum!" << std::endl;
        return false;
      case GL_INVALID_VALUE:
        vLog << "Invalid value!" << std::endl;
        return false;
      case GL_INVALID_OPERATION:
        vLog << "Invalid operation!" << std::endl;
        return false;
      case GL_STACK_OVERFLOW:
        vLog << "Stack overflow!" << std::endl;
        return false;
      case GL_STACK_UNDERFLOW:
        vLog << "Stack underflow!" << std::endl;
        return false;
      case GL_OUT_OF_MEMORY:
        vLog << "Out of memory!" << std::endl;
        return false;
      case GL_TABLE_TOO_LARGE:
        vLog << "Table too large!" << std::endl;
        return false;
      case GL_INVALID_FRAMEBUFFER_OPERATION_EXT:
        vLog << "Invalid framebuffer operation!" << std::endl;
        return false;
      case GL_NO_ERROR:
        if(confirmOK)
          vLog << "No error." << std::endl;
        return true;
      default:
        vLog << "Unknown error!" << std::endl;
        return false;
    }
  }

 /*
  *  CgRenderer class implementation
  */
  bool CgRenderer::CheckFramebufferStatus(bool confirmOK/*=false*/)
  {
    GLenum stat = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    switch(stat) {
      case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
        vLog << err("GL")
             << "Framebuffer incomplete, missing attachment!" << std::endl;
        return false;
      case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
        vLog << err("GL") << "Framebuffer incomplete, "
                "attached images must have same dimension!" << std::endl;
        return false;
      case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
        vLog << err("GL") << "Framebuffer incomplete, attached "
                "images must have same format!" << std::endl;
        return false;
      case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
        vLog << err("GL")
             << "Framebuffer incomplete, missing draw buffer!" << std::endl;
        return false;
      case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
        vLog << err("GL")
             << "Framebuffer incomplete, missing read buffer!" << std::endl;
        return false;
      case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
        vLog << err("GL")
             << "Unsupported framebuffer format!" << std::endl;
        return false;
      case GL_FRAMEBUFFER_COMPLETE_EXT:
        if(confirmOK)
          vLog << ok("GL") << "Framebuffer is complete!" << std::endl;
        return false;
      default:
        vLog << err("GL")
             << "Unknown framebuffer status error!" << std::endl;
        return false;
    }
  }

  bool CgRenderer::CheckCgError(bool confirmOK/*=false*/)
  {
    CGerror error = cgGetError();
    if(error != CG_NO_ERROR) {
      vLog << ok("Cg") << cgGetErrorString(error) << std::endl;
      return false;
    } else if(confirmOK) {
      vLog << ok("Cg") << "No error." << std::endl;
    }
    return true;
  }

  CgRenderer::CgRenderer()
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Initializing renderer." << std::endl;
#endif /* LOG_SINGLETONS */
  }

  CgRenderer::~CgRenderer()
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Releasing renderer." << std::endl;
#endif /* LOG_SINGLETONS */
  }

  void CgRenderer::Init(void)
  {
    const char * glstr;

    glstr = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    vLog << info("GL") << "Driver vendor: " << glstr << std::endl;

    glstr = reinterpret_cast<const char*>(glGetString(GL_RENDERER));
    vLog << info("GL") << "Renderer: " << glstr << std::endl;

    glstr = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    vLog << info("GL") << "Version: " << glstr << std::endl;

    glstr = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));

    vLog << info("GL")
         << "Available OpenGL Extensions:\n" << glstr << "\n" << std::endl;

    GLenum error = glewInit();

    if (error != GLEW_OK) {
      // Problem: glewInit failed, something is seriously wrong.
      vLog << err("GL") << glewGetErrorString(error) << std::endl;
      throw(Exception("Error intializing display"));
    }

    if(!GLEW_VERSION_2_0) {
      vLog << err("GL") << "Your OpenGL 2.0 not supported. "
              "Check if you have latest drivers installed." << std::endl;
      throw(Exception("Error intializing display"));
    } else {
      vLog << ok("GL") << "OpenGL 2.0 present." << std::endl;
    }
    if(!GLEW_EXT_framebuffer_object) {
      vLog << err("GL")
           << "Framebuffer object support missing." << std::endl;
      throw(Exception("Error intializing display"));
    } else {
      vLog << ok("GL") << "Framebuffer objects supported." << std::endl;
    }
    if(!GLEW_ARB_texture_float) {
      vLog << warn("GL") << "No floating point texture support." << std::endl;
    } else {
      vLog << ok("GL") << "Floating point textures supported." << std::endl;
    }
    if(!GLEW_ARB_half_float_pixel) {
      vLog << warn("GL")
           << "Half precision floating point texture support missing."
           << std::endl;
    } else {
      vLog << ok("GL")
           << "Half precision floating point textures supported." << std::endl;
    }
    if(!GLEW_ARB_texture_non_power_of_two) {
      vLog << err("GL")
           << "Non power of two texture support missing." << std::endl;
    } else {
      vLog << ok("GL") << "Non power of two textures supported." << std::endl;
    }
    // nvidia float textures

    // cg context
    context_ = cgCreateContext();
    cgGLRegisterStates(context_);
    cgGLSetManageTextureParameters(context_, GL_TRUE);
  }

  void CgRenderer::NewFrame(void)
  {
    // reset frame stats
    frame.triangles = 0;
    frame.batches = 0;
    frame.time = Timer::GetSystemTime();
    ++frame.count;

    cstack_.resize(0);
  }

  void CgRenderer::FlushFrame(void)
  {
    glFlush();

    frame.time = Timer::GetSystemTime() - frame.time;

#ifdef DEBUG
    CheckErrors(__FUNCTION__);
    CheckFramebufferStatus();
    CheckCgError();
#endif /* DEBUG */
  }

  void CgRenderer::PresentFrame(void)
  {
    SDL_GL_SwapBuffers();
  }

  void CgRenderer::SetCamera(const PVolume & cam)
  {
    DEBUG_ASSERT(!cstack_.empty());

    if(cam.GetProjectionType() == ptPerspective) {
      SetCameraPerspective(cam.GetProjection());
    } else {
      SetCameraOrtho(cam.GetProjection());
    }

    cstack_.back().projType = cam.GetProjectionType();
    cstack_.back().projBase = cam.GetProjection();
  }

  void CgRenderer::SetCameraTransform(const Matrix4x4f * trans)
  {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    if(trans)
      glLoadMatrixf(&trans->c1.x);
  }

  void CgRenderer::PushCanvas(const Canvas * cnv)
  {
    glPushMatrix();

    cstack_.push_back(CanvasView());
    cstack_.back().canvas = cnv;

    // set the current canvas and viewport
    glViewport(0, 0, cnv->GetWidth(), cnv->GetHeight());

    // set the framebuffer
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,
      static_cast<const GLCanvas*>(cnv)->GetFBO());

    // clears the render target (background window)
    glClearColor(cnv->bgColour.x, cnv->bgColour.y,
      cnv->bgColour.z, cnv->bgColour.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  }

  void CgRenderer::PopCanvas(void)
  {
    cstack_.pop_back();

    // reset former canvas
    glPopMatrix();

    if(cstack_.empty())
      return;

    // reset the viewport
    glViewport(0, 0, cstack_.back().canvas->GetWidth(),
      cstack_.back().canvas->GetHeight());

    // reset the framebuffer
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,
      static_cast<const GLCanvas*>(cstack_.back().canvas)->GetFBO());

    // reset the camera
    if(cstack_.back().projType == ptPerspective)
      SetCameraPerspective(cstack_.back().projBase);
    else
      SetCameraOrtho(cstack_.back().projBase);
  }

  void CgRenderer::DrawGeometry
  (const GeometryBatch::iterator & begin, const GeometryBatch::iterator & end)
  {
    for(GeometryBatch::iterator pos = begin; pos != end; ++pos) {
      if(!pos->effect)
        continue;

      // set transformation
      glPushMatrix();

      if(pos->trans)
        glMultMatrixf(&pos->trans->c1.x);

      SetupBuiltins(static_cast<const CgEffect*>(pos->effect)->GetInternals());

      // set variables
      if(pos->fxVars)
        static_cast<const CgEffect*>(pos->effect)->SetParameters(*pos->fxVars);

      CGpass pass = cgGetFirstPass(static_cast<const CgEffect*>(
        pos->effect)->GetTechnique());
      while(pass) {
        cgSetPassState(pass);

        RenderMesh(static_cast<const GLAttribBufferSet*>(pos->attribs),
          static_cast<const GLIndexBuffer*>(pos->indices), pos->trans);

        cgResetPassState(pass);
        pass = cgGetNextPass(pass);
      }

      // reset transform
      glPopMatrix();

      // update frame statistics
      ++frame.batches;
      frame.triangles += static_cast<const GLIndexBuffer*>(
        pos->indices)->GetSize() / 3;
    }
  }

  // private rendering functions
  void CgRenderer::SetCameraPerspective(const ProjectionBase & proj)
  {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glFrustum(proj.left, proj.right, proj.bottom,
        proj.top, proj.near, proj.far);

    glMatrixMode(GL_MODELVIEW);
  }

  void CgRenderer::SetCameraOrtho(const ProjectionBase & proj)
  {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(proj.left, proj.right, proj.bottom,
        proj.top, proj.near, proj.far);

    glMatrixMode(GL_MODELVIEW);
  }

  void CgRenderer::SetupBuiltins(const CgEffect::InternalsVector & vars)
  {
    for(CgEffect::InternalsVector::const_iterator pos = vars.begin();
        pos != vars.end(); ++pos) {
      switch(pos->first) {
        case itTime:
        case itSysTime:
          break;
        case itMVPMatrix:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
          break;
        case itMVPMatrixInv:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_INVERSE);
          break;
        case itMVPMatrixTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_TRANSPOSE);
          break;
        case itMVPMatrixInvTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);
          break;
        case itMVMatrix:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
          break;
        case itMVMatrixInv:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE);
          break;
        case itMVMatrixTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_TRANSPOSE);
          break;
        case itMVMatrixInvTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);
          break;
        case itPMatrix:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
          break;
        case itPMatrixInv:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_PROJECTION_MATRIX, CG_GL_MATRIX_INVERSE);
          break;
        case itPMatrixTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_PROJECTION_MATRIX, CG_GL_MATRIX_TRANSPOSE);
          break;
        case itPMatrixInvTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_PROJECTION_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);
          break;
        case itTMatrix:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_TEXTURE_MATRIX, CG_GL_MATRIX_IDENTITY);
          break;
        case itTMatrixInv:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_TEXTURE_MATRIX, CG_GL_MATRIX_INVERSE);
          break;
        case itTMatrixTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_TEXTURE_MATRIX, CG_GL_MATRIX_TRANSPOSE);
          break;
        case itTMatrixInvTrans:
          cgGLSetStateMatrixParameter(pos->second,
            CG_GL_TEXTURE_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);
          break;
        default:
          break;
      }
    }
  }

  void CgRenderer::RenderMesh(const GLAttribBufferSet * attribs,
    const GLIndexBuffer * indices, const Matrix4x4f * trans)
  {
    if(!attribs || !indices)
      return;

    for(uint i = 0; i < 16; ++i) {
      if(attribs->GetBuffer(i) != 0) {
        glBindBufferARB(GL_ARRAY_BUFFER_ARB, attribs->GetBuffer(i));
        if(attribs->GetBufferType(i) >= adUInt1)
          glVertexAttribPointer(i, attribs->GetBufferType(i) - adUInt1 + 1,
            GL_UNSIGNED_INT, false, 0, NULL);
        else
          glVertexAttribPointer(i, attribs->GetBufferType(i) - adFloat1 + 1,
            GL_FLOAT, false, 0, NULL);
        glEnableVertexAttribArray(i);
      } else {
        glDisableVertexAttribArray(i);
      }
    }

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, indices->GetBuffer());
    glDrawRangeElements(GL_TRIANGLES, indices->vFrom, indices->vTo,
      indices->GetSize(), GL_UNSIGNED_INT, NULL);
  }

}
