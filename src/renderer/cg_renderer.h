//
//  Copyright (C) 2007-2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file cg_renderer.h
 *
 *  Encapsulates chosen renderer/display
 */

#pragma once

#include <Cg/cg.h>

#include "math/volumes.h"
#include "renderer/renderer.h"

#include "cg_effect.h"

namespace tre {

  class GLAttribBufferSet;
  class GLIndexBuffer;

  class CgRenderer : public Renderer {
    public:
      static bool CheckFramebufferStatus(bool confirmOK=false);
      static bool CheckCgError(bool confirmOK=false);

    public:
      CgRenderer();
      ~CgRenderer();

      void Init(void);

      void NewFrame(void);
      void FlushFrame(void);
      void PresentFrame(void);

      void SetCamera(const PVolume & cam);
      void SetCameraTransform(const Matrix4x4f * trans);

      void PushCanvas(const Canvas * cnv);
      void PopCanvas(void);

      void DrawGeometry(const GeometryBatch::iterator & begin,
                        const GeometryBatch::iterator & end);

      CGcontext GetCgContext(void) {return context_;}

    private:
      struct CanvasView {
        const Canvas * canvas;
        ProjectionType projType;
        ProjectionBase projBase;
      };

      typedef std::vector<CanvasView> CanvasStack;

    private:
      CGcontext context_;

      CanvasStack cstack_;

    private:
      void SetCameraPerspective(const ProjectionBase & proj);
      void SetCameraOrtho(const ProjectionBase & proj);
      void SetupBuiltins(const CgEffect::InternalsVector & vars);
      void RenderMesh(const GLAttribBufferSet * attribs,
        const GLIndexBuffer * indices, const Matrix4x4f * trans);
  };

}
