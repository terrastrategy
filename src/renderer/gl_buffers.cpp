//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gl_buffers.cpp
 *
 *  blahblah
 */

#include <GL/glew.h>
#include <string.h>

#include "vfs/logfile.h"

#include "gl_buffers.h"

#include "cg_renderer.h"

namespace tre {

  namespace {

    uint GetBaseSize(AttribDataType dat)
    {
      uint res;

      switch(dat) {
        case adFloat1:
          res = sizeof(float);
          break;
        case adFloat2:
          res = sizeof(float) * 2;
          break;
        case adFloat3:
          res = sizeof(float) * 3;
          break;
        case adFloat4:
          res = sizeof(float) * 4;
          break;
        case adUInt1:
          res = sizeof(uint);
          break;
        case adUInt2:
          res = sizeof(uint) * 2;
          break;
        case adUInt3:
          res = sizeof(uint) * 3;
          break;
        case adUInt4:
          res = sizeof(uint) * 4;
          break;
      }
      return res;
    }

  }

 /*
  *  Factories
  */
  AttribBufferSet * AttribBufferSetFactory::CreateInstance(void)
  {
    return new GLAttribBufferSet();
  }

  IndexBuffer * IndexBufferFactory::CreateInstance(void)
  {
    return new GLIndexBuffer();
  }

 /*
  *  GLAttribBufferSet implementation
  */
  GLAttribBufferSet::GLAttribBufferSet()
  {
    std::fill(buffers_, buffers_ + 16, 0);
  }

  GLAttribBufferSet::~GLAttribBufferSet()
  {
    glDeleteBuffers(16, buffers_);
  }

  // initialization & destruction
  void GLAttribBufferSet::Init(AttribType loc, AttribDataType dat,
    uint size, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = dat;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, size * GetBaseSize(dat), NULL,
      mode == amRead ? GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Clear(AttribType loc)
  {
    glDeleteBuffers(1, &buffers_[loc]);
    buffers_[loc] = 0;
  }

  // memory mapping
  void * GLAttribBufferSet::Lock
  (AttribType loc, AccessMode mode/*=amWrite*/, uint offset/*=0*/, uint)
  {
    if(!buffers_[loc]) {
      vLog << warn("GL") << "Attempting to lock unitialized buffer!"
           << std::endl;
      return NULL;
    }

    GLenum access;
    switch(mode) {
      case amRead:
        access = GL_READ_ONLY;
        break;
      case amWrite:
        access = GL_WRITE_ONLY;
        break;
      case amReadWrite:
        access = GL_READ_WRITE;
        break;
    };

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    void * res = glMapBuffer(GL_ARRAY_BUFFER, access);
    if(!res) {
      Renderer::CheckErrors(__FUNCTION__);
      return NULL;
    }
    return reinterpret_cast<byte*>(res) + offset * GetBaseSize(types_[loc]);
  }

  void GLAttribBufferSet::Unlock(AttribType loc)
  {
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glUnmapBuffer(GL_ARRAY_BUFFER);
  }

  // Set automatically initializes (creates) buffer
  void GLAttribBufferSet::Set(AttribType loc,
    const FloatVector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adFloat1;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(float),
      &data.front(), mode == amRead ? GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Set(AttribType loc,
    const Float2Vector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adFloat2;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(float) * 2,
      &data.front().x, mode == amRead ?
      GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Set(AttribType loc,
    const Float3Vector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adFloat3;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(float) * 3,
      &data.front().x, mode == amRead ?
      GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Set(AttribType loc,
    const Float4Vector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adFloat4;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(float) * 4,
      &data.front().x, mode == amRead ?
      GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Set(AttribType loc,
    const UIntVector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adUInt1;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(uint),
      &data.front(), mode == amRead ? GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Set(AttribType loc,
    const UInt2Vector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adUInt2;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(uint) * 2,
      &data.front().x, mode == amRead ?
      GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Set(AttribType loc,
    const UInt3Vector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adUInt3;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(uint) * 3,
      &data.front().x, mode == amRead ?
      GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::Set(AttribType loc,
    const UInt4Vector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffers_[loc])
      glGenBuffers(1, &buffers_[loc]);

    types_[loc] = adUInt4;

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, data.size() * sizeof(uint) * 4,
      &data.front().x, mode == amRead ?
      GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const FloatVector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adFloat1) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(float),
      size * sizeof(float), &data.front());
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const Float2Vector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adFloat2) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(float) * 2,
      size * sizeof(float) * 2, &data.front().x);
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const Float3Vector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adFloat3) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(float) * 3,
      size * sizeof(float) * 3, &data.front().x);
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const Float4Vector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adFloat4) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(float) * 4,
      size * sizeof(float) * 4, &data.front().x);
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const UIntVector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adUInt1) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(uint),
      size * sizeof(uint), &data.front());
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const UInt2Vector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adUInt2) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(uint) * 2,
      size * sizeof(uint) * 2, &data.front().x);
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const UInt3Vector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adUInt3) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(uint) * 3,
      size * sizeof(uint) * 3, &data.front().x);
  }

  void GLAttribBufferSet::UpdateRange(AttribType loc, const UInt4Vector & data,
    uint offset/*=0*/, uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffers_[loc] || types_[loc] != adUInt4) {
      vLog << warn("GL") << "Error updating buffer buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, buffers_[loc]);
    glBufferSubData(GL_ARRAY_BUFFER_ARB, offset * sizeof(uint) * 4,
      size * sizeof(uint) * 4, &data.front().x);
  }

 /*
  *  GLIndexBuffer implementation
  */
  GLIndexBuffer::GLIndexBuffer() : buffer_(0), size_(0)
  {
  }

  GLIndexBuffer::~GLIndexBuffer()
  {
    Clear();
  }

  // initialization & destruction
  void GLIndexBuffer::Init(uint size, AccessMode mode/*=amRead*/)
  {
    if(!buffer_)
      glGenBuffers(1, &buffer_);

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, buffer_);
    glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, size * sizeof(uint), NULL,
      mode == amRead ? GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);

    size_ = size;
  }

  void GLIndexBuffer::Clear(void)
  {
    glDeleteBuffers(1, &buffer_);
    buffer_ = 0;
    size_ = 0;
  }

  // memory mapping
  uint * GLIndexBuffer::Lock
  (AccessMode mode/*=amWrite*/, uint offset/*=0*/, uint)
  {
    if(!buffer_) {
      vLog << warn("GL") << "Attempting to lock unitialized buffer!"
           << std::endl;
      return NULL;
    }

    GLenum access;
    switch(mode) {
      case amRead:
        access = GL_READ_ONLY;
        break;
      case amWrite:
        access = GL_WRITE_ONLY;
        break;
      case amReadWrite:
        access = GL_READ_WRITE;
        break;
    };

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, buffer_);
    uint * res = reinterpret_cast<uint*>(glMapBuffer(
      GL_ELEMENT_ARRAY_BUFFER, access));

    if(!res) {
      Renderer::CheckErrors(__FUNCTION__);
      return NULL;
    }
    return res + offset;
  }

  void GLIndexBuffer::Unlock(void)
  {
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, buffer_);
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
  }

  // Set automatically initializes (creates) buffer
  void GLIndexBuffer::Set(const UIntVector & data, AccessMode mode/*=amRead*/)
  {
    if(!buffer_)
      glGenBuffers(1, &buffer_);

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, buffer_);
    glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, data.size() * sizeof(uint),
      &data.front(), mode == amRead ? GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB);

    size_ = data.size();
  }

  void GLIndexBuffer::UpdateRange(const UIntVector & data, uint offset/*=0*/,
    uint size/*=std::numeric_limits<uint>::max()*/)
  {
    if(!buffer_) {
      vLog << warn("GL") << "Attempting to update unitialized buffer!"
           << std::endl;
      return;
    }

    // update the size argumant if necessary
    if(size > data.size())
      size = data.size();

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, buffer_);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER_ARB, offset * sizeof(uint),
      size * sizeof(uint), &data.front());
  }

}
