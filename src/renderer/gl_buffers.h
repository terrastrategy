//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gl_buffers.h
 *
 *  Ble.
 */

#pragma once

#include "renderer/buffers.h"

namespace tre {

  class GLAttribBufferSet : public AttribBufferSet {
    public:
      GLAttribBufferSet();
      ~GLAttribBufferSet();

      // initialization & destruction
      void Init(AttribType loc, AttribDataType dat,
        uint size, AccessMode mode=amRead);
      void Clear(AttribType loc);

      // memory mapping
      void * Lock(AttribType loc, AccessMode mode=amWrite,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void Unlock(AttribType loc);

      // Set automatically initializes (creates) buffer
      void Set(AttribType loc, const FloatVector & data,
        AccessMode mode=amRead);
      void Set(AttribType loc, const Float2Vector & data,
        AccessMode mode=amRead);
      void Set(AttribType loc, const Float3Vector & data,
        AccessMode mode=amRead);
      void Set(AttribType loc, const Float4Vector & data,
        AccessMode mode=amRead);
      void Set(AttribType loc, const UIntVector & data,
        AccessMode mode=amRead);
      void Set(AttribType loc, const UInt2Vector & data,
        AccessMode mode=amRead);
      void Set(AttribType loc, const UInt3Vector & data,
        AccessMode mode=amRead);
      void Set(AttribType loc, const UInt4Vector & data,
        AccessMode mode=amRead);

      void UpdateRange(AttribType loc, const FloatVector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void UpdateRange(AttribType loc, const Float2Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void UpdateRange(AttribType loc, const Float3Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void UpdateRange(AttribType loc, const Float4Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void UpdateRange(AttribType loc, const UIntVector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void UpdateRange(AttribType loc, const UInt2Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void UpdateRange(AttribType loc, const UInt3Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());
      void UpdateRange(AttribType loc, const UInt4Vector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());

      // getters
      const uint GetBuffer(uint i) const
      {DEBUG_ASSERT(i < 16); return buffers_[i];}
      const uint GetBufferType(uint i) const
      {DEBUG_ASSERT(i < 16); return types_[i];}

    private:
      uint buffers_[16];
      AttribDataType types_[16];
  };

  class GLIndexBuffer : public IndexBuffer {
    public:
      GLIndexBuffer();
      ~GLIndexBuffer();

      // initialization & destruction
      void Init(uint size, AccessMode mode=amRead);
      void Clear(void);

      // memory mapping
      uint * Lock(AccessMode mode=amWrite, uint offset=0,
        uint size=std::numeric_limits<uint>::max());
      void Unlock(void);

      // Set automatically initializes (creates) buffer
      void Set(const UIntVector & data, AccessMode mode=amRead);
      void UpdateRange(const UIntVector & data,
        uint offset=0, uint size=std::numeric_limits<uint>::max());

      // getters
      const uint GetBuffer(void) const {return buffer_;}
      uint GetSize(void) const {return size_;}

    private:
      uint buffer_;
      uint size_;
  };

}
