//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gl_canvas.cpp
 *
 *  blahblah
 */

#include <GL/glew.h>
#include <Cg/cgGL.h>

#include "vfs/logfile.h"

#include "core/client/display.h"

#include "cg_renderer.h"
#include "gl_canvas.h"

namespace tre {

  CanvasPtr CanvasFactory::CreateInstance(void)
  {
    return CanvasPtr(new GLCanvas());
  }

  CanvasPtr CanvasFactory::CreateInstance(uint w, uint h)
  {
    if(w == 0 || h == 0) {
      vLog << warn("Canvas") << "Invalid canvas dimensions, ignored."
           << std::endl;

      return CanvasPtr(new GLCanvas());
    } else {
      return CanvasPtr(new GLCanvas(w, h));
    }
  }

  // implicit canvas
  GLCanvas::GLCanvas() : Canvas(0, 0), FBO_(0)
  {
    DisplayInfo info = sDisplay().GetDisplayInfo();
    width_ = info.width;
    height_ = info.height;

    std::fill(GBuffers_, GBuffers_ + 2, 0);
  }

  // render-to-texture canvas
  GLCanvas::GLCanvas(uint w, uint h) : Canvas(w, h), FBO_(0)
  {
    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_FALSE);

    // create new GBuffers
    glGenTextures(2, GBuffers_);

    // diffuse
    glBindTexture(GL_TEXTURE_2D, GBuffers_[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width_, height_, 0,
      GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // depth for rendering
    glBindTexture(GL_TEXTURE_2D, GBuffers_[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width_, height_, 0,
      GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // attach G-Buffers to FBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, FBO_);

    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
    GL_TEXTURE_2D, GBuffers_[0], 0);

    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT,
    GL_TEXTURE_2D, GBuffers_[1], 0);

    CgRenderer::CheckFramebufferStatus();

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_TRUE);
  }

  GLCanvas::~GLCanvas()
  {
    ClearBuffers();
  }

  void GLCanvas::ClearBuffers(void)
  {
    glDeleteFramebuffersEXT(1, &FBO_);
    glDeleteTextures(2, GBuffers_);

    FBO_ = 0;
    std::fill(GBuffers_, GBuffers_ + 2, 0);
  }

}
