//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gl_canvas.h
 *
 *  Ble.
 */

#pragma once

#include "renderer/canvas.h"

namespace tre {

  class GLCanvas : public Canvas {
    public:
      GLCanvas();   // creates the implicit canvas (display)
      GLCanvas(uint w, uint h);
      ~GLCanvas();

      uint GetFBO(void) const {return FBO_;}
      const uint * GetBuffers(void) const {return GBuffers_;}

    private:
      uint FBO_;
      uint GBuffers_[2];

    private:
      void ClearBuffers(void);
  };
}
