//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gl_texture.cpp
 *
 *  ble.
 */

#include <GL/glew.h>
#include <Cg/cgGL.h>

#include "platform.h"
#include "exception.h"

#include "vfs/logfile.h"

#include "gl_texture.h"
#include "image_tga.h"
#include "image_dds.h"

#include "renderer/renderer.h"
#include "cg_renderer.h"

namespace tre {

  const TexturePtr TextureFactory::CreateInstance(const std::string & name)
  {
    TextureMap::iterator pos = map_.find(name);

    if(pos != map_.end() && !pos->second.expired())
      return pos->second.lock();

    if(pos == map_.end())
      pos = map_.insert(pos, std::make_pair(name, TexturePtr()));

    DEBUG_ASSERT(pos != map_.end());

    TexturePtr res;
    try {
      // need to pass the name stored in the element
      res = TexturePtr(new GLTexture(name));

      vLog << ok("GL") << "Texture loaded successfully ("
           << name << ")." << std::endl;
    } catch(Exception & exc) {
      vLog << err("GL") << "Unable to load texture (" + name + ").\nReason: "
           << exc.what() << std::endl;
    }
    pos->second = res;
    return res;
  }

  const TexturePtr TextureFactory::CreateInstance(const std::string & name,
    TextureType type, uint width, uint height, TextureFormat format)
  {
    TextureMap::iterator pos = map_.find(name);

    if(pos != map_.end() && !pos->second.expired())
      return pos->second.lock();

    if(pos == map_.end())
      pos = map_.insert(pos, std::make_pair(name, TexturePtr()));

    DEBUG_ASSERT(pos != map_.end());

    TexturePtr res;

    // need to pass the name stored in the element
    res = CreateInstance(type, width, height, format);
    pos->second = res;

    return res;
  }

  const TexturePtr TextureFactory::CreateInstance
  (TextureType type, uint width, uint height, TextureFormat format)
  {
    uint fmt, ifmt, dtp;
    switch(format) {
      case tfR8G8B8:
        fmt = GL_RGB;
        ifmt = GL_RGB;
        dtp = GL_UNSIGNED_BYTE;
        break;
      case tfR8G8B8A8:
        fmt = GL_RGBA;
        ifmt = GL_RGBA8;
        dtp = GL_UNSIGNED_BYTE;
        break;
      default:
        return TexturePtr();
    }

    return TexturePtr(new GLTexture(type, width, height, fmt, ifmt, dtp));
  }

  GLTexture::~GLTexture()
  {
    if(handle_)
      glDeleteTextures(1, &handle_);
  }

  GLTexture::GLTexture(const std::string & filename) : handle_(0)
  {
    if(filename.length() <= 4) // "*.tga/*.dds"
      throw(Exception("Unknown file type."));

    if(filename.compare(filename.length() - 4, 4, ".dds")  == 0) {
      if(!LoadFromDds(filename))
        throw(Exception("Error loading dds file."));
    } else if(filename.compare(filename.length() - 4, 4, ".tga")  == 0) {
      if(!LoadFromTga(filename))
        throw(Exception("Error loading tga file."));
    } else {
      throw(Exception("Unknown file type."));
    }
  }

  GLTexture::GLTexture(TextureType type, uint width,
    uint height, uint format, uint iformat, uint dtype)
  {
    glGenTextures(1, &handle_);

    size_.x = width;
    size_.y = height;
    format_ = format;
    iformat_ = iformat;
    dtype_ = dtype;

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_FALSE);

    switch(type) {
      case ttTexture1D:
        target_ = GL_TEXTURE_1D;
        throw(Exception("Not supported."));
        break;
      case ttTexture2D:
        target_ = GL_TEXTURE_2D;
        glBindTexture(GL_TEXTURE_2D, handle_);
        glTexImage2D(GL_TEXTURE_2D, 0, iformat, width,
          height, 0, format, dtype, NULL);
        break;
      case ttTexture3D:
        target_ = GL_TEXTURE_3D;
        throw(Exception("Not supported."));
        break;
      case ttCubeMap:
        throw(Exception("Not supported."));
    }

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_TRUE);

#ifdef DEBUG
    Renderer::CheckErrors(__FUNCTION__);
#endif /* DEBUG */
  }

  bool GLTexture::LoadFromDds(const std::string & path)
  {
    ImageDds ddsImage;

    if(!ddsImage.LoadFromFile(path))
      return false;

    target_ = ddsImage.target;
    size_.x = ddsImage.width;
    size_.y = ddsImage.height;
    format_ = ddsImage.format;
    iformat_ = ddsImage.internalFormat;
    dtype_ = ddsImage.dataType;

    if(!handle_)
      glGenTextures(1, &handle_);

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_FALSE);

    glBindTexture(target_, handle_);

    switch(target_) {
      case GL_TEXTURE_1D:
        for(uint i = 0; i < ddsImage.mipMapCount; ++i) {
          const ImageDds::Surface & surf = ddsImage.GetSurface(0, i);
          if(ddsImage.compressed) {
            if(GLEW_ARB_texture_compression) {
              glCompressedTexImage1DARB(target_, i, ddsImage.internalFormat,
                surf.width != 1 ? surf.width : surf.height, 0,
                surf.size, &surf.surfaceData.front());
            } else {
              vLog << warn("Texture") << "Attempted to load "
                      "compressed texture without hardware support ("
                   << path << ")!" << std::endl;
            }
          } else {
            glTexImage1D(target_, i, ddsImage.internalFormat,
              surf.width != 1 ? surf.width : surf.height, 0, ddsImage.format,
              ddsImage.dataType, &surf.surfaceData.front());
          }
        }
        break;
      case GL_TEXTURE_2D:
        for(uint i = 0; i < ddsImage.mipMapCount; ++i) {
          const ImageDds::Surface & surf = ddsImage.GetSurface(0, i);
          if(ddsImage.compressed) {
            if(GLEW_ARB_texture_compression) {
              glCompressedTexImage2DARB(target_, i, ddsImage.internalFormat,
                surf.width, surf.height, 0, surf.size,
                &surf.surfaceData.front());
            } else {
              vLog << warn("Texture") << "Attempted to load "
                      "compressed texture without hardware support ("
                   << path << ")!" << std::endl;
            }
          } else {
            glTexImage2D(target_, i, ddsImage.internalFormat, surf.width,
              surf.height, 0, ddsImage.format, ddsImage.dataType,
              &surf.surfaceData.front());
          }
        }
        break;
      case GL_TEXTURE_3D:
        for(uint i = 0; i < ddsImage.mipMapCount; ++i) {
          const ImageDds::Surface & surf = ddsImage.GetSurface(0, i);
          if(ddsImage.compressed) {
            if(GLEW_ARB_texture_compression) {
              glCompressedTexImage3DARB(target_, i, ddsImage.internalFormat,
                surf.width, surf.height, surf.depth, 0, surf.size,
                &surf.surfaceData.front());
            } else {
              vLog << warn("Texture") << "Attempted to load "
                      "compressed texture without hardware support ("
                   << path << ")!" << std::endl;
            }
          } else {
            glTexImage3D(target_, i, ddsImage.internalFormat, surf.width,
              surf.height, surf.depth, 0, ddsImage.format, ddsImage.dataType,
              &surf.surfaceData.front());
          }
        }
        break;
      case GL_TEXTURE_CUBE_MAP:
        for(uint j = 0; j < 6;++j) {
          for(uint i = 0; i < ddsImage.mipMapCount; ++i) {
            const ImageDds::Surface & surf = ddsImage.GetSurface(j, i);
            if(ddsImage.compressed) {
              if(GLEW_ARB_texture_compression) {
                glCompressedTexImage2DARB(
                  GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB + j, i,
                  ddsImage.internalFormat, surf.width, surf.height, 0,
                  surf.size, &surf.surfaceData.front());
              } else if(i == 0) { // don't print six warning messages
                vLog << warn("Texture") << "Attempted to load "
                        "compressed texture without hardware support ("
                     << path << ")!" << std::endl;
              }
            } else {
              glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB + j, i,
                ddsImage.internalFormat, surf.width, surf.height, 0,
                ddsImage.format, ddsImage.dataType,
                &surf.surfaceData.front());
            }
          }
        }
        break;
      case GL_TEXTURE_RECTANGLE_ARB:
        vLog << warn("Texture") << "Attempted to load texture "
                "rectangle as common texture (" << path << ")!" << std::endl;
        break;
    }

    glTexParameteri(target_, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    if(ddsImage.mipMapCount > 1)
      glTexParameteri(target_, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    else
      glTexParameteri(target_, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_TRUE);

#ifdef DEBUG
    Renderer::CheckErrors(__FUNCTION__);
#endif /* DEBUG */

    return true;
  }

  void GLTexture::UpdateRect(const Vector2<uint> & origin,
    const Vector2<uint> & size, const ByteVector & data)
  {
    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_FALSE);

    glBindTexture(target_, handle_);

    switch(target_) {
      case GL_TEXTURE_1D:
        BREAK_ASSERT(true);
        break;
      case GL_TEXTURE_2D:
        target_ = GL_TEXTURE_2D;
        glTexSubImage2D(GL_TEXTURE_2D, 0, origin.x, origin.y, size.x, size.y,
          format_, dtype_, &data.front());
        break;
      case GL_TEXTURE_3D:
      default:
        BREAK_ASSERT(true);
        break;
    }

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_TRUE);

#ifdef DEBUG
    Renderer::CheckErrors(__FUNCTION__);
#endif /* DEBUG */
  }

  bool GLTexture::LoadFromTga(const std::string & path)
  {
    ImageTga tgaImage;

    if(!tgaImage.LoadFromFile(path))
      return false;

    target_ = GL_TEXTURE_2D;

    size_.x = tgaImage.width;
    size_.y = tgaImage.height;
    if((tgaImage.bpp / 8) == 4) {
      format_ = GL_RGBA;
      iformat_ = GL_RGBA8;
    } else {
      format_ = GL_RGB;
      iformat_ = GL_RGB8;
    }
    dtype_ = GL_UNSIGNED_BYTE;

    if(!handle_)
      glGenTextures(1, &handle_);

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_FALSE);

    glBindTexture(target_, handle_);

    gluBuild2DMipmaps(target_, iformat_, tgaImage.width,
      tgaImage.height, format_, GL_UNSIGNED_BYTE, &tgaImage.imageData.front());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR);

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_TRUE);

#ifdef DEBUG
    Renderer::CheckErrors(__FUNCTION__);
#endif /* DEBUG */
    return true;
  }

#ifdef DEBUG
  void GLTexture::DumpToFile(const std::string & name) const
  {
    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_FALSE);

    glPixelStoref(GL_UNPACK_ALIGNMENT, 1);

    ImageTga tex(size_.x, size_.y, 32);

    glBindTexture(target_, handle_);
    glGetTexImage(target_, 0, GL_RGBA,
      GL_UNSIGNED_BYTE, &tex.imageData.front());

    Renderer::CheckErrors(__FUNCTION__);

    tex.SaveToFile(name);

    cgGLSetManageTextureParameters(
      static_cast<CgRenderer&>(sRenderer()).GetCgContext(), CG_TRUE);
  }
#endif
}
