//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file gl_texture.h
 *
 *  Ble.
 */

#pragma once

#include <string>

#include "renderer/texture.h"

namespace tre {

  class GLTexture : public Texture {
    public:
      GLTexture() : handle_(0) {}
      GLTexture(const std::string & filename);
      GLTexture(TextureType type, uint width, uint height,
        uint format, uint iformat, uint dtype);
      ~GLTexture();

      uint32 GetTarget(void) const {return target_;}
      uint GetHandle(void) const {return handle_;}

      void UpdateRect(const Vector2<uint> & origin,
        const Vector2<uint> & size, const ByteVector & data);

#ifdef DEBUG
      void DumpToFile(const std::string & name) const;
#endif

    private:
      uint handle_;
      uint32 target_;

      uint format_;
      uint iformat_;
      Vector2<uint> size_;
      uint dtype_;

    private:
      bool LoadFromDds(const std::string & path);
      bool LoadFromTga(const std::string & path);
  };
}
