//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file image_dds.cpp
 *
 *  ble.
 */

#include <GL/glew.h>

#include <string.h>

#include "platform.h"
#include "exception.h"

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "image_dds.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  DDS header, caps & stuff
  */

#pragma pack(push,1)

  struct DdPixelFormat {
    uint32 dwSize;  // Size of structure. This member must be set to 32.
    uint32 dwFlags;
    uint32 dwFourCC;
    uint32 dwRGBBitCount;
    uint32 dwRBitMask;
    uint32 dwGBitMask;
    uint32 dwBBitMask;
    uint32 dwRGBAlphaBitMask;
  };

  struct DdCaps2 {
    uint32 dwCaps1;
    uint32 dwCaps2;
    uint32 Reserved[2];
  };

  struct DdsSurfaceDesc2 {
    uint32 dwSize;  // Size of structure. This member must be set to 124.
    uint32 dwFlags; // Always include DDSD_CAPS, DDSD_PIXELFORMAT,
    uint32 dwHeight;               // DDSD_WIDTH, DDSD_HEIGHT.
    uint32 dwWidth;
    uint32 dwPitchOrLinearSize;
    uint32 dwDepth;
    uint32 dwMipMapCount;
    uint32 dwReserved1[11];

    DdPixelFormat ddpfPixelFormat;
    DdCaps2 ddsCaps;
    uint32 dwReserved2;
  };

#pragma pack(pop)

 /*
  *  DDS flags & stuff
  */

  // DDSStruct Flags
  const uint32 DDSD_CAPS             = 0x00000001l;
  const uint32 DDSD_HEIGHT           = 0x00000002l;
  const uint32 DDSD_WIDTH            = 0x00000004l;
  const uint32 DDSD_PITCH            = 0x00000008l;
  const uint32 DDSD_PIXELFORMAT      = 0x00001000l;
  const uint32 DDSD_MIPMAPCOUNT      = 0x00020000l;
  const uint32 DDSD_LINEARSIZE       = 0x00080000l;
  const uint32 DDSD_DEPTH            = 0x00800000l;

  // pixel format flags
  const uint32 DDPF_ALPHAPIXELS      = 0x00000001l;
  const uint32 DDPF_ALPHA            = 0x00000002l;
  const uint32 DDPF_FOURCC           = 0x00000004l;
  const uint32 DDPF_RGB              = 0x00000040l;
  const uint32 DDPF_LUMINANCE        = 0x00020000l;

  // ddscaps1 flags
  const uint32 DDSCAPS_COMPLEX           = 0x00000008l;
  const uint32 DDSCAPS_TEXTURE           = 0x00001000l;
  const uint32 DDSCAPS_MIPMAP            = 0x00400000l;

  // ddscaps2 flags
  const uint32 DDSCAPS2_CUBEMAP           = 0x00000200l;
  const uint32 DDSCAPS2_CUBEMAP_POSITIVEX = 0x00000400l;
  const uint32 DDSCAPS2_CUBEMAP_NEGATIVEX = 0x00000800l;
  const uint32 DDSCAPS2_CUBEMAP_POSITIVEY = 0x00001000l;
  const uint32 DDSCAPS2_CUBEMAP_NEGATIVEY = 0x00002000l;
  const uint32 DDSCAPS2_CUBEMAP_POSITIVEZ = 0x00004000l;
  const uint32 DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x00008000l;
  const uint32 DDSCAPS2_CUBEMAP_ALL_FACES = 0x0000FC00l;
  const uint32 DDSCAPS2_VOLUME            = 0x00200000l;

#ifndef MAKEFOURCC
  #define MAKEFOURCC(c0, c1, c2, c3) \
    ((c0) | ((c1) << 8) | ((c2) << 16) | ((c3) << 24))
#endif

 /*
  *  Helper functions
  *  taken from DevIL
  */

  static void FlipColorBlock(byte * data)
  {
    byte tmp;

    tmp = data[4];
    data[4] = data[7];
    data[7] = tmp;

    tmp = data[5];
    data[5] = data[6];
    data[6] = tmp;
  }

  // NOTE: does this work on BIG_ENDIAN machines?
  static void FlipSimpleAlphaBlock(ushort * data)
  {
    ushort tmp;

    tmp = data[0];
    data[0] = data[3];
    data[3] = tmp;

    tmp = data[1];
    data[1] = data[2];
    data[2] = tmp;
  }

  static void ComplexAlphaHelper(byte * data)
  {
    ushort tmp[2];

    //one 4 pixel line is 12 bit, copy each line into
    //a ushort, swap them and copy back
    tmp[0] = (data[0] | (data[1] << 8)) & 0xfff;
    tmp[1] = ((data[1] >> 4) | (data[2] << 4)) & 0xfff;

    data[0] = tmp[1];
    data[1] = (tmp[1] >> 8) | (tmp[0] << 4);
    data[2] = tmp[0] >> 4;
  }

  static void FlipComplexAlphaBlock(byte * data)
  {
    byte tmp[3];
    data += 2; //Skip 'palette'

    //swap upper two rows with lower two rows
    memcpy(tmp, data, 3);
    memcpy(data, data + 3, 3);
    memcpy(data + 3, tmp, 3);

    //swap 1st with 2nd row, 3rd with 4th
    ComplexAlphaHelper(data);
    ComplexAlphaHelper(data + 3);
  }

  static void FlipDxt1(byte * data, uint count)
  {
    for (uint i = 0; i < count; ++i) {
      FlipColorBlock(data);
      data += 8; //advance to next block
    }
  }

  static void FlipDxt3(byte * data, uint count)
  {
    for(uint i = 0; i < count; ++i) {
      FlipSimpleAlphaBlock((ushort*) data);
      FlipColorBlock(data + 8);
      data += 16; //advance to next block
    }
  }

  static void FlipDxt5(byte * data, uint count)
  {
    for(uint i = 0; i < count; ++i) {
      FlipComplexAlphaBlock(data);
      FlipColorBlock(data + 8);
      data += 16; //advance to next block
    }
  }

 /*
  *  ImageDds member functions
  */

  const uint ImageDds::CalculateSize
  (uint width, uint height, uint depth, uint linearSize/* = 0*/) const
  {
    uint blockSize;

    if(compressed) {
      if(linearSize > 0) {
        blockSize = linearSize / height / width / depth;
        blockSize *= width * height * depth;
      } else {
        blockSize = ((width + 3) / 4) * ((height + 3) / 4) * depth;
        switch(internalFormat) {
          case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
            vLog << warn("Dds")
                 << "Guessing texture size." << std::endl;
          case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
            blockSize *= 8;
            break;
          case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
          case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
            blockSize *= 16;
            break;
        }
      }
    } else {
      blockSize = width * height * depth * bpp / 8;
    }
    return blockSize;
  }

  void ImageDds::FlipSurface(Surface & surf)
  {
    if(!compressed) {
      FlipUncompressedSurface(surf);
    } else {
      FlipCompressedSurface(surf);
    }
  }

  void ImageDds::FlipUncompressedSurface(Surface & surf)
  {
    uint lineSize = (surf.size / surf.depth) / surf.height;
    uint sliceSize = surf.size / surf.depth;

    byte * buffer = new byte[lineSize];

    for(uint i = 0; i < surf.depth; ++i) {
      for(uint j = 0; j < surf.height / 2; ++j) {
        memcpy(buffer, &surf.surfaceData[i * sliceSize + j * lineSize],
               sizeof(byte) * lineSize);
        memcpy(&surf.surfaceData[i * sliceSize + j * lineSize],
               &surf.surfaceData[
                 i * sliceSize + (surf.height - j - 1) * lineSize],
               sizeof(byte) * lineSize);
        memcpy(&surf.surfaceData[
                 i * sliceSize + (surf.height - j - 1) * lineSize],
               buffer, sizeof(byte) * lineSize);
      }
    }
    delete [] buffer;
  }

  void ImageDds::FlipCompressedSurface(Surface & surf)
  {
    uint blockSize = 0, lineSize;
    byte *temp, *runner, *top, *bottom;

    void (*FlipBlocks)(byte* data, uint count) = NULL;

    uint numXBlocks = (surf.width + 3) / 4;
    uint numYBlocks = (surf.height + 3) / 4;

    switch(internalFormat) {
      case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
      case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
        blockSize = 8;
        FlipBlocks = FlipDxt1;
        break;
      case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        blockSize = 16;
        FlipBlocks = FlipDxt3;
        break;
      case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
        blockSize = 16;
        FlipBlocks = FlipDxt5;
        break;
    }
    lineSize = numXBlocks * blockSize;
    temp = new byte[lineSize];
    runner = &surf.surfaceData.front();

    for(uint i = 0; i < surf.depth; ++i) {
      top = runner;
      bottom = runner + (numYBlocks - 1) * lineSize;

      for(uint j = 0; j < numYBlocks / 2; ++j) {
        //swap block row
        memcpy(temp, top, lineSize);
        memcpy(top, bottom, lineSize);
        memcpy(bottom, temp, lineSize);

        //swap blocks
        FlipBlocks(top, numXBlocks);
        FlipBlocks(bottom, numXBlocks);

        top += lineSize;
        bottom -= lineSize;
      }

      //middle line
      if (numYBlocks % 2 != 0)
        FlipBlocks(top, numXBlocks);

      runner += lineSize * numYBlocks;
    }
    delete [] temp;
  }

  bool ImageDds::LoadFromFile(const std::string & path)
  {
    // open dds file
    FileIStream file(path);
    if(!file.is_open()) {
      vLog << err("Dds") << "Unable to open texture: "
           << path << "!" << std::endl;
      return false;
    }

    try {
      // read magic value
      uint32 magic;
      file.read(reinterpret_cast<char*>(&magic), sizeof(uint32));

      // fix endianness
      magic = ToLittleEndian(magic);

      if(magic != MAKEFOURCC('D', 'D', 'S', ' '))
        throw(Exception("File is not proper DDS file"));

      DdsSurfaceDesc2 header;
      if(!(file.read(reinterpret_cast<char*>(&header),
         sizeof(DdsSurfaceDesc2))))
        throw(Exception("Could not read texture header"));

#ifdef PLATFORM_BIG_ENDIAN
      header.size = ToLittleEndian(header.size);
      header.flags = ToLittleEndian(header.flags);
      header.height = ToLittleEndian(header.height);
      header.width = ToLittleEndian(header.width);
      header.pitchOrLinearSize = ToLittleEndian(header.pitchOrLinearSize);
      header.depth = ToLittleEndian(header.depth);
      header.mipMapCount = ToLittleEndian(header.mipMapCount);
      header.pixelFormat.size = ToLittleEndian(header.pixelFormat.size);
      header.pixelFormat.flags = ToLittleEndian(header.pixelFormat.flags);
      header.pixelFormat.fourCc = ToLittleEndian(header.pixelFormat.fourCc);
      header.pixelFormat.rgbBitCount =
        ToLittleEndian(header.pixelFormat.rgbBitCount);
      header.pixelFormat.rBitMask =
        ToLittleEndian(header.pixelFormat.rBitMask);
      header.pixelFormat.gBitMask =
        ToLittleEndian(header.pixelFormat.gBitMask);
      header.pixelFormat.bBitMask =
        ToLittleEndian(header.pixelFormat.bBitMask);
      header.pixelFormat.alphaBitask =
        ToLittleEndian(header.pixelFormat.alphaBitask);
      header.caps.caps1 = ToLittleEndian(header.caps.caps1);
      header.caps.caps2 = ToLittleEndian(header.caps.caps2);
#endif

      if(header.dwSize != 124)
        throw(Exception("Wrong size in texture header"));

      // retrieve mandatory information about texture
      if(!(header.dwFlags & (DDSD_CAPS | DDSD_PIXELFORMAT |
                             DDSD_WIDTH | DDSD_HEIGHT)))
        throw(Exception("Errorneous DDS header"));

      width = header.dwWidth;
      height = header.dwHeight;

      // retrieve depth
      if(header.dwFlags & DDSD_DEPTH)
        depth = header.dwDepth;
      DEBUG_ASSERT(depth > 0);

      // determine texture type
      if((header.ddsCaps.dwCaps1 & DDSCAPS_COMPLEX) &&
         (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP)) {
        if((header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_POSITIVEX) == 0 ||
           (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_NEGATIVEX) == 0 ||
           (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_POSITIVEY) == 0 ||
           (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_NEGATIVEY) == 0 ||
           (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_POSITIVEZ) == 0 ||
           (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_NEGATIVEZ) == 0)
          throw(Exception("Incomplete cube maps are not supported"));
        target = GL_TEXTURE_CUBE_MAP;
      } else if((header.ddsCaps.dwCaps1 & DDSCAPS_COMPLEX) &&
                (header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME) &&
                (header.dwFlags & DDSD_DEPTH)) {
        target = GL_TEXTURE_3D;
      } else if((width == 1 || height == 1) && (width != height)) {
        target = GL_TEXTURE_1D;
      } else {
        target = GL_TEXTURE_2D;
      }

      dataType = GL_UNSIGNED_BYTE;

      // determine texture format
      if(header.ddpfPixelFormat.dwFlags & DDPF_FOURCC)  {
        switch(header.ddpfPixelFormat.dwFourCC) {
          case MAKEFOURCC('D', 'X', 'T', '1'):
            if(header.ddpfPixelFormat.dwFlags & DDPF_ALPHAPIXELS) {
              internalFormat = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
              bpp = 32;
            } else {
              internalFormat = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
              bpp = 24;
            }
            compressed = true;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized DXT1 format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_EXT_texture_compression_s3tc) {
              vLog << warn("DDS") << "Attempting to load DXT1 texture "
                      "without hardware support! ("
                   << path << ")." << std::endl;
            }
            break;
          case MAKEFOURCC('D', 'X', 'T', '3'):
            internalFormat = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
            bpp = 32;
            compressed = true;
#ifdef DEBUG
            vLog << msg("DDS") << "Texture DDS: Recognized DXT3 format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_EXT_texture_compression_s3tc) {
              vLog << warn("DDS") << "Attempting to load DXT3 texture "
                      "without hardware support! ("
                   << path << ")." << std::endl;
            }
            break;
          case MAKEFOURCC('D', 'X', 'T', '5'):
            internalFormat = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
            bpp = 32;
            compressed = true;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized DXT5 format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_EXT_texture_compression_s3tc) {
              vLog << warn("DDS") << "Attempting to load DXT5 texture "
                      "without hardware support! ("
                   << path << ")." << std::endl;
            }
            break;
          case MAKEFOURCC('$', '\0', '\0', '\0'):
            internalFormat = GL_RGBA16;
            format = GL_RGBA;
            dataType = GL_UNSIGNED_SHORT;
            bpp = 64;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized A16B16G16R16 format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            break;
          // FIXME: in case of not having ARB_texture_float use ATI version (or
          // the NVIDIA one, but that might work only with texture rectangles)
          case MAKEFOURCC('q', '\0', '\0', '\0'):  // a16b16g16r16f
            internalFormat = GL_RGBA16F_ARB;
            dataType = GL_HALF_FLOAT_ARB;
            format = GL_RGBA;
            bpp = 64;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized A16B16G16R16F format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_ARB_texture_float || !GLEW_ARB_half_float_pixel)
              vLog << warn("DDS")
                   << "Attempting to load 16-bit floating-point "
                      "texture without adequte hardware support! ("
                   << path << ")." << std::endl;
            // requires ARB_texture_float & ARB_half_float_pixel
            break;
          case MAKEFOURCC('t', '\0', '\0', '\0'):  // a32b32g32r32f
            internalFormat = GL_RGBA32F_ARB;
            dataType = GL_FLOAT;
            format = GL_RGBA;
            bpp = 128;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized A32B32G32R32F format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_ARB_texture_float || !GLEW_ARB_half_float_pixel)
              vLog << warn("DDS")
                   << "Warning: Attempting to load 32-bit floating-point "
                      "texture without adequte hardware support! ("
                   << path << ")." << std::endl;
            break;
          // The following requires use of texture rectangles
          // and NVidia specific extensions
          case MAKEFOURCC('p', '\0', '\0', '\0'):  // g16r16f
            target = GL_TEXTURE_RECTANGLE_ARB;
            internalFormat = GL_FLOAT_RG16_NV;
            dataType = GL_HALF_FLOAT_NV;
            format = GL_RGBA;
            bpp = 32;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized G16R16F format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_ARB_texture_rectangle || !GLEW_NV_float_buffer ||
               !GLEW_NV_half_float)
              vLog << warn("DDS")
                   << "Attempting to load 16-bit floating-point texture "
                      "rectangle without adequte hardware support! ("
                   << path << ")." << std::endl;
            break;
          case MAKEFOURCC('s', '\0', '\0', '\0'):  // g32r32f
            target = GL_TEXTURE_RECTANGLE_ARB;
            internalFormat = GL_FLOAT_RG32_NV;
            dataType = GL_FLOAT;
            format = GL_RGBA;
            bpp = 64;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized G32R32F format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_ARB_texture_rectangle || !GLEW_NV_float_buffer)
              vLog << warn("DDS")
                   << "Attempting to load 32-bit floating-point texture "
                      "rectangle without adequte hardware support! ("
                   << path << ")." << std::endl;
            break;
          case MAKEFOURCC('o', '\0', '\0', '\0'):  // r16f
            target = GL_TEXTURE_RECTANGLE_ARB;
            internalFormat = GL_FLOAT_R16_NV;
            dataType = GL_HALF_FLOAT_NV;
            format = GL_RGBA;
            bpp = 16;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized R16F format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_ARB_texture_rectangle || !GLEW_NV_float_buffer ||
               !GLEW_NV_half_float)
              vLog << warn("DDS")
                   << "Attempting to load 16-bit floating-point texture "
                      "rectangle without adequte hardware support! ("
                   << path << ")." << std::endl;
            break;
          case MAKEFOURCC('r', '\0', '\0', '\0'):  // r32f
            target = GL_TEXTURE_RECTANGLE_ARB;
            internalFormat = GL_FLOAT_R32_NV;
            dataType = GL_FLOAT;
            format = GL_FLOAT_R_NV;
            bpp = 32;
#ifdef DEBUG
            vLog << msg("DDS") << "Recognized R32F format ("
                 << path << ")." << std::endl;
#endif /* DEBUG */
            if(!GLEW_ARB_texture_rectangle || !GLEW_NV_float_buffer)
              vLog << warn("DDS")
                   << "Attempting to load 32-bit floating-point texture "
                      "rectangle without adequte hardware support! ("
                   << path << ")." << std::endl;
            break;
          case MAKEFOURCC('D', 'X', 'T', '2'):
          case MAKEFOURCC('D', 'X', 'T', '4'):
          case MAKEFOURCC('A', 'T', 'I', '1'):
          case MAKEFOURCC('A', 'T', 'I', '2'):
          case MAKEFOURCC('R', 'X', 'G', 'B'):
          default:
            throw(Exception("Unsupported texture format (fourCC)"));
        }
      // all possible RGBA formats
      } else if(header.ddpfPixelFormat.dwFlags & DDPF_RGB) {
        bpp = header.ddpfPixelFormat.dwRGBBitCount;

        if(header.ddpfPixelFormat.dwFlags & DDPF_ALPHAPIXELS) {
          //  Supported formats without alpha component:
          //  A1R5G5B5 -> bpp = 16
          //  A4R4B4G4 -> bpp = 16
          //  A8R8G8B8 and variants -> bpp = 32
          //  A2R10G10B10 and variants -> bpp = 32
          switch(bpp) {
            case 16:
              if(header.ddpfPixelFormat.dwRBitMask == 0x7c00) {
                internalFormat = GL_RGB5_A1;
                dataType = GL_UNSIGNED_SHORT_1_5_5_5_REV;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized A1R5G5 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else if(header.ddpfPixelFormat.dwRBitMask == 0x0f00) {
                internalFormat = GL_RGBA4;
                dataType = GL_UNSIGNED_SHORT_4_4_4_4_REV;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized A4R4G4B4 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else {
                throw(Exception("Unsupported texture RGB format"));
              }
              format = GL_BGRA;
              break;
            case 32:
              if(header.ddpfPixelFormat.dwRBitMask == 0xff0000) {
                internalFormat = GL_RGBA8;
                format = GL_BGRA;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized A8R8G8B8 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else if(header.ddpfPixelFormat.dwBBitMask == 0xff0000) {
                internalFormat = GL_RGBA8;
                format = GL_RGBA;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized A8B8G8R8 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else if(header.ddpfPixelFormat.dwBBitMask == 0x3ff00000) {
                internalFormat = GL_RGB10_A2;           // weird mask, chmm?
                format = GL_BGRA;
                dataType = GL_UNSIGNED_INT_2_10_10_10_REV;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized A2R10G10B10 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else if(header.ddpfPixelFormat.dwRBitMask == 0x3ff00000) {
                internalFormat = GL_RGB10_A2;
                format = GL_RGBA;
                dataType = GL_UNSIGNED_INT_2_10_10_10_REV;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized A2B10G10R10 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else {
                throw(Exception("Unsupported texture RGB format"));
              }
              break;
            default:
              throw(Exception("Unsupported texture RGBA format"));
          }
        } else {
          //  Supported formats without alpha component:
          //  R3G3B2 -> bpp = 8
          //  X1R5G5B5 -> bpp = 16
          //  X4R4G4B4 -> bpp = 16
          //  R8G8B8 -> bpp = 24
          //  X8R8G8B8 and variants -> bpp = 32
          switch(bpp) {
            case 8:
              if(header.ddpfPixelFormat.dwRBitMask == 0xe0) {
                internalFormat = GL_R3_G3_B2;
                dataType = GL_UNSIGNED_BYTE_3_3_2;
                format = GL_RGB;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized R3G3B2 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else {
                throw(Exception("Unsupported texture RGB format"));
              }
              break;
            case 16:
              if(header.ddpfPixelFormat.dwRBitMask == 0x7c00) {
                internalFormat = GL_RGB5;
                dataType = GL_UNSIGNED_SHORT_1_5_5_5_REV;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized X1R5G5 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else if(header.ddpfPixelFormat.dwRBitMask == 0x0f00) {
                internalFormat = GL_RGB4;
                dataType = GL_UNSIGNED_SHORT_4_4_4_4_REV;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized X4R4G4B4 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else {
                throw(Exception("Unsupported texture RGB format"));
              }
              format = GL_BGRA;
              break;
            case 24:
              if(header.ddpfPixelFormat.dwRBitMask == 0xff0000) {
                internalFormat = GL_RGB8;
                format = GL_BGR;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized R8G8B8 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else {
                throw(Exception("Unsupported texture RGB format"));
              }
              break;
            case 32:
              internalFormat = GL_RGB8;
              if(header.ddpfPixelFormat.dwRBitMask == 0xff0000) {
                format = GL_BGRA;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized X8R8G8B8 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else if(header.ddpfPixelFormat.dwBBitMask == 0xff0000) {
                format = GL_RGBA;
#ifdef DEBUG
                vLog << msg("DDS") << "Recognized X8B8G8R8 format ("
                     << path << ")." << std::endl;
#endif /* DEBUG */
              } else {
                throw(Exception("Unsupported texture RGB format"));
              }
              break;
            default:
              throw(Exception("Unsupported texture RGB format"));
          }
        }
      // luminance
      } else if(header.ddpfPixelFormat.dwFlags & DDPF_LUMINANCE) {
        bpp = header.ddpfPixelFormat.dwRGBBitCount;

        if(header.ddpfPixelFormat.dwFlags & DDPF_ALPHAPIXELS) {
          switch(bpp) {
            // A4L4 format is not supported because I didn't find a way
            // to load it directly to graphics hardware
            /*case 8:
              internalFormat = GL_LUMINANCE4_ALPHA4;
              dataType = GL_UNSIGNED_BYTE;
              vLog << msg("DDS")
                        << "Recognized A4L4 format (" << path << ")."
                        << std::endl;
              break;*/
            case 16:
              internalFormat = GL_LUMINANCE8_ALPHA8;
#ifdef DEBUG
              vLog << msg("DDS") << "Recognized A4L4 format ("
                   << path << ")." << std::endl;
#endif /* DEBUG */
              break;
            default:
              throw(Exception("Unsupported texture luminance format"));
          }
          format = GL_LUMINANCE_ALPHA;
        } else {
          switch(bpp) {
            case 8:
              internalFormat = GL_LUMINANCE8;
#ifdef DEBUG
              vLog << msg("DDS") << "Recognized L8 format ("
                   << path << ")." << std::endl;
#endif /* DEBUG */
              break;
            case 16:
              internalFormat = GL_LUMINANCE16;
              dataType = GL_UNSIGNED_SHORT;
#ifdef DEBUG
              vLog << msg("DDS") << "Recognized L16 format ("
                   << path << ")." << std::endl;
#endif /* DEBUG */
              break;
            default:
              throw(Exception("Unsupported texture luminance format"));
          }
          format = GL_LUMINANCE;
        }
      } else if(header.ddpfPixelFormat.dwFlags & DDPF_ALPHA) {
        bpp = header.ddpfPixelFormat.dwRGBBitCount;

        if(bpp != 8)
          throw(Exception("Unsupported texture alpha format"));
        internalFormat = GL_ALPHA8;
        format = GL_ALPHA;
#ifdef DEBUG
        vLog << msg("DDS") << "Recognized A8 format ("
             << path << ")." << std::endl;
#endif /* DEBUG */
      } else {
        throw(Exception("Unsupported texture format"));
      }

      // number of mip-maps includes the texture itself!!!
      if(header.dwFlags & DDSD_MIPMAPCOUNT)
        mipMapCount = header.dwMipMapCount;

      // clear all previously allocated memory
      textures.clear();

      // allocate new array of textures
      textureCount = (target == GL_TEXTURE_CUBE_MAP) ? 6 : 1;
      textures.reserve(textureCount);
      textures.insert(textures.end(), textureCount,SubTexture());

      for(uint i = 0; i < textureCount; ++i) {
        uint w = width;
        uint h = height;
        uint d = depth;

        textures[i].levels.reserve(mipMapCount);
        textures[i].levels.insert(textures[i].levels.end(), mipMapCount,
                                   Surface());

        for(uint j = 0; j < mipMapCount; ++j) {
          textures[i].levels[j].size = CalculateSize(w, h, d);
          textures[i].levels[j].width = w;
          textures[i].levels[j].height = h;
          textures[i].levels[j].depth = d;

          textures[i].levels[j].surfaceData.reserve
            (textures[i].levels[j].size);
          textures[i].levels[j].surfaceData.insert(
            textures[i].levels[j].surfaceData.end(),
          textures[i].levels[j].size,'\0');

            if(!(file.read(reinterpret_cast<char*>(
               &textures[i].levels[j].surfaceData[0]),
               sizeof(byte)*textures[i].levels[j].size)))
           throw(Exception("Could not read texture data"));

          // we must flip the surface along the X axis, because DDS stores
          // images in DirectX-friendly way
          FlipSurface(textures[i].levels[j]);

          w = w > 2 ? w >> 1 : 1;
          h = h > 2 ? h >> 1 : 1;
          d = d > 2 ? d >> 1 : 1;
        }
      }

      // swap cubemaps on y axis (since image is flipped in OpenGL)
      if(target == GL_TEXTURE_CUBE_MAP) {
        SubTexture tmpSubTex;
        tmpSubTex = textures[3];
        textures[3] = textures[2];
        textures[2] = tmpSubTex;
      }
    } catch(Exception & exc) {
      vLog << err("DDS") << exc.what()
           << ": " << path << "!" << std::endl;
      textures.clear();
      return false;
    } catch(std::bad_alloc) {
      textures.clear();
      throw;
    }
    return true;
  }

}
