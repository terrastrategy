//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file image_dds.h
 *
 *  ble.
 */

#pragma once

#include <vector>

#include "stdtypes.h"

namespace tre {
  class ImageDds {
    public:
      struct Surface {
        ByteVector surfaceData;
        uint width;
        uint height;
        uint depth;
        uint32 size;
      };

      struct SubTexture {
        std::vector<Surface> levels;
      };

    public:
      std::vector<SubTexture> textures;
      uint bpp;
      uint width;
      uint height;
      uint depth;
      uint mipMapCount;
      uint textureCount;

      uint32 target;
      uint32 format;
      uint32 internalFormat;
      uint32 dataType;

      bool compressed;

    public:
      // init reasonable values
      ImageDds() : bpp(0), width(0), height(0), depth(1), mipMapCount(1),
                   textureCount(0), target(0), format(0), internalFormat(0),
                   dataType(0), compressed(false) {}

      const Surface & GetSurface(uint subtex, uint mipmap) const
      {
        return textures.at(subtex).levels.at(mipmap);
      }

      bool LoadFromFile(const std::string & path);

    private:
      void FlipSurface(Surface & surf);
      void FlipUncompressedSurface(Surface & surf);
      void FlipCompressedSurface(Surface & surf);
      const uint CalculateSize(uint width,
        uint height, uint depth, uint linearSize = 0) const;
  };
}
