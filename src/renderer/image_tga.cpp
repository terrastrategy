//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file image_tga.cpp
 *
 *  ble.
 */

#include <string.h>

#include "platform.h"
#include "exception.h"

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "image_tga.h"

namespace tre {

 /*
  *  Local helper structures and functions
  */
  namespace {

#pragma pack(push,1)
    struct TgaHeader {
      byte idSize;            // size of ID field that follows 18 byte header)
      byte colourMapType;     // type of colour map 0=none, 1=has palette
      byte imageType;         // type of image 0=none,1=indexed,2=rgb,3=grey,+8=rle packed
      ushort colourMapStart;   // first colour map entry in palette
      ushort colourMapLength;  // number of colours in palette
      byte colourMapBits;     // number of bits per palette entry 15,16,24,32
      ushort xStart;           // image x origin
      ushort yStart;           // image y origin
      ushort width;            // image width in pixels
      ushort height;           // image height in pixels
      byte bpp;               // image bits per pixel 8,16,24,32
      byte descriptor;        // image descriptor bits (vh flip bits)
    };
#pragma pack(pop)

  }

  ImageTga::ImageTga(uint width, uint height, uint bpp)
    : imageData(NULL), bpp(bpp), width(width), height(height)
  {
    imageData.reserve(width * height * bpp / 8);
  }

  bool ImageTga::LoadFromFile(const std::string & path)
  {
    FileIStream file(path);

    if(!file.is_open()) {
      vLog << err("ImageTga") << "Unable to open texture: "
           << path << "!" << std::endl;
      return false;
    }

    TgaHeader header;

    try {
      if(!(file.read(reinterpret_cast<char*>(&header), sizeof(TgaHeader))))
        throw(Exception("Cannot read TGA header"));

      bpp = header.bpp;
      width = ToLittleEndian(header.width);
      height = ToLittleEndian(header.height);

      // make sure all information is valid
      if((bpp != 24 && bpp != 32) || header.colourMapType != 0)
        throw(Exception("Unsupported image format"));
      if(width == 0 || height == 0)
        throw(Exception("Invalid texture dimensions"));

      imageData.resize(width * height * bpp / 8);

      // seek to image data
      if(header.idSize)
        file.seekg(header.idSize , std::ios_base::cur);

      switch(header.imageType) {  // version
        case 2:
          // load an uncompressed tga
          LoadUncompressedTga(file);
          break;
        case 10:
          // load a compressed tga
          LoadCompressedTga(file);
          break;
        default:
          throw(Exception("Unsupported image type"));
      }

      // mirroring
      if(header.descriptor & 0x20) {
        uint lineSize = width * bpp / 8;
        ByteVector tmpdata(lineSize);
        for(uint i = 0; i < height / 2; ++i) {
          memcpy(&tmpdata.front(), &imageData[i * lineSize], lineSize);
          memcpy(&imageData[i * lineSize],
            &imageData[(height - i - 1) * lineSize], lineSize);
          memcpy(&imageData[(height - i - 1) * lineSize],
            &tmpdata.front(), lineSize);
        }
      }
      if(header.descriptor & 0x10) {
        vLog << warn("TGA") << "Horizontal mirroring not supported."
             << std::endl;
      }
    } catch (Exception & exc) {
      vLog << err("TGA") << exc.what()
           << ": " << path << "!" << std::endl;
      return false;
    }

    // close the file and return success
    return true;
  }

  // Save uncompressed TGA
  void ImageTga::SaveToFile(const std::string & path)
  {
    FileOStream file(path);

    if(!file.is_open())
      return;

    TgaHeader header;
    header.idSize = 0;
    header.colourMapType = 0;
    header.imageType = 2;
    header.colourMapStart = 0;
    header.colourMapLength = 0;
    header.colourMapBits = 0;
    header.xStart = 0;
    header.yStart = 0;
    header.width = ToLittleEndian(width);
    header.height = ToLittleEndian(height);
    header.bpp = bpp;
    header.descriptor = 0;

    file.write(reinterpret_cast<char*>(&header), sizeof(TgaHeader));

    // support variables
    uint bytesPerPixel = bpp / 8;
    ulong imageSize = bytesPerPixel * width * height;

    ByteVector tmpData(imageData.begin(), imageData.begin() + imageSize);

    // start the loop
    byte tmpbyte;
    for(uint cswap = 0; cswap < imageSize; cswap += bytesPerPixel) {
      tmpbyte = tmpData[cswap + 2];
      tmpData[cswap + 2] = tmpData[cswap];
      tmpData[cswap] = tmpbyte;
    }
    file.write(reinterpret_cast<char*>(&tmpData.front()), imageSize);
  }

  void ImageTga::LoadUncompressedTga(FileIStream & file)
  {
    // support variables
    uint bytesPerPixel = bpp / 8;
    ulong imageSize = bytesPerPixel * width * height;

    // attempt to read all the image data
    if(!(file.read(reinterpret_cast<char*>(
        &imageData.front()), imageSize)))
      throw(Exception("Cannot read texture data"));

    // start the loop
    byte tmpbyte;
    for(ulong cswap = 0; cswap < imageSize; cswap += bytesPerPixel) {
      tmpbyte = imageData[cswap];
      imageData[cswap] = imageData[cswap + 2];
      imageData[cswap + 2] = tmpbyte;
    }
  }

  void ImageTga::LoadCompressedTga(FileIStream & file)
  {
    // support variables
    uint bytesPerPixel = bpp / 8;
    uint pixelcount = height * width;
    uint currentpixel = 0;
    uint currentbyte = 0;

    // storage for 1 pixel (max allowed bpp is 32)
    byte colourbuf[4];      // max

    do { // start Loop
      // variable to store the value of the id chunk
      byte chunkheader = 0;

      // attempt to read the chunk's header
      if(!(file.read(reinterpret_cast<char*>(&chunkheader), sizeof(byte))))
        throw(Exception("Cannot read texture data"));

      // if the chunk is a 'raw' chunk
      if(chunkheader < 128) {
        // add 1 to the value to get total number of raw pixels
        ++chunkheader;

        // start pixel reading loop
        for(short counter = 0; counter < chunkheader; ++counter) {
          // try to read 1 pixel
          if(!(file.read(reinterpret_cast<char*>(colourbuf), bytesPerPixel)))
            throw(Exception("Cannot read texture data"));

          // write the RGB bytes (stored BGR)
          imageData[currentbyte] = colourbuf[2];
          imageData[currentbyte + 1] = colourbuf[1];
          imageData[currentbyte + 2] = colourbuf[0];

          if(bytesPerPixel == 4) // if it's a 32bpp image...
            // write the 'A' byte
            imageData[currentbyte + 3] = colourbuf[3];

          // increment the byte counter by the number of bytes in a pixel
          currentbyte += bytesPerPixel;
          ++currentpixel;  // increment the number of pixels by 1
        }
      // if it's an RLE header
      } else {
        chunkheader -= 127; // subtract 127 to get rid of the id bit

        // read the next pixel
        if(!(file.read(reinterpret_cast<char*>(colourbuf), bytesPerPixel)))
          throw(Exception("Cannot read texture data"));

        for(short counter = 0; counter < chunkheader; ++counter) {
          // copy the RGB bytes
          imageData[currentbyte] = colourbuf[2];
          imageData[currentbyte + 1] = colourbuf[1];
          imageData[currentbyte + 2] = colourbuf[0];

          if(bytesPerPixel == 4)
            imageData[currentbyte + 3] = colourbuf[3];

            currentbyte += bytesPerPixel; // increment the byte counter
            ++currentpixel;               // increment the pixel counter
        }
      }
    // more pixels to read? ... start loop over
    } while(currentpixel < pixelcount); 
  }
}
