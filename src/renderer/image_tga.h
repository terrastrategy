//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file image_tga.h
 *
 *  ble.
 */

#pragma once

#include <string>

#include "stdtypes.h"

namespace tre {

  class FileIStream;

  class ImageTga {
    public:
      ByteVector imageData;
      uint bpp;
      uint width;
      uint height;

    public:
      ImageTga() : bpp(0), width(0), height(0) {}
      ImageTga(uint width, uint height, uint bpp);

      bool LoadFromFile(const std::string & path);
      void SaveToFile(const std::string & path);

    private:
      void LoadUncompressedTga(FileIStream & file);
      void LoadCompressedTga(FileIStream & file);
  };
}
