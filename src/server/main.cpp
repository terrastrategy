//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file server/main.cpp
 *
 *  This file contains the main() function for the terra client.
 *
 *  main() simply initializes few core subsystems, parses command line and
 *  executes the application instance.
 */

#include "../core/platform.h"
#include "../core/exception.h"

#include "../support/vfs.h"

#include "../framework/application.h"

void ParseArguments(int argc, char * argv[])
{
  for(int i = 1; i < argc; ++i) {
    if(argv[i][0] == '-') {
      switch(argv[i][1]) {
        case 'm':  // mount directory
          if(i >= argc - 2) {
            vfs::vLog << "Error: Too few parameters for mounting.\n";
            exit(EXIT_FAILURE);
          }
          if(!vfs::sInterface().HasRoot() && strcmp(argv[i + 2], "/") != 0) {
            vfs::vLog << "Error: First mounted directory "
              "must be root.\n";
            exit(EXIT_FAILURE);
          }
          if(!vfs::sInterface().MountPath(argv[i + 1], argv[i + 2])) {
            vfs::vLog << "Error: Unable to mount directory ";
//              "'%s' as '%s',\n", argv[i + 1], argv[i + 2]);
            exit(EXIT_FAILURE);
          }
          i += 2;
          break;
        default:
          vfs::vLog << "Warning: Unknown command line ";
//            "argument '%s'.\n", argv[i];
          return;
      }
    }
  }
}

int main(int argc, char * argv[])
{
  try {
    // turn off exhaustive logging
    SET_LOG_ALWAYS(false);

    // check program arguments
    ParseArguments(argc,argv);

    // mount default root if needed
    if(!vfs::sInterface().HasRoot())
      throw(Exception("Unable to initialize vfs"));

    // create and run the game instance
    framework::Application server;

    server.Run();

  } catch(std::exception & exc) {
    vfs::vLog << "Error: Caught fatal exception: %s!\n";
//                         "Error: Program termination imminent!\n",exc.what());
    return 1;
  } catch(...) {
    vfs::vLog << "Error: Caught unhandled exception!\n";
//                         "Error: Program termination imminent!\n");
    return 1;
  }

  return 0;
}
