//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file console.cpp
 *
 *  Ble.
 */

#include <string.h>
#include <stdarg.h>

#include <string>
#include <istream>
#include <streambuf>
#include <deque>

#include "vfs/console.h"
#include "vfs/vario.h"

namespace tre {

  bool ConsoleStream::ready = false;

  // write one character
  int ConsoleBuffer::overflow(int c)
  {
    if(c != EOF) {
      if(c != '\n') {
        sq_.front().push_back(c);
      } else {
        sq_.push_front(std::string());
        sq_.pop_back();
      }
    }
    return c;
  }

  // write multiple characters
  std::streamsize ConsoleBuffer::xsputn(const char * s, std::streamsize num)
  {
    const char * sptr = s, * eptr;

    // split on newlines
    while((eptr = strchr(sptr, '\n'))) {
      sq_.push_front(std::string(sptr, eptr - sptr));
      sq_.pop_back();
      sptr = eptr + 1;
    }
    sq_.push_front(std::string(sptr));
    sq_.pop_back();
    return strlen(s);
  }

  ConsoleStream::ConsoleStream(uint lines/*=defConsoleLines*/)
    : std::iostream(&buf_), buf_(lines)
  {
      ready = true;
  }

  ConsoleStream::~ConsoleStream()
  {
      ready = false;
  }

  int ConsoleStream::format(const char * fmt, ...)
  {
    if(!rdbuf())
      return -1;

    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamPrintf(*this, fmt, l_va);

    va_end(l_va);
    return t;
  }
}
