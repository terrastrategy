//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file diskfile.cpp
 *
 *  This file implements vfs mechanics for accessing the "real" files.
 */

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

#include <string>
#include <streambuf>
#include <istream>
#include <ostream>

#include "platform.h"

#ifdef PLATFORM_MSVC
#  include <io.h>

#  define ssize_t long
#endif

#include "assertion.h"
#include "exception.h"
#include "diskfile.h"

#include "vfs/fstream.h"
#include "vfs/logfile.h"

#include "memory/mmgr.h"

namespace tre {

  const uint defDiskBufferSize = 8192;

 /*
  *  DiskBuffer implementation
  */
  DiskBuffer::~DiskBuffer()
  {
    sync();
    delete [] buffer_;
  }

  bool DiskBuffer::Init(const std::string & path, std::ios_base::openmode mode)
  {
    // rw files are forbidden due to limits of c++ streams
    if((mode & std::ios_base::in) && (mode & std::ios_base::out))
      return false;

    struct stat data;
    bool exists = true;

#ifdef PLATFORM_MSVC
    int flags = _O_BINARY;
#else
//    int flags = O_DIRECT;
    int flags = 0;
#endif

    if(stat(path.c_str(), &data) != 0)
      exists = false;

    // choose the correct file open mode, if the passed mode is unsuitable,
    // don't correct it and return false
    if(mode & std::ios_base::out) { // w+, nebo r+/w+
      // we need read access for buffering, otherwise everything
      // becomes soooooooooooooooooooooo slooooooooooooooooooooooooooow
      flags |= O_RDWR | O_CREAT;
      if(mode & std::ios_base::trunc)
        flags |= O_TRUNC;
    } else if(mode & std::ios_base::in) {
      if(!exists)
        return false;
      flags |= O_RDONLY;
    } else {
      return false;   // no operation specified
    }

#ifdef PLATFORM_MSVC
    fileHandle_ = _open(path.c_str(), flags, _S_IREAD | _S_IWRITE);
#else
    fileHandle_ = open(path.c_str(), flags,
        S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
#endif

    if(fileHandle_ == -1)
      return false;

    // init internal state
    if(exists && !(mode & std::ios_base::trunc))
      fileSize_ = data.st_size;
    else
      fileSize_ = 0;

    filePosition_ = 0;

    fileMode_ = mode;

    bufferStart_ = 0;
    bufferSize_ = defDiskBufferSize;

    // minimize memory usage
    if(!(fileMode_ & std::ios_base::out) && fileSize_ <= bufferSize_)
      bufferSize_ = fileSize_ + 1;

    // allocate buffer
    try {
      setbuf(new char_type[bufferSize_], bufferSize_);
    }
    catch(std::bad_alloc) {
      close(fileHandle_);
      throw;
    }
    return true;
  }

  // buffer management and positioning
  std::streambuf * DiskBuffer::setbuf(char_type * s, std::streamsize n)
  {
    DEBUG_ASSERT(s != NULL);

    // write down contents of the previous buffer
    if(!FlushBuffer())
      return NULL;

    delete buffer_;
    buffer_ = s;

    bufferSize_ = n;

    ulong pos;
    if(fileMode_ & std::ios_base::in)
      pos = bufferStart_ + (eback() - gptr());
    else
      pos = bufferStart_ + (pbase() - pptr());

    // seek to the start of the new buffer
    if(lseek(fileHandle_, pos, SEEK_SET) != static_cast<off_t>(pos))
      return NULL;

    // read new buffer from file
    ulong readsz = pos + bufferSize_ - 1 > fileSize_ ?
      fileSize_ - pos : bufferSize_ - 1;
    ulong res = read(fileHandle_, buffer_, readsz);

    if(res != readsz)
      return NULL;

    filePosition_ = pos + readsz;

    if(fileMode_ & std::ios_base::out) {
      setp(buffer_, buffer_ + readsz);
    } else {
      setg(buffer_, buffer_, buffer_ + readsz);
    }
    return this;
  }

  std::streampos DiskBuffer::seekoff
  (std::streamoff off, std::ios_base::seekdir way, std::ios_base::openmode m)
  {
    long pos;
    switch(way) {
      case std::ios_base::beg:
        pos = off;
        break;
      case std::ios_base::cur:
        if(fileMode_ & std::ios_base::in)
          pos = bufferStart_ + (gptr() - eback());
        else
          pos = bufferStart_ + (pptr() - pbase());
        pos += off;
        break;
      case std::ios_base::end:
        pos = fileSize_ - off;
      default:
        return -1;
    }
    return seekpos(pos, m);
  }

  std::streampos DiskBuffer::seekpos(std::streampos sp, std::ios_base::openmode)
  {
    if(!FlushBuffer())
      return -1;

    if(static_cast<ulong>(sp) == bufferStart_)
      return sp;

    if(sp < 0 || static_cast<ulong>(sp) > fileSize_)
      return -1;

    if(!RepositionBuffer(sp))
      return -1;

    // return success (and current position)
    return sp;
  }

  int DiskBuffer::sync(void)
  {
    if(!FlushBuffer())
      return -1;

    if(fileMode_ & std::ios_base::in) {
      if(eback() < gptr())
        if(RepositionBuffer(bufferStart_ + gptr() - eback()))
          return -1;
    } else {
      if(pbase() < pptr())
        if(RepositionBuffer(bufferStart_ + pptr() - pbase()))
          return -1;
    }
    return 0;
  }

  // input functions
  std::streamsize DiskBuffer::showmanyc(void)
  {
    if(fileSize_ - (bufferStart_ + bufferSize_ - 1) > bufferSize_ - 1)
      return bufferSize_ - 1;
    else
      return fileSize_ - (bufferStart_ + bufferSize_ - 1);
  }

#ifdef PLATFORM_MSVC
  std::streamsize DiskBuffer::_Xsgetn_s(char_type * s, size_t ptr_sz, std::streamsize n)
  {
#else /* PLATFORM_MSVC */
  std::streamsize DiskBuffer::xsgetn(char_type * s, std::streamsize n)
  {
#endif /* PLATFORM_MSVC */
    if(fileMode_ & std::ios_base::out)
      return 0;

    ulong tmp_pos = bufferStart_ + egptr() - eback();

    // fits into current buffer
    if(egptr() - gptr() > n) {
      memcpy(s, gptr(), n);
      gbump(n);

      if(egptr() == gptr()) {
        if(!RepositionBuffer(tmp_pos))
          return 0;
      }
      return n;
    }

    // get all you can from the buffer
    ulong res = egptr() - gptr();
    memcpy(s, gptr(), res);

    // read the rest from the file
    if(tmp_pos != filePosition_) {
      // seek to the new position
      if(lseek(fileHandle_, tmp_pos, SEEK_SET) != static_cast<off_t>(tmp_pos))
        return 0;
      filePosition_ = tmp_pos;
    }

    ulong readsz = tmp_pos + n - res >= fileSize_ ?
      fileSize_ - tmp_pos : n - res;

    // read the rest
    if(readsz > 0 && read(fileHandle_, s + res, readsz)
       != static_cast<ssize_t>(readsz))
      return 0;

    res += readsz;
    filePosition_ += readsz;

    RepositionBuffer(filePosition_);

    return res;
  }

  DiskBuffer::int_type DiskBuffer::underflow(void)
  {
    // no need to flush
    if(fileMode_ & std::ios_base::out)
      return traits_type::eof();

	uint off = gptr() - eback();
    if(fileSize_ == bufferStart_ + off)
	  return traits_type::eof();

    RepositionBuffer(bufferStart_ + off);

    return traits_type::to_int_type(*gptr());
  }

  DiskBuffer::int_type DiskBuffer::pbackfail(int_type c/*=traits_type::eof()*/)
  {
    if(fileMode_ & std::ios_base::out)
      return traits_type::eof();

    // move one stap backwards
    if(eback() < gptr()) {
      gbump(-1);
    } else if(bufferStart_ > 0) {
      if(!RepositionBuffer(bufferStart_ - 1))
        return traits_type::eof();
    } else {
      // seek failed
      return traits_type::eof();
    }

    // try to put back i
    if(c != traits_type::eof()) {
      *gptr() = c;
    }
    return *gptr();
  }

  // output functions
  std::streamsize DiskBuffer::xsputn(const char_type * s, std::streamsize num)
  {
    if(fileMode_ & std::ios_base::in)
      return 0;

    // fits into the buffer
    if(bufferSize_ - (pptr() - pbase()) > static_cast<uint>(num)) {
      memcpy(pptr(), s, num);
      pbump(num);

      if(epptr() < pptr())
        setpp(buffer_, pptr(), pptr());

      if(static_cast<uint>(pptr() - pbase()) >= bufferSize_ - 1) {
        if(sync() != 0)
          return 0;
      }
      return num;
    }

    // fill the rest of the buffer and flush it
    ulong res = bufferSize_ - (pptr() - pbase());
    memcpy(pptr(), s, res);
    fileSize_ = bufferStart_ + bufferSize_;
    pbump(res);
    FlushBuffer();

    // write the rest of the data
    if(write(fileHandle_, s + res, num - res)
       != static_cast<ssize_t>(num - res))
      return 0;
    fileSize_ += num - res;

    if(!RepositionBuffer(fileSize_))
      return 0;

    return res;
  }

  DiskBuffer::int_type DiskBuffer::overflow(int_type c/*=traits_type::eof()*/)
  {
    if(c == traits_type::eof())
      return 0;

    *pptr() = c;
    ++fileSize_;

    // either enlarge buffer
    if(static_cast<uint>(pbase() - pptr()) < bufferSize_ - 1) {
      pbump(1);
      setpp(pbase(), pptr(), epptr() + 1);
    // or flush to disk and reload
    } else {
      pbump(1);
      if(!FlushBuffer() ||  RepositionBuffer(bufferStart_ + bufferSize_ - 1))
        return traits_type::eof();
    }
    return c;
  }

  // internal functions
  bool DiskBuffer::FlushBuffer(void)
  {
    // read-only mode or no changes done
    if(!(fileMode_ & std::ios_base::out) || pptr() == pbase())
      return true;

    // ptr usually points to the next character
    std::streamsize sz = pptr() - pbase();

    // goto bufferStart
    if(filePosition_ != bufferStart_) {
      if(lseek(fileHandle_, bufferStart_, SEEK_SET)
         != static_cast<off_t>(bufferStart_))
        return false;
      filePosition_ = bufferStart_;
    }

    if(write(fileHandle_, buffer_, sz) != sz)
      return false;

    filePosition_ += sz;

    return true;
  }

  bool DiskBuffer::RepositionBuffer(ulong newpos)
  {
    if(newpos == bufferStart_)
      return true;

    ulong start, size, dest;
    if(newpos + bufferSize_ < bufferStart_ ||
       newpos >= bufferStart_ + bufferSize_) {
      start = newpos;
      size = newpos + bufferSize_ - 1 > fileSize_ ?
        fileSize_ - newpos : bufferSize_ - 1;
      dest = 0;

      if(fileMode_ & std::ios_base::out) {
        setp(buffer_, buffer_ + size);
      } else {
        setg(buffer_, buffer_, buffer_ + size);
      }
    } else if(newpos < bufferStart_) {
      memmove(&buffer_[bufferStart_ - newpos], &buffer_[0],
        sizeof(char_type) * (newpos + bufferSize_ - bufferStart_ - 1));

      start = newpos;
      size = bufferStart_ - newpos;
      dest = 0;

      if(fileMode_ & std::ios_base::out) {
        setp(buffer_, buffer_ + bufferSize_ - 1);
      } else {
        setg(buffer_, buffer_, buffer_ + bufferSize_ - 1);
      }
    } else {
      memmove(&buffer_[0], &buffer_[newpos - bufferStart_],
        sizeof(char_type) * (bufferStart_ + bufferSize_ - newpos - 1));

      uint buffend = fileSize_ >= newpos + bufferSize_ - 1 ? bufferSize_ - 1:
        fileSize_ - newpos;
      if(fileMode_ & std::ios_base::out) {
        setp(buffer_, buffer_ + buffend);
      } else {
        setg(buffer_, buffer_, buffer_ + buffend);
      }

      start = bufferStart_ + bufferSize_ - 1;

      // we don't need to read anything if we already passed the end of file
      if(fileSize_ <= start) {
        bufferSize_ = newpos;
        return true;
      }

      size = fileSize_ >= newpos + bufferSize_ - 1 ?
        newpos - bufferStart_ : fileSize_ - (bufferStart_ + bufferSize_ - 1);
      dest = bufferStart_ + bufferSize_ - 1 - newpos;
    }
    // seek to the reading position
    if(start != filePosition_) {
      if(lseek(fileHandle_, start, SEEK_CUR) != static_cast<off_t>(start))
        return false;
      filePosition_ = start;
    }
    // read data
    if(read(fileHandle_, buffer_ + dest, size) != static_cast<off_t>(size))
      return false;

    filePosition_ += size;
    bufferStart_ = newpos;

    return true;
  }

 /*
  *  DiskNode implementation
  */
  bool DiskNode::OpenFile(const std::string & filename,
    FileIStream & file, std::ios_base::openmode mode) const
  {
    if(mode & std::ios_base::out)
      mode ^= std::ios_base::out;
    mode |= std::ios_base::in;

    std::string tmpstr(path_);
    tmpstr.push_back('/');
    tmpstr.append(filename);

    DiskBuffer * buf = new DiskBuffer();

    if(!buf->Init(tmpstr, mode)) {
      delete buf;
      return false;
    }
    file.rdbuf(buf);

    return true;
  }

  bool DiskNode::OpenFile(const std::string & filename,
    FileOStream & file, std::ios_base::openmode mode) const
  {
    if(mode & std::ios_base::in)
      mode ^= std::ios_base::in;
    mode |= std::ios_base::out;

    std::string tmpstr(path_);
    tmpstr.push_back('/');
    tmpstr.append(filename);

    DiskBuffer * buf = new DiskBuffer();

    if(!buf->Init(tmpstr, mode)) {
      delete buf;
      return false;
    }
    file.rdbuf(buf);

    return true;
  }

  bool DiskNode::OpenFile(const std::string & filename,
    FileIOStream & file, std::ios_base::openmode mode) const
  {
    std::string tmpstr(path_);
    tmpstr.push_back('/');
    tmpstr.append(filename);

    DiskBuffer * buf = new DiskBuffer();

    if(!buf->Init(tmpstr, mode)) {
      delete buf;
      return false;
    }
    file.rdbuf(buf);

    return true;
  }

  bool DiskNode::Stat(const std::string & filename, FileInfo & info) const
  {
    std::string tmpstr(path_);
    tmpstr.push_back('/');
    tmpstr.append(filename);

    struct stat data;

    if(stat(tmpstr.c_str(), &data) != 0)
      return false;

    info.isDir = S_ISDIR(data.st_mode);
    info.readOnly = readOnly_ ? true : access(tmpstr.c_str(), W_OK) == 0;
    info.size = data.st_size;
    info.mtime = data.st_mtime;
    return true;
  }

  bool DiskNode::Exists(const std::string & filename) const
  {
    std::string tmpstr(path_);
    tmpstr.push_back('/');
    tmpstr.append(filename);

    struct stat data;

    return stat(tmpstr.c_str(), &data) == 0;
  }

  const StrVector DiskNode::QueryDir(const std::string & dir) const
  {
    std::string tmpstr(path_);
    tmpstr.push_back('/');
    tmpstr.append(dir);

    DIR * dr;
    StrVector res;

    if(!(dr = opendir(dir.c_str()))) {
      vLog << warn("DiskNode") << "Cannot open dir '"
           << dir << "'" << std::endl;
      return res;
    }

    dirent * entry;
    while((entry = readdir(dr))) {
// TODO: convert to lowercase on windows
      res.push_back(entry->d_name);
    }
    closedir(dr);
    return res;
  }
}
