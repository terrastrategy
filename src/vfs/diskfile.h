//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file diskfile.h
 *
 *  This file provides vfs mechanics for accessing the "real" files.
 *
 *  Contains declaration of the DiskNode class, which is used behind the
 *  scenes to proide read/write access to real files on the disk.
 */

#pragma once

#include <vector>

#include "platform.h"

#include "vfs/fstream.h"
#include "vfs/interface.h"

namespace tre {

 /**
  *  @class DiskNode
  *
  *  Provides access to the files on the disk.
  *
  *  Real files and directory structure on the disk are available under the
  *  same names with the same rights. This class is created when MountPath
  *  of Vfs is called and is accessed through the MountNode
  *  interface.
  */
  class DiskNode : public MountNode {
    public:
      DiskNode(const std::string & path, bool mode) : MountNode(path, mode) {}

      bool OpenFile(const std::string & filename,
        FileIStream & file, std::ios_base::openmode mode) const;

      bool OpenFile(const std::string & filename,
        FileOStream & file, std::ios_base::openmode mode) const;

      bool OpenFile(const std::string & filename,
        FileIOStream & file, std::ios_base::openmode mode) const;

      bool Stat(const std::string & filename, FileInfo & info) const;

      bool Exists(const std::string & filename) const;

      const StrVector QueryDir(const std::string & dir) const;
  };

  class DiskBuffer : public FileBuffer {
    public:
      DiskBuffer() : buffer_(NULL) {}
      ~DiskBuffer();

      bool Init(const std::string & path, std::ios_base::openmode mode);

      ulong GetSize(void) const {return fileSize_;}

    protected:
      // buffer management and positioning
      std::streambuf * setbuf(char_type * s, std::streamsize n);
      std::streampos seekoff(std::streamoff off,
        std::ios_base::seekdir way, std::ios_base::openmode);
      std::streampos seekpos(std::streampos sp, std::ios_base::openmode);
      int sync(void);

      // input functions
      std::streamsize showmanyc(void);

#ifdef PLATFORM_MSVC
      std::streamsize _Xsgetn_s(char_type * s,
        size_t ptr_sz, std::streamsize n);
#else /* PLATFORM_MSVC */
      std::streamsize xsgetn(char_type * s, std::streamsize n);
#endif /* PLATFORM_MSVC */

      int_type underflow(void);
//      int uflow(void);    // the default behaviour is acceptable
      int_type pbackfail(int_type c=traits_type::eof());

      // output functions
      std::streamsize xsputn(const char_type * s, std::streamsize num);
      int_type overflow(int_type c=traits_type::eof());

    private:
      char_type * buffer_;       // character buffer

      ulong bufferStart_;   // buffer's position in the file
      uint bufferSize_;     // size of the buffer

      int fileHandle_;      // ties to OS
      ulong filePosition_;  // real position in the file
      ulong fileSize_;      // last writen size of the file

      std::ios_base::openmode fileMode_;  // open mode

  private:
      bool FlushBuffer(void);
      bool RepositionBuffer(ulong newpos);

      void setpp(char_type * pbeg, char_type * pcur, char_type * pend)
      {
        setp(pbeg, pend);
        pbump(pcur - pbeg);
      }
  };
}
