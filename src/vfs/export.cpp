//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file export.h
 *
 *  Contains definitions of functions and variables exported from vfs.
 *
 *  This file contains definitions of functions and variables to be "exported"
 *  from C++ vfs namespace into ANSI C parts of the program (lua).
 *
 *  You can see, how the vfs was designed to resemble standard C io.
 */

#include <errno.h>
#include <stdarg.h>
#include <string.h>

#include <istream>

#include "assertion.h"

#include "vfs/export.h"
#include "vfs/interface.h"
#include "vfs/fstream.h"
#include "vfs/vario.h"
#include "vfs/logfile.h"

#include "memory/mmgr.h"

/*
 *  vfs_stdin. vfs_stdout, vfs_stderr are defined in static.cpp
 *  since they depend on successfull instantiation of vfs globals
 */

/*
 *  vfs_fwhatever file functions
 *
 *  Operations on files
 */

/** Removes file.
 *
 *  This function is not implemented due to the nature of the vfs.
 *
 *  @param[in] filename File to be removed.
 *  @return -1
 */
int vfs_remove(const char * filename)
{
  errno = ENOSYS;
  return -1;
}

/** Renames file.
 *
 *  This function is not implemented due to the nature of the vfs.
 *
 *  @param[in] oldname Source filename.
 *  @param[in] newname Destination filename.
 *  @return -1
 */
int vfs_rename(const char * oldname, const char * newname)
{
  errno = ENOSYS;
  return -1;
}

/** Opens a temporary file.
 *
 *  This function is not implemented due to the nature of the vfs.
 *
 *  @return NULL
 */
VFS_FILE * vfs_tmpfile(void)
{
  errno = ENOSYS;
  return NULL;
}

/** Generates temporary filename.
 *
 *  This function is not implemented due to the nature of the vfs.
 *
 *  @param[in] s Buffer to be filled with the generated filename.
 *  @return NULL
 */
char * vfs_tmpnam(char *)
{
  return NULL;
}

/*
 *  File access
 */

/** Closes file.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @return 0 on success, otherwise VFS_EOF and sets errno accordingly.
 */
int vfs_fclose(VFS_FILE * stream)
{
  if(stream != NULL) {
    std::iostream * file = reinterpret_cast<std::iostream*>(stream);
    delete file;
    return 0;
  } else {
    errno = EINVAL;
    return VFS_EOF;
  }
}

/** Flushes stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file or NULL for flushing all files.
 *  @return 0
 */
int vfs_fflush(VFS_FILE * stream)
{
  if(stream == NULL)
    return 0;
  else
    (reinterpret_cast<std::iostream*>(stream))->flush();

  return 0;
}

/** Opens file.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] filename Name of the file to be opened.
 *  @param[in] mode Mode in which the file should be opened.
 *  @return pointer to the newly open file if successful, otherwise NULL
 */
VFS_FILE * vfs_fopen(const char * filename, const char * mode)
{
  DEBUG_ASSERT(filename && mode);

  std::ios_base::openmode opMode;

  switch(mode[0]) {
    case 'r':
      opMode = std::ios_base::in;
//      if(mode[1] == '+' || (mode[1] == 'b' && mode[2] == '+'))
//        opMode |= omWrite;
      break;
    case 'w':
      opMode = std::ios_base::out | std::ios_base::trunc;
//      if(mode[1] == '+' || (mode[1] == 'b' && mode[2] == '+'))
//        opMode |= omRead;
      break;
    default:
      errno = EINVAL;
      return NULL;
  }

  try {
    tre::FileIOStream * file = new tre::FileIOStream(filename, opMode);
    if(file->is_open()) {
      return file;
    } else {
      delete file;
      return NULL;
    }
  } catch(std::bad_alloc) {
    errno = ENOMEM;
    return NULL;
  }
}

/** Reopens stream with different file or mode.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] filename Name of the file to be opened.
 *  @param[in] mode Mode in which the file should be opened.
 *  @param[in] stream Stream to be replaced.
 *  @return pointer to the newly open file if successful, otherwise NULL
 */
VFS_FILE * vfs_freopen
(const char * filename, const char * mode, VFS_FILE * stream)
{
  DEBUG_ASSERT(filename && mode);

  BREAK_ASSERT(0);

  return NULL;

  /*if(vfs_fclose(stream) ==  VFS_EOF)
    return NULL;

  File * file = reinterpret_cast<File*>(stream);

  OpenMode opMode = 0;

  int l = strlen(mode);
  for(int i = 0;i < l;++i) {
    switch(mode[l]) {
      case 'r': opMode |= omRead; break;
      case 'w': opMode |= omWrite; break;
    }
  }

  if(sVfs().FileOpen(filename,opMode,*file)) {
    return file;
  } else {
    // do not delete these, since they're static
    if(file != &gCinFile && file != &gCerrFile && 
       file != &gCoutFile) {
      delete file;
    }
    return NULL;
  }*/
}

/** Changes stream buffering.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[in] buf Buffer to be assigned to file, can be NULL.
 *  @param[in] mode Buffering mode, ignored.
 *  @param[in] size Size of the new buffer.
 *  @return 0 on success, -1 on error and sets errno accordingly.
 */
int vfs_setvbuf(VFS_FILE * stream, char * buf, int mode, vfs_size_t size)
{
  BREAK_ASSERT(0);
  /*if(stream == NULL || (mode != VFS_IOFBF && 
     mode != VFS_IOLBF && mode != VFS_IONBF)) {
    errno = EINVAL;
    return -1;
  }

  File * file = reinterpret_cast<File*>(stream);
  try {
    if(!file->SetBuffer(reinterpret_cast<byte*>(buf),size)) {
      errno = EIO;
      return -1;
    }
  } catch(std::bad_alloc) {
    errno = ENOMEM;
    return -1;
  }*/
  return 0;
}

/*
 *  Formatted input/output
 */

/** Writes formatted output to stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[in] format Format string.
 *  @param[in] ... Represent additional arguments.
 *  @return number of characters written.
 */
int vfs_fprintf(VFS_FILE * stream, const char * format, ...)
{
  DEBUG_ASSERT(stream && format);

  va_list l_va;

  va_start(l_va,format);

  int res = tre::StreamPrintfV(
    *reinterpret_cast<std::iostream*>(stream),format,l_va);

  va_end(l_va);

  return res;
}

/** Reads formatted data from stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[in] format Format string.
 *  @param[in] ... Represent additional arguments.
 *  @return number of items successfully retrieved.
 */
int vfs_fscanf(VFS_FILE * stream, const char * format, ...)
{
  DEBUG_ASSERT(stream && format);

  std::iostream * file = reinterpret_cast<std::iostream*>(stream);

  va_list l_va;

  va_start(l_va,format);

  int res = tre::StreamScanfV(*file, format, l_va);

  va_end(l_va);

  if(res == -1 && file->fail())
    errno = EIO;

  return res;
}

/** Prints formatted data to stdout.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] format Format string.
 *  @param[in] ... Represent additional arguments.
 *  @return number of characters written.
 */
int vfs_printf(const char * format, ...)
{
  DEBUG_ASSERT(format);
  BREAK_ASSERT(0);
  /*va_list l_va;

  va_start(l_va,format);

  int res = gCoutFile.PrintfVar(format,l_va);

  va_end(l_va);

  return res;*/
  return 0;
}

/** Reads formatted data from stdin.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] format Format string.
 *  @param[in] ... Represent additional arguments.
 *  @return number of items successfully retrieved.
 */
int vfs_scanf(const char * format, ...)
{
  DEBUG_ASSERT(format);
  BREAK_ASSERT(0);
//  va_list l_va;
//
//  va_start(l_va,format);
//
//  int res = gCinFile.ScanfVar(format,l_va);
//
//  va_end(l_va);
//
//  if(res == -1 && gCinFile.GetError() != feNone)
//    errno = EIO;
//
//  return res;
//  
  return 0;
}

/** Writes formatted variable argument list to stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[in] format Format string.
 *  @param[in] ap Variable argument list.
 *  @return number of characters written.
 */
int vfs_vfprintf(VFS_FILE * stream, const char * format, va_list ap)
{
  DEBUG_ASSERT(stream && format);

  return tre::StreamPrintfV(
    *reinterpret_cast<std::iostream*>(stream), format, ap);
}

/** Reads formatted variable argument list from stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[in] format Format string.
 *  @param[in] a Variable argument list.
 *  @return number of items successfully retrieved.
 */
int vfs_vfscanf(VFS_FILE * stream, const char * format, va_list ap)
{
  DEBUG_ASSERT(stream && format);

  std::iostream * file = reinterpret_cast<std::iostream*>(stream);

  int res = tre::StreamScanfV(*file, format, ap);

  if(res == -1 && file->fail())
    errno = EIO;

  return res;
}

/** Prints formatted variable argument list to stdout.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] format Format string.
 *  @param[in] ap Variable argument list.
 *  @return number of characters written.
 */
int vfs_vprintf(const char * format, va_list ap)
{
  DEBUG_ASSERT(format);
  BREAK_ASSERT(0);

  //return gCoutFile.PrintfVar(format,ap);
  return 0;
}

/** Reads formatted variable argument list from stdin.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] format Format string.
 *  @param[in] ap Variable argument list.
 *  @return number of items successfully retrieved.
 */
int vfs_vscanf(const char * format, va_list ap)
{
  DEBUG_ASSERT(format);
  BREAK_ASSERT(0);

//  int res = gCinFile.ScanfVar(format,ap);
//
//  if(res == -1 && gCinFile.GetError() != feNone)
//    errno = EIO;
//
//  return res;
  return 0;
}

/*
 *  Character input/output
 */

/** Gets character from stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @return read character as an unsigned char cast to an int, or EOF
 *          on end of file or error
 */
int vfs_fgetc(VFS_FILE * stream)
{
  DEBUG_ASSERT(stream);

  int c = (reinterpret_cast<std::iostream*>(stream))->get(); 
//   bool f  = (reinterpret_cast<std::iostream*>(stream))->fail();
  return c;
}

/** Gets string from stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[out] s Buffer to be filled with data.
 *  @param[in] size Size of the output buffer.
 *  @param[in] stream Target file.
 *  @return string that was just read or NULL on error.
 */
char * vfs_fgets(char * s, int size, VFS_FILE * stream)
{
  DEBUG_ASSERT(s && stream);

  std::iostream * file = reinterpret_cast<std::iostream*>(stream);

  file->getline(s, size - 1);
  if(file->gcount() != (long)strlen(s) + 1) {
    s[strlen(s) + 1] = 0;
    s[strlen(s)] = '\n';
  } else {
    char c = file->get();
    if(c != EOF) {
      s[strlen(s) + 1] = 0;
      s[strlen(s)] = c;
    }
  }
  if(file->fail())
    return NULL;
  return s;
}

/** Writes character to stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] c Character to be sent to the output.
 *  @param[in] stream Target file.
 *  @return the character writen or VFS_EOF on error.
 */
int vfs_fputc(int c, VFS_FILE * stream)
{
  DEBUG_ASSERT(stream);

  if(!(reinterpret_cast<std::iostream*>(stream)->put(c)))
    return EOF;
  return c;
}

/** Writes string to stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] s String to be sent to output.
 *  @param[in] stream Target file.
 *  @return 0 on success, VFS_EOF on error
 */
int vfs_fputs(const char * s, VFS_FILE * stream)
{
  DEBUG_ASSERT(s && stream);

  if(!(*reinterpret_cast<std::iostream*>(stream)  << s))
    return EOF;
  return 0;
}

/** Ungets character from stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] c Character to bes ent back to stream.
 *  @param[in] stream Target file.
 *  @return character c on success, VFS_EOF on error.
 */
int vfs_ungetc(int c, VFS_FILE * stream)
{
  DEBUG_ASSERT(stream);

  if(!(reinterpret_cast<std::iostream*>(stream))->putback(c))
    return EOF;
  return c;
}

/*
 *  Direct input/output
 */

/** Reads block of data from stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[out] ptr Buffer to be filled with read items.
 *  @param[in] size Size of one item.
 *  @param[in] nmemb Number of items to read.
 *  @param[in] stream Target file.
 *  @return number of items read, or 0 on error.
 */
vfs_size_t vfs_fread
(void * ptr, vfs_size_t size, vfs_size_t nmemb, VFS_FILE * stream)
{
  DEBUG_ASSERT(ptr && stream);

  std::iostream * file = reinterpret_cast<std::iostream*>(stream);

  bool f  = ((reinterpret_cast<std::iostream*>(stream)
    )->rdstate() & std::ios_base::failbit) != 0;

  file->read(reinterpret_cast<char*>(ptr), size * nmemb);

  std::ios_base::iostate st = file->rdstate();
  if(st & std::ios_base::failbit)
    st ^= std::ios_base::failbit;

  file->clear(st);

  f  = ((reinterpret_cast<std::iostream*>(stream)
    )->rdstate() & std::ios_base::failbit) != 0;

  return file->gcount() / size;
}

/** Writes block of data to stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[out] ptr Buffer with items to be written.
 *  @param[in] size Size of one item.
 *  @param[in] nmemb Number of source items.
 *  @param[in] stream Target file.
 *  @return number of items written, or 0 on error.
 */
vfs_size_t vfs_fwrite
(const void * ptr, vfs_size_t size, vfs_size_t nmemb, VFS_FILE * stream)
{
  DEBUG_ASSERT(ptr && stream);

  std::iostream * file = reinterpret_cast<std::iostream*>(stream);

  if((file->write(reinterpret_cast<const char*>(ptr), size * nmemb)))
    return nmemb;

  return 0;
}

/*
 * File positioning
 */

/** Gets current position in stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[out] position Current position in the file.
 *  @return 0 on success, otherwise -1.
 */
int vfs_fgetpos(VFS_FILE * stream,vfs_fpos_t * position)
{
  if(stream == NULL) {
    errno = EINVAL;
    return -1;
  }

  *position = (static_cast<std::iostream*>(stream))->tellg();
  return 0;
}

/** Repositions stream position indicator.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[in] offset Offset from current position.
 *  @param[in] whence Where to seek from.
 *  @return 0 on success, otherwise -1 and sets errno accordingly.
 */
int vfs_fseek(VFS_FILE * stream, long offset, int whence)
{
  if(stream == NULL) {
    errno = EBADF;
    return -1;
  }

  std::ios_base::seekdir seekPos;
  switch(whence) {
    case VFS_SEEK_SET: seekPos = std::ios_base::beg; break;
    case VFS_SEEK_CUR: seekPos = std::ios_base::cur; break;
    case VFS_SEEK_END: seekPos = std::ios_base::end; break;
    default:
      errno = EINVAL;
      return -1;
  }

  if((reinterpret_cast<std::iostream*>(stream))->seekg(offset,seekPos)) {
    return 0;
  } else {
    errno = EINVAL;
    return -1;
  }
}

/** Sets position indicator of stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @param[in] pos Desired position in the stream.
 *  @return 0 on success, otherwise -1 and sets errno accordingly.
 */
int vfs_fsetpos(VFS_FILE * stream, const vfs_fpos_t * pos)
{
  DEBUG_ASSERT(pos);

  if(stream == NULL) {
    errno = EBADF;
    return -1;
  }

  if((reinterpret_cast<std::iostream*>(stream)
    )->seekg(*pos, std::ios_base::beg)) {
    return 0;
  } else {
    errno = EINVAL;
    return -1;
  }
}

/** Gets current position in stream.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @return current position in the stream on success or -1l on failure
 *          and sets errno accordingly.
 */
long vfs_ftell(VFS_FILE * stream)
{
  if(stream == NULL) {
    errno = EINVAL;
    return -1l;
  }

  return (reinterpret_cast<std::iostream*>(stream))->tellg();
}

/*
 *  Error-handling
 */

/** Clears error indicators.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @return 1 if eof flag was set, otherwise 0
 */
void vfs_clearerr(VFS_FILE * stream)
{
  DEBUG_ASSERT(stream);

  (reinterpret_cast<std::iostream*>(stream))->clear();
}

/** Checks End-of-File indicator.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @return 1 if eof flag was set, otherwise 0
 */
int vfs_feof(VFS_FILE * stream)
{
  DEBUG_ASSERT(stream);

  bool e  = (reinterpret_cast<std::iostream*>(stream))->eof();
//   bool f  = (reinterpret_cast<std::iostream*>(stream))->fail();

  return e ? 1 : 0;
}

/** Checks error indicator.
 *
 *  Wrapper for File calls.
 *
 *  @param[in] stream Target file.
 *  @return 1 if error flag was set, otherwise 0
 */
int vfs_ferror(VFS_FILE * stream)
{
  DEBUG_ASSERT(stream);

//   bool f  = (reinterpret_cast<std::iostream*>(stream))->fail();

  return (reinterpret_cast<std::iostream*>(stream))->fail() ? 1 : 0;
}

/** Prints error message on stderr.
 *
 *  Wrapper for File calls.
 *
 *  This function is equivalent to vfs_fprintf(vfs_stderr,s);
 *
 *  @param[in] s Message to be printed.
 */
void vfs_perror(const char * s)
{
  if(s != NULL)
    tre::vLog << s << ": " << strerror(errno) << std::endl;
  else
    tre::vLog << strerror(errno) << ":" << std::endl;
}
