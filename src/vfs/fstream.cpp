//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file fstream.cpp
 *
 *  blabla
 */

#include <string.h>
#include <errno.h>
#include <sys/stat.h>

#  include <string>
#  include <algorithm>
#  include <istream>
#  include <ostream>

#include "platform.h"

#ifdef PLATFORM_MSVC
// access function
#  include <io.h>

// kvuli MountPath()
#  define S_ISDIR(x) ((x) & S_IFDIR)

#else /* PLATFORM_MSVC */

// access function
#  include <unistd.h>

#endif /* PLATFORM_MSVC */

#include "exception.h"

#include "vfs/interface.h"
#include "vfs/fstream.h"
#include "vfs/vario.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  Implementation of stream classes
  */
  FileIStream::FileIStream() : std::istream(NULL)
  {
  }

  FileIStream::FileIStream(const std::string & filename,
    std::ios_base::openmode mode/*=std::ios_base::binary*/) : std::istream(NULL)
  {
    open(filename, mode);
  }

  FileIStream::~FileIStream()
  {
    close();
  }

  bool FileIStream::open(const std::string & filename,
    std::ios_base::openmode mode/*=std::ios_base::binary*/)
  {
    if(rdbuf())
      close();

    return sVfs().OpenFile(filename, *this, mode);
  }

  void FileIStream::close(void)
  {
    if(rdbuf()) {
      delete rdbuf();
      rdbuf(NULL);
    }
  }

  bool FileIStream::is_open(void) const
  {
    return rdbuf() != 0;
  }

  ulong FileIStream::length(void) const
  {
    FileBuffer * fbptr = dynamic_cast<FileBuffer*>(rdbuf());

    if(fbptr) {
      return fbptr->GetSize();
    }
    return 0;
  }

  int FileIStream::iformat(const char * fmt, ...)
  {
    if(!rdbuf())
      return -1;

    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamScanfV(*this, fmt, l_va);

    va_end(l_va);
    return t;
  }

  FileOStream::FileOStream() : std::ostream(NULL)
  {
  }

  FileOStream::FileOStream(const std::string & filename,
    std::ios_base::openmode mode/*=std::ios_base::binary
    | std::ios_base::trunc*/) : std::ostream(NULL)
  {
    open(filename, mode);
  }

  FileOStream::~FileOStream()
  {
    close();
  }

  bool FileOStream::open(const std::string & filename,
    std::ios_base::openmode mode/*=std::ios_base::binary | std::ios_base::trunc*/)
  {
    if(rdbuf())
      close();

    return sVfs().OpenFile(filename, *this, mode);
  }

  void FileOStream::close(void)
  {
    if(rdbuf()) {
      rdbuf()->pubsync();

      delete rdbuf();
      rdbuf(NULL);
    }
  }

  bool FileOStream::is_open(void) const
  {
    return rdbuf() != 0;
  }

  ulong FileOStream::length(void) const
  {
    FileBuffer * fbptr = dynamic_cast<FileBuffer*>(rdbuf());

    if(fbptr) {
      return fbptr->GetSize();
    }
    return 0;
  }

  int FileOStream::oformat(const char * fmt, ...)
  {
    if(!rdbuf())
      return -1;

    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamPrintfV(*this, fmt, l_va);

    va_end(l_va);
    return t;
  }

  FileIOStream::FileIOStream() : std::iostream(NULL)
  {
  }

  FileIOStream::FileIOStream(const std::string & filename,
    std::ios_base::openmode mode/*=std::ios_base::binary
    | std::ios_base::in*/) : std::iostream(NULL)
  {
    open(filename, mode);
  }

  FileIOStream::~FileIOStream()
  {
    close();
  }

  bool FileIOStream::open(const std::string & filename,
    std::ios_base::openmode mode/*=std::ios_base::binary
    | std::ios_base::in*/)
  {
    if(rdbuf())
      close();

    return sVfs().OpenFile(filename, *this, mode);
  }

  void FileIOStream::close(void)
  {
    if(rdbuf()) {
      rdbuf()->pubsync();

      delete rdbuf();
      rdbuf(NULL);
    }
  }

  bool FileIOStream::is_open(void) const
  {
    return rdbuf() != 0;
  }

  ulong FileIOStream::length(void) const
  {
    FileBuffer * fbptr = dynamic_cast<FileBuffer*>(rdbuf());

    if(fbptr) {
      return fbptr->GetSize();
    }
    return 0;
  }

  int FileIOStream::iformat(const char * fmt, ...)
  {
    if(!rdbuf())
      return -1;

    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamScanfV(*this, fmt, l_va);

    va_end(l_va);
    return t;
  }

  int FileIOStream::oformat(const char * fmt, ...)
  {
    if(!rdbuf())
      return -1;

    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamPrintfV(*this, fmt, l_va);

    va_end(l_va);
    return t;
  }
}  // vfs
