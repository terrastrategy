//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file interface.cpp
 *
 *  Contains implementation for the common parts of the vfs interface.
 */

#include <string.h>
#include <errno.h>
#include <sys/stat.h>

#include <string>
#include <algorithm>
#include <istream>

#include "platform.h"

#ifdef PLATFORM_MSVC
// access function
#  include <io.h>

// required by MountPath()
#  define S_ISDIR(x) ((x) & S_IFDIR)

#else /* PLATFORM_MSVC */

// access function
#  include <unistd.h>

#endif /* PLATFORM_MSVC */

#include "exception.h"

#include "diskfile.h"
//#include "vfs_zipfile.h"

#include "vfs/interface.h"
#include "vfs/logfile.h"

#include "memory/mmgr.h"

namespace tre {

 /*
  *  Local functions
  */
  namespace {

    std::string NormalizeVirtualDir(const std::string & dir)
    {
      std::string res("/");
      res.append(dir);

      std::string::size_type pos = 0;

      while((pos = res.find('\\', pos)) != std::string::npos) {
        res[pos] = '/';
      }

      pos = 0;

      while((pos = res.find("//", pos)) != std::string::npos) {
        // found two slashes, erase the first
        res.erase(res.begin() + pos);
      }

      if(res[res.length() - 1] == '/') {
        res.erase(res.end() - 1);
      }

      return res;
    }
  }

  /*
  *  Implementation of public members of the Vfs class
  */
  Vfs::Vfs()
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Initializing vfs." << std::endl;
#endif /* LOG_SINGLETONS */
  }

  Vfs::~Vfs()
  {
#ifdef LOG_SINGLETONS
    vLog << info() << "Releasing vfs." << std::endl;
#endif /* LOG_SINGLETONS */
    if(!mounts_.empty()) {
      for(MountVector::iterator pos = mounts_.begin();
          pos != mounts_.end(); ++pos) {
        delete pos->second;
      }
    }
  }

 /** Attempts to mount the specified file/dir as a virtual directory.
  *
  *  The specified path may point either to real directory or to a file,
  *  provided it's one of the supported archives. User can also explicitly
  *  specify whether he wants the path to be mounted in read-only mode, or not.
  *  This might be useful for providing read-only root directory and writable
  *  override directory. Please note that specific media (CD-ROM) and archives
  *  are automatically mounted in read-only mode. If the user would want the
  *  path mounted at all costs, but preferably read/write, the correct procedure
  *  would be to try to mount it read/write first and then read-only (if the
  *  previous attempt wasn't successful).
  *
  *  @param[in] path Path to the real directory/file to be mounted.
  *  @param[in] dir Target virtual directory.
  *  @param[in] readOnly Whether the path should be mounted in read only mode.
  *  @return bool if mounting process was successful, otherwise false.
  *
  *  @throw std::bad_alloc
  */
  bool Vfs::MountPath
  (const std::string & path, const std::string & dir, bool readOnly)
  {
    struct stat data;

    if(stat(path.c_str(), &data) != 0) {
      vLog << warn("Vfs") << "Attempted to mount nonexistent path "
              "in Vfs::MountPath('" << path << "', '" << dir << "', "
           << readOnly << ")." << std::endl;
      return false;
    }

    MountNode * node = NULL;

    try {
      // we are mounting a directory
      if(S_ISDIR(data.st_mode)) {
        // if pMountTree is NULL, then we have to be mounting root
#ifdef PLATFORM_MSVC
        if(readOnly) {
          if(_access(path.c_str(), 06) != 0 && _access(path.c_str(), 04) != 0) {
            vLog << warn("Vfs") << "Attempted to mount path '"
                 << path << "' as '" << dir
                 << "' with read only permissions, that is impossible."
                 << std::endl;
            return false;
          }
        } else {
          // we usually nead read permissions for buffering
          if(_access(path.c_str(), 06) != 0) {
            vLog << warn("Vfs") << "Attempted to mount path '"
                 << path << "' as '" << dir
                 << "' with write (and read) permissions, that is impossible."
                 << std::endl;
            return false;
          }
        }
#else /* PLATFORM_MSVC */
        if(readOnly) {
          if(access(path.c_str(),R_OK) != 0) {
            vLog << warn("Vfs") << "Attempted to mount path '"
                 << path << "' as '" << dir
                 << "' with read only permissions, that is impossible."
                 << std::endl;
            return false;
          }
        } else {
          // we usually nead read permissions for buffering
          if(access(path.c_str(), R_OK) != 0 ||
             access(path.c_str(), W_OK) != 0) {
            vLog << warn("Vfs") << "Attempted to mount path '"
                 << path << "' as '" << dir
                 << "' with write (and read) permissions, that is impossible."
                 << std::endl;
            return false;
          }
        }
#endif
        // create new DiskNode object
        node = new DiskNode(path, readOnly);
      } else {
        // TODO: check registered file types
      }
    } catch(Exception & exc) {
      vLog << err("Vfs") << exc.what() << std::endl;
      return false;
    }

    mounts_.push_back(std::make_pair(NormalizeVirtualDir(dir), node));

    return true;
  }

 /** Unmounts virtual directory specified by it's real path.
  *
  *  Any files opened by this node remain open even if the node is unmounted.
  *
  *  @param[in] path Real path of the directory to be unmounted.
  *  @return true if operation was successful, otherwise false
  */
  bool Vfs::UnMountPath(const std::string & path)
  {
    for(MountVector::reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      if(pos->second->GetPath() == path) {
        mounts_.erase(pos.base());
        return true;
      }
    }
    return false;
  }

 /** Unmounts virtual directory specified by it's virtual path.
  *
  *  Any files opened by this node remain open even if the node is unmounted.
  *
  *  @param[in] dir Virtual path of the directory to be unmounted.
  *  @return true if operation was successful, otherwise false
  */
  bool Vfs::UnMountDir(const std::string & dir)
  {
    std::string dir2(NormalizeVirtualDir(dir));

    for(MountVector::reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      if(pos->first == dir2) {
        mounts_.erase(pos.base());
        return true;
      }
    }
    return false;
  }

 /** Unmounts virtual directory specified by it's virtual and real path.
  *
  *  Any files opened by this node remain open even if the node is unmounted.
  *
  *  @param[in] dir Virtual path of the directory to be unmounted.
  *  @param[in] path Real path of the directory to be unmounted.
  *  @return true if operation was successful, otherwise false
  */
  bool Vfs::UnMountDirPath
  (const std::string & dir, const std::string & path)
  {
    std::string dir2(NormalizeVirtualDir(dir));

    for(MountVector::reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      if(pos->second->GetPath() == path && pos->first == dir2) {
        mounts_.erase(pos.base());
        return true;
      }
    }
    return false;
  }

  bool Vfs::OpenFile(const std::string & filename, FileIStream & file,
    std::ios_base::openmode mode/*=std::ios_base::binary*/) const
  {
    std::string f(NormalizeVirtualDir(filename));
    std::string::size_type l;

    for(MountVector::const_reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      l = pos->first.length();
      if(f.length() > l + 1 && f[l] == '/' &&
         f.compare(0, l, pos->first) == 0 && pos->second->OpenFile(
         std::string(f.begin() + l + 1, f.end()), file, mode)) {
        return true;
      }
    }
    return false;
  }

  bool Vfs::OpenFile(const std::string & filename, FileOStream & file,
    std::ios_base::openmode mode/*=std::ios_base::binary
    | std::ios_base::trunc*/) const
  {
    std::string f(NormalizeVirtualDir(filename));
    std::string::size_type l;

    for(MountVector::const_reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      //  read-only
      if(pos->second->IsReadOnly())
        continue;

      l = pos->first.length();
      if(f.length() > l + 1 && f[l] == '/' &&
         f.compare(0, l, pos->first) == 0 && pos->second->OpenFile(
         std::string(f.begin() + l + 1, f.end()), file, mode)) {
        return true;
      }
    }
    return false;
  }

  bool Vfs::OpenFile(const std::string & filename, FileIOStream & file,
    std::ios_base::openmode mode/*=std::ios_base::binary
    | std::ios_base::in*/) const
  {
    std::string f(NormalizeVirtualDir(filename));
    std::string::size_type l;

    for(MountVector::const_reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      //  read-only
      if((mode & std::ios_base::out) && pos->second->IsReadOnly())
        continue;

      l = pos->first.length();
      if(f.length() > l + 1 && f[l] == '/' &&
         f.compare(0, l, pos->first) == 0 && pos->second->OpenFile(
         std::string(f.begin() + l + 1, f.end()), file, mode)) {
        return true;
      }
    }
    return false;
  }

  const std::string Vfs::TranslatePath(const std::string & filename,
    std::ios_base::openmode mode/*=std::ios_base::in*/)
  {
    std::string f(NormalizeVirtualDir(filename));
    std::string::size_type l;
    FileInfo info;

    for(MountVector::const_reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      l = pos->first.length();
      if(f.length() > l + 1 && f[l] == '/' &&
         f.compare(0, l, pos->first) == 0 && pos->second->Stat(
         std::string(f.begin() + l + 1, f.end()), info) && !info.isDir) {
        if(info.readOnly && (mode & std::ios_base::out))
          continue;
        std::string res(pos->second->GetPath());
        res.append("/");
        res.append(f.begin() + l + 1, f.end());
        return res;
      }
    }
    return std::string();
  }

  bool Vfs::Stat(const std::string & filename, FileInfo & info) const
  {
    std::string f(NormalizeVirtualDir(filename));
    std::string::size_type l;

    for(MountVector::const_reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      l = pos->first.length();
      if(f.length() > l + 1 && f[l] == '/' &&
         f.compare(0, l, pos->first) == 0 && pos->second->Stat(
         std::string(f.begin() + l + 1, f.end()), info)) {
        return true;
      }
    }
    return false;
  }

 /** This function tests if the specified file exists in the vfs or not.
  *
  *  @param[in] file Virtual path to the file to be tested.
  *  @return true if the file is found, otherwise false.
  */
  bool Vfs::Exists(const std::string & filename) const
  {
    std::string f(NormalizeVirtualDir(filename));
    std::string::size_type l;

    for(MountVector::const_reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      l = pos->first.length();
      if(f.length() > l + 1 && f[l] == '/' &&
         f.compare(0, l, pos->first) == 0 && pos->second->Exists(
         std::string(f.begin() + l + 1, f.end()))) {
        return true;
      }
    }
    return false;
  }

  const StrVector Vfs::QueryDir(const std::string & dir) const
  {
    StrVector res, tmp;

    std::string f(NormalizeVirtualDir(dir));
    std::string::size_type l;

    for(MountVector::const_reverse_iterator pos = mounts_.rbegin();
        pos != mounts_.rend(); ++pos) {
      l = pos->first.length();
      if(f.length() > l + 1 && f[l] == '/' &&
         f.compare(0, l, pos->first) == 0) {
        tmp = pos->second->QueryDir(std::string(f.begin() + l + 1, f.end()));
        res.insert(res.end(), tmp.begin(), tmp.end());
      }
    }
    // erase duplicates
    std::sort(res.begin(), res.end());

    for(uint i = 1; i < res.size(); ++i) {
      if(res[i - 1] == res[i])
        res[i - 1] = "";
    }
    res.erase(std::remove(res.begin(), res.end(), ""), res.end());

    return res;
  }

  const Vfs::MountInfoVector Vfs::QueryMounts(void) const
  {
    MountInfoVector  res;
    MountInfo nfo;

    res.reserve(mounts_.size());

    for(MountVector::const_iterator pos = mounts_.begin();
        pos != mounts_.end(); ++pos) {
      nfo.name = pos->first;
      nfo.path = pos->second->GetPath();
      nfo.readOnly = pos->second->IsReadOnly();
      res.push_back(nfo);
    }
    return res;
  }
}  // vfs
