//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file logfile.cpp
 *
 *  Contains implementation of the log stream.
 */

#include <stdio.h>
#include <time.h>
#include <stdarg.h>

#include <ostream>
#include <istream>
#include <streambuf>

#include "types.h"
#include "assertion.h"

#include "vfs/logfile.h"
#include "vfs/console.h"
#include "vfs/vario.h"

namespace tre {

 /*
  *  Log stream manipulatoors
  */
  LogStream & operator << (LogStream & ls, const fac & m)
  {
    ls.SetFacility(m.fc_);
    return ls;
  }

  std::ostream & operator << (std::ostream & ls, const msg & m)
  {
    ls << m.snd_ << "> ";
    return ls;
  }

  std::ostream & operator << (std::ostream & ls, const ok & m)
  {
    ls << "Ok";

    if(m.snd_) {
      ls.put(':');
      ls << m.snd_;
    }
    ls << "> ";
    return ls;
  }

  std::ostream & operator << (std::ostream & ls, const info & m)
  {
    ls << "Info";

    if(m.snd_) {
      ls.put(':');
      ls << m.snd_;
    }
    ls << "> ";
    return ls;
  }

  std::ostream & operator << (std::ostream & ls, const warn & m)
  {
    ls << "Warning";

    if(m.snd_) {
      ls.put(':');
      ls << m.snd_;
    }
    ls << "> ";
    return ls;
  }

  std::ostream & operator << (std::ostream & ls, const err & m)
  {
    ls << "Error";

    if(m.snd_) {
      ls.put(':');
      ls << m.snd_;
    }
    ls << "> ";
    return ls;
  }

 /*
  *  LogBuffer implementation
  */
  LogBuffer::~LogBuffer()
  {
    for(uint j = 0; j < lfLast; ++j) {
      for(uint i = 0; i < maxOutPerFac; ++i) {
        if(out_[j][i].type == ltNone)
          break;

        switch(out_[j][i].type) {
          case ltCerr:
          case ltConsole:
            // vOut no longer exists at this time
            break;
          case ltFile:
            fflush(out_[j][i].file);
            fclose(out_[j][i].file);
            break;
          default:
            break;
        }
      }
    }
    fflush(stderr);
  }

  LogBuffer::LogBuffer(const char * deffile) : activeFac_(lfDefault)
  {
    if(deffile == NULL) {
      AddOutput(lfDefault, ltCerr, NULL, false);
    } else {
      AddOutput(lfDefault, ltFile, deffile, true);
    }
  }

  void LogBuffer::AddOutput(LogFacility facility,
    LogType type, const char * file, bool trunc)
  {
    DEBUG_ASSERT(facility < lfLast);

    FILE * f;

    // find first free output slot;
    for(uint i = 0; i < maxOutPerFac; ++i) {
      // assign output and return
      if(out_[facility][i].type == ltNone) {
        switch(type) {
          case ltCerr:
          case ltConsole:
            out_[facility][i].type = type;
            break;
          case ltFile:
            if(!file)
              return;

            if(trunc) {
              if((f = fopen(file, "w")))
                fclose(f);
            }

            if((f = fopen(file, "a"))) {
              out_[facility][i].type = type;
              out_[facility][i].file = f;

              // add init line if starting new file
              if(trunc) {
                time_t t = time(NULL);
                tm * lt = localtime(&t);

                fprintf(f, "Log started: %s\n", asctime(lt));
              }
            }
            break;
          default:
            break;
        }
        break;
      }
    }
  }

  // write one character at a time
  int LogBuffer::overflow(int c) {
    if(c != EOF) {
      // iterate over active facility
      for(uint i = 0; i < maxOutPerFac; ++i) {
        switch(out_[activeFac_][i].type) {
          case ltCerr:
            fputc(c, stderr);
            break;
          case ltConsole:
            if(ConsoleStream::ready)
              vOut.put(c);
            break;
          case ltFile:
            fputc(c, out_[activeFac_][i].file);
            break;
          default:
            return c;
        }
      }
    }
    return c;
  }

  // write multiple characters
  std::streamsize LogBuffer::xsputn(const char * s, std::streamsize num) {
    // iterate over active facility
    for(uint i = 0; i < maxOutPerFac; ++i) {
      switch(out_[activeFac_][i].type) {
        case ltCerr:
          fwrite(s, 1, num, stderr);
          break;
        case ltConsole:
          if(ConsoleStream::ready)
            vOut.write(s, num);
          break;
        case ltFile:
          fwrite(s, 1, num, out_[activeFac_][i].file);
          break;
        default:
          return num;
      }
    }
    return num;
  }

  // flush the data in the buffer
  int LogBuffer::sync(void) {
    activeFac_ = lfDefault;
    // iterate over active facility
    for(uint i = 0; i < maxOutPerFac; ++i) {
      switch(out_[activeFac_][i].type) {
        case ltCerr:
          fflush(stderr);
          break;
        case ltConsole:
          if(ConsoleStream::ready)
            vOut.flush();
          break;
        case ltFile:
          fflush(out_[activeFac_][i].file);
          break;
        default:
          return 0;
      }
    }
    return 0;
  }

 /*
  *  LogStream implementation
  */
  LogStream::LogStream(const char * logfile)
    : std::iostream(&buf_), buf_(logfile)
  {
  }

  void LogStream::AddOutput(LogFacility facility,
    LogType type, const char * file/*=NULL*/, bool trunc/*=true*/)
  {
    buf_.AddOutput(facility, type, file, trunc);
  }

  int LogStream::oformat(const char * fmt, ...)
  {
    if(!rdbuf())
      return -1;

    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamPrintf(*this, fmt, l_va);

    va_end(l_va);
    return t;
  }

}
