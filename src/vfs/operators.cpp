//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file operators.cpp
 *
 *  blahblah
 */

#include "utf8.h"

#include "vfs/operators.h"

std::ostream & operator << (std::ostream & o, const tre::Vector2f & v)
{
  o << "(" << v.x << ", " << v.y << ")";
  return o;
}

std::ostream & operator << (std::ostream & o, const tre::Vector3f & v)
{
  o << "(" << v.x << ", " << v.y << ", " << v.z << ")";
  return o;
}

std::ostream & operator << (std::ostream & o, const tre::Vector4f & v)
{
  o << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
  return o;
}

std::ostream & operator << (std::ostream & o, const std::wstring & str)
{
  std::string tmp_str;
  utf8::utf16to8(str.begin(), str.end(), std::back_inserter(tmp_str));
  o << tmp_str;
  return o;
}
