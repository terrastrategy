//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file vario.cpp
 *  Contains implementation of standard var io helper functions.
 *
 *  This file contains implementation of all functions for standard input and
 *  output with variable arguments. Belongs to the vfs namespace.
 */

#include <string.h>

#include <sstream>
#include <istream>
#include <ostream>
#include <iomanip>
#include <limits>
#include <math.h>

#include "types.h"

#include "vfs/vario.h"

namespace tre {

 /*
  *  Local constants and functions
  */
  namespace {
    /// Specifies string format options.
    typedef char FormatFlag;

    const FormatFlag flDefault = 0;   /// No special options.
    const FormatFlag flAlternate = 1; /// Alternate form.
    const FormatFlag flZero = 2;      /// Width padded with zeros.
    const FormatFlag flSpace = 3;     /// Space instead of '+' sign.
    const FormatFlag flSign = 4;      /// Always show decimal sign.
    const FormatFlag flAlign = 5;     /// Left alignment (negates zero padding).

    /// Read, bud do not assign. Scanf specific.
    const FormatFlag flIgnore    = 6;  

    /// Specifies conversion type byte length
    enum FormatLength {
      lnDefault = 0,   /// int
      lnShort,         /// short int
      lnShortShort,    /// char
      lnLong,          /// long int
      lnLongLong,      /// long long int (or long double in scanf)
      lnLongDouble     /// long double
    };

    // Helper formatting function
    void FormatSignedDecimal(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va);
    void FormatChar(std::ostream & os, FormatFlag flags,
      int width, VFS_VA_LIST l_va);
    void FormatUnsignedDecimal(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va);
    void FormatOctal(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va);
    void FormatHex(std::ostream & os, FormatFlag flags, uint width,
      int prec, FormatLength length, bool upper, VFS_VA_LIST l_va);
    void FormatEDouble(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, bool upper, VFS_VA_LIST l_va);
    void FormatDouble(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va);
    void FormatGDouble(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, bool upper, VFS_VA_LIST l_va);
    void FormatString(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va);

    bool InputFormatChar(std::istream & is,
      int width, bool ignore, VFS_VA_LIST l_va);
    bool InputFormatBool(std::istream & is, bool ignore, VFS_VA_LIST l_va);
    bool InputFormatDecimal(std::istream & is, int width,
      FormatLength length, bool ignore, bool uns, VFS_VA_LIST l_va);
    bool InputFormatOctal(std::istream & is, int width,
      FormatLength length, bool ignore, VFS_VA_LIST l_va);
    bool InputFormatHex(std::istream & is, int width,
      FormatLength length, bool ignore, bool ptr, VFS_VA_LIST l_va);
    bool InputFormatDouble(std::istream & is, int width,
      FormatLength length, bool ignore, VFS_VA_LIST l_va);
    bool InputFormatString(std::istream & is,
      int width, FormatFlag flags, VFS_VA_LIST l_va);
    bool InputFormatQuotedString(std::istream & is,
      int width, FormatFlag flags, VFS_VA_LIST l_va);
    bool InputFormatSet(std::istream & is, int width, bool circ,
      const std::string & setstr, FormatFlag flags, VFS_VA_LIST l_va);

    bool IsInRange(char c, bool circ, const std::string & setstr);
  }

  int StreamScanf(std::istream & is, const char * fmt, ...)
  {
    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamScanfV(is, fmt, l_va);

    va_end(l_va);
    return t;
  }


  int StreamPrintf(std::ostream & os, const char * fmt, ...)
  {
    va_list l_va;
    va_start(l_va, fmt);

    int t = StreamPrintfV(os, fmt, l_va);

    va_end(l_va);
    return t;
  }


  int StreamScanfV(std::istream & is, const char * fmt, VFS_VA_LIST l_va)
  {
    // end of file a has already been reached, no error flag, just eof
    if(!is.good())
      return -1;

    int result = 0, width = -1;
    FormatFlag flag = 0;
    FormatLength length = lnDefault;
    char c,c2;

    is >> std::noskipws;

    std::stringstream ss(fmt);
    ss >> std::noskipws;

    while((ss >> c)) {
      if(c != '%') {
        if(isspace(c)) {
          is >> std::ws;
        } else {
          is >> c2;
          if(is.eof() || c != c2) {
            is >> std::skipws;
            return result;
          }
        }
      } else if(ss.peek() == '%') {
        ss.get();
        if(!(is >> c2) || c2 != '%') {
          is >> std::skipws;
          return result;
        }
      } else {
        // reset all format specs
        flag = 0, width = 0, length = lnDefault;
        bool bbreak = false;
        // parse format flags
        while((ss >> c)) {
          switch(c) {
            case '#':
              flag |= flAlternate;
              break;
            case '*':
              flag |= flIgnore;
              break;
            default:
              bbreak = true;
          }
          if(bbreak) {
            ss.unget();
            break;
          }
        }
        // try and get width
        if(isdigit(ss.peek()))
          ss >> width;
        // now length
        if((ss >> c)) {
          switch(c) {
            case 'h':
              if(ss.peek() == 'h') {
                ss.get();
                length = lnShortShort;
              } else {
                length = lnShort;
              }
              break;
            case 'l':
              if(ss.peek() == 'l') {
                ss.get();
                length = lnLongLong;
              } else {
                length = lnLong;
              }
              break;
            case 'q':
            case 'L':
              length = lnLongLong;  // long long or long double
            case 'I':  // Bill's stuff
              if(ss.peek() == '3') {
                ss.get();
                if(ss.peek() != '2') {
                  is >> std::skipws;
                  return result;
                }
                length = lnDefault;   
              } else if(ss.peek() == '6') {
                ss.get();
                if(ss.peek() != '4') {
                  is >> std::skipws;
                  return result;
                }
                length = lnLongLong;   
              } else {
                is >> std::skipws;
                return result;
              }
              break;
            default:
              ss.unget();
            case 't':
            case 'z':
            case 'j':
              break;
          }
        }
        if((ss >> c)) {
          switch(c) {
            case 'c':
            case 'C':
              if(!InputFormatChar(is, width, (flag & flIgnore) != 0, l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'b':
              if(!InputFormatBool(is, (flag & flIgnore) != 0, l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'u': // unsigned decimal
            case 'd': // signed decimal
              if(!InputFormatDecimal(is, width, length,
                 (flag & flIgnore) != 0, (c == 'u'), l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'o': // octal
              if(!InputFormatOctal(is, width, length,
                 (flag & flIgnore) != 0, l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'p': // address
            case 'x':
            case 'X': // hexa
              if(!InputFormatHex(is, width, length,
                 (flag & flIgnore) != 0, (c == 'p'), l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'i': // integer, everything
              if(is.peek() == '0') {
                is.get();
                if(tolower(is.peek()) == 'x') {
                  is.unget();
                  if(!InputFormatHex(is, width, length,
                     (flag & flIgnore) != 0, false, l_va)) {
                    is >> std::skipws;
                    return result;
                  }
                } else {
                  is.unget();
                  if(!InputFormatOctal(is, width, length,
                     (flag & flIgnore) != 0, l_va)) {
                    is >> std::skipws;
                    return result;
                  }
                }
              } else {
                if(!InputFormatDecimal(is, width, length,
                   (flag & flIgnore) != 0, (c == 'u'), l_va)) {
                  is >> std::skipws;
                  return result;
                }
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'f':
            case 'e':
            case 'g':
            case 'E':
            case 'a': // float
              if(!InputFormatDouble(is, width,
                 length, (flag & flIgnore) != 0, l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 's': // string
              if(!InputFormatString(is, width, flag, l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'S': // string
              if(!InputFormatQuotedString(is, width, flag, l_va)) {
                is >> std::skipws;
                return result;
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'n':
              *va_arg(l_va, int*) = result;
              break;
            case '[':  // shit
              // first get the [string]
              {
                bool circumflex = false;
                std::string setstr;
                if(ss.peek() == '^') {
                  ss.get();
                  circumflex = true;
                }
                if(ss.peek() == ']') {
                  ss.get();
                  setstr.push_back(']');
                }
                while((ss >> c) && c != ']') {
                  setstr.push_back(c);
                }
                if(ss.eof() || !InputFormatSet(is, width, circumflex,
                   setstr, flag, l_va)) {
                  return result;
                }
              }
              if(!(flag & flIgnore))
                ++result;
              break;
            case 'D':  // ignore
              break;
            default:
              return result;
          }
        }
      }
    }
    is >> std::skipws;
    return result;
  }

  int StreamPrintfV(std::ostream & os, const char * fmt, VFS_VA_LIST l_va)
  {
    int width, precision;
    FormatFlag flag;
    FormatLength length;
    char c;
    std::streamoff start, start_out = os.tellp();

    std::stringstream ss(fmt);
    ss >> std::noskipws;

    while((ss >> c)) {
      if(c != '%') {
        os << c;
      } else if(ss.peek() == '%') {
        ss.get();
        os << '%';
      } else {
        // reset all format specs
        width = 0, precision = -1;
        flag = 0, length = lnDefault;
        start = ss.tellg();
        start -= 1;
        bool bbreak = false;
        // parse format flags
        while((ss >> c)) {
          switch(c) {
            case '#':
              flag |= flAlternate;
              break;
            case '0':
              flag |= flZero;
              break;
            case ' ':
              flag |= flSpace;
              break;
            case '+':
              flag |= flSign;
              break;
            case '-':
              flag |= flAlign;
              break;
            default:
              bbreak = true;
            case '`':  // we don't support these
            case '|':
              break;
          }
          if(bbreak) {
            ss.unget();
            break;
          }
        }
        // read the width
        if(isdigit(ss.peek()) != 0) {  // it can't be <= 0, since -0 are flags
          ss >> width;
        }
        // get precision
        if(ss.peek() == '.') {
          ss.get();
          if(isdigit(ss.peek()))
            ss >> precision;
          if(precision < 0)
            precision = 0;
        }
        // length
        try {
          if((ss >> c)) {
            switch(c) {
              case 'h':
                if(ss.peek() == 'h') {
                  ss.get();
                  length = lnShortShort;
                } else {
                  length = lnShort;
                }
                break;
              case 'l':
                if(ss.peek() == 'l') {
                  ss.get();
                  length = lnLongLong;
                } else {
                  length = lnLong;
                }
                break;
              case 'L':
                length = lnLongDouble;
                break;
              case 'q':
                length = lnLongLong;
                break;
              case 'I':  // MS' stuff
                if(ss.peek() == '3') {
                  ss.get();
                  if(ss.peek() != '2')
                    throw ss.tellg();
                  length = lnDefault;   
                } else if(ss.peek() == '6') {
                  ss.get();
                  if(ss.peek() != '4')
                    throw ss.tellg();
                  length = lnLongLong;   
                } else {
                  throw ss.tellg();
                } 
                break;
              default:
                ss.unget();
              case 'j':  // ignored
              case 'z':
              case 'Z':
              case 't':
                break;
            }
          }
          if((ss >> c)) {
            switch(c) {
              case 'd':
              case 'i':
                FormatSignedDecimal(os, flag, width, precision, length, l_va);
                break;
              case 'C':
              case 'c':
                FormatChar(os, flag, width, l_va);
                break;
              case 'u':
                FormatUnsignedDecimal(os, flag, width, precision, length, l_va);
                break;
              case 'o':
                FormatOctal(os, flag, width, precision, length, l_va);
                break;
              case 'x':
              case 'X':
                FormatHex(os, flag, width, precision, length, (c == 'X'), l_va);
                break;
              case 'e':
              case 'E':
                if(precision == -1)
                  precision = 6;
                FormatEDouble(os, flag, width,
                  precision, length, c == 'E', l_va);
                break;
              case 'f':
              case 'F':
                if(precision == -1)
                  precision = 6;
                FormatDouble(os, flag, width, precision, length, l_va);
                break;
              case 'g':
              case 'G':
                if(precision == -1)
                  precision = 6;
                FormatGDouble(os, flag, width,
                  precision, length, c == 'G', l_va);
                break;
              case 'n':  // number of byter successfully written so far
                if(length == lnLong) {
                  unsigned int * ptr = va_arg(l_va, unsigned int*);
                  *ptr = os.tellp() - start_out;
                } else {
                  long int * ptr = va_arg(l_va, long*);
                  *ptr = os.tellp() - start_out;
                }
                break;
              case 'p':
                if(precision == -1)
                  precision = 1;
                flag |= flAlternate;
                length = lnDefault;
                if(sizeof(void*) != sizeof(int)) {
                  if(sizeof(void*) == sizeof(long long))
                    length = lnLongLong;
                  else if(sizeof(void*) == sizeof(long))
                    length = lnLong;
                }
                FormatHex(os, flag, width, precision, length, false, l_va);
                break;
              case 's':
              case 'S':   // Goddamn...
                // default precision is -1
                FormatString(os, flag, width, precision, length, l_va);
                break;
              default:
                throw ss.tellg();  // no conversion specificator
            }
          }
        } catch(int pos) {
          // wrong format option
          os << std::string(fmt + start, pos - start);
        }
      }
    }
    return os.tellp() - start_out;
  }

 /*
  *  Local helper functions
  */
  namespace {

    void FormatSignedDecimal(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va)
    {
      // conversion variables
      long long lonlon;

      // length
      switch(length) {
        case lnShort:
          lonlon = static_cast<short>(va_arg(l_va, int));
          break;
        case lnShortShort:
          lonlon = static_cast<char>(va_arg(l_va, int));
          break;
        case lnLong:
          lonlon = va_arg(l_va, long);
          break;
        case lnLongLong:
          lonlon = va_arg(l_va, long long);
          break;
        default:
          lonlon = va_arg(l_va, int);
          break;
      }

      std::stringstream ss;
      long long aNum = lonlon > 0 ? lonlon : -lonlon;

      bool sign = false;

      if(lonlon >= 0) {
        if(flags & flSign) {
          ss << '+';
          sign = true;
        } else if(flags & flSpace) {
          ss << ' ';
          sign = true;
        }
      } else {
        ss << '-';
        sign = true;
      }

      // if precision is unspecified, format using width if possible
      if(prec == -1 && !(flags & flAlign) && (flags & flZero)) {
        prec = sign ? width - 1 : width;
        width = 0;
      }

      if(prec != -1)
        ss << std::setfill('0') << std::setw(prec);
      ss << aNum;

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << ss.str() << std::right;
      } else {
        os << std::setw(width) << ss.str();
      }
    }

    void FormatChar(std::ostream & os,
      FormatFlag flags, int width, VFS_VA_LIST l_va)
    {
      // conversion
      if(flags & flAlign) {
        os << std::left << std::setw(width)
           << static_cast<byte>(va_arg(l_va, int)) << std::right;
      } else {
        os << std::setw(width) << static_cast<byte>(va_arg(l_va, int));
      }
    }

    void FormatUnsignedDecimal(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va)
    {
      // conversion variables
      unsigned long long lonlon;

      // length
      switch(length) {
        case lnLong:
          lonlon = va_arg(l_va, unsigned long);
          break;
        case lnLongLong:
          lonlon = va_arg(l_va, unsigned long long);
          break;
        case lnShort:
        case lnShortShort:
        default:
          lonlon = va_arg(l_va, uint);
          break;
      }

      std::stringstream ss;

      // if precision is unspecified, format using width if possible
      if(prec == -1 && !(flags & flAlign) && (flags & flZero)) {
        prec = width;
        width = 0;
      }

      if(prec != -1)
        ss << std::setfill('0') << std::setw(prec);
      ss << lonlon;

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << ss.str() << std::right;
      } else {
        os << std::setw(width) << ss.str();
      }
    }

    void FormatOctal(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va)
    {
      // conversion variables
      unsigned long long lonlon;

      // length
      switch(length) {
        case lnLong:
          lonlon = va_arg(l_va, unsigned long);
          break;
        case lnLongLong:
          lonlon = va_arg(l_va, unsigned long long);
          break;
        case lnShort:
        case lnShortShort:
        default:
          lonlon = va_arg(l_va, unsigned);
          break;
      }

      std::stringstream ss;

      // if precision is unspecified, format using width if possible
      if(prec == -1 && !(flags & flAlign) && (flags & flZero)) {
        prec = width;
        width = 0;
      }

      // alternate flag, show numerical base prefix
      if(flags & flAlternate)
        ss << std::showbase;

      if(prec != -1)
        ss << std::setfill('0') << std::setw(prec);
      ss << std::oct << lonlon;

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << ss.str() << std::right;
      } else {
        os << std::setw(width) << ss.str();
      }
    }

    void FormatHex(std::ostream & os, FormatFlag flags, uint width,
      int prec, FormatLength length, bool upper, VFS_VA_LIST l_va)
    {
      // conversion variables
      unsigned long long lonlon;

      // length
      switch(length) {
        case lnLong:
          lonlon = va_arg(l_va, unsigned long);
          break;
        case lnLongLong:
          lonlon = va_arg(l_va, unsigned long long);
          break;
        case lnShort:
        case lnShortShort:
        default:
          lonlon = va_arg(l_va, unsigned);
          break;
      }

      std::stringstream ss;

      // alternate flag
      if(flags & flAlternate) {
        if(upper)
          ss << "0x";
        else
          ss << "0X";
      }

      if(upper)
        ss << std::uppercase;

      // if precision is unspecified, format using width if possible
      if(prec == -1 && !(flags & flAlign) && (flags & flZero)) {
        prec = width;
        width = 0;
      }

      if(prec != -1)
        ss << std::setfill('0') << std::setw(prec);
      ss << std::hex << lonlon;

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << ss.str() << std::right;
      } else {
        os << std::setw(width) << ss.str();
      }
    }

    void FormatEDouble(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, bool upper, VFS_VA_LIST l_va)
    {
      long double dbl;

      switch(length) {
        case lnLongDouble:
          dbl = va_arg(l_va,long double);
          break;
        default:
          dbl = va_arg(l_va,double);
          break;
      }

      std::stringstream ss;

      bool sign = false;
      long double aDbl = fabs(dbl);

      if(dbl >= 0) {
        if(flags & flSign) {
          ss << '+';
          sign = true;
        } else if(flags & flSpace) {
          ss << ' ';
          sign = true;
        }
      } else {
        ss << '-';
        sign = true;
      }

      ss << std::scientific << std::setprecision(prec);

      // zero padding?
      if(!(flags & flAlign) && (flags & flZero)) {
        ss << std::setfill('0') << std::setw(sign ? width - 1 : width);
        width = 0;
      }

      ss << aDbl;

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << ss.str() << std::right;
      } else {
        os << std::setw(width) << ss.str();
      }
    }

    void FormatDouble(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va)
    {
      long double dbl;

      switch(length) {
        case lnLongDouble:
          dbl = va_arg(l_va,long double);
          break;
        default:
          dbl = va_arg(l_va,double);
          break;
      }

      std::stringstream ss;

      bool sign = false;
      long double aDbl = fabs(dbl);

      if(dbl >= 0) {
        if(flags & flSign) {
          ss << '+';
          sign = true;
        } else if(flags & flSpace) {
          ss << ' ';
          sign = true;
        }
      } else {
        ss << '-';
        sign = true;
      }

      ss << std::fixed << std::setprecision(prec);

      // zero padding?
      if(!(flags & flAlign) && (flags & flZero)) {
        ss << std::setfill('0') << std::setw(sign ? width - 1 : width);
        width = 0;
      }

      ss << aDbl;

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << ss.str() << std::right;
      } else {
        os << std::setw(width) << ss.str();
      }

      if(prec == 0 && (flags & flAlternate))
        os << '.';
    }

    void FormatGDouble(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, bool upper, VFS_VA_LIST l_va)
    {
      long double dbl;

      switch(length) {
        case lnLongDouble:
          dbl = va_arg(l_va,long double);
          break;
        default:
          dbl = va_arg(l_va,double);
          break;
      }

      std::stringstream ss;

      bool sign = false;
      long double aDbl = fabs(dbl);

      if(dbl >= 0) {
        if(flags & flSign) {
          ss << '+';
          sign = true;
        } else if(flags & flSpace) {
          ss << ' ';
          sign = true;
        }
      } else {
        ss << '-';
        sign = true;
      }

      // ignore the alternate flag
      ss << std::setprecision(prec);

      // zero padding?
      if(!(flags & flAlign) && (flags & flZero)) {
        ss << std::setfill('0') << std::setw(sign ? width - 1 : width);
        width = 0;
      }

      ss << aDbl;

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << ss.str() << std::right;
      } else {
        os << std::setw(width) << ss.str();
      }
    }

    void FormatString(std::ostream & os, FormatFlag flags,
      int width, int prec, FormatLength length, VFS_VA_LIST l_va)
    {
      char * str = va_arg(l_va, char*);

      // width padding
      if(flags & flAlign) {
        os << std::left << std::setw(width) << str << std::right;
      } else {
        os << std::setw(width) << str;
      }
    }

    bool InputFormatChar
    (std::istream & is, int width, bool ignore, VFS_VA_LIST l_va)
    {
      // n chars
      if(ignore) {
        is.ignore(std::max(1, width));
      } else {
        is.read(va_arg(l_va, char*), std::max(1, width));
      }
      return (!is.eof() || is.gcount() > 0);
    }

    bool InputFormatBool(std::istream & is, bool ignore, VFS_VA_LIST l_va)
    {
      int n;

      if(!(is >> std::skipws >> n >> std::noskipws))
        return false;

      if(!ignore) {
        *va_arg(l_va, bool*) = (n != 0);
      }
      return true;
    }

    bool InputFormatDecimal(std::istream & is, int width,
      FormatLength length, bool ignore, bool uns, VFS_VA_LIST l_va)
    {
      char c;

      // skip whitespace characters
      if(!(is >> std::ws))
        return false;

      std::stringstream ss;

      if((c = is.peek()) == '+' || c == '-') {
        is.get();
        ss << c;
      }

      if(width <= 0)
        width = std::numeric_limits<int>::max();

      while(width > 0 && isdigit(is.peek())) {
        is >> c;
        ss << c;
        --width;
      }

      long long num;
      ss >> num;

      if(!ss)
        return false;

      if(!ignore) {
        // assign values
        switch(length) {
          case lnShortShort:
            if(uns)
              *va_arg(l_va, byte*) = num;
            else
              *va_arg(l_va, char*) = num;
            break;
          case lnShort:
            if(uns)
              *va_arg(l_va, ushort*) = num;
            else
              *va_arg(l_va, short*) = num;
            break;
          case lnLong:
            if(uns)
              *va_arg(l_va, ulong*) = num;
            else
              *va_arg(l_va, long*) = num;
            break;
          case lnLongLong:
            if(uns)
              *va_arg(l_va, unsigned long long*) = num;
            else
              *va_arg(l_va, long long*) = num;
            break;
          default:
            if(uns)
              *va_arg(l_va, uint*) = num;
            else
              *va_arg(l_va, int*) = num;
            break;
        }
      }
      return true;
    }

    bool InputFormatOctal(std::istream & is, int width,
      FormatLength length, bool ignore, VFS_VA_LIST l_va)
    {
      char c;

      // skip whitespace characters
      if(!(is >> std::ws))
        return false;

      is.unget();

      std::stringstream ss;

      if(width <= 0)
        width = std::numeric_limits<int>::max();

      while(width > 0) {
        c = is.peek();
        if(!isdigit(c) || c == '9' || c == '8')
          break;
        is.get();
        ss << c;
        --width;
      }

      long long num;
      ss >> std::oct >> num;

      if(!ss)
        return false;

      if(!ignore) {
        // assign values
        switch(length) {
          case lnShortShort:
            *va_arg(l_va, byte*) = num;
            break;
          case lnShort:
            *va_arg(l_va, ushort*) = num;
            break;
          case lnLong:
            *va_arg(l_va, ulong*) = num;
            break;
          case lnLongLong:
            *va_arg(l_va, unsigned long long*) = num;
            break;
          default:
            *va_arg(l_va, uint*) = num;
            break;
        }
      }
      return true;
    }

    bool InputFormatHex(std::istream & is, int width,
      FormatLength length, bool ignore, bool ptr, VFS_VA_LIST l_va)
    {
      char c;

      // skip whitespace characters
      if(!(is >> std::ws))
        return false;

      is.unget();

      std::stringstream ss;

      if(width <= 0)
        width = std::numeric_limits<int>::max();

      while(width > 0) {
        c = is.peek();
        if(isdigit(c) || (tolower(c) >= 'a' && tolower(c) <= 'f')) {
          is.get();
          ss << c;
          --width;
        } else {
          break;
        }
      }

      long long num;
      ss >> std::hex >> num;

      if(!ss)
        return false;

      if(!ignore) {
        if(ptr) {
          *va_arg(l_va, void**) = reinterpret_cast<void*>(
                                    static_cast<uptr>(num));
        } else {
          // assign values
          switch(length) {
            case lnShortShort:
              *va_arg(l_va, byte*) = num;
              break;
            case lnShort:
              *va_arg(l_va, ushort*) = num;
              break;
            case lnLong:
              *va_arg(l_va, ulong*) = num;
              break;
            case lnLongLong:
              *va_arg(l_va, unsigned long long*) = num;
              break;
            default:
              *va_arg(l_va, uint*) = num;
              break;
          }
        }
      }
      return true;
    }

    bool InputFormatDouble(std::istream & is, int width,
      FormatLength length, bool ignore, VFS_VA_LIST l_va)
    {
      char c;

      // skip whitespace characters
      if(!(is >> std::ws))
        return false;

      std::stringstream ss;

      if((c = is.peek()) == '+' || c == '-') {
        is.get();
        ss << c;
      }

      if(width <= 0)
        width = std::numeric_limits<int>::max();

      // number
      while(width > 0 && isdigit(is.peek())) {
        is >> c;
        ss << c;
        --width;
      }
      // dot
      if(width > 0 && is.peek() == '.') {
        is.get();
        ss << '.';
        --width;
      } else {
        width = 0;  // quit parsing
      }
      // number
      while(width > 0 && isdigit(is.peek())) {
        is >> c;
        ss << c;
        --width;
      }
      // eE
      if(width > 0 && tolower(is.peek()) == 'e') {
        is >> c;
        ss << c;
        --width;
      } else {
        width = 0;  // quit parsing
      }
      // sign
      if(width > 0 && ((c = is.peek()) == '+' || c == '-' || isdigit(c))) {
        is.get();
        ss << c;
        --width;
      } else {
        width = 0;  // quit parsing
      }
      // number
      while(width > 0 && isdigit(is.peek())) {
        is >> c;
        ss << c;
        --width;
      }

      // assign values
      if(!ignore) {
        switch(length) {
          case lnLong:
            if(!(ss >> *va_arg(l_va, double*)))
              return false;
            break;
          case lnLongLong:
            if(!(ss >> *va_arg(l_va, long double*)))
              return false;
            break;
          default:
            if(!(ss >> *va_arg(l_va, float*)))
              return false;
            break;
        }
      }
      return true;
    }

    bool InputFormatString
    (std::istream & is, int width, FormatFlag flags, VFS_VA_LIST l_va)
    { 
      if(width <= 0)
        width = std::numeric_limits<int>::max();

      is >> std::skipws >> std::setw(width + 1);

      if(flags & flIgnore) {
        std::string dump;
        is >> dump;
      } else if(flags & flAlternate) {
        is >> *va_arg(l_va, std::string*);
      } else {
        is >> *va_arg(l_va, char*);
      }
      is >> std::noskipws;

      return !is != false;
    }

    bool InputFormatQuotedString
    (std::istream & is, int width, FormatFlag flags, VFS_VA_LIST l_va)
    {
      char c;

      // skip whitespace characters and get the first char
      if(!(is >> std::ws >> c))
        return false;

      if(c != '"') {
        is.unget();
        return InputFormatString(is, width, flags, l_va);
      }

      std::string tmp_str;

      if(width <= 0)
        width = std::numeric_limits<int>::max();

      while(width > 0 && (is >> c)) {
        if(c == '\\') {
          if(is.peek() == '\\') {
            is.get();
            tmp_str.push_back('\\');
          } else if(is.peek() == '"') {
            is.get();
            tmp_str.push_back('"');
          }
        } else if(c == '"') {
          if(!(flags & flIgnore)) {
            if(flags & flAlternate) {
              *va_arg(l_va, std::string*) = tmp_str;
            } else {
              strcpy(va_arg(l_va, char*), tmp_str.c_str());
            }
          }
          return true;
        } else {
          tmp_str.push_back(c);
        }
        --width;
      }
      return false;   // run out of width
    }

    bool InputFormatSet(std::istream & is, int width, bool circ,
      const std::string & setstr, FormatFlag flags, VFS_VA_LIST l_va)
    {
      char c;
      std::string tmp_str;

      if(width <= 0)
        width = std::numeric_limits<int>::max();

      // read chars
      while(width > 0 && IsInRange(is.peek(), circ, setstr)) {
        is >> c;
        tmp_str.push_back(c);
        --width;
      }

      // and assign value
      if(!(flags & flIgnore)) {
        if(flags & flAlternate)
          *va_arg(l_va, std::string*) = tmp_str;
        else
          strcpy(va_arg(l_va, char*), tmp_str.c_str());
      }
      return true;
    }

    bool IsInRange(char c, bool circ, const std::string & setstr)
    {
      bool res = false;
      // go through [string]
      for(uint i = 0; i < setstr.length(); ++i) {
        if(setstr[i] != '-') {
          if(c == setstr[i]) {
            res = true;
            break;
          }
        } else {
          if(i == setstr.length() - 1) {
            if(c == '-') {
              res = true;
              break;
            }
          } else if(i != 0) {
            if(c >= setstr[i - 1] && c <= setstr[i + 1]) {
              res = true;
              break;
            }
          }
        }
      }
      // if circumvent flag is on, negate result
      return circ ? !res : res;
    }
  }
}
