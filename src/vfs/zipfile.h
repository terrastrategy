//
//  Copyright (C) 2007 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file zipfile.h
 *
 *  bleble
 */

#pragma once

#include "vfs/interface.h"

namespace tre {
  class ZipNode : private MountNode {
    public:
      ZipNode(const std::string & path, bool mode) : MountNode(path, mode) {}

      bool OpenFile(const std::string & filename,
        FileIStream & file, std::ios_base::openmode mode) const;

      bool OpenFile(const std::string & filename, FileOStream & file,
        std::ios_base::openmode mode) const;

      bool Stat(const std::string & filename, FileInfo & info) const;

      bool Exists(const std::string & filename) const;

      const StrVector QueryDir(const std::string & dir) const;
  };

}
