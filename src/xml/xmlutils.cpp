//
//  Copyright (C) 2008 by Martin Moracek
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

/**
 *  @file xmlutils.cpp
 *
 *  blahblah
 */

#include "assertion.h"
#include "exception.h"

#include "tinyxml.h"
#include "xmlutils.h"

namespace tre {

  std::string CheckText(TiXmlElement & el, bool require/*=true*/)
  {
    const char * str = el.GetText();

    if(require && (!str || *str == 0)) {
      throw(Exception("Missing element text"));
    }

    return str ? str : "";
  }

  int CheckIntText(TiXmlElement & el, bool require/*=true*/, int def/*=0*/)
  {
    const char * str = el.GetText();

    if(require && (!str || *str == 0)) {
      throw(Exception("Missing element text"));
    }

    return str ? atoi(str) : def;
  }

  float CheckFloatText
  (TiXmlElement & el, bool require/*=true*/, float def/*=0.0f*/)
  {
    const char * str = el.GetText();

    if(require && (!str || *str == 0)) {
      throw(Exception("Missing element text"));
    }

    return str ? static_cast<float>(atof(str)) : def;
  }

  double CheckFloatText
  (TiXmlElement & el, bool require/*=true*/, double def/*=0.0*/)
  {
    const char * str = el.GetText();

    if(require && (!str || *str == 0))
      throw(Exception("Missing element text"));

    return str ? static_cast<float>(atof(str)) : def;
  }

  std::string CheckAttribute
  (TiXmlElement & el, const char * name, bool require/*=true*/)
  {
    DEBUG_ASSERT(name);

    const char * str = el.Attribute(name);

    if(require && (!str || *str == 0)) {
      throw(Exception("Missing element attribute"));
    }

    return str ? str : "";
  }

  int CheckIntAttribute(TiXmlElement & el, const char * name,
    bool require/*=true*/, int def/*=0*/)
  {
    DEBUG_ASSERT(name);

    int val = def;
    int r = el.QueryIntAttribute(name, &val);

    if(r != TIXML_SUCCESS && require) {
      if(r == TIXML_WRONG_TYPE) {
        throw(Exception("Wrong attribute type"));
      } else {  // TIXML_NO_ATTRIBUTE
        throw(Exception("Missing element attribute"));
      }
    }
    return val;
  }

  float CheckFloatAttribute(TiXmlElement & el, const char * name,
    bool require/*=true*/, float def/*=0.0f*/)
  {
    DEBUG_ASSERT(name);

    float val = def;
    int r = el.QueryFloatAttribute(name, &val);

    if(r != TIXML_SUCCESS && require) {
      if(r == TIXML_WRONG_TYPE) {
        throw(Exception("Wrong attribute type"));
      } else {  // TIXML_NO_ATTRIBUTE
        throw(Exception("Missing element attribute"));
      }
    }
    return val;
  }

  float CheckDoubleAttribute(TiXmlElement & el, const char * name,
    bool require/*=true*/, double def/*=0.0*/)
  {
    DEBUG_ASSERT(name);

    double val = def;
    int r = el.QueryDoubleAttribute(name, &val);

    if(r != TIXML_SUCCESS && require) {
      if(r == TIXML_WRONG_TYPE) {
        throw(Exception("Wrong attribute type"));
      } else {  // TIXML_NO_ATTRIBUTE
        throw(Exception("Missing element attribute"));
      }
    }
    return val;
  }

}
